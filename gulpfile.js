"use strict";

let SERVER_PORT = 9192;

let gulp = require("gulp"),
    gutil = require("gulp-util"),
    prettyHrtime = require("pretty-hrtime"),
    globule = require("globule"),
    browserify = require("browserify"),
    watchify = require("watchify"),
    babelify = require("babelify"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer"),
    del = require("del"),
    runSequence = require("run-sequence"),
    eslint = require("gulp-eslint"),
    express = require("express"),
    mocha = require("gulp-mocha"),
    uglify = require("gulp-uglify");

let paths = {
    dist: "./dist/",
    src: "./src/",
    test: "./test/"
};

gulp.task("clean", function() {
    return del(paths.dist);
});

gulp.task("lint", function() {
    return gulp.src(paths.src + "/**/*.js")
        .pipe(eslint())
        .pipe(eslint.format());
        //.pipe(eslint.failOnError());
});

let createBundler = function(entries) {
    let bundler = browserify({
        entries: entries,
        paths: [paths.src, paths.test],
        debug: true,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: true
    })
    .transform(babelify.configure({
        blacklist: [
/*                "es3.memberExpressionLiterals",
            "es3.propertyLiterals",
            "es5.properties.mutators",
            "es6.arrowFunctions",
            "es6.blockScoping",
            "es6.classes",
            "es6.constants",
            "es6.destructuring",
            "es6.forOf",
            //"es6.modules",
            "es6.objectSuper",
            "es6.parameters",
            "es6.properties.computed",
            "es6.properties.shorthand",
            "es6.regex.sticky",
            "es6.regex.unicode",
            "es6.spread",
            "es6.tailCall"*/
        ],
        optional: [
            "asyncToGenerator",
            "minification.memberExpressionLiterals",
            "minification.propertyLiterals",
            "utility.inlineEnvironmentVariables"
        ],
        compact: true
    }));

    return bundler;
};

gulp.task("build", function() {
    let bundler = createBundler(paths.src + "/OipfStubContext.js");
    return bundler.bundle()
                    .pipe(source("oipf-stub.js"))
                    .pipe(buffer())
                    .pipe(uglify())
                    .pipe(gulp.dest(paths.dist));
});

gulp.task("dist", function() {
    let bundler = watchify(createBundler(paths.src + "/OipfDist.js"));

    let onError = function(e) {
        gutil.log("Finished '" + gutil.colors.cyan("bundle") + "' " +
                gutil.colors.red("error"), "\n" + e.toString() + "\n" + e.codeFrame);
        this.emit("end");
    };

    let start;
    let onFinish = function() {
        let end = process.hrtime(start);
        gutil.log("Finished '" + gutil.colors.cyan("bundle") + "' after " + gutil.colors.magenta(prettyHrtime(end)));
    };

    function rebundle() {
        gutil.log("Starting '" + gutil.colors.cyan("bundle") + "' ...");
        start = process.hrtime();

        return bundler.bundle()
                        .on("error", onError)
                        .pipe(source("oipf-dist.js"))
                        .pipe(gulp.dest(paths.dist))
                        .on("finish", onFinish);
    }

    bundler.on("update", rebundle);

    return rebundle();
});

gulp.task("run-test", function() {

    let app = express();
    app.use(express.static("./test-ui/"));
    app.use(express.static("./dist/"));
    app.use(express.static("./data/"));
    app.listen(SERVER_PORT);

    gutil.log("Starting '" + gutil.colors.cyan("server") + "' " +
        gutil.colors.grey("http://localhost:" + SERVER_PORT + "/index.html"));
});

gulp.task("mocha", function() {
    return gulp.src(paths.test + "/**/*.js", {read: false})
        .pipe(mocha({
            require: [paths.test + "/InitTestInNode.js"]
        }));
});

gulp.task("default", ["clean"], function(cb) {
    runSequence("lint", /*"mocha",*/ "build", "dist", "run-test", cb);
});
