/*global ctx*/
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
let assert = require("assert");

describe("SearchManagerObject", function() {

    let oipfObjectFactory = ctx.createOipfObjectFactory();
    let searchManager = oipfObjectFactory.createSearchManagerObject();

    before(function(done) {
        this.timeout(5000); // for zip, can take a while
        if (searchManager.getChannelConfig().channelList.length) {
            done();
        } else {
            searchManager.getChannelConfig().onChannelListUpdate = done;
        }
    });

    /**
     * Try to get programmes in test xml file (data/fr_tnt.xml) for a specific channel
     * using channel ccid.
     */
    describe("Search programme with channel", function() {
        this.timeout(5000); // for zip, can take a while

        it("all", function(done) {
            searchManager.onMetadataSearch = function(search, state) {
                console.log("Result found", search.result.length);
                if (state !== 0) {
                    assert(false);
                } else {
                    assert(search.result.length > 0);
                }
                done();
            };

            let search = searchManager.createSearch(1);

            // channel 0 is EUR2.kazer.org
            let channelEurope2 = searchManager.getChannelConfig().channelList[0];
            search.addChannelConstraint(channelEurope2);
            search.orderBy("Programme.startDate", true);
            search.result.getResults(0, 100);
        });
    });

    /**
     * Try to get programmes in test xml file (data/fr_tnt.xml) for a specific date query.
     */
    describe.skip("Search programme with query", function() {
        this.timeout(5000); // for zip, can take a while

        it("all", function(done) {
            searchManager.onMetadataSearch = function(search, state) {
                console.log("Result found", search.result.length);
                if (state !== 0) {
                    assert(false);
                } else {
                    assert(search.result.length > 0);
                }
                done();
            };

            let search = searchManager.createSearch(1);

            // Programme.startDate >= new Date()
            let query = search.createQuery("Programme.startDate", new Date(), 3);
            search.setQuery(query);
            search.orderBy("Programme.startDate", true);
            search.result.getResults(0, 100);
        });
    });
});
