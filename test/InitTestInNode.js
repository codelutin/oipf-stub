"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Mocks
global.window = {};

global.HTMLElement = function() {};
global.HTMLObjectElement = function() {
    this.data = "";
    this.height = 50;
    this.with = 100;
};

global.document = {
    registerElement: function(name, opts) {
        let f = function() {
            if (this.createdCallback) {
                this.createdCallback();
            }
        };
        f.prototype = opts.prototype;
        return f;
    }
};

let wgxpath = require("wgxpath");
wgxpath.install(global);

global.fetch = require("node-fetch");
global.DOMParser = require("xmldom").DOMParser;
global.localStorage = require("localStorage");

// Init context
let XmlTvProvider = require("../src/provider/XmlTvProvider.js");

let OipfStubContext = require("../src/OipfStubContext");
global.ctx = new OipfStubContext();
global.ctx.dataProvider = new XmlTvProvider({
    endpoint: "./data/fr_tnt.xml"
});
