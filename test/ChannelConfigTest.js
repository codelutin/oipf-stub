/*global ctx*/
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
let assert = require("assert");

describe("ChannelConfig", function() {

    let oipfObjectFactory = ctx.createOipfObjectFactory();

    /**
     * Try to get channel list with default configuration (data/fr_tnt.xml) file
     * and test that channel list contains at least one element.
     */
    describe("Get channel list", function() {
        this.timeout(5000); // for zip, can take a while

        it("ChannelConfig", function(done) {

            let channelConfig = oipfObjectFactory.createChannelConfig();
            let channelList = channelConfig.channelList;

            assert(channelList.length === 0);

            channelConfig.onChannelListUpdate = function() {
                assert(channelConfig.channelList.length > 1);
                for (let c of channelConfig.channelList) {
                    console.log("      found channel", c.name);
                }
                done();
            };

            // force channel change
            //ctx.dataProvider = ctx.dataProvider;

        });
    });

});
