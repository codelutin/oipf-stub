/*global ctx*/
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
let assert = require("assert");
let Utils = require("../src/OipfStubUtils");

describe("VideoBroadcastObject", function() {

    let oipfObjectFactory = ctx.createOipfObjectFactory();
    let videoBroadcast = oipfObjectFactory.createVideoBroadcastObject();

    before(function(done) {
        this.timeout(5000); // for zip, can take a while
        if (videoBroadcast.getChannelConfig().channelList.length) {
            done();
        } else {
            videoBroadcast.getChannelConfig().onChannelListUpdate = done;
        }
    });

    describe("Event", function() {
        it("FullScreenChange", function(done) {
            videoBroadcast.onFullScreenChange = done;
            videoBroadcast.setFullScreen(!videoBroadcast.fullScreen);
        });
        it("FullScreenChange2", function(done) {
            videoBroadcast.onFullScreenChange = done;
            videoBroadcast.setFullScreen(!videoBroadcast.fullScreen);
        });

        it("setChannel(null)", function(done) {
            videoBroadcast.onPlayStateChange = function() {
                assert(videoBroadcast.playState === 0);
                done();
            };
            videoBroadcast.setChannel(null);
        });

        it("prevChannel()", function(done) {
            let promisePlayState = new Promise(function(resolve, reject) {
                let call = 0;
                videoBroadcast.onPlayStateChange = function() {
                    // connecting == 1; presenting == 2
                    assert(videoBroadcast.playState === ++call);
                    if (call == 2) {
                        resolve();
                    }
                };
            });

            let promiseChannelChange = new Promise(function(resolve, reject) {
                videoBroadcast.onChannelChangeSucceeded = function(channel) {
                    let channels = videoBroadcast.getChannelConfig().channelList;
                    assert(Utils.isSameChannel(channel, channels[channels.length-1]));
                    resolve();
                }
                videoBroadcast.onChannelChangeError = function(channel, error) {
                    reject()
                }
            });

            Promise.all([promisePlayState, promiseChannelChange]).then(function() {
                done();
            });

            videoBroadcast.prevChannel();
        });

        it("setChannel(Bad channel)", function(done) {
            let currentChannel = videoBroadcast.currentChannel;
            videoBroadcast.onPlayStateChange = function() {
                assert(false);
            };

            videoBroadcast.onChannelChangeSucceeded = function(channel) {
                assert(false);
                done();
            }
            videoBroadcast.onChannelChangeError = function(channel, error) {
                assert(channel);
                assert(error === 5);
                assert(currentChannel === videoBroadcast.currentChannel);
                done();
            }


            let channel = videoBroadcast.createChannelObject(242, 0, 0);
            videoBroadcast.setChannel(channel);
        });

        it("setChannel(Good Channel)", function(done) {
            let channels = videoBroadcast.getChannelConfig().channelList;

            let promisePlayState = new Promise(function(resolve, reject) {
                let call = 0;
                videoBroadcast.onPlayStateChange = function() {
                    // connecting == 1; presenting == 2
                    assert(videoBroadcast.playState === ++call);
                    if (call == 2) {
                        resolve();
                    }
                };
            });

            let promiseChannelChange = new Promise(function(resolve, reject) {
                videoBroadcast.onChannelChangeSucceeded = function(channel) {
                    assert(channel === channels[0]);
                    resolve();
                }
                videoBroadcast.onChannelChangeError = function(channel, error) {
                    reject()
                }
            });

            Promise.all([promisePlayState, promiseChannelChange]).then(function() {
                done();
            });

            videoBroadcast.setChannel(channels[0]);
        });


    });

});
