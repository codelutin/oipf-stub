"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;
let videobcastCount = 0;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let ConfigurationObject = require("./configuration/ConfigurationObject");
    let ParentalControlManagerObject = require("./parentalControlManager/ParentalControlManagerObject");
    let SearchManagerObject = require("./searchManager/SearchManagerObject");
    let VideoBroadcastObject = require("./broadcast/VideoBroadcastObject");
    let ChannelConfig = require("./broadcast/ChannelConfig");
    let ApplicationManagerObject = require("./application/ApplicationManagerObject");
    let RecordingSchedulerObject = require("./recording/RecordingSchedulerObject");

    return class OipfObjectFactory {

        constructor() {
            ctx.__internal__.init(this);

            this.__internal__.oipfObject = {
                ConfigurationObject,
                ChannelConfig,
                ParentalControlManagerObject,
                SearchManagerObject,
                VideoBroadcastObject,
                ApplicationManagerObject,
                RecordingSchedulerObject
            };
            this.__internal__.instance = {};
            let self = this; // eslint-disable-line consistent-this
            this.__internal__.createOipfObject = function(singleton, name, args, createFn) {
                let type = ctx.config && ctx.config.oipfObject && ctx.config.oipfObject[name];
                let result;
                if (type === "none") {
                    console.warn("[oipfObjectFactory] " + name + " deactivate by configuration");
                } else if (type) {
                    let factory = ctx.config.oipfObjectFactory[type];
                    if (factory) {
                        let fn = factory["create" + name];
                        result = fn();
                    } else {
                        console.error("[oipfObjectFactory] can't find factory '" + type + "' in configuration");
                    }
                } else if (!this.oipfObject[name]) {
                    console.error("[oipfObjectFactory] " + name + " is not implemented");
                } else if (singleton) {
                    if (!this.instance[name]) {
                        if (createFn) {
                            result = this.instance[name] = createFn(...args);
                        } else {
                            result = this.instance[name] = new this.oipfObject[name](...args);
                        }
                    } else {
                        result = this.instance[name];
                    }
                } else if (createFn) {
                    result = createFn(...args);
                } else {
                    result = new this.oipfObject[name](...args);
                }

                // Add reference to factory in all objects except those that come from native
                if (result.hasOwnProperty("__internal__")) {
                    result.__internal__.oipfObjectFactory = self;
                }

                return result;
            };

            // register elements managed by this factory
            this._registerElements();
        }

        /**
         * Register DOM related element to be called from constructor and overridden.
         */
        _registerElements() {

            // register dom element (only one per factory instance)
            if (!this.__internal__.VideoBroadcastObjectElt) {
                this.__internal__.VideoBroadcastObjectElt =
                    document.registerElement("video-broadcast" + (videobcastCount ? videobcastCount : ""), {
                        prototype: VideoBroadcastObject.prototype,
                        extends: "object"
                    });
                videobcastCount++;
            }

        }

        /**
         * This method SHALL return true if and only if an object of the specified type is supported by the OITF, otherwise
         * it SHALL return false.
         *
         * @param   {String} mimeType If the value of the argument is one of the MIME types defined in tables 1 to 4 of
         *                            [OIPF_MEDIA2] or one of the DAE defined mime types listed below then an accurate
         *                            indication of the OITF’s support for that MIME type SHALL be returned.
         *
         *                            For other values, it is recommended that an OITF returns a value which accurately
         *                            reflects its support for the specified MIME type.
         *
         *                            --------------------------------------------
         *                                          DAE MIME Type
         *                            --------------------------------------------
         *                            application/notifsocket
         *                            --------------------------------------------
         *                            application/oipfApplicationManager
         *                            --------------------------------------------
         *                            application/oipfCapabilities
         *                            --------------------------------------------
         *                            application/oipfCodManager
         *                            --------------------------------------------
         *                            application/oipfCommunicationServices
         *                            --------------------------------------------
         *                            application/oipfConfiguration
         *                            --------------------------------------------
         *                            application/oipfDownloadManager
         *                            --------------------------------------------
         *                            application/oipfDownloadTrigger,
         *                            --------------------------------------------
         *                            application/oipfDrmAgent
         *                            --------------------------------------------
         *                            application/oipfGatewayInfo
         *                            --------------------------------------------
         *                            application/oipfCommunicationServices
         *                            --------------------------------------------
         *                            application/oipfMDTF
         *                            --------------------------------------------
         *                            application/oipfParentalControlManager
         *                            --------------------------------------------
         *                            application/oipfRecordingScheduler
         *                            --------------------------------------------
         *                            application/oipfRemoteControlFunction
         *                            --------------------------------------------
         *                            application/oipfRemoteManagement
         *                            --------------------------------------------
         */
        isObjectSupported(mimeType) {

            let mimeTypeMethod = {
                "application/notifsocket": false,
                "application/oipfApplicationManager": true,
                "application/oipfCapabilities": true,
                "application/oipfCodManager": false,
                "application/oipfCommunicationServices": false,
                "application/oipfConfiguration": true,
                "application/oipfDownloadManager": false,
                "application/oipfDownloadTrigger": false,
                "application/oipfDrmAgent": false,
                "application/oipfGatewayInfo": false,
                "application/oipfMDTF": false,
                "application/oipfParentalControlManager": true,
                "application/oipfRecordingScheduler": true,
                "application/oipfRemoteControlFunction": false,
                "application/oipfRemoteManagement": false,
                "application/oipfSearchManager": true,
                "application/oipfStatusView": false,
                "video/broadcast": true,
                "video/mpeg": false
            };

            return mimeTypeMethod[mimeType];
        }

        /**
         * If the object type is supported, each of these methods shall return an instance of the corresponding embedded
         * object.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation shall never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the error.name set to the value "TypeError".
         *
         * If the object type is supported, the method shall return an HTMLObjectElement equivalent to the specified
         * object. The value of the type attribute of the HTMLObjectElement SHALL match the mimetype of the instantiated
         * object, for example "video/broadcast" in case of method oipfObjectFactory.createVideoBroadcastObject().
         *
         * @param {Array} requiredCapabilities An optional argument indicating the formats to be supported by the resulting
         *                                     player. Each item in the argument SHALL be one of the formats specified in
         *                                     [OIPF_MEDIA2]. Scarce resources will be claimed by the object at the time of
         *                                     instantiation. The allocationMethod property SHALL be set STATIC_ALLOCATION.
         *                                     If the OITF is unable to create the player object with the requested
         *                                     capabilities, the method SHALL return null.
         *
         *                                     If this argument is omitted, objects do not claim scarce resources so
         *                                     instantiation shall never fail if the object type is supported. The
         *                                     allocationMethod property SHALL be set to DYNAMIC_ALLOCATION.
         */
        createVideoBroadcastObject(/*requiredCapabilities*/) {

            // register dom element (only one per factory instance)
            let VideoBroadcastObjectElt = this.__internal__.VideoBroadcastObjectElt;

            // new instance
            let videoBroadcastObject = this.__internal__.createOipfObject(false, "VideoBroadcastObject", arguments, function() {
                return new VideoBroadcastObjectElt();
            });

            return videoBroadcastObject;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createSearchManagerObject() {
            let result = this.__internal__.createOipfObject(false, "SearchManagerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createApplicationManagerObject() {
            let result = this.__internal__.createOipfObject(true, "ApplicationManagerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createCapabilitiesObject() {
            let result = this.__internal__.createOipfObject(true, "CapabilitiesObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createChannelConfig() {
            let result = this.__internal__.createOipfObject(true, "ChannelConfig", arguments, () => {
                return ctx.dataProvider.getChannelConfig();
            });
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createCodManagerObject() {
            let result = this.__internal__.createOipfObject(true, "CodManagerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createConfigurationObject() {
            let result = this.__internal__.createOipfObject(true, "ConfigurationObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createDownloadManagerObject() {
            let result = this.__internal__.createOipfObject(true, "DownloadManagerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createDownloadTriggerObject() {
            let result = this.__internal__.createOipfObject(true, "DownloadTriggerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createDrmAgentObject() {
            let result = this.__internal__.createOipfObject(true, "DrmAgentObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createGatewayInfoObject() {
            let result = this.__internal__.createOipfObject(true, "GatewayInfoObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createIMSObject() {
            let result = this.__internal__.createOipfObject(true, "IMSObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createMDTFObject() {
            let result = this.__internal__.createOipfObject(true, "MDTFObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createNotifSocketObject() {
            let result = this.__internal__.createOipfObject(true, "NotifSocketObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createParentalControlManagerObject() {
            let result = this.__internal__.createOipfObject(true, "ParentalControlManagerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createRecordingSchedulerObject() {
            let result = this.__internal__.createOipfObject(true, "RecordingSchedulerObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createRemoteControlFunctionObject() {
            let result = this.__internal__.createOipfObject(true, "RemoteControlFunctionObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createRemoteManagementObject() {
            let result = this.__internal__.createOipfObject(true, "RemoteManagementObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods shall return an instance of the corresponding embedded
         * object.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation shall never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the error.name set to the value "TypeError".
         *
         * If the object type is supported, the method shall return an HTMLObjectElement equivalent to the specified
         * object. The value of the type attribute of the HTMLObjectElement SHALL match the mimetype of the instantiated
         * object, for example "video/broadcast" in case of method oipfObjectFactory.createVideoBroadcastObject().
         *
         * @param {Array} requiredCapabilities An optional argument indicating the formats to be supported by the resulting
         *                                     player. Each item in the argument SHALL be one of the formats specified in
         *                                     [OIPF_MEDIA2]. Scarce resources will be claimed by the object at the time of
         *                                     instantiation. The allocationMethod property SHALL be set STATIC_ALLOCATION.
         *                                     If the OITF is unable to create the player object with the requested
         *                                     capabilities, the method SHALL return null.
         *
         *                                     If this argument is omitted, objects do not claim scarce resources so
         *                                     instantiation shall never fail if the object type is supported. The
         *                                     allocationMethod property SHALL be set to DYNAMIC_ALLOCATION.
         */
        createStatusViewObject(/*requiredCapabilities*/) {
            let result = this.__internal__.createOipfObject(false, "StatusViewObject", arguments);
            return result;
        }

        /**
         * If the object type is supported, each of these methods shall return an instance of the corresponding embedded
         * object.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation shall never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the error.name set to the value "TypeError".
         *
         * If the object type is supported, the method shall return an HTMLObjectElement equivalent to the specified
         * object. The value of the type attribute of the HTMLObjectElement SHALL match the mimetype of the instantiated
         * object, for example "video/broadcast" in case of method oipfObjectFactory.createVideoBroadcastObject().
         *
         * @param {Array} requiredCapabilities An optional argument indicating the formats to be supported by the resulting
         *                                     player. Each item in the argument SHALL be one of the formats specified in
         *                                     [OIPF_MEDIA2]. Scarce resources will be claimed by the object at the time of
         *                                     instantiation. The allocationMethod property SHALL be set STATIC_ALLOCATION.
         *                                     If the OITF is unable to create the player object with the requested
         *                                     capabilities, the method SHALL return null.
         *
         *                                     If this argument is omitted, objects do not claim scarce resources so
         *                                     instantiation shall never fail if the object type is supported. The
         *                                     allocationMethod property SHALL be set to DYNAMIC_ALLOCATION.
         */
        createVideoMpegObject(/*requiredCapabilities*/) {
            let result = this.__internal__.createOipfObject(false, "VideoMpegObject", arguments);
            return result;
        }
    };
};
