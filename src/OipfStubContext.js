/* eslint camelcase: 0 */
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let EventEmitter = require("events").EventEmitter;
let Utils = require("./OipfStubUtils");

/**
 * Context use to configure and interact with oipf stub implementation
 * When change change all implementation sub with
 * OipfStubContext#setRequire("path/name", Class)
 *
 * overidable class is:
 * - ChannelConfig
 * - provider/DataProvider
 * - provider/OipfDataProvider
 * - ...
 *
 * All call to #setRequire must be done before OipfStubContext is realy use.
 *
 * Example:
 *
 * let ctx = new OipfStubContext();
 * ctx.dataProvider = new MyOtherProvider();
 * window.oipfObjectFactory = ctx.createOipfObjectFactory();
 * let ChannelConfig = oipfObjectFactory.createChannelConfig();
 * ...
 *
 * You can give config object as argument to context to configure oipf behaviour.
 * Example:
 * let ctx = new OipfStubContext({oipfObject: {VideoBroadcastObject: "native", "SearchManagerObject": "none"}});
 * native use window.oipfObjectFactory
 *
 * or
 * let ctx = new OipfStubContext({oipfObject:
 *    {oipfObjectFactory: {native: window.oipfObjectFactory},
 *    {VideoBroadcastObject: "native", "SearchManagerObject": "none"}});
 * in this case you force native factory. You can with this configuration use
 * another name or multiple oipfObjectFactory.
 *
 * let ctx = new OipfStubContext({oipfObject:
 *    {oipfObjectFactory: {myNative: window.oipfObjectFactory},
 *    {VideoBroadcastObject: "myNative", "SearchManagerObject": "none"}});
 *
 *
 * we wan use native VideoBroadcastObject and no SearchManagerObject. All other
 * object are stub implementation.
 *
 *
 * Extensions:
 *
 * You can use "context.registerExtension("xxx")" to register custom extension for curent context.
 * Check "extensions" folder for available extensions.
 */

// keep native OipfObjectFactory to use if config need native object
window.__nativeOipfObjectFactory__ = window.oipfObjectFactory;

module.exports = class OipfStubContext {

    constructor(config) {
        this.config = config = JSON.parse(JSON.stringify(config || {}));
        // put some defaults values
        this.config.now = this.config.now || Date.now;
        this.config.oipfObjectFactory = this.config.oipfObjectFactory || {};
        this.config.oipfObjectFactory.native = this.config.oipfObjectFactory.native || window.__nativeOipfObjectFactory__;
        // init default configuration
        let defaultConfig = require("../data/default.json");
        this.config.default = Object.assign({}, defaultConfig, this.config.default);

        // map use to keep __internal__ for each name
        let __internal__Map = new Map();

        /**
         * Create no enumerable property __internal__ on me argument
         * initialized with Utils.createGetSetObject() object and eventEmitter
         * if parameter eventNames is present.
         *
         * @param {Object} me object where we want new property __internal__
         * @param {String} name [optional] if present return all time same
         * __internal__ object for this name
         * @param {array of String } eventNames if present list of all event name
         */
        let init = function(name, me, eventNames) {
            let value = __internal__Map.get(name);
            if (!value) {
                value = Utils.createGetSetObject();
                if (name) {
                    // store for next call, only if name is defined
                    __internal__Map.set(name, value);
                }
            }
            Object.defineProperty(me, "__internal__", {enumerable: false, value: value});

            if (eventNames && eventNames.length) {
                if (!me.__internal__.eventEmitter) {
                    me.__internal__.eventEmitter = new EventEmitter();

                    // this is a temp fix because videobcast object can be instanciated multiple times
                    // and ChannelListUpdate can be registered more than 10 times
                    // because registered by external app and videobcast object himself
                    me.__internal__.eventEmitter.setMaxListeners(config.emitterMaxListeners || 10);

                    // used to keep only one callback, useful to onEvent property on object
                    me.__internal__.eventEmitter.addOneListener = function(type, callback) {
                        let internalProp = "__" + type;
                        let oldCallback = this[internalProp];
                        oldCallback && this.removeListener(type, this[internalProp]);
                        this.addListener(type, callback);
                        this[internalProp] = callback;
                    };
                    me.__internal__.eventEmitter.getOneListener = function(type) {
                        let internalProp = "__" + type;
                        return this[internalProp];
                    };
                    me.__internal__.supportedEvent = {};
                }

                for (let e of eventNames) {
                    me.__internal__.supportedEvent[e] = true;
                }

                /**
                 *  method registers the specified listener for event type
                 *
                 * - type: [type String]
                 *   A string representing the event type to listen for.
                 *
                 * - listener: [type Function]
                 *   The object that receives a notification when an event of the
                 *   specified type occurs. This must be an object implementing the
                 *   EventListener interface, or simply a JavaScript function.
                 *
                 * - useCapture: [type Boolean Optional]
                 *   If true, useCapture indicates that the user wishes to initiate
                 *   capture. After initiating capture, all events of the specified type
                 *   will be dispatched to the registered listener before being
                 *   dispatched to any EventTarget beneath it in the DOM tree. Events
                 *   which are bubbling upward through the tree will not trigger a
                 *   listener designated to use capture. See DOM Level 3 Events and
                 *   JavaScript Event order for a detailed explanation. If not
                 *   specified, useCapture defaults to false.
                 */
                me.addEventListener = function(type, listener, useCapture) {
                    if (this.__internal__.supportedEvent[type]) {
                        this.__internal__.eventEmitter.addListener(type, listener);
                    } else if (Object.getPrototypeOf(this).addEventListener) {
                        Object.getPrototypeOf(this).addEventListener(type, listener, useCapture);
                    }
                };

                /**
                 * Removes the event listener previously registered with
                 * addEventListener().
                 *
                 * - type: [type string]
                 *   A string representing the event type to remove.
                 *
                 * - listener: [type Function]
                 *   The EventListener function to remove from the event target.
                 *
                 * - useCapture: [type Boolean Optional]
                 *   Specifies whether the EventListener to be removed was registered as
                 *    a capturing listener or not. If this parameter is absent, a
                 *    default value of false is assumed.
                 *
                 *    If a listener was registered twice, one with capture and one
                 *    without, each must be removed separately. Removal of a capturing
                 *    listener does not affect a non-capturing version of the same
                 *    listener, and vice versa.
                 */
                me.removeEventListener = function(type, listener, useCapture) {
                    if (this.__internal__.supportedEvent[type]) {
                        this.__internal__.eventEmitter.removeListener(type, listener);
                    } else if (Object.getPrototypeOf(this).removeEventListener) {
                        Object.getPrototypeOf(this).removeEventListener(type, listener, useCapture);
                    }
                };
            }
        };

        init(null, this, ["DataProviderChange"]);
        this.__internal__.sharedInit = init;
        this.__internal__.init = init.bind(init, undefined); // force no name

        this.__internal__.requireMap = new Map();

        this.__internal__.requireBound = this.require.bind(this, require);
    }

    require(requireJs, className, noCacheForExtends) {
        let result = this.__internal__.requireMap.get(className);
        if (!result || noCacheForExtends) {
            result = requireJs(className)(this, this.__internal__.requireBound);
            this.__internal__.requireMap.set(className, result);
        }
        return result;
    }

    /**
     * Use to reconfigure class type return by #require().
     * @param {String} className className represented by initial file path
     *                           ex: 'search/MetadataSearch'
     * @param {Class} type      Class to used for this className
     */
    setRequire(className, type) {
        this.__internal__.requireMap.set(className, type);
    }

    /**
     * Call file extension/<extensionName>/Register.js to configure extension behaviour into current context.
     */
    registerExtensions(extensionName) {
        let require = this.__internal__.requireBound;

        // the register need to be compiled as this, can't use dynamic code here,
        // otherwise, Register file won't be included at compilation time
        if (extensionName === "opera") {
            let ExtensionRegister = require("./extensions/opera/Register");
            ExtensionRegister.register();
        }
    }

    createOipfObjectFactory() {
        let require = this.__internal__.requireBound;
        let OipfObjectFactory = require("./OipfObjectFactory");
        return new OipfObjectFactory();
    }

    get dataProvider() {
        if (!this.__internal__.OipfDataProvider) {
            // not use this.require, because DataProvider is directly exported as class
            let DataProvider = require("./provider/DataProvider");
            this.dataProvider = new DataProvider();
        }
        return this.__internal__.OipfDataProvider;
    }

    set dataProvider(provider) {
        let require = this.__internal__.requireBound;
        this.__internal__.OipfDataProvider = this.__internal__.OipfDataProvider
                || new (require("./provider/OipfDataProvider"))();
        this.__internal__.eventEmitter.emit("DataProviderChange", provider);
    }

    set onDataProviderChange(callback) {
        this.__internal__.eventEmitter.addOneListener("DataProviderChange", callback);
    }

    get onDataProviderChange() {
        return this.__internal__.eventEmitter.getOneListener("DataProviderChange");
    }

    get actions() {
        let require = this.__internal__.requireBound;
        let OipfStubActions = require("./OipfStubActions");
        return new OipfStubActions();
    }
};
