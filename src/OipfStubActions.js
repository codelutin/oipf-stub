"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * This actions class allow external actor to change some internal mecanisms on a context with method not available
 * on OipfContextFactory.
 */
module.exports = function() {

    return class OipfStubActions {

        /**
         * Simulate action that user plugged headphones, speakers... on tv.
         */
        nextAudioOutput(oipfObjectFactory) {
            let outputs = oipfObjectFactory.createConfigurationObject().localSystem.outputs;

            // search for current active audio
            let audioOutput = [];
            let indexActive = 0;
            let index = 0;
            for (let o of outputs) {
                if (o.type === "audio") {
                    audioOutput.push(o);
                    if (o.enabled) {
                        indexActive = index;
                        // inactivate all
                        o.enabled = false;
                    }
                    index++;
                }
            }

            // active next audio output
            let nextActive = (indexActive + 1) % audioOutput.length;
            audioOutput[nextActive].enabled = true;
            console.log("[OIPF] Activate audio output :", audioOutput[nextActive].name);
        }

        /**
         * Simulate user plug device into HDMI1 and HDMI3 input.
         */
        toggleHDMIInput(oipfObjectFactory) {
            let inputs = oipfObjectFactory.createConfigurationObject().localSystem.inputs;

            for (let i of inputs) {
                if (i.name === "HDMI1" || i.name === "HDMI3") {
                    if (i.currentAVInputStatus === 1) {
                        i._currentAVInputStatus = 2;
                        console.log("[OIPF]", i.name, "input is now inactive");
                    } else {
                        i._currentAVInputStatus = 1;
                        console.log("[OIPF]", i.name, "input is now active");
                    }
                }
            }
        }

        /**
         * Simulate user plug device into PC input.
         */
        togglePCInput(oipfObjectFactory) {
            let inputs = oipfObjectFactory.createConfigurationObject().localSystem.inputs;

            for (let i of inputs) {
                if (i.name === "PC") {
                    if (i.currentAVInputStatus === 1) {
                        i._currentAVInputStatus = 2;
                        console.log("[OIPF]", i.name, "input is now inactive");
                    } else {
                        i._currentAVInputStatus = 1;
                        console.log("[OIPF]", i.name, "input is now active");
                    }
                }
            }
        }

    };
};
