"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.22 The ATSCTChannelScanParameters class
 *
 * The ATSCTChannelScanParameters class represents the parameters needed to perform a channel scan on an ATSC-
 * T network. This class implements the interface defined by ChannelScanParameters, with the following additions.
 * The properties that are undefined when performing startScan() are considered to be auto detected.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./ChannelScanParameters");

    return class ATSCTChannelScanParameters {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         *Integer startFrequency
         *
         * The start frequency of the scan, in kHz.
         */
        get startFrequency() {
            // FIXME what'is correct default value for ATSC-T
            return this.__internal__.getField("startFrequency", 30000);
        }

        set startFrequency(value) {
            this.__internal__.setField("startFrequency", value);
        }


        /**
         * Integer endFrequency
         *
         * The end frequency of the scan, in kHz.
         */
        get endFrequency() {
            // FIXME what'is correct default value for ATSC-T
            return this.__internal__.getField("endFrequency", 30000000);
        }

        set endFrequency(value) {
            this.__internal__.setField("endFrequency", value);
        }


        /**
         * Integer raster
         *
         * The raster size represented in kHz, typically 6000 as this is the ATSC channel separation.
         */
        get raster() {
            return this.__internal__.getField("raster", 6000);
        }

        set raster(value) {
            this.__internal__.setField("raster", value);
        }


        /**
         * Integer modulationModes
         * The modulation modes to be scanned. Valid values are:
         *   Value        Description
         *     2          2VSB
         *     4          4VSB
         *     8          8VSB
         *     16         16VSB
         * More than one of these values may be arithmetically summed in order to indicate that more than one
         * modulation mode should be scanned.
         */
        get modulationModes() {
            return this.__internal__.getField("modulationModes", 30);
        }

        set modulationModes(value) {
            this.__internal__.setField("modulationModes", value);
        }

    };
};
