"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.13 The FavouriteList class
 *
 *          typedef Collection<Channel> FavouriteList
 *
 * The FavouriteList class represents a list of favourite channels. See Annex K for the definition of the collection
 * template. In addition to the methods and properties defined for generic collections, the FavouriteList class supports
 * the additional properties and methods defined below.
 *
 * In order to preserve backwards compatibility with already existing DAE content the JavaScript toString() method
 * SHALL return the FavouriteList.id for FavouriteList objects.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Collection = require("../shared/Collection");

    return class FavouriteList extends Collection {

        constructor(fields) {
            ctx.__internal__.init(this);
            this.__internal__.setValues(fields);
            this.__internal__.setField("favID", "favID:" + Date.now());
        }

        /**
         * readonly String favID
         *
         * A unique identifier by which the favourite list can be identified.
         */
        get favID() {
            return this.__internal__.getField("favID", undefined);
        }

        /**
         * String name
         * A descriptive name given to the favourite list.
         */
        get name() {
            return this.__internal__.getField("name", this.favID);
        }

        set name(value) {
            return this.__internal__.setField("name", value);
        }

        /**
         * Channel getChannel( String channelID )
         *
         * Description:
         *
         * Return the first channel in the favourite list with the specified channel identifier. Returns
         * null if no corresponding channel can be found.
         *
         * Arguments:
         *
         * - channelID
         *   The channel identifier of the channel to be retrieved, which is a value as
         *   defined for property ccid of the Channel object or a value as defined for
         *   property ipBroadcastID of the Channel object as defined in section
         *   7.13.11.
         */
        getChannel(channelID) {
            let result;
            for (let c of this) {
                if (channelID === c.ccid || channelID === c.ipBroadcastID) {
                    result = c;
                    break;
                }
            }

            return result;
        }

        /**
         * Channel getChannelByTriplet( Integer onid, Integer tsid, Integer sid )
         *
         * Description:
         *
         * Return the first (IPTV or non-IPTV) channel in the list that matches the specified DVB or
         * ISDB triplet (original network ID, transport stream ID, service ID).
         * Where no channels of type ID_ISDB_* or ID_DVB_* are available, or no channel identified
         * by this triplet are found, this method SHALL return null.
         *
         * Arguments:
         *
         * - onid
         *   The original network ID of the channel to be retrieved.
         *
         * - tsid
         *   The transport stream ID of the channel to be retrieved. If set to null the client
         *   SHALL retrieve the channel defined by the combination of onid and sid. This
         *   makes it possible to retrieve the correct channel also in case a remultiplexing took
         *   place which led to a changed tsid.
         *
         * - sid
         *   The service ID of the channel to be retrieved.
         *
         */
        getChannelByTriplet(onid, tsid, sid) {
            let result;
            for (let c of this) {
                if (onid === c.onid && (tsid == null || tsid === c.tsid) && sid === c.sid) {
                    result = c;
                    break;
                }
            }

            return result;

        }

        /**
         * Channel getChannelBySourceID( Integer sourceID )
         *
         * Description:
         *
         * Return the first (IPTV or non-IPTV) channel in the list with the specified ATSC source ID.
         * Where no channels of type ID_ATSC_* are available, or no channel with the specified
         * source ID is found in the channel list, this method SHALL return null.
         *
         * Arguments:
         *
         * - sourceID
         *   The ATSC source_ID of the channel to be returned.
         *
         */
        getChannelBySourceID(sourceID) {
            let result;
            for (let c of this) {
                if (sourceID === c.sourceID) {
                    result = c;
                    break;
                }
            }

            return result;

        }

        /* ------------------------------------------------------------------- *
         * 7.13.13.3 Extensions to FavouriteList
         *
         * If an OITF has indicated support for extended tuner control (i.e. by giving value true to element
         * <extendedAVControl> as specified in section 9.3.6 in its capability description), the OITF SHALL support the
         * following additional constants and methods on the FavouriteList object.
         *
         * When the FavouriteList object is updated with new or removed channels it does not take effect until the object is
         * committed. Only after commit() will the updates of a FavouriteList object become available to other DAE
         * applications.
         *
         * The name property of the FavouriteList object SHALL be read/write for OITFs which are controlled by a service
         * provider. The following methods SHALL also be supported:
         * ------------------------------------------------------------------ */

        /**
         * Boolean insertBefore( Integer index, String ccid )
         *
         * Description:
         *
         * Insert a new favourite into the favourites list at the specified index. In order to add a ccid at
         * the end of the favourite list the index shall be equal to length. This method SHALL return
         * true of the operation succeeded, or false if an invalid index was specified (e.g. index >
         * (length) ).
         *
         * Arguments:
         * - index
         *   The index in the list before which the favourite should be inserted.
         *
         * - ccid
         *   The ccid of the channel to be added.
         *
         */
        insertBefore(index, ccid) {
            let result = index >= 0 && index < this.length;

            if (result) {
                let channel = ctx.dataProvider.getChannelConfig().channelList.getChannel(ccid);
                this.splice(index, 0, channel);
            }

            return result;
        }

        /**
         * Boolean remove( Integer index )
         *
         *  Description:
         *
         *  Remove the item at the specified index from the favourites list. Returns true of the
         *  operation succeeded, or false if an invalid index was specified.
         *
         *  Arguments:
         *
         *  - index
         *    The index of the item to be removed.
         *
         */
        remove(index) {
            let result = index >= 0 && index < this.length;

            if (result) {
                let rest = this.slice(index + 1);
                this.length = index;
                this.push.apply(this, rest);
            }

            return result;
        }

        /**
         * Boolean commit()
         *
         * Description:
         *
         * Commit any changes to the favourites list to persistent storage. This method SHALL return
         * true of the operation succeeded, or false if it failed (e.g. due to insufficient space to store
         * the list on the OITF).
         *
         * If a server has indicated that it requires control of the tuner functionality of an OITF in the
         * server capability description for a particular service, then the OITF SHOULD send an
         * updated Client Channel Listing to the server using HTTP POST over TLS as described in
         * section 4.8.1.1.
         */
        commit() {
            localStorage.setItem("oipf.favouriteList." + this.favID, JSON.stringify(this));
            return true;
        }

    };
};
