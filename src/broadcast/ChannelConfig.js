"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*The ChannelConfig class provides the entry point for applications to get information about the list of channels
 * available. It can be obtained in two ways:
 * -By calling the method getChannelConfig() of the video/broadcast embedded object as defined in
 *  section 7.13.1.3.
 * -By calling the method createChannelConfig() of the object factory API as defined in section 7.1.1.
 * The availability of the properties and methods are dependent on the capabilities description as specified in section 9.3.
 * The following table provides a list of the capabilities and the associated properties and methods. If the capability is false
 * the properties and methods SHALL NOT be available to the application. Properties and methods not listed in the
 * following table SHALL be available to all applications as long as the OITF has indicated support for tuner control (i.e.
 * <video_broadcast>true</video_broadcast> as defined in section 9.3.1) in their capability.
 * ------------------------------------------------------------------------
 * Capability                       | Properties    | Methods
 * --------------------------------- --------------- ----------------------
 * Element <extendedAVControl>      | onChannelScan | startScan()
 * is set to “ true ” as defined in |               | stopScan()
 * section 9.3.6.                   |               |
 * ---------------------------------|---------------|----------------------
 * Element <video_broadcast         |               |createChannelList()
 * type="ID_IPTV_SDS"> is set as    |               |
 * defined in section 9.3.6.        |               |
 */

let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let ChannelList = require("./ChannelList");
    let Channel = require("./Channel");

    return class ChannelConfig {

        constructor() {
            ctx.__internal__.init(this, ["ChannelListUpdate", "ChannelScan"]);

            this.__internal__.supportedEvent = {
                ChannelListUpdate: true,
                ChannelScan: true
            };

            // init currentChannel to not loose it
            this.__internal__.getField("currentChannel", function() {
                // this is a strange dependency with videobroadcast :(
                let configChannel = localStorage.getItem("oipf.videobroadcast.channelIndex");
                let configChannelIndex = configChannel ? parseInt(configChannel, 10) : 0;
                return configChannelIndex;
            });

            // force first init
            this._onProviderChannelListChanged();

            ctx.dataProvider.addEventListener("ChannelListUpdate", this._onProviderChannelListChanged.bind(this));
        }

        _onProviderChannelListChanged() {
            let channelList = ctx.dataProvider.getChannels();
            this.__internal__.setField("channels", channelList);

            // fix invalid current index (only if there is at least one channel)
            let current = this.__internal__.getField("currentChannel", 0);
            if (channelList.length > 0 && current >= channelList.length) {
                this.__internal__.setField("currentChannel", 0);
            }

            // reset channelList for futur computation on demande
            this.__internal__.setField("channelList", undefined);
            this.__internal__.eventEmitter.emit("ChannelListUpdate");
        }

        /*
         * Description:
         * The current channel of the OITF if the user has given permission
         * to share this information, possibly through a
         * mechanism outside the scope of this specification.
         * If no channel is being presented, or if this information is
         * not visible to the caller, the value of this property SHALL be null.
         * In an OITF where exactly one video/broadcast object is in any state other than
         * Unrealized and the
         * channel being presented by that video/broadcast object is the only broadcast
         * channel being presented by
         * the OITF then changes to the channel presented by that video/broadcast object
         * SHALL result in changes
         * to the current channel of the OITF.
         * In an OITF which is presenting more than one broadcast channel at the same time,
         * the current channel of
         * the OITF is the channel whose audio is being presented (as defined in the
         * bindToCurrentChannel()
         * method). If that current channel is under the control of a DAE application via a
         * video/broadcast object
         * then changes to the channel presented by that video/broadcast object SHALL
         * result in changes to the
         * current channel of the OITF
         *
         * Visibility Type : readonly Channel
         */
        get currentChannel() {
            return this.channelList[this.__internal__.getField("currentChannel", 0)];
        }

        /*
         * Description:
         * The list of channels.
         * If an OITF includes a platform-specific application that enables the end-user to choose a channel to be
         * presented from a list then all the channels in the list offered to the user by that application SHALL be
         * included in this ChannelList.
         * he list of channels will be a subset of all those available to the OITF. The precise algorithm by which this
         * subset is selected will be market and/or implementation dependent. For example;
         *   •   If an OITF with a DVB-T tuner receives multiple versions of the same channel, one would be
         *       included in the list and the duplicates discarded
         *   •   An OITF with a DVB tuner will often filter services based on service type to discard those which are
         *       obviously inappropriate or impossible for that device to present to the end-user, e.g. firmware
         *       download services.
         * The order of the channels in the list corresponds to the channel ordering as managed by the OITF. SHALL
         * return the value null if the channel list is not (partially) managed by the OITF (i.e., if the channel list
         * information is managed entirely in the network).
         * The properties of channels making up the channel list SHALL be set by the OITF to the appropriate values
         * as determined by the tables in section 8.4.3. The OITF SHALL store all these values as part of the channel
         * list.
         * Some values are set according to the data carried in the broadcast stream. In this case, the OITF MAY set
         * these values to undefined until such time as the relevant data has been received by the OITF, for example
         * after tuning to the channel. Once the data has been received, the OITF SHALL update the properties of the
         * channel in the channel list according to the received data.
         * Note: There is no requirement for the OITF to pro-actively tune to every channel to gather such data.
         *
         * Visibility Type : readonly ChannelList
         */
        get channelList() {
            let result = this.__internal__.getField("channelList", undefined);
            if (!result) {
                result = new ChannelList();
                let channels = this.__internal__.getField("channels", []);
                for (let i = 0; i < channels.length; i++) {
                    let c = channels[i];
                    result.push(new Channel(i, c));
                }
                this.__internal__.setField("channelList", result);
            }

            return result;
        }

        /*
         * Description:
         * This function is the DOM 0 event handler for events relating to channel list updates. Upon receiving a
         * ChannelListUpdate event, if an application has references to any Channel objects then it SHOULD
         * dispose of them and rebuild its references. Where possible Channel objects are updated rather than
         * removed, but their order in the ChannelConfig.all collection MAY have changed. Any lists created with
         * ChannelConfig.createFilteredList() SHOULD be recreated in case channels have been removed.
         */
        get onChannelListUpdate() {
            this.__internal__.eventEmitter.getOneListener("ChannelListUpdate");
        }

        set onChannelListUpdate(callback) {
            this.__internal__.eventEmitter.addOneListener("ChannelListUpdate", callback);
        }

        /*
         * This function is the DOM 0 event handler for events relating to channel scanning. On IP-only receivers,
         * setting this property SHALL have no effect.
         *
         * The specified function is called with the following arguments:
         * • Integer scanEvent - The type of event. Valid values are:
         * ----------------------------------------------------------------------------------------------------------------
         * Value |                                          Description
         * ------ ---------------------------------------------------------------------------------------------------------
         * 0     |  A channel scan has started.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 1     |  Indicates the current progress of the scan.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 2     |  A new channel has been found.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 3     |  A new transponder has been found.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 4     |  A channel scan has completed.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 5     |  A channel scan has been aborted.
         * ------ ---------------------------------------------------------------------------------------------------------
         * • Integer progress - the progress of the scan. Valid values are in the range 0 - 100, or -1 if the progress is unknown.
         * • Integer frequency - The frequency of the transponder in kHz (for scans on RF sources only).
         * • Integer signalStrength - The signal strength for the current channel. Valid values are in the range 0 - 100,
         *   or  -1  if the signal strength is unknown.
         * • Integer channelNumber - The logical channel number of the channel that has been found.
         * • Integer channelType - The type of channel that has been found.  Valid values are the same as for Channel.channelType.
         * • Integer channelCount - The total number of channels found so far during the scan.
         * • Integer transponderCount - The total number of transponders found so far during the scan (RF sources only).
         * • Channel newChannel - When scanEvent equals 2, this argument provides a reference to the Channel object that represents
         * the newly identified channel. For other scanEvent values this argument SHALL be NULL.
         */
        get onChannelScan() {
            this.__internal__.eventEmitter.getOneListener("ChannelScan");
        }

        set onChannelScan(callback) {
            this.__internal__.eventEmitter.addOneListener("ChannelScan", callback);
        }

    };
};
