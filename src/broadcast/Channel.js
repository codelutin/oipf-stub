"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The Channel object represents a broadcast stream or service.
 * Channel objects typically represent channels stored in the channel list (see section 7.13.10). Channel objects may also
 * represent locally defined channels created by an application using the createChannelObject() methods on the
 * video/broadcast embedded object or the ChannelConfig class or the createChannelList() method on the
 * ChannelConfig class. Accessing the channel property of a ScheduledRecording object or Recording object
 * which is scheduled on a locally defined channel SHALL return a Channel object representing that locally defined
 * channel.
 * Except for the hidden property, writing to the writable properties on a Channel object SHALL have no effect for
 * Channel objects representing channels stored in the channel list. Applications SHOULD only change these writable
 * properties of a locally defined channel before the Channel object is referenced by another object or passed to an API
 * call as an input parameter. The effects of writing to these properties at any other time is implementation dependent.
 * The LocalSystem object allows hardware settings related to the local device to be read and modified.
 *
 * Note: The standbyState property has been removed from this class.
 */
module.exports = function(ctx) {

    return class Channel {

        constructor(index, fields) {
            ctx.__internal__.init(this);
            this.__internal__.index = index;
            this.__internal__.setValues(fields);
        }

        /**
         * Used in the channelType property to indicate a TV channel.
         */
        static get TYPE_TV() {
            return 0;
        }

        /**
         * Used in the channelType property to indicate a radio channel.
         *
         * Visibility Type: CONSTANT
         */
        static get TYPE_RADIO() {
            return 1;
        }

        /**
         * Used in the channelType property to indicate that the type of the channel
         * is unknown, or known but not of type TV or radio.
         *
         * Visibility Type: CONSTANT
         */
        static get TYPE_OTHER() {
            return 2;
        }

        /**
         * Used during channel scanning to indicate that all TV, radio and other
         * channel types should scanned.
         *
         * Visibility Type: CONSTANT
         */
        static get TYPE_ALL() {
            return 128;
        }

        /**
         * Reserved for data services defined by [TS 102 796].
         *
         * Visibility Type: CONSTANT
         */
        static get TYPE_HBBTV_DATA() {
            return 256;
        }

        /**
         * Used in the idType property to indicate an analogue channel identified by
         * the property `freq' and optionally `cni' or `name'.
         *
         * Visibility Type: CONSTANT
         */
        static get ID_ANALOG() {
            return 0;
        }

        /**
         * Used in the idType property to indicate a DVB-C channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_C() {
            return 2;
        }

        /**
         * Used in the idType property to indicate a DVB-S channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_S() {
            return 11;
        }

        /**
         * Used in the idType property to indicate a DVB-T channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_T() {
            return 12;
        }


        /**
         * Used in the idType property to indicate a channel that is identified through its delivery system descriptor as defined by DVB-SI [EN 300 468] section 6.2.13.
         */
        static get ID_DVB_SI_DIRECT() {
            return 13;
        }


        /**
         * Used in the idType property to indicate a DVB-C or DVB-C2 channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_C2() {
            return 14;
        }


        /**
         * Used in the idType property to indicate a DVB-S or DVB-S2 channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_S2() {
            return 15;
        }


        /**
         * Used in the idType property to indicate a DVB-T or DVB-T2 channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_DVB_T2() {
            return 16;
        }


        /**
         * Used in the idType property to indicate an ISDB-C channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_ISDB_C() {
            return 20;
        }


        /**
         * Used in the idType property to indicate an ISDB-S channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_ISDB_S() {
            return 21;
        }


        /**
         * Used in the idType property to indicate an ISDB-T channel identified by the three properties: `onid', `tsid', `sid'.
         */
        static get ID_ISDB_T() {
            return 22;
        }


        /**
         * Used in the idType property to indicate a terrestrial ATSC channel identified by the property `sourceID'.
         */
        static get ID_ATSC_T() {
            return 30;
        }

        /**
         * Used in the idType property to indicate an IP broadcast channel identified
         * through SD&S by a DVB textual service identifier specified in the format
         * "ServiceName.DomainName" as value for property `ipBroadcastID', with
         * ServiceName and DomainName as defined in [DVB-IPTV]. This idType
         * SHALL be used to indicate Scheduled content service defined by
         * [OIPF_PROT2].
         */
        static get ID_IPTV_SDS() {
            return 40;
        }

        /**
         * Used in the idType property to indicate an IP broadcast channel identified
         * by a DVB MCAST URI (i.e. dvb-mcast://) or by a URI referencing a HAS
         * or MPEG DASH MPD (i.e. http:// or https://), as value for property
         * ipBroadcastID.
         */
        static get ID_IPTV_URI() {
            return 41;
        }

        /**
         * The type of channel. The value MAY be indicated by one of the TYPE_* constants defined above. If the
         * type of the channel is unknown then the value SHALL be “ undefined ”.
         * NOTE: Values of this type between 256 and 511 are reserved for use by related specifications on request by
         * liaison.
         *
         * Visibility Type: readonly Integer
         */
        get channelType() {
            return this.__internal__.getField("channelType", undefined);
        }

        /**
         * The type of identification for the channel, as indicated by one of the ID_* constants defined above.
         *
         * Visibility Type: readonly Integer
         */
        get idType() {
            return this.__internal__.getField("idType", 0);
        }

        /**
         * Unique identifier of a channel within the scope of the OITF. The ccid is defined by the OITF and SHALL have
         * prefix ‘ ccid ’ : e.g. ‘ccid:{tunerID.}majorChannel{.minorChannel}’.
         *
         * Note: the format of this string is platform-dependent.
         *
         * Visibility Type: readonly String
         */
        get ccid() {
            return this.__internal__.getField("ccid", "ccid:" + this.tunerID + "." + this.majorChannel + "." + this.minorChannel);
        }

        /**
         * Optional unique identifier of the tuner within the scope of the OITF that is able to receive the given channel.
         *
         * Visibility Type: readonly String
         */
        get tunerID() {
            return this.__internal__.getField("tunerID", "0");
        }

        /**
         * DVB or ISDB original network ID.
         *
         * Visibility Type: readonly Integer
         */
        get onid() {
            return this.__internal__.getField("onid", this.__internal__.index);
        }

        /**
         * The DVB or ISDB network ID.
         *
         * Visibility Type: readonly Integer
         */
        get nid() {
            return this.__internal__.getField("nid", this.__internal__.index);
        }

        /**
         * DVB or ISDB transport stream ID.
         *
         * Visibility Type: readonly Integer
         */
        get tsid() {
            return this.__internal__.getField("tsid", this.__internal__.index);
        }

        /**
         * DVB or ISDB service ID.
         *
         * Visibility Type: readonly Integer
         */
        get sid() {
            return this.__internal__.getField("sid", this.__internal__.index);
        }

        /**
         * ATSC source_ID value.
         *
         * Visibility Type: readonly Integer
         */
        get sourceID() {
            return this.__internal__.getField("sourceID", this.__internal__.index);
        }

        /**
         * For analogue channels, the frequency of the video carrier in kHz.
         *
         * Visibility Type: readonly Integer
         */
        get freq() {
            return this.__internal__.getField("freq", this.majorChannel * 1000 + this.minorChannel);
        }

        /**
         * For analogue channels, the VPS/PDC confirmed network identifier.
         *
         * Visibility Type: readonly Integer
         */
        get cni() {
            return this.__internal__.getField("cni", this.__internal__.index);
        }

        /**
         * The name of the channel. Can be used for linking analog channels without CNI. Typically, it will contain the
         * call sign of the station (e.g. 'HBO').
         *
         * Visibility Type: readonly String
         */
        get name() {
            return this.__internal__.getField("name", this.__internal__.index + "");
        }

        /**
         * The major channel number, if assigned. Value undefined otherwise. Typically used for channels of type ID_ATSC_*
         * or for channels of type ID_DVB_* or ID_IPTV_SDSin markets where logical channel numbers are used.
         *
         * Visibility Type: readonly Integer
         */
        get majorChannel() {
            return this.__internal__.getField("majorChannel", this.__internal__.index);
        }

        /**
         * The minor channel number, if assigned. Value undefined otherwise. Typically used for channels of type ID_ATSC_*.
         *
         * Visibility Type: readonly Integer
         */
        get minorChannel() {
            return this.__internal__.getField("minorChannel", this.__internal__.index);
        }

        /**
         * For channels of type ID_DVB_SI_DIRECT created through createChannelObject() , this property defines
         * the delivery system descriptor (tuning parameters) as defined by DVB-SI [EN 300 468] section 6.2.13.
         *
         * The dsd property provides a string whose characters shall be restricted to the ISO Latin-1 character set.
         * Each character in the dsd represents a byte of a delivery system descriptor as defined by DVB-SI [EN 300
         * 468] section 6.2.13, such that a byte at position "i" in the delivery system descriptor is equal the Latin-1
         * character code of the character at position "i" in the dsd.
         *
         * Described in the syntax of JavaScript: let sdd[] be the byte array of a system delivery descriptor, in which
         * sdd[0] is the descriptor_tag, then, dsd is its equivalent string, if :
         * dsd.length==sdd.length and
         * for each integer i : 0<=i<dsd.length holds: sdd[i] == dsd.charCodeAt(i).
         *
         * Visibility Type: readonly String
         */
        get dsd() {
            return this.__internal__.getField("dsd", undefined);
        }

        /**
         * Flag indicating whether the channel is marked as a favourite channel or not in one of the favourite lists as
         * defined by the property favIDs.
         *
         * Visibility Type: readonly Boolean
         */
        get favourite() {
            return this.__internal__.getField("favourite", false);
        }

        /**
         * The names of the favourite lists to which this channel belongs (see the favouriteLists property on the ChannelConfigclass).
         *
         * Visibility Type: readonly StringCollection
         */
        get favIDs() {
            let result = this.__internal__.getField("favIDs", undefined);
            // FIXME convert Array to StringCollection
            return result;
        }

        /**
         * Flag indicating whether the current state of the parental control system prevents the channel from being viewed
         * (e.g. a correct parental control pin has not been entered).
         *
         * Note that this property supports the option of client-based management of parental control without excluding
         * server-side implementation of parental control.
         *
         * Visibility Type: readonly Boolean
         */
        get locked() {
            return this.__internal__.getField("locked", false);
        }

        /**
         * Flag indicating whether the user has manually blocked viewing of this channel.  Manual blocking of a channel
         * will treat the channel as if its parental rating value always exceeded the system threshold.
         *
         * Note that this property supports the option of client-based management of manual blocking without excluding
         * server-side management of blocked channels.
         *
         * Visibility Type: readonly Boolean
         */
        get manualBlock() {
            return this.__internal__.getField("manualBlock", false);
        }

        /**
         * If the channel has an idType ofID_IPTV_SDS, this property denotes the DVB textual service identifier of
         * the IP broadcast service, specified in the format “ServiceName.DomainName” with the ServiceName and DomainName
         * as defined in [DVB-IPTV].
         *
         * If the Channel has an idTypeof ID_IPTV_URI, this element denotes a URI of the IP broadcast service.
         *
         * Visibility Type: readonly String
         */
        get ipBroadcastID() {
            return this.__internal__.getField("ipBroadcastID", undefined);
        }

        /**
         * If the channel has an idType of ID_IPTV_SDS, this property denotes the maximum bitrate associated to
         * the channel.
         *
         * Visibility Type: readonly Integer
         */
        get channelMaxBitRate() {
            return this.__internal__.getField("channelMaxBitRate", undefined);
        }

        /**
         * If the channel has idType ID_IPTV_SDS, this property denotes the TimeToRenegotiate associated to the channel.
         *
         * Visibility Type: readonly Integer
         */
        get channelTTR() {
            return this.__internal__.getField("channelTTR", undefined);
        }

        /**
         * Flag indicating whether the channel is available to the recording functionality of the OITF. If the value
         * of the pvrSupport property on the application/oipfConfiguration object as defined in section 7.3.3.2 is 0,
         * this property SHALL be false for all Channel objects.
         *
         * Visibility Type: readonly Boolean
         */
        get recordable() {
            return this.__internal__.getField("recordable", true);
        }

        /* ------------------------------------------------------------------- *
         * 7.13.11.3 Metadata extensions to Channel
         *
         * This subsections SHALL apply for OITFs that have indicated <clientMetadata> with value "true" and a type
         * attribute with values "bcg", "sd-s", "eit-pf" or "dvb-si" as defined in section 9.3.7 in their capability profile.
         * The OITF SHALL extend the Channel class with the properties and methods described below.
         * The values of many of these properties may be derived from elements in the BCG metadata. For optional elements that
         * are not present in the metadata, the default value of any property that derives its value from one of those elements
         * SHALL be undefined.
         * ------------------------------------------------------------------ */

        /**
         * The long name of the channel. If both short and long names are being transmitted, this property SHALL
         * contain the long name of the station (e.g. 'Home Box Office'). If the long name is not available, this property
         * SHALL be undefined.
         *
         * The value of this property may be derived from the Name element that is a child of the BCG
         * ServiceInformation element describing the channel, where the length attribute of the Name element has
         * the value `long'.
         *
         * Visibility Type: readonly String (readonly but not clear in specifications)
         */
        get longName() {
            return this.__internal__.getField("longName", undefined);
        }

        /**
         * The description of the channel. If no description is available, this property SHALL be undefined.
         * The value of this field may be taken from the ServiceDescription element that is a child of the BCG
         * ServiceInformation element describing this channel.
         *
         * Visibility Type: readonly String (readonly but not clear in specifications)
         */
        get description() {
            return this.__internal__.getField("description", undefined);
        }

        /**
         * Flag indicating whether the receiver is currently authorised to view the channel. This describes the
         * conditional access restrictions that may be imposed on the channel, rather than parental control restrictions.
         *
         * Visibility Type: readonly Boolean
         */
        get authorised() {
            return this.__internal__.getField("authorised", undefined);
        }

        /**
         * A collection of genres that describe the channel.
         * The value of this field may be taken from the ServiceGenre elements that are children of the BCG
         * ServiceInformation element describing the channel.
         *
         * Visibility Type: readonly StringCollection (readonly but not clear in specifications)
         */
        get genre() {
            return this.__internal__.getField("genre", undefined);
        }

        /**
         * Flag indicating whether the channel SHALL be included in the default channel list.
         *
         * Visibility Type: readonly Boolean (readonly but not clear in specifications)
         */
        get hidden() {
            return this.__internal__.getField("hidden", false);
        }

        /**
         * Flag indicating whether the channel is a 3D channel.
         *
         * Visibility Type: readonly Boolean
         */
        get is3D() {
            return this.__internal__.getField("is3D", false);
        }

        /**
         * Flag indicating whether the channel is an HD channel.
         *
         * Visibility Type: readonly Boolean
         */
        get isHD() {
            return this.__internal__.getField("isHD", false);
        }

        /**
         * The URL for the default logo image for this channel.
         * The value of this field may be derived from the value of the first Logo element that is a child of the BCG
         * ServiceInformation element describing the channel. If this element specifies anything other than the
         * URL of an image, the value of this filed SHALL be undefined.
         *
         * Visibility Type: readonly String (readonly but not clear in specifications)
         */
        get logoURL() {
            let result = this.__internal__.getField("logoURL", undefined);
            if (!result || !result.match(/[\w]+:.+/)) { // try to match URL
                result = undefined;
            }
            return result;
        }

        /**
         * String getField( String fieldId )
         * Description:
         * Get the value of the field referred to by fieldId that is contained in the BCG metadata for
         * this channel. If the field does not exist, this method SHALL return undefined.
         *
         * Arguments:
         * - fieldId: [type String]
         *   The name of the field whose value SHALL be retrieved.

         * Return:
         * - String
         */
        getField(fieldId) {
            return this.__internal__.getField(fieldId, undefined);
        }

        /**
         * Description:
         *
         * Get the URI for the logo image for this channel. The width and height parameters specify the
         * desired width and height of the image; if an image of that size is not available, the URI of the
         * logo with the closest available size not exceeding the specified dimensions SHALL be
         * returned. If no image matches these criteria, this method SHALL return null.
         * The URI returned SHALL be suitable for use as the SRC attribute in an HTML IMG element or
         * as a background image.
         * The URIs returned by this method will be derived from the values of the Logo elements that
         * are children of the BCG ServiceInformation element describing the channel.
         *
         * Arguments:
         * - width: [type Integer]
         *   The desired width of the image
         *
         * - height: [type Integer]
         *   The desired height of the image
         *
         * Return:
         * - String
         */
        getLogo(/*width, height*/) {
            return undefined;
        }

        // to json is used because getter are not enumerable in ES6 classes
        toJSON() {
            return this.__internal__.getValues();
        }
    };
};
