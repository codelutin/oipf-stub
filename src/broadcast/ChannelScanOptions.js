"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.16 The ChannelScanOptions class
 *
 * The ChannelScanOptions class defines the options that should be applied during a channel scan operation. This
 * class does not define parameters for the channel scan itself.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./Channel");

    return class ChannelScanOptions {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         * Integer channelType
         *
         * The types of channel that should be discovered during the scan. Valid values are TYPE_RADIO, TYPE_TV, or
         * TYPE_OTHER or TYPE_ALL as defined in section 7.13.11.1.
         *
         */
        get channelType() {
            return this.__internal__.getField("channelType", Channel.TYPE_ALL);
        }

        set channelType(value) {
            this.__internal__.setField("channelType", value);
        }

        /**
         * Boolean replaceExisting
         *
         * If true, any existing channels in the channel list managed by the OITF SHALL be removed and the new
         * channel list SHALL consist only of channels found during the channel scan operation. If false, any channels
         * discovered during the channel scan SHALL be added to the existing channel list.
         *
         */
        get replaceExisting() {
            return this.__internal__.getField("replaceExisting", true);
        }

        set replaceExisting(value) {
            this.__internal__.setField("replaceExisting", value);
        }

    };
};
