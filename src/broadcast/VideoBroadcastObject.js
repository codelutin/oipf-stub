"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Description:
 * If the object type is supported, this method shall return an instance of the
 * corresponding embedded object.
 * Since objects do not claim scarce resources when they are instantiated, instantiation shall
 * never fail if the object type is supported. If the method name to create the object is not
 * supported, the OITF SHALL throw an error with the error.name set to the value
 * " TypeError ".
 * If the object type is supported, the method shall return an HTMLObjectElement equivalent
 * to the specified object. The value of the type attribute of the HTMLObjectElement SHALL
 * match the mimetype of the instantiated object, for example " video/broadcast " in case of
 * method oipfObjectFactory.createVideoBroadcastObject() .
 *
 * Arguments:
 * -requiredCapabilities :
 * An optional argument indicating the formats to be supported by
 * the resulting player. Each item in the argument SHALL be one of
 * the formats specified in [OIPF_MEDIA2]. Scarce resources will
 * be claimed by the object at the time of instantiation. The
 * allocationMethod property SHALL be set
 * STATIC_ALLOCATION . If the OITF is unable to create the player
 * object with the requested capabilities, the method SHALL return
 * null .
 * If this argument is omitted, objects do not claim scarce resources
 * so instantiation shall never fail if the object type is supported. The
 * allocationMethod property SHALL be set to DYNAMIC_ALLOCATION .
 */
let Utils = require("../OipfStubUtils");
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./Channel");

    class VideoBroadcastObject extends HTMLObjectElement {

        createdCallback() {
            // configure video source
            ctx.config.videoBroadcastVideoEndpoint = ctx.config.videoBroadcastVideoEndpoint;
            ctx.config.videoBroadcastVideoExtension = ctx.config.videoBroadcastVideoExtension || ".mp4";
            ctx.config.videoBroadcastStartDate = ctx.config.videoBroadcastStartDate || ctx.config.now();

            // use __internal__ with name, all VideoBroadcastObject
            // use same __internal__ object
            ctx.__internal__.sharedInit("VideoBroadcastObject", this, [
                "ChannelChangeError", "ChannelChangeSucceeded",
                "ProgrammesChanged",
                "PlayStateChange", "FullScreenChange"
            ]);

            // if first instanciation fixe all fields
            if (!this.__internal__.intances) {
                this.__internal__.intances = [];

                this.__internal__.UNREALIZED = 0;
                this.__internal__.CONNECTING = 1;
                this.__internal__.PRESENTING = 2;
                this.__internal__.STOPPED = 3;

                this.__internal__.playState = this.UNREALIZED;
                this.__internal__.width = "100%";
                this.__internal__.height = "100%";
                this.__internal__.fullScreen = false;
                this.__internal__.volume = 100;

                // 7.13.2 Extensions to video/broadcast for recording and time-shift
                this.__internal__.recordingState = 0;
                this.__internal__.playbackOffset = 0;
                this.__internal__.maxOffset = undefined;
                this.__internal__.playSpeed = 1;
                this.__internal__.playSpeeds = [-32, -16, -8, -4, -2, -1, 0, 1, 2, 4, 8, 16, 32];
            }

            this.__internal__.intances.push(this);
            this.getChannelConfig().addEventListener("ChannelListUpdate", this._channelListUpdate.bind(this));
        }

        attachedCallback() {
            // force redraw to be in sync with other instance of VideoBroadcastObject
            this._setupCurrentChannelImage(this.__internal__.playState);
        }

        /*
         * The width of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false . Changing the width property corresponds to changing the width property
         * through the HTMLObjectElement interface, and must have the same effect as changing the width through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.width ), at least for values
         * specified in pixels.
         *
         * Visibility Type: read/write Integer
         */
        set width(value) {
            if (!this.__internal__.fullScreen) {
                super.width = value;
            }
        }

        /*
         * The height of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false . Changing the height property corresponds to changing the height property
         * through the HTMLObjectElement interface, and must have the same effect as changing the height through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.height ), at least for values
         * specified in pixels.
         *
         * Visibility Type: read/write Integer
         *
         */
        set height(value) {
            if (!this.__internal__.fullScreen) {
                super.height = value;
            }
        }

        /*
         * Returns true if this video object is in full-screen mode, false otherwise. The default value is false .
         *
         * Visibilité Type: readonly Boolean
         */
        get fullScreen() {
            return this.__internal__.fullScreen;
        }

        /**
         * Setting the value of the data property SHALL have no effect on the
         *  video/broadcast object. If this property is read, the value returned
         *  SHALL always be the empty string.
         */
        get data() {
            return "";
        }

        set data(value) {
            // no effect
        }

        /*
         * The function that is called when a request to switch a tuner to another channel resulted in an error preventing
         * the broadcasted content from being rendered. The specified function is called with the arguments channel
         * and errorState . This function may be called either in response to a channel change initiated by the
         * application, or a channel change initiated by the OITF (see section 7.13.1.1).
         *
         * These arguments are defined as follows:
         *
         * • Channel channel:
         *  the Channel object to which a channel switch was requested, but for which the
         *  error occurred. This object SHALL have the same properties as the channel that was requested,
         *  except that for channels of type ID_DVB_* the values for the onid and tsid properties SHALL be
         *  extracted from the transport stream when one was found (e.g. when errorState is 12).
         *
         * • Number errorState: error code detailing the type of error:
         * ----------------------------------------------------------------------------------------------------------------
         * Value |                                          Description
         * ------ ---------------------------------------------------------------------------------------------------------
         * 0     |  channel not supported by tuner.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 1     |  cannot tune to given transport stream (e.g. no signal)
         * ------ ---------------------------------------------------------------------------------------------------------
         * 2     |  tuner locked by other object.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 3     |  parental lock on channel.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 4     |  encrypted channel, key/module missing.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 5     |  unknown channel (e.g. can’t resolve DVB or ISDB triplet).
         * ------ ---------------------------------------------------------------------------------------------------------
         * 6     |  channel switch interrupted (e.g. because another channel switch was activated before the previous one
         *       |  completed).
         * ------ ---------------------------------------------------------------------------------------------------------
         * 7     |  channel cannot be changed, because it is currently being recorded.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 8     |  cannot resolve URI of referenced IP channel.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 9     |  insufficient bandwidth.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 10    |  channel cannot be changed by nextChannel()/prevChannel() methods either because the OITF does not
         *       |  maintain a favourites or channel list or because the video/broadcast object is in the Unrealized state.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 11    |  insufficient resources are available to present the given channel (e.g. a lack of available codec resources).
         * ------ ---------------------------------------------------------------------------------------------------------
         * 12    |  specified channel not found in transport stream.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 100   |  unidentified error.
         * ----------------------------------------------------------------------------------------------------------------
         */
        get onChannelChangeError() {
            this.__internal__.eventEmitter.getOneListener("ChannelChangeError");
        }

        set onChannelChangeError(callback) {
            this.__internal__.eventEmitter.addOneListener("ChannelChangeError", callback);
        }

        /*
         * Description:
         * Visibility Type: readonly Integer
         * The current play state of the video/broadcast object. Valid values are:
         * ------------------------------------------------------------------------------------------------------------
         * Value |                                            Description
         * ------ -----------------------------------------------------------------------------------------------------
         * 0     |   unrealized; the application has not made a request to start presenting a channel or has stopped
         *       |   presenting a channel and released any  resources. The content of the video/broadcast object should
         *       |   be transparent but if not shall be an opaque black rectangle. Control of media presentation is
         *       |   under the control of the OITF, as defined in section H.2.
         * ------ -----------------------------------------------------------------------------------------------------
         * 1     |   connecting; the terminal is connecting to the media source in order to begin playback. Objects in
         *       |   this state may be buffering data in order to start playback. Control of media the control of the
         *       |   OITF, as defined in section H.2. The content of the video/broadcast object is implementation
         *       |   dependent.
         * ------ -----------------------------------------------------------------------------------------------------
         * 2     |   presenting; the media is currently being presented to the user. The object is in this state
         *       |   regardless of whether the media is playing at normal speed, paused, or playing in a trick mode
         *       |   (e.g. at a speed other than normal speed). Control of media presentation is under the control of
         *       |   the application, as defined in section H.2. The video/broadcast object contains the video being
         *       |   presented.
         * ------ -----------------------------------------------------------------------------------------------------
         * 3     |   stopped; the terminal is not presenting media, either inside the video/broadcast object or in
         *       |   the logical video plane. The logical video plane is disabled. The content of the video/broadcast
         *       |   object SHALL be an opaque black rectangle. Control of media presentation is under the control of
         *       |   the application, as defined in section H.2
         * ------ -----------------------------------------------------------------------------------------------------
         * See section 7.13.1.1 for a description of the state model for a video/broadcast object.
         * NOTE: Implementations where the content of the video/broadcast object is transparent in the unrealized state will give
         * a better user experience than ones where it is black. This happens for an application with video in the background
         * between when it includes a video/broadcast object in the page and when a call to bindToCurrentChannel() completes.
         * Applications which do not need to call bindToCurrentChannel() should not do so. The current channel can be obtained
         * from the currentChannel property on the ApplicationPrivateData object which is the same as that on the video/broadcast
         * object under most normal conditions.
         *
         * Visibility Type: readonly Integer
         */
        get playState() {
            return this.__internal__.playState;
        }

        /*
         * The function that is called when the play state of the video/broadcast object changes. This function may
         * be called either in response to an action initiated by the application, an action initiated by the OITF or an
         * error (see section 7.13.1.1).
         *
         * The specified function is called with the arguments state and error . These arguments are defined as follows:
         *
         * • Number state:
         *  the new state of the video/broadcast object. Valid values are given in the definition of the playState property above.
         *
         * • Number error – if the state has changed due to an error, this field contains an error code detailing the type of error.
         * See the definition of onChannelChangeError above for valid values. If no error has occurred, this argument SHALL take the value undefined .
         */
        get onPlayStateChange() {
            this.__internal__.eventEmitter.getOneListener("PlayStateChange");
        }

        set onPlayStateChange(callback) {
            this.__internal__.eventEmitter.addOneListener("PlayStateChange", callback);
        }

        /*
         * The function that is called when a request to switch a tuner to another channel has successfully completed.
         * This function may be called either in response to a channel change initiated by the application, or a channel
         * change initiated by the OITF (see section 7.13.1.1). The specified function is called with argument channel,
         * which is defined as follows:
         *
         * • Channel channel – the channel to which the tuner switched. This object SHALL have the same properties with
         *   the same values as the currentChannel object (see section 7.13.7).
         */
        get onChannelChangeSucceeded() {
            return this.__internal__.eventEmitter.getOneListener("ChannelChangeSucceeded");
        }

        set onChannelChangeSucceeded(callback) {
            this.__internal__.eventEmitter.addOneListener("ChannelChangeSucceeded", callback);
        }

        /**
         * The function that is called when the value of fullScreen changes.
         */
        get onFullScreenChange() {
            return this.__internal__.eventEmitter.getOneListener("FullScreenChange");
        }

        set onFullScreenChange(callback) {
            this.__internal__.eventEmitter.addOneListener("FullScreenChange", callback);
        }

        //  onfocus, onblur should be done via ancestor HTMLElement

        /**
         * The list of media formats that are supported by the object. Each item
         * SHALL contain a format label according to [OIPF_MEDIA2].
         *
         * If scarce resources are not claimed by the object, the value of this
         * property SHALL be null.
         *
         * Visibility Type: readonly StringCollection
         */
        get playerCapabilities() {
            return null;
        }

        /**
         * Returns the resource allocation method currently in use by the
         * object. Valid values as defined in section 7.14.13.1 are:
         * - STATIC_ALLOCATION = 1
         * - DYNAMIC_ALLOCATION = 2
         *
         * Visibility Type: readonly Integer
         */
        get allocationMethod() {
            return 2;
        }

        /*
         * Returns the channel line-up of the tuner in the form of a
         * ChannelConfig object as defined in section 7.13.9. The method SHALL
         * return the value null if the channel list is not (partially) managed
         * by the OITF (i.e., if the channel list information is managed
         * entirely in the network).
         *
         * @return {ChannelConfig}
         */
        getChannelConfig() {
            return ctx.dataProvider.getChannelConfig();
        }

        /*
         * If the video/broadcast object is in the unrealized state and video from exactly one
         * channel is currently being presented by the OITF then this binds the video/broadcast
         * object to that video.
         * If the video/broadcast object is in the stopped state then this restarts presentation of
         * video and audio from the current channel under the control of the video/broadcast object.
         *
         * If video from more than one channel is currently being presented by the OITF then this binds
         * the video/broadcast object to the channel whose audio is being presented.
         *
         * If there is no channel currently being presented, or binding to the necessary resources to
         * play the channel through the video/broadcast object fails for whichever reason, the OITF
         * SHALL dispatch an event to the onPlayStateChange listener(s) whereby the state
         * parameter is given value 0 (“ unrealized ”) and the error parameter is given the
         * appropriate error code.
         *
         * Calling this method from any other states than the unrealized or stopped states SHALL have
         * no effect.
         *
         * See section 7.13.1.1 for more information of its usage.
         *
         * NOTE: Returning a Channel object from this method does not guarantee that video or audio
         * from that channel is being presented. Applications should listen for the video/broadcast
         * object entering state 2 (“ presenting ”) in order to determine when audio or video is being
         * presented.
         *
         * @return {Channel}
         */
        bindToCurrentChannel() {
            if (this.__internal__.playState !== this.__internal__.UNREALIZED
            || this.__internal__.playState !== this.__internal__.STOPPED) {
                if (this.currentChannel) {
                    this.setChannel(this.currentChannel);
                } else {
                    this._setPlayState(this.__internal__.UNREALIZED);
                }
            }

            return this.currentChannel;
        }

        /*
         * Description:
         * Creates a Channel object of the specified idType . This method is typically used to create a
         * Channel object of type ID_DVB_SI_DIRECT or any other type. The Channel object can subsequently be
         * used by the setChannel() method to switch a tuner to this channel, which may or may not
         * be part of the channel list in the OITF. The resulting Channel object represents a locally
         * defined channel which, if not already present there, does not get added to the channel list
         * accessed through the ChannelConfig class (see section 7.13.9).
         *
         * If the channel of the given type cannot be created or the delivery system descriptor is not
         * valid, the method SHALL return null .
         *
         * If the channel of the given type (ID_DVB_SI_DIRECT) can be created and the delivery system descriptor is valid,
         * the method SHALL return a Channel object whereby at a minimum the properties with the
         * same names (i.e. idType , dsd and sid ) are given the same value as argument idType ,
         * dsd and sid of the createChannelObject method.
         *
         * Else, if the channel of the given type can be created and arguments are considered valid and
         * complete, then either:
         *
         * 1. If the channel is in the channel list then a new object of the same type and with
         * properties with the same values SHALL be returned as would be returned by calling
         * getChannelWithTriplet() with the same parameters as this method.
         *
         * 2. Otherwise, the method SHALL return a Channel object whereby at a minimum the
         * properties with the same names are given the same value as the given arguments of
         * the createChannelObject() method. The values specified for the remaining
         * properties of the Channel object are set to undefined .
         *
         * Arguments:
         *
         * Either (if idType ID_DVB_SI_DIRECT):
         * - idType: [type Integer]
         * The type of channel, as indicated by one of the ID_* constants defined in
         * section 7.13.11.1. Valid values for idType include : ID_DVB_SI_DIRECT . For
         * other values this behaviour is not specified.
         *
         * - dsd: [type String]
         * The delivery system descriptor (tuning parameters) represented as a string
         * whose characters shall be restricted to the ISO Latin-1 character set. Each
         * character in the dsd represents a byte of a delivery system descriptor as defined
         * by DVB-SI [EN 300 468] section 6.2.13, such that a byte at position "i" in the
         * delivery system descriptor is equal the Latin-1 character code of the character at
         * position "i" in the dsd.
         *
         * - sid: [type Integer]
         * The service ID, which must be within the range of 1 to 65535.
         *
         * Or (Any other type):
         * - idType : [type Integer]
         * The type of channel, as indicated by one of the ID_* constants defined in
         * section 7.13.11.1. Valid values for idType include : ID_DVB_SI_DIRECT . For
         * other values this behaviour is not specified.
         *
         * - onid : [type Integer]
         * The original network ID. Optional argument that SHALL be specified
         * when the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI ,
         * or ID_ISDB_* and SHALL otherwise be ignored by the OITF.
         *
         * - tsid : [type Integer]
         * The transport stream ID. Optional argument that MAY be specified when
         * the idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
         * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
         *
         * - sid : [type Integer]
         * The service ID. Optional argument that SHALL be specified when the
         * idType specifies a channel of type ID_DVB_* , ID_IPTV_URI , or
         * ID_ISDB_* and SHALL otherwise be ignored by the OITF.
         *
         * - sourceID : [type Integer]
         * The source_ID. Optional argument that SHALL be specified when the
         * idType specifies a channel of type ID_ATSC_T and SHALL otherwise be
         * gnored by the OITF.
         *
         * - ipBroadcastID : [type String]
         * The DVB textual service identifier of the IP broadcast service, specified in
         * the format “ ServiceName.DomainName ” when idType specifies a
         * channel of type ID_IPTV_SDS , or the URI of the IP broadcast service
         * when idType specifies a channel of type ID_IPTV_URI . Optional
         * argument that SHALL be specified when the idType specifies a channel
         * of type ID_IPTV_SDS or ID_IPTV_URI and SHALL otherwise be ignored
         * by the OITF.
         */
        createChannelObject(idType, onidOrdsd, tsidOrsid, sid, sourceID, ipBroadcastID) {
            let data;
            if (sid === undefined && sourceID === undefined && ipBroadcastID === undefined) {
                let dsd = onidOrdsd;
                sid = tsidOrsid;
                data = {
                    idType: idType,
                    dsd: dsd,
                    sid: sid
                };
            } else {
                let onid = onidOrdsd;
                let tsid = tsidOrsid;
                data = {
                    idType: idType,
                    onid: onid,
                    tsid: tsid,
                    sid: sid,
                    sourceID: sourceID,
                    ipBroadcastID: ipBroadcastID
                };
            }
            return new Channel(undefined, data);
        }

        /*
         * Description: (Validation on Transport Stream won't be done here because this criteria not relevant)
         *
         * Requests the OITF to switch a (logical or physical) tuner to the
         * channel specified by channel and render the received broadcast
         * content in the area of the browser allocated for the video/broadcast
         * object.
         *
         * If the channel specifies an idType attribute value which is not
         * supported by the OITF or a combination of properties that does not
         * identify a valid channel, the request to switch channel SHALL fail
         * and the OITF SHALL trigger the function specified by the
         * onChannelChangeError property, specifying the value 0 (“Channel not
         * supported by tuner”) for the errorState , and dispatch the
         * corresponding DOM event (see below).
         *
         * If the channel specifies an idType attribute value supported by the
         * OITF, and the combination of properties defines a valid channel, the
         * OITF SHALL relay the channel switch request to a local physical tuner
         * that is currently not in use by another video/broadcast object and
         * that can tune to the specified channel. If no tuner satisfying these
         * requirements is available (i.e. all physical tuners that could
         * receive the specified channel are in use), the request SHALL fail and
         * OITF SHALL trigger the function specified by the onChannelChangeError
         * property, specifying the value `2' ("tuner locked by other object")
         * for the errorState and dispatch the corresponding DOM event
         * (see below). If multiple tuners satisfying these requirements are
         * available, the OITF selects one.
         *
         * If the channel specifies an IP broadcast channel, and the OITF
         * supports idType ID_IPTV_SDS or ID_IPTV_URI, the OITF SHALL relay the
         * channel switch request to a logical `tuner' that can resolve the URI
         * of the referenced IP broadcast channel. If no logical tuner can
         * resolve the URI of the referenced IP broadcast channel, the request
         * SHALL fail and the OITF SHOULD trigger the function specified by the
         * onChannelChangeError property, specifying the value 8 ("cannot
         * resolve URI of referenced IP channel") for the errorState, and
         * dispatch the corresponding DOM event.
         *
         * The optional attribute contentAccessDescriptorURL allows for the
         * inclusion of a Content Access Streaming Descriptor (the format of
         * which is defined in Annex E.2) to provide additional information for
         * dealing with IPTV broadcasts that are (partially) DRM-protected. The
         * descriptor may for example include Marlin action tokens or a
         * previewLicense. The attribute SHALL be undefined or null if it is not
         * applicable. If the attribute contentAccessDescriptorURL is present,
         * the trickplay attribute shall take a value of either true or false.
         *
         * If the Transport Stream cannot be found, either via the DSD or the
         * (ONID,TSID) pair, then a call to onChannelChangeError with
         * errorstate=5 ("unknown channel") SHALL be triggered, and the
         * corresponding DOM event dispatched.
         *
         * If the OITF succeeds in tuning to a valid transport stream but this
         * transport stream does not contain the requested service in the PAT,
         * the OITF SHALL remain tuned to that location and SHALL trigger a call
         * to onChannelChangeError with errorstate=12 ("specified channel not
         * found in transport stream"), and dispatch the corresponding DOM
         * event.
         *
         * If, following this procedure, the OITF selects a tuner that was not
         * already being used to display video inside the video/broadcast
         * object, the OITF SHALL claim the selected tuner and the associated
         * resources (e.g., decoding and rendering resources) on behalf of the
         * video/broadcast object.
         *
         * If all of the following are true:
         *
         * - the video/broadcast object is successfully switched to the new
         *   channel the channel is a locally defined channel (created using the
         *   createChannelObject method)
         *
         * - the new channel has the same tuning parameters as a channel already
         *   in the channel list in the OITF
         *
         * - the idType is a value other than ID_IPTV_URI
         *
         * then the result of this operation SHALL be the same as calling
         * setChannel with the channel argument being the corresponding channel
         * object in the channel list, such that:
         *
         * - the values of the properties of the video/broadcast object
         *   currentChannel SHALL be the same as those of the channel in the
         *   channel list
         *
         * - any subsequent call to nextChannel or prevChannel SHALL switch the
         *   tuner to the next or previous channel in the favourite list or
         *   channel list as appropriate, as described in the definitions of
         *   these methods
         *
         * Otherwise, if any of the above conditions is not true, then:
         *
         * - the values of the properties of the video/broadcast object
         *   currentChannel SHALL be the same as those provided in the channel
         *   argument to this method, updated as defined in section 8.4.3
         *
         * - the channel is not considered to be part of the channel list
         *
         * the resulting current channel after any subsequent call to
         * nextChannel() or prevChannel() is implementation dependent, however
         * all appropriate functions SHALL be called and DOM events dispatched.
         * The OITF SHALL visualize the video content received over the tuner in
         * the area of the browser allocated for the video/broadcast object. If
         * the OITF cannot visualize the video content following a successful
         * tuner switch (e.g., because the channel is under parental lock), the
         * OITF SHALL trigger the function specified by the onChannelChangeError
         * property with the appropriate channel and errorState value, and
         * dispatch a corresponding DOM event (see below). If successful, the
         * OITF SHALL trigger the function specified by the
         * onChannelChangeSucceeded property with the given channel value, and
         * also dispatch a corresponding DOM event.
         *
         *
         * From 7.13.2 Extensions to video/broadcast for recording and time-shift
         *
         * The additional offSet attribute optionally specifies the desired offset with respect to the live
         * broadcast in number of seconds from which the OITF SHOULD start playback immediately
         * after the channel switch (whereby offSet is given as a positive value for seeking to a time
         * in the past). If an OITF cannot start playback from the desired position, as indicated by the
         * specified offSet (e.g. because the OITF did not, or could not, record the specified channel
         * prior to the call to setChannel), if the specified offSet is `0', or if the offSet is not
         * specified, the OITF SHALL start playback from the live position after the specified channel
         * switch.
         *
         * Arguments:
         *
         * - channel: [type Channel]
         *   The channel to which a switched is requested.
         *
         *   If the channel object specifies a ccid, the ccid identifies the
         *   channel to be set. If the channel does not specify a ccid, the idType
         *   determines which properties of the channel are used to define the
         *   channel to be set, for example, if the channel is of type ID_IPTV_SDS
         *   or ID_IPTV_URI, the ipBroadcastID identifies the channel to be set.
         *
         *   If null, the video/broadcast object SHALL transition to the
         *   unrealized state and release any resources used for decoding video
         *   and/or audio. A ChannelChangeSucceeded event SHALL be generated when
         *   the operation has completed.
         *
         * - trickplay: [type Boolean]
         *   Optional flag indicating whether resources SHOULD be allocated to
         *   support trick play. This argument provides a hint to the receiver
         *   in order that it may allocate appropriate resources. Failure to
         *   allocate appropriate resources, due to a resource conflict, a lack
         *   of trickplay support, or due to the OITF ignoring this hint, SHALL
         *   have no effect on the success or failure of this method. If
         *   trickplay is not supported, this SHALL be indicated through the
         *   failure of later calls to methods invoking trickplay functionality.
         *
         *   The timeShiftMode property defined in section 7.13.2.2 shall
         *   provide information as to type of trickplay resources that should
         *   be allocated.
         *
         *   If argument contentAccessDescriptorURL is included then the
         *   trickplay argument SHALL be included.
         *
         * - contentAccessDescriptorURL: [type String]
         *   Optional argument containing a Content Access Streaming descriptor
         *   (the format of which is defined in Annex E.2) that can be included
         *   to provide additional information for dealing with IPTV broadcasts
         *   that are (partially) DRM-protected. The argument SHALL be undefined
         *   or null if it is not applicable.
         *
         * - offset: [type Integer]
         *   The optional offset attribute MAY be used to specify the
         *   desired offset with respect to the live broadcast in number
         *   of seconds from which the OITF SHOULD start playback
         *   immediately after the channel switch (whereby offset is
         *   given as a negative value for seeking to a time in the
         *   past).
         */
        setChannel(channel, trickplay, contentAccessDescriptorURL, offset) {
            setTimeout(this._setChannelAsync.bind(this, channel, trickplay, contentAccessDescriptorURL, offset), 0);
        }

        /**
         * Perfrom setChannel async to not block caller during channel change.
         */
        _setChannelAsync(channel, trickplay, contentAccessDescriptorURL, offset) {
            if (channel == null) {
                this.release();
            } else {

                let channels = this.getChannelConfig().channelList;
                let realChannel = channels[channel.__internal__ && channel.__internal__.index];
                if (!realChannel) {
                    // channel not find with index, channel arguement is channel create
                    // by user. Try to find channel in channel list
                    for (let c of channels) {
                        if (Utils.isSameChannel(c, channel)) {
                            realChannel = c;
                            break;
                        }
                    }
                }
                if (realChannel) {
                    this._onCurrentChannelChanged(channel, offset);
                } else {
                    this._onCurrentChannelChangedError(channel, 5);
                }
            }
        }

        /*
         * Description:
         * Requests the OITF to switch the tuner that is currently in use by the video/broadcast
         * object to the channel that precedes the current channel in the active favourite list, or, if no
         * favourite list is currently selected, to the previous channel in the channel list. If it has reached
         * the start of the favourite/channel list, it SHALL cycle to the last channel in the list.
         *
         * If the current channel is not part of the channel list, it is implementation dependent whether
         * the method call succeeds or fails and, if it succeeds, which channel is selected. In both
         * cases, all appropriate functions SHALL be called and DOM events dispatched.
         *
         * If the previous channel is a channel that cannot be received over the tuner currently used by
         * the video/broadcast object, the OITF SHALL relay the channel switch request to a local
         * physical or logical tuner that is not in use and that can tune to the specified channel. The
         * behaviour is defined in more detail in the description of the setChannel method.
         *
         * If an error occurs during switching to the previous channel, the OITF SHALL trigger the
         * function specified by the onChannelChangeError property with the appropriate channel
         * and errorState value, and dispatch the corresponding DOM event (see below).
         *
         * If the OITF does not maintain the channel list and favourite list by itself, the request SHALL
         * fail and the OITF SHALL trigger the onChannelChangeError function with the channel
         * property having the value null , and errorState=10 (“channel cannot be changed by
         * nextChannel()/prevChannel() methods”).
         *
         * If successful, the OITF SHALL trigger the function specified by the
         * onChannelChangeSucceeded property with the appropriate channel value, and also
         * dispatch the corresponding DOM event.
         * Calls to this method are valid in the Connecting, Presenting and Stopped states. They are
         * not valid in the Unrealized state and SHALL fail.
         */
        prevChannel() {
            let channels = this.getChannelConfig().channelList;

            let current = this.currentChannel;
            let index = current.__internal__.index - 1;

            let result;
            if (index < 0) {
                result = channels[channels.length - 1];
            } else {
                result = channels[index];
            }
            if (result) {
                this._onCurrentChannelChanged(result);
            } else {
                this._onCurrentChannelChangedError(null, 10);
            }
        }

        /*
         * Description:
         * Requests the OITF to switch the tuner that is currently in use by the video/broadcast
         * object to the channel that precedes the current channel in the active favourite list, or, if no
         * favourite list is currently selected, to the previous channel in the channel list. If it has reached
         * the start of the favourite/channel list, it SHALL cycle to the last channel in the list.
         *
         * If the current channel is not part of the channel list, it is implementation dependent whether
         * the method call succeeds or fails and, if it succeeds, which channel is selected. In both
         * cases, all appropriate functions SHALL be called and DOM events dispatched.
         *
         * If the next channel is a channel that cannot be received over the tuner currently used by
         * the video/broadcast object, the OITF SHALL relay the channel switch request to a local
         * physical or logical tuner that is not in use and that can tune to the specified channel. The
         * behaviour is defined in more detail in the description of the setChannel method.
         *
         * If an error occurs during switching to the next channel, the OITF SHALL trigger the
         * function specified by the onChannelChangeError property with the appropriate channel
         * and errorState value, and dispatch the corresponding DOM event (see below).
         *
         * If the OITF does not maintain the channel list and favourite list by itself, the request SHALL
         * fail and the OITF SHALL trigger the onChannelChangeError function with the channel
         * property having the value null , and errorState=10 (“channel cannot be changed by
         * nextChannel()/prevChannel() methods”).
         *
         * If successful, the OITF SHALL trigger the function specified by the
         * onChannelChangeSucceeded property with the appropriate channel value, and also
         * dispatch the corresponding DOM event.
         * Calls to this method are valid in the Connecting, Presenting and Stopped states. They are
         * not valid in the Unrealized state and SHALL fail.
         */
        nextChannel() {
            let channels = this.getChannelConfig().channelList;

            let current = this.currentChannel;
            let index = current.__internal__.index + 1;

            let result = channels[index % channels.length];

            if (result) {
                this._onCurrentChannelChanged(result);
            } else {
                this._onCurrentChannelChangedError(null, 10);
            }

        }

        /*
         * Description:
         * Sets the rendering of the video content to full-screen ( fullscreen = true ) or windowed
         * ( fullscreen = false ) mode (as per [Req. 5.7.1.c] of [CEA-2014-A]). If this indicates a
         * change in mode, this SHALL result in a change of the value of property fullScreen .
         * Changing the mode SHALL NOT affect the z-index of the video object.
         *
         * Arguments:
         * - fullScreen : Boolean to indicate whether video content should be rendered full-screen or not.
         */
        setFullScreen(fullscreen) {
            // FIXME poussin 20150803 do something graphicaly ?
            if (this.__internal__.fullScreen !== fullscreen) {
                this.__internal__.fullScreen = fullscreen;
                this.__internal__.eventEmitter.emit("FullScreenChange");
            }
        }

        /*
         * Description:
         * Adjusts the volume of the currently playing media to the volume as indicated by volume.
         * Allowed values for the volume argument are all the integer values starting with 0 up to and
         * including 100.
         * A value of 0 means the sound will be muted.
         * A value of 100 means that the
         * volume will become equal to current “master” volume of the device, whereby the “master”
         * volume of the device is the volume currently set for the main audio output mixer of the
         * device.
         * All values between 0 and 100 define a linear increase of the volume as a percentage
         * of the current master volume, whereby the OITF SHALL map it to the closest volume level
         * supported by the platform.
         * The method returns true if the volume has changed. Returns false if the volume has not
         * changed. Applications MAY use the getVolume() method to retrieve the actual volume set.
         *
         * Arguments:
         * - volume: Integer value between 0 up to and including 100 to indicate volume level.
         *
         * Actually the norm don't talk about the way to obtain the current master volume for the main
         * audio output mixer; So we'll base on the system volume.
         */
        setVolume(volume) {
            let result = false;
            if (volume >= 0 && volume <= 100) {
                this.__internal__.volume = volume;
                result = true;
            }
            return result;
        }

        /*
         * Description:
         * Returns the actual volume level set; for systems that do not support individual volume
         * control of players, this method will have no effect and will always return 100.
         *
         */
        getVolume() {
            return this.__internal__.volume;
        }

        /*
         * Description:
         * Releases the decoder/tuner used for displaying the video broadcast inside the
         * video/broadcast object, stopping any form of visualization of the video inside the
         * video/broadcast object and releasing any other associated resources.
         * If the object was created with an allocationMethod of STATIC_ALLOCATION ,
         * the releasing of resources shall change this to DYNAMIC_ALLOCATION .
         */
        release() {
            this._setPlayState(this.__internal__.UNREALIZED);
        }

        /*
         * Description:
         * Stop presenting broadcast video. If the video/broadcast object is in any state other than the
         * unrealized state, it SHALL transition to the stopped state and stop video and audio
         * presentation. This SHALL have no effect on access to non-media broadcast resources such
         * as EIT information.
         * Calling this method from the unrealized state SHALL have no effect.
         * See section 7.13.1.1 for more information of its usage.
         */
        stop() {
            this._setPlayState(this.__internal__.STOPPED);
        }

        /* ------------------------------------------------------------------- *
         * 7.13.2 Extensions to video/broadcast for recording and time-shift
         *
         * If an OITF has indicated support for recording functionality (i.e. by giving value true to element <recording> as
         * specified in section 9.3.3 in its capability description), the OITF SHALL support the following additional constants,
         * properties and methods on the video/broadcast object, in order to start a recording and/or time-shift of a current
         * broadcast.
         * Note that this functionality is subject to the security model as specified in section 10.1.
         * This functionality is subject to the state transitions represented in the following state diagram:
         * ------------------------------------------------------------------ */

        /**
         * Indicates a playback position relative to the start of the buffered content.
         */
        get POSITION_START() {
            return 0;
        }

        /**
         * Indicates a playback position relative to the current playback position.
         */
        get POSITION_CURRENT() {
            return 1;
        }

        /**
         * Indicates a playback position relative to the end of the buffered content.
         */
        get POSITION_END() {
            return 2;
        }

        /**
         * The function that is called when the playback speed of a channel changes.
         *
         * The specified function is called with one argument, speed, which is defined as follows:
         *
         * - Number speed: [type Number]
         *   the playback speed of the media at the time the event was dispatched.
         *
         * If the playback reaches the beginning of the time-shift buffer at rewind playback speed, then the play state is
         * changed to 2 (`paused') and a PlaySpeedChanged event with a speed of 0 is generated. If the playback
         * reaches the end of the time-shift buffer at fast-forward playback speed, then the play speed is set to 1.0 and
         * a PlaySpeedChanged event is generated.
         */
        get onPlaySpeedChanged() {
            return this.__internal__.eventEmitter.getOneListener("PlaySpeedChanged");
        }

        set onPlaySpeedChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("PlaySpeedChanged", callback);
        }

        /**
         * The function that is called when change occurs in the play position
         * of a channel due to the use of trick play functions.
         *
         * The specified function is called with one argument, position, which is defined as follows:
         * - position: [type Integer]
         *   the playback position of the media at the time the event was dispatched,
         *   measured from the start of the timeshift buffer. If the value of the currentTimeShiftMode property
         *   is 1, this is measured in milliseconds from the start of the timeshift buffer. If the value of the
         *   currentTimeShiftMode property is 2, this is measured in milliseconds from the start of the media
         *   item. If the play position cannot be determined, this argument takes the value undefined.
         */
        get onPlayPositionChanged() {
            return this.__internal__.eventEmitter.getOneListener("PlayPositionChanged");
        }

        set onPlayPositionChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("PlayPositionChanged", callback);
        }

        /**
         * Returns the playback position, specified as the positive offset of the live broadcast in seconds, in the
         * currently rendered (timeshifted) broadcast.
         *
         * When the currentTimeShiftMode property has the value 1, the value of this property is undefined.
         *
         * Visibility Type: readonly Integer
         */
        get playbackOffset() {
            let result;
            if (this.currentTimeShiftMode !== 1) {
                result = this.__internal__.playbackOffset;
            }
            return result;
        }

        /**
         * Returns the maximum playback offset, in seconds of the live broadcast, which is supported for the currently
         * rendered (timeshifted) broadcast. If the maximum offset is unknown, the value of this property SHALL be
         * undefined.
         *
         * When the currentTimeShiftMode property has the value 1, the value of this property is undefined.
         *
         * Visibility Type: readonly Integer
         */
        get maxOffset() {
            return this.__internal__.maxOffset;
        }

        /**
         * Returns the state of the OITF's timeshift and recordNow functionality for the channel shown in the
         * video/broadcast object. One of:
         *  +---------+----------------------------------------------------------------------------------+
         *  |  Value  |  Description                                                                     |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    0    |  Unrealized: user/application has not requested timeshift or recordNow           |
         *  |         |  functionality for the channel shown. No timeshift or recording resources are    |
         *  |         |  claimed in this state.                                                          |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    1    |  Recording has been newly scheduled.                                             |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    2    |  Recording is about to start. The receiver may be monitoring EPG data in order   |
         *  |         |  to ensure that the programme scheduled to be recorded has not been moved,       |
         *  |         |  or to support "accurate recording" functionality as defined in section 11 of TS |
         *  |         |  102 323 [TS 102 323], where slight changes in the start time of the recording   |
         *  |         |  do not result in the start of the recording being missed. No recording          |
         *  |         |  resources have yet been acquired, although the OITF may have tuned to the       |
         *  |         |  channel which is to be recorded.                                                |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    3    |  Acquiring recording resources (incl. media connection).                         |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    4    |  Recording has started.                                                          |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    5    |  Recording has been updated.                                                     |
         *  +---------+----------------------------------------------------------------------------------+
         *  |    6    |  Recording has successfully completed.                                           |
         *  +---------+----------------------------------------------------------------------------------+
         *  |   10    |  Acquiring timeshift resources (incl. media connection).                         |
         *  +---------+----------------------------------------------------------------------------------+
         *  |   11    |  Timeshift mode has started.                                                     |
         *  +---------+----------------------------------------------------------------------------------+
         *
         * Visibility Type: readonly Integer
         */
        get recordingState() {
            return this.__internal__.recordingState;
        }

        /**
         * This function is the DOM 0 event handler for notification of state changes of the recording functionality. The
         * specified function is called with the following arguments:
         *
         ·    * - state [type Integer]
         *   The current state of the recording. One of:
         *   +-------+-----------------------------------------------------------------------------------+
         *   | Value | Description                                                                       |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   0   | Unrealized: user/application has not requested timeshift or recordNow             |
         *   |       | functionality for the channel shown. No timeshift or recording resources are      |
         *   |       | claimed in this state.                                                            |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   1   | Recording has been newly scheduled.                                               |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   2   | Recording is about to start . The receiver may be monitoring EPG data in          |
         *   |       | order to ensure that the programme scheduled to be recorded has not been          |
         *   |       | moved, or to support "accurate recording" functionality as defined in section     |
         *   |       | 11 of TS 102 323 [TS 102 323], where slight changes in the start time of the      |
         *   |       | recording do not result in the start of the recording being missed. No recording  |
         *   |       | resources have yet been acquired, although the OITF may have tuned to the         |
         *   |       | channel which is to be recorded.--------------------------------------------------|
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   3   | Acquiring recording resources (incl. media connection).---------------------------|
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   4   | Recording has started.                                                            |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   5   | Recording has been updated.                                                       |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   6   | Recording has successfully completed.                                             |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |  10   | Acquiring timeshift resources (incl. media connection).                           |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |  11   | Timeshift mode has started.                                                       |
         *   +-------+-----------------------------------------------------------------------------------+
         *
         * - error: [type Integer]
         *   If the state of the recording has changed due to an error, this field contains an
         *   error code detailing the type of error. One of:
         *   +-------+-----------------------------------------------------------------------------------+
         *   | Value | Description                                                                       |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |    0  | The recording sub-system is unable to record due to resource limitations.         |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |    1  | There is insufficient storage space available. (Some of the recording may be      |
         *   |       | available).                                                                       |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |    2  | Tuner conflict (e.g. due to conflicting scheduled recording).                     |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |    3  | Recording not allowed due to DRM restrictions.                                    |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |    4  | Recording has stopped before completion due to unknown (probably                  |
         *   |       | hardware) failure.                                                                |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   10  | Timeshift not possible due to resource limitations.                               |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   11  | Timeshift not allowed due to DRM restrictions.                                    |
         *   +-------+-----------------------------------------------------------------------------------+
         *   |   12  | Timeshift ended due to unknown failure.                                           |
         *   +-------+-----------------------------------------------------------------------------------+
         *   If no error has occurred, this argument SHALL take the value undefined.
         *
         * - recordingId: [type String]
         *   The identifier of the recording to which this event refers, This SHALL be
         *   equal to the value of the id property for the affected recording, if the event is associated with a
         *   specific recording.
         */
        get onRecordingEvent() {
            this.__internal__.eventEmitter.getOneListener("RecordingEvent");
        }

        set onRecordingEvent(callback) {
            this.__internal__.eventEmitter.addOneListener("RecordingEvent", callback);
        }

        /**
         * If the value of the currentTimeShiftMode property is 1, the current playback position of the media,
         * measured in milliseconds from the start of the timeshift buffer.
         * If the value of the currentTimeShiftMode property is 2, the current playback position of the media,
         * measured in milliseconds from the start of the media item.
         *
         * Visibility Type: readonly Integer
         */
        get playPosition() {
            // FIXME
        }

        /**
         * The current play speed of the media.
         *
         * Visibility Type: readonly Number
         */
        get playSpeed() {
            return this.__internal__.playSpeed;
        }

        /**
         * Returns the ordered list of playback speeds, expressed as values relative to the normal playback speed
         * (1.0), at which the currently specified A/V content can be played (as a time-shifted broadcast in the
         * video/broadcast object), or undefined if the supported playback speeds are not known or the
         * video/broadcast object is not in timeshift mode.
         * If the video/broadcast object is in timeshift mode, the playSpeeds array SHALL always include at least values
         * 1.0 and 0.0.
         *
         * Visibility Type: readonly Array of Number
         */
        get playSpeeds() {
            let result;
            if (this.recordingState === 11) {
                result = this.__internal__.playSpeeds;
            }
            return result;
        }

        /**
         * The function that is called when the playSpeeds array values have changed. An application that makes use
         * of the playSpeeds array needs to read the values of the playSpeeds property again.
         */
        get onplaySpeedsArrayChanged() {
            return this.__internal__.eventEmitter.getOneListener("playSpeedsArrayChanged");
        }

        set onplaySpeedsArrayChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("playSpeedsArrayChanged", callback);
        }

        /**
         * The time shift mode indicates the mode of operation for support of timeshift playback in the video/broadcast
         * object. Valid values are:
         * +-------+------------------------------------------------------------------------------------+
         * | Value | Description                                                                        |
         * +-------+------------------------------------------------------------------------------------+
         * |   0   | Timeshift is turned off.                                                           |
         * +-------+------------------------------------------------------------------------------------+
         * |   1   | Timeshift shall use "local resource".                                              |
         * +-------+------------------------------------------------------------------------------------+
         * |   2   | Timeshift shall use "network resources".                                           |
         * +-------+------------------------------------------------------------------------------------+
         * |   3   | Timeshift shall first use "local resource" when available and fallback to "network |
         * |       | resources".                                                                        |
         * +-------+------------------------------------------------------------------------------------+
         * If property is not set the default value of the property is according to preferredTimeShiftMode in section
         * 7.3.2.1.
         *
         * Visibility Type: read/write Integer
         */
        get timeShiftMode() {
            return this.__internal__.timeShiftMode;
        }

        set timeShiftMode(value) {
            if (value >= 0 && value <= 3) {
                this.__internal__.timeShiftMode = value;
            }
        }

        /**
         * When timeshift is in operation the property indicates which resources
         * are currently being used. Valid values are:
         * +-------+---------------------------------------+
         * | Value | Description                           |
         * +-------+---------------------------------------+
         * |   0   | No timeshift.                         |
         * +-------+---------------------------------------+
         * |   1   | Timeshift using "local resource".     |
         * +-------+---------------------------------------+
         * |   2   | Timeshift using "network resources".  |
         * +-------+---------------------------------------+

         * Visibility Type: read/write Integer
         */
        get currentTimeShiftMode() {
            // FIXME, we must can have access to Configuration object (perhaps with ctx.dataProvider.getConfigurationObject ?)
        }

        /**
         * Description Starts recording the broadcast currently rendered in the video/broadcast object. If the
         * OITF has buffered the broadcasted content, the recording starts from the current playback
         * position in the buffer, otherwise start recording the broadcast stream as soon as possible
         * after the recording resources have been acquired. The specified duration is used by the
         * OITF to determine the minimum duration of the recording in seconds from the current
         * starting point.
         *
         * Calling recordNow() while the broadcast that is currently rendered in the
         * video/broadcast object is already being recorded, SHALL have no effect on the recording
         * and SHALL return the value null.
         *
         * In other cases, this method returns a String value representing a unique identifier to
         * identify the recording. If the OITF provides recording management functionality through the
         * APIs defined in section 7.10.4, this SHALL be the value of the id property of the associated
         * Recording object defined in section 7.10.5.
         *
         * The OITF SHALL guarantee that recording identifiers are unique in relation to download
         * identifiers and CODAsset identifiers.
         *
         * The method returns undefined if the given argument is not accepted to trigger a recording.
         *
         * If the OITF supports metadata processing in the terminal, the fields of the resulting
         * Recording object MAY be populated using metadata retrieved by the terminal. Otherwise,
         * the values of these fields SHALL be implementation-dependent.
         *
         * Arguments:
         * - duration [type Integer]
         *   The minimum duration of the recording in seconds. A value of -1 indicates that
         *   the recording SHOULD continue until stopRecording() is called, storage
         *   space is exhausted, or an error occurs. In this case it is essential that
         *   stopRecording() is called later.
         *
         * Return:
         * - String
         */
        recordNow(/*duration*/) {
            // FIXME
        }

        /**
         * Description:
         * Stops the current recording started by recordNow().
         *
         * Return:
         * - undefined
         */
        stopRecording() {
            // FIXME
        }

        /**
         * Description:
         *
         * Pause playback of the broadcast.
         *
         * The action taken depends on the value of the timeShiftMode property.
         *
         * If the value of the timeShiftMode property is 0, if trick play is not supported for the channel
         * currently being rendered, or if the current time shift mode is not supported for the type of
         * channel being presented (e.g. attempting to use network resource to time shift a DVB or
         * analogue channel) this method shall return false.
         *
         * If the timeshift mode is set to 1 or 3 (local resources) and if recording has not yet been
         * started, this method will start recording the broadcast that is currently being rendered live
         * (i.e., not time-shifted) in the video/broadcast object. If the OITF has buffered the `live'
         * broadcasted content, the recording starts with the content that is currently being rendering in
         * the video/broadcast object. If the recording started successfully, the rendering of the
         * broadcasted content is paused, i.e. a still-image video frame is shown.
         *
         * If the timeshift mode is set to 2 (network resources) then the OITF shall follow the
         * procedures defined in section 8.2.6.4 and returns true. Since this operation is asynchronous
         * when the procedure are executed successful the rendering of the broadcasted content is
         * paused, i.e. a still-image video frame is shown, and PlaySpeedChanged event is generated.
         *
         * If the specified timeshift mode is not supported, this method shall return false. Otherwise,
         * this method shall return true. Acquiring the necessary resources to enable the specified
         * timeshift mode may be an asynchronous operation; applications may receive updates of this
         * process by registering a listener for RecordingEvents as defined in section 7.13.2.4.
         *
         * If trick play is not supported for the channel currently being rendered, this method shall
         * return false, otherwise true is returned.
         *
         * This operation may be asynchronous, and presentation of the video may not pause until
         * after this method returns. For this reason, a PlaySpeedChanged event will be generated
         * when the operation has completed, regardless of the success of the operation. If the
         * operation fails, the argument of the event SHALL be set to the previous play speed.
         *
         * Return:
         * - Boolean
         */
        pause() {
            // FIXME
        }

        /**
         * Description:
         *
         * Resumes playback of the time-shifted broadcast channel that is currently being rendered in
         * the video/broadcast object at the speed specified by setSpeed(). If the desired speed was
         * not set via setSpeed(), playback is resumed at normal speed (i.e. speed 1.0). If the
         * video/broadcast object is currently not rendering a time-shifted channel, the OITF shall
         * ignore the request to start playback and shall return false. If playback cannot be resumed
         * the OITF shall also return false, otherwise true is returned.
         *
         * This operation may be asynchronous, and presentation of the video may not resume until
         * after this method returns. For this reason, a PlaySpeedChanged event will be generated
         * when the operation has completed, regardless of the success of the operation. If the
         * operation fails, the argument of the event SHALL be set to the previous play speed.
         *
         * The action taken depends on the value of the timeShiftMode property.
         *
         * If the value of the timeShiftMode property is 1 or 3 (local resources) then the OITF shall
         * resume playback of the broadcast channel as specified above and return true.
         *
         * If the value of the timeShiftMode property is 2 (network resources) then the OITF shall
         * follow the procedures defined in section 8.2.6.4 and return true. Since this operation is
         * asynchronous when the procedure is successfully executed a PlaySpeedChanged event is
         * generated with current speed.
         *
         * After initial operation of resume() several events may affect the operation.
         *
         * If during fast forward the end of stream is reached the playback SHALL resume at normal
         * speed and a PlaySpeedChanged event is generated. If the end of stream is reached due to
         * end of content the playback will automatically be paused and a PlaySpeedChanged event is
         * generated. Any resources used for time-shifting SHALL NOT be discarded.
         *
         * If during rewinding the playback reaches the point that it cannot be rewound further,
         * playback will automatically be paused (i.e. the play speed will be changed to 0) and a
         * PlaySpeedChanged event is generated.
         *
         * If for any of these events timeShiftMode is set to 3 and local resources are not available
         * anymore then network sources SHALL be used according to the procedures defined in
         * section 8.2.6.4. The OITF SHALL perform a smooth transition of the stream between local
         * and network resources.
         *
         * Return:
         * - Boolean
         */
        resume() {
            // FIXME
        }

        /**
         * Description:
         *
         * Sets the playback speed of the time-shifted broadcast to the value speed, without changing
         * the paused/resumed state of the time-shifted broadcast.
         *
         * When playback is paused (i.e. by setting the play speed to 0), the last decoded frame of
         * video is displayed.
         *
         * If the time-shifted broadcast cannot be played at the desired speed, specified as a value
         * relative to the normal playback speed, the playback speed will be set to the best
         * approximation of speed. Applications are not required to pause playback of the broadcast or
         * take any other action before calling setSpeed().
         *
         * If the video/broadcast object is currently not rendering a time-shifted channel, the OITF
         * shall ignore the request to change the playback speed and shall return false, otherwise
         * true is returned.
         *
         * This operation may be asynchronous, and presentation of the video may not be affected until
         * after this method returns. For this reason, a PlaySpeedChanged event will be generated
         * when the operation has completed, regardless of the success of the operation. If the
         * operation fails, the argument of the event SHALL be set to the previous play speed.
         * The action taken depends on the value of the timeShiftMode property.
         *
         * If the value of the timeShiftMode property is 1 or 3 (local resources) then the setSpeed()
         * method sets the playback speed of the time-shifted broadcast to the value speed.
         *
         * If the timeShiftMode is set to 2 (network resources) the OITF shall follow the procedures
         * defined in section 8.2.6.4 and return true. Since this operation is asynchronous when the
         * procedure is successfully executed PlaySpeedChanged event is generated with the new
         * speed.
         *
         * After initial operation of setSpeed() several events may affect the operation.
         *
         * If during fast forward the end of stream is reached the playback SHALL resume at normal
         * speed and a PlaySpeedChanged event is generated. If the end of stream is reached due to
         * end of content the playback will automatically be paused and a PlaySpeedChanged event is
         * generated. Any resources used for time-shifting SHALL NOT be discarded.
         *
         * If during rewinding the playback has reaches the point that it cannot be rewound further,
         * playback SHALL resume at normal speed and a PlaySpeedChanged event is generated.
         *
         * If for any of these events if timeShiftMode is set to 3 and local resources are not available
         * anymore then network sources SHALL be used according to the procedures defined in
         * section 8.2.6.4. The OITF SHALL perform a smooth transition of the stream between local
         * and network resources.
         *
         * Arguments:
         * - speed: [Type Number]
         *   The desired relative playback speed, specified as a float value relative to the
         *   normal playback speed of 1.0. A negative value indicates reverse playback. If the
         *   time-shifted broadcast cannot be played at the desired speed, the playback
         *   speed will be set to the best approximation.

         * Return
         * - Number
         */
        setSpeed(/*speed*/) {
            // FIXME
        }

        /**
         * Description:
         *
         * Sets the playback position of the time-shifted broadcast that is being rendered in the
         * video/broadcast object to the position specified by the offset and the reference point as
         * specified by one of the constants defined in section 7.13.2.1. Returns true if the playback
         * position is a valid position to seek to, false otherwise. If the video/broadcast object is
         * currently not rendering a time-shifted channel or if the position falls outside the time-shift
         * buffer, the OITF shall ignore the request to seek and shall return the value false.
         *
         * Applications are not required to pause playback of the broadcast or take any other action
         * before calling seek().
         *
         * This operation may be asynchronous, and presentation of the video may not be affected
         * until after this method returns. For this reason, a PlayPositionChanged event will be
         * generated when the operation has completed, regardless of the success of the operation. If
         * the operation fails, the argument of the event SHALL be set to the previous play position.
         * The action taken depends on the value of the timeShiftMode property.
         * If the timeShiftMode is set to 1 (local resources) the seek() method sets the playback
         * position of the time-shifted broadcast that is being rendered in the video/broadcast object as
         * defined above. Playback of live content is resumed if the new position equals the end of the
         * time-shift buffer.
         *
         * If the timeShiftMode is set to 2 (network resources) the OITF shall follow the procedures
         * defined in section 8.2.6.4 and return true. Since this operation is asynchronous when the
         * procedure is successfully executed PlayPositionChanged event is generated with the new
         * position.
         *
         * Note that if timeShiftMode is set to 3 then local resources are used over network
         * resources.
         *
         * After initial operation of seek() several events may affect the operation.
         * If during fastforward the end of stream is reached the playback SHALL resume at normal
         * speed and a PlaySpeedChanged event is generated. If the end of stream is reached due to
         * end of content the playback will automatically be paused and a PlaySpeedChanged event is
         * generated. Any resources used for time-shifting SHALL NOT be discarded.
         *
         * If for any of these events if timeShiftMode is set to 3 and local resources are not available
         * anymore then network sources SHALL be used according to the procedures defined in
         * section 8.2.6.4. The OITF SHALL perform a smooth transition of the stream between local
         * and network resources.
         *
         * Arguments:
         * - offset: [type: Integer]
         *   The offset from the reference position, in seconds. This can be either a
         *   positive value to indicate a time later than the reference position or a negative
         *   value to indicate a time earlier than the reference position.
         *
         * - reference: [type: Integer]
         *   The reference point from which the offset SHALL be measured. The reference
         *   point can be either POSITION_CURRENT, POSITION_START, or
         *   POSITION_END.
         *
         * Return:
         * - Boolean
         */
        seek(/*offset, reference*/) {
            // FIXME
        }

        /**
         * Description:
         *
         * Stops rendering in time-shifted mode of the broadcast channel in the video/broadcast
         * object and, if applicable, plays the current broadcast from the live point and stops time-
         * shifting the broadcast. The OITF SHALL release all resources that were used to support
         * time-shifted rendering of the broadcast.
         *
         * Returns true if the time-shifted broadcast was successfully stopped and resources were
         * released and false otherwise. If the video/broadcast object is currently not rendering a
         * time-shifted channel, the OITF shall ignore the request to stop the time-shift and shall return
         * the value false.
         *
         * Return:
         * - Boolean
         */
        stopTimeshift() {
            // FIXME
        }

        /* ------------------------------------------------------------------- *
         * 7.13.3 Extensions to video/broadcast for access to EIT p/f
         * The following properties and events SHALL be added to the video/broadcast embedded object, if the OITF has indicated
         * support for accessing DVB-SI EIT p/f information, by giving the value "true" to element <clientMetadata> and
         * the value "eit-pf" or "dvb-si" to the type attribute of that element as defined in section 9.3.7 in their capability
         * profile.
         * Access to these properties SHALL adhere to the security model in section 10. The associated permission name is
         * "permission_metadata".
         * ------------------------------------------------------------------- */

        /**
         * The collection of programmes available on the currently tuned channel. This list is a ProgrammeCollection
         * as defined in section 7.16.3 and is ordered by start time, so index 0 will always refer to the present
         * programme (if this information is available).
         * If the type attribute of the <clientMetadata> element in the OITF's capability description has the value
         * "eit-pf", this list SHALL at least provide Programme objects as defined in section 7.16.2 for the present
         * and the directly following programme on the currently tuned channel, if that information is available. In other
         * words, the DAE application should not expect programmes.length to be larger than 2.
         * If the video/broadcast object is not currently tuned to a channel, or if the present/following information has
         * not yet been retrieved (e.g. the object has just tuned to a new channel and present/following information has
         * not yet been broadcast), or if present/following information is not available for the current channel, the length
         * of this collection SHALL be 0.
         * If the type attribute of the <clientMetadata> element in the OITF's capability description has a value other
         * than "eit-pf", an OITF MAY populate this field from other metadata sources described in [OIPF_META2].
         * The programmes.length property SHALL indicate the number of items that are currently known and up to
         * date (i.e. whereby the "startTime + duration" is not smaller than the current time). This may be 0 if no
         * programme information is currently known for the currently tuned channel.
         * In order to prevent misuse of this information, access to this property SHALL adhere to the security model in
         * section 10. The associated permission name is "permission_metadata".
         *
         * Visibility type: readonly ProgrammeCollection
         */
        get programmes() {
            return this.__internal__.programmes;
        }

        /**
         * The function that is called when the programmes property has been updated with new programme
         * information, e.g. when the current broadcast programme is finished and a new one has started. The specified
         * function is called with no arguments.
         */
        get onProgrammesChanged() {
            return this.__internal__.eventEmitter.getOneListener("ProgrammesChanged");
        }

        set onProgrammesChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("ProgrammesChanged", callback);
        }

        /* ------------------------------------------------------------------- *
         * 7.13.4 Extensions to video/broadcast for playback of selected components
         *
         * To support the selection of specific A/V components for playback (e.g. a specific subtitle language, audio language, or
         * camera angle), the classes defined in sections 7.16.5.2 ­ 7.16.5.5 SHALL be supported and the constants, properties and
         * methods defined in section 7.16.5.1 SHALL be supported on the video/broadcast object.
         * ------------------------------------------------------------------ */

        /**
         * Represents a video component. This constant is used for
         * all video components regardless of encoding.
         */
        static get COMPONENT_TYPE_VIDEO() {
            return 0;
        }

        /**
         * Represents an audio component. This constant is used for
         * all audio components regardless of encoding.
         */
        static get COMPONENT_TYPE_AUDIO() {
            return 1;
        }

        /**
         * Represents a subtitle component. This constant is used for
         * all subtitle components regardless of subtitle format. NOTE:
         * A subtitle component may also be related to closed
         * captioning as part of a video stream.
         */
        static get COMPONENT_TYPE_SUBTITLE() {
            return 2;
        }

        /**
         * function onSelectedComponentChanged( Integer componentType )
         *
         * This function is called when there is a change in the set of components being presented. This may occur if
         * one of the currently selected components is no longer available and an alternative is chosen based on user
         * preferences, or when presentation has changed due to a different component or set of components being
         * selected.
         *
         * OITFs MAY optimise event dispatch by dispatching a single event in response to several calls to
         * selectComponent() or unselectComponent() made in rapid succession.
         *
         * The specified function is called with one argument:
         *
         * - Integer componentType -
         *   The type of component whose presentation has changed, as
         *   represented by one of the constant values listed in section 7.16.5.1.1. If more than one component
         *   type has changed, this argument will take the value undefined.
         */
        get onSelectedComponentChanged() {
            return this.__internal__.eventEmitter.getOneListener("SelectedComponentChanged");
        }

        set onSelectedComponentChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("SelectedComponentChanged", callback);
        }

        /**
         * AVComponentCollection getComponents( Integer componentType )
         *
         * Description:
         *
         * If the set of components is known, returns a collection of AVComponent values representing
         * the components of the specified type in the current stream. If componentType is set to null
         * or undefined then all components are returned if they are known.
         *
         * For a video/broadcast object, the set of components SHALL be known if the
         * video/broadcast object is in the presenting state and MAY be known if the object is in other
         * states. For an A/V Control object, the set of components SHALL be known if the A/V Control
         * object is in the playing state and MAY be known if the object is in other states.
         *
         * NOTE: In the case of broadcast MPEG-2 transport streams, this method returns in
         * formation from the PMT but the PMT is not always accurate. Components may be signalled
         * in the PMT which are not actually present all the time. Components may be present but
         * carrying information inconsistent with the PMT, for example a secondary audio stream may
         * be signalled but carrying a copy of the primary audio stream when content for the
         * secondary audio has not been produced. Applications can use the getSIDescriptors()
         * method defined in section 7.16.2.4 to obtain descriptors from the EIT where these subtleties
         * are normally signalled. Exactly how they are "normally signalled" is generally market
         * specific.
         *
         * One or more of the components returned MAY be passed back to one of the other methods
         * unchanged (e.g. selectComponent()).
         * If property preferredAudioLanguage in the Configuration object (refer to section 7.3.2
         * is set then a component is by default selected and is considered as an active component.
         *
         * If property preferredSubtitleLanguage in the Configuration object (refer to section
         * 7.3.2 is set and property subtitleEnabled in AVOutput class (refer to section 7.3.5.1) is
         * enabled then a component is by default selected and is considered as an active component.
         *
         * Arguments:
         *
         * - componentType
         *   The type of component to be returned , as represented by one of the
         *   constant values listed in section 7.16.5.1.1.
         */
        getComponents(/*componentType*/) {
            // FIXME to implement
        }

        /**
         * AVComponentCollection getCurrentActiveComponents( Integer componentType )
         *
         * Description
         *
         * If the set of components is known, returns a collection of AVComponent values representing
         * the currently active components of the specified type that are being rendered. Otherwise
         * returns undefined.
         *
         * For a video/broadcast object, the set of components SHALL be known if the
         * video/broadcast object is in the presenting state and MAY be known if the object is in other
         * states. For an A/V Control object, the set of components SHALL be known if the A/V Control
         * object is in the playing state and MAY be known if the object is in other states.
         * One or more of the components returned MAY be passed back to one of the other methods
         * unchanged (e.g. selectComponent()).
         *
         * Arguments:
         * - componentType
         *   The type of currently active component to be returned. represented
         *   by one of the constant values listed in section 7.16.5.1.1.
         */
        getCurrentActiveComponents(/*componentType*/) {
            // FIXME to implement
        }

        /**
         * void selectComponent( AVComponent component )
         *
         * Description:
         *
         * Select the component that will be subsequently rendered when A/V playback starts or select
         * the component for rendering if A/V playback has already started.
         * If playback has started, this SHALL replace any other components of the same type that are
         * currently playing.
         *
         * If property preferredAudioLanguage in the Configuration object (refer to section 7.3.2) is
         * set then a component is by default selected and it is not necessary to perform
         * selectComponent().
         *
         * If property preferredSubtitleLanguage in the Configuration object (refer to section
         * 7.3.2) is set and property subtitleEnabled in AVOutput class (refer to section 7.3.5.1) is
         * enabled then a component is by default selected and it is not necessary to perform
         * selectComponent().
         *
         * Arguments:
         *
         * - component
         *   A component object available in the stream currently being played.
         *
         *           -----------------------------------------------
         *
         * void selectComponent( Integer componentType )
         *
         * Description
         *
         * If A/V playback has already started, start rendering the default component of the specified
         * type in the current stream. This SHALL replace any other components of the same type
         * that are currently playing.
         * If A/V playback has not started, the default component of the specified type will be
         * subsequently rendered once playback does start.
         *
         * Arguments
         *
         * - componentType
         *   The type of component for which the default component should be
         *   rendered.
         */
        selectComponent(/*componentOrComponentType*/) {
            // FIXME to implement
        }

        /**
         * void unselectComponent( AVComponent component )
         *
         * Description:
         *
         * Stop rendering of the specified component of the stream.
         *
         * If property preferredAudioLanguage in the Configuration object (see section 7.3.2) is
         * set then unselecting a specific component returns to the default preferred audio language.
         *
         * If property preferredSubtitleLanguage in the Configuration object (see section
         * 7.3.2) is set and property subtitleEnabled in AVOutput class (see section 7.3.5.1) is
         * enabled then unselecting a specific component returns to the default preferred subtitle
         * language. In order to stop rendering subtitles completely it is necessary to disable subtitles
         * with property subtitleEnabled in AVOutput class.
         *
         * Arguments:
         *
         * - component
         *   The component to be stopped.
         *
         *           -----------------------------------------------
         *
         * void unselectComponent( Integer componentType )
         *
         * Description
         *
         * If A/V playback has already started, stop rendering of the specified type of component. If
         *
         * A/V playback has not started, no components of the specified type will be subsequently
         * rendered once playback does start.
         *
         * Arguments:
         *
         * - componentType:
         *   The type of component to be stopped.
         */
        unselectComponent(/*componentOrComponentType*/) {
            // FIXME to implement
        }

        /* ------------------------------------------------------------------- *
         * 7.13.5 Extensions to video/broadcast for parental ratings errors
         *
         * For parental rating related errors or changes during playback of A/V content through the video/broadcast object an
         * OITF SHALL support the following intrinsic event properties and corresponding DOM events for the
         * video/broadcast object:
         * ------------------------------------------------------------------ */

        /**
         * The function that is called whenever the parental rating of the content being played inside the embedded
         * object changes.
         *
         * These events may occur at the start of a new content item, or during playback of a content item (e.g. during
         * playback of linear TV content).
         *
         * The specified function is called with four arguments contentID, rating, DRMSystemID and blocked which
         * are defined as follows:
         *
         * - String contentID: [type String]
         *   the content ID to which the parental rating change applies. If the event is
         *   generated by the DRM system, it SHALL be the unique identifier for that content in the context of the
         *   DRM system (i.e. in the case of Marlin BB it is the Marlin contentID, in the case of CSPG-CI+ the
         *   value of this field is null). Otherwise it MAY be null or undefined.
         *
         * - ParentalRatingCollection ratings: [type ParentalRatingCollection]
         *   the parental ratings of the currently playing content. The
         *   ParentalRatingCollection object is defined in section 7.9.
         *
         * - String DRMSystemID: [type String]
         *   the DRM System ID of the DRM system that generated the event as
         *   defined by element DRMSystemID in section 3.3.2 of [OIPF_META2]. The value SHALL be null if
         *   the parental control is not enforced by a particular DRM system.
         *
         * - Boolean blocked: [type Boolean]
         *   flag indicating whether consumption of the content is blocked by the parental
         *   control system as a result of the new parental rating value.
         */
        get onParentalRatingChange() {
            return this.__internal__.eventEmitter.getOneListener("ParentalRatingChange");
        }

        set onParentalRatingChange(callback) {
            this.__internal__.eventEmitter.addOneListener("ParentalRatingChange", callback);
        }

        /**
         * The function that is called when a parental rating error occurs during playback of A/V content inside the
         * embedded object, and is triggered whenever one or more parental ratings are discovered and none of them
         * are valid. A valid parental rating is defined as one which uses a parental rating scheme that is supported by
         * the OITF and which has a parental rating value that is supported by the OITF.
         * The specified function is called with three arguments contentID, rating, and DRMSystemID which are
         * defined as follows:
         *
         * - String contentID: [type ]
         *   the content ID to which the parental rating error applies. If the event is
         *   generated by the DRM system, it SHALL be the unique identifier for that content in the context of the
         *   DRM system (i.e. in the case of Marlin BB it is the Marlin contentID, in the case of CSPG-CI+ the
         *   value of this field is null). Otherwise it MAY be null or undefined.
         *
         * - ParentalRatingCollection: [type ]
         *   ratings ­ the parental ratings of the currently playing content. The
         *   ParentalRatingCollection object is defined in section 7.9.
         *
         * - String DRMSystemID: [type  Optional]
         *   optional argument that specifies the DRM System ID of the DRM system
         *   that generated the event as defined by element DRMSystemID in section 3.3.2 of [OIPF_META2].
         *   The value SHALL be null if the parental control is not enforced by a particular DRM system.
         */
        get onParentalRatingError() {
            this.__internal__.eventEmitter.getOneListener("ParentalRatingError");
        }

        set onParentalRatingError(callback) {
            this.__internal__.eventEmitter.addOneListener("ParentalRatingError", callback);
        }

        /* ------------------------------------------------------------------- *
         * 7.13.6 Extensions to video/broadcast for DRM rights errors
         *
         * This section SHALL apply to OITF and/or server devices which have indicated support for DRM protection by providing
         * one or more <drm> elements as specified in section 9.3.10:
         * For notifying JavaScript about DRM licensing errors during playback of DRM protected A/V content through the
         * "video/broadcast" object, an OITF SHALL support the following intrinsic event property and corresponding DOM
         * event for the "video/broadcast" object:
         * ------------------------------------------------------------------ */

        /**
         * The function that is called:
         *
         * - Whenever a rights error occurs for the A/V content (no license, license invalid), which has led to
         *       blocking consumption of the content.
         * - Whenever a rights change occurs for the A/V content (license valid), which leads to unblocking the
         *       consumption of the content.
         *
         * This may occur during playback, recording or timeshifting of DRM protected AV content.
         * The specified function is called with four arguments errorState, contentID, DRMSystemID and
         * rightsIssuerURL which are defined as follows:
         *
         * - errorState: [type Integer]
         *   error code detailing the type of error:
         *    0: no license, consumption of the content is blocked.
         *    1: invalid license, consumption of the content is blocked.
         *    2: valid license, consumption of the content is unblocked.
         *
         * - contentID: [type String]
         *   the unique identifier of the protected content in the scope of the DRM system
         *   that raises the error (i.e. in the case of Marlin BB it is the Marlin contentID, in the case of CSPG-CI+
         *   the value of this field is null).
         *
         * - DRMSystemID: [type String]
         *   DRMSystemID as defined by element DRMSystemID in section 3.3.2 of
         *   [OIPF_META2]. For example, for Marlin, the DRMSystemID value is
         *   "urn:dvb:casystemid:19188".
         *
         * - rightsIssuerURL: [type String Optional]
         *   optional element indicating the value of the rightsIssuerURL that can
         *   be used to non-silently obtain the rights for the content item currently being played for which this
         *   DRM error is generated, in cases whereby the rightsIssuerURL is known. Cases whereby the
         *   rightsIssuerURL is known include cases whereby the rightsIssuerURL has been extracted
         *   from the MPEG2_TS of the protected content, retrieved from the SD&S discovery record or from the
         *   associated BCG metadata. The corresponding rightsIssuerURL fields are defined in section
         *   4.1.3.4 of [OIPF_CSP2] and in section 3.3.2 of [OIPF_META2] respectively. If different URLs are
         *   retrieved from the stream and the metadata, then the conflict resolution is implementation-
         *   dependent.
         */
        get onDRMRightsError() {
            return this.__internal__.eventEmitter.getOneListener("DRMRightsError");
        }

        set onDRMRightsError(callback) {
            this.__internal__.eventEmitter.addOneListener("DRMRightsError", callback);
        }

        /* ------------------------------------------------------------------- *
         * 7.13.7 Extensions to video/broadcast for current channel information
         *
         * If an OITF has indicated support for extended tuner control (i.e. by giving value true to element
         * <extendedAVControl> as specified in section 9.3.6 in its capability description), the OITF SHALL support the
         * following additional properties and methods on the video/broadcast object.
         * The functionality as described in this section is subject to the security model of section 10.1.3.8.
         * ------------------------------------------------------------------ */

        /*
         * The channel currently being presented by this embedded object if the user has given permission to share this
         * information, possibly through a mechanism outside the scope of this specification. If no channel is being
         * presented, or if this information is not visible to the caller, the value of this property SHALL be null.
         * The value of this property is not affected during timeshift operations and SHALL reflect the value prior to the
         * start of a timeshift operation, for both local and network timeshift resources.
         *
         * Extensions to video/broadcast for current channel information:
         * If an OITF has indicated support for extended tuner control (i.e. by giving value true to element
         * <extendedAVControl> as specified in section 9.3.6 in its capability description), the OITF SHALL support the
         * following additional properties and methods on the video/broadcast object.
         * The functionality as described in this section is subject to the security model of section 10.1.3.8.
         * Note the property onChannelScan and methods startScan and stopScan have been moved to section 7.13.9.
         *
         * Visibility Type: readonly Channel
         */
        get currentChannel() {
            let currentChannel = this.__internal__.currentChannel;

            if (!currentChannel) {
                let configChannel = localStorage.getItem("oipf.videobroadcast.channelIndex");
                let configChannelIndex = configChannel ? parseInt(configChannel, 10) : 0;

                currentChannel =
                this.__internal__.currentChannel =
                this.getChannelConfig().channelList[configChannelIndex];
            }

            return currentChannel;
        }

        _onCurrentChannelChangedError(channel, errorCode) {
            this.__internal__.eventEmitter.emit("ChannelChangeError", channel, errorCode);
        }

        _onCurrentChannelChanged(channel/*, offset*/) {
            if (this.recordingState !== 0) {
                this._onCurrentChannelChangedError(channel, 7);
            } else {
                // FIXME do something with offset (7.13.2.3 setChannel())
                this.__internal__.currentChannel = channel;
                localStorage.setItem("oipf.videobroadcast.channelIndex", channel.__internal__.index);

                this._setPlayState(this.__internal__.CONNECTING);

                this._setPlayState(this.__internal__.PRESENTING);
                this._updateProgrammes();

                this.__internal__.eventEmitter.emit("ChannelChangeSucceeded", channel);
            }
        }

        _updateProgrammes() {

            clearTimeout(this.__internal__.firstProgrammeTimeout);

            let now = ctx.config.now() / 1000;

            let promise;
            if (ctx.dataProvider.useMetadataConstraint()) {
                let searchManager = this.__internal__.oipfObjectFactory.createSearchManagerObject();
                let metadataSearch = searchManager.createSearch(1);
                let q1 = metadataSearch.createQuery("Programme._endTime", 2, now);
                metadataSearch.setQuery(q1);
                metadataSearch.addChannelConstraint(this.currentChannel);
                let cons = metadataSearch.__internal__.constraints;
                cons.maxResult = 2;
                promise = ctx.dataProvider.getProgrammesByConstraint(cons);

            } else {
                // update programme with no constraints query
                let channelList = ctx.dataProvider.getChannelConfig().channelList;
                let channel = Utils.getChannel(channelList, this.currentChannel);
                promise = ctx.dataProvider.getProgrammes(Utils.getChannelIndex(channelList, channel), channel);
            }

            promise.then(function(progs) {

                // sort
                progs = progs.sort(function(p1, p2) {
                    return p1.startTime - p2.startTime;
                });

                // check condition start date
                let index = 0;
                for (let max = progs.length; index < max; index++) {
                    let p = progs[index];

                    // if programme if not yet finished
                    if (p.startTime + p.duration > now) {
                        break;
                    }
                }
                this.__internal__.programmes = progs.slice(index);

                // send events
                this.__internal__.eventEmitter.emit("ProgrammesChanged");

                // monitor for first programme to end
                if (this.__internal__.programmes.length) {
                    let currentProg = this.__internal__.programmes[0];
                    let endTime = currentProg.startTime + currentProg.duration;
                    let delaySeconds = endTime - now;
                    this.__internal__.firstProgrammeTimeout = setTimeout(this._updateProgrammes.bind(this), delaySeconds * 1000);
                }

                // update logo
                this._setupCurrentChannelImage(this.__internal__.playState);
            }.bind(this));
        }

        _channelListUpdate() {
            this.setChannel(this.getChannelConfig().currentChannel);
        }

        _setPlayState(playState) {
            this.__internal__.playState = playState;
            this.__internal__.eventEmitter.emit("PlayStateChange", this.__internal__.playState);

            this._setupCurrentChannelImage(playState);
        }

        /**
         * Modify style to display channel information on HTML Element (as image, or text).
         * This class returns a structure that completely describes the video/broadcast object
         *
         * @param playState
         * @param style (optionnal style)
         */
        _setupCurrentChannelImage(playState, style) {
            let state = {
                clazz: "",
                message: "",
                style: style,
                progImage: "",
                defaultProgImage: ctx.config.videoBroadcastDefaultImage || ""
            };

            if (playState === this.__internal__.UNREALIZED) {
                if (this.getChannelConfig().channelList.length === 0) {
                    state.clazz = "error";
                    state.message = "Tuning ongoing, please wait a few moments...";
                } else {
                    state.clazz = "unrealized";
                    state.message = "No Channel";
                }
            } else if (playState === this.__internal__.CONNECTING) {
                state.clazz = "connecting";
                state.message = "Connecting";
            } else if (playState === this.__internal__.PRESENTING) {
                state.clazz = "presenting";
                // programme logo
                let p = this.__internal__.programmes && this.__internal__.programmes[0];
                if (p && p.getField("_logoURL")) {
                    state.progImage = p.getField("_logoURL");
                }
                state.message = `Channel ${this.currentChannel.minorChannel}: ${this.currentChannel.name}`;
            } else if (playState === this.__internal__.STOPPED) {
                state.clazz = "stopped";
                state.message = "Stopped";
            }

            if (state.clazz !== "") {
                if (ctx.config.videoBroadcastVideoEndpoint) {
                    this._setVideoContent(state);
                } else {
                    this._setStaticContent(state);
                }
            }
        }

        _removeContent() {
            this.__internal__.intances.forEach(function(videoBroadcast) {
                if (!videoBroadcast.firstElementChild) {
                    return;
                }

                let root = videoBroadcast.firstElementChild,
                    img = root.firstElementChild,
                    msg = root.lastElementChild;

                img._inLoad = null;
                img.classList.remove("fitHeight");
                img.style.backgroundImage = "none";
                if (msg) {
                    msg.innerHTML = "";
                }
            });
        }

        _setStaticContent(content) {
            let hasProgrammes = typeof this.__internal__.programmes !== "undefined";
            this.__internal__.intances.forEach(function(videoBroadcast) {

                if (!videoBroadcast.firstElementChild) {
                    videoBroadcast.innerHTML = "<div class='creating'><div class='programmeImage'></div><div class='message'></div></div>";
                }

                let root = videoBroadcast.firstElementChild,
                    img = root.firstElementChild,
                    msg = root.lastElementChild;

                root.className = content.clazz;
                root.style.cssText = content.style || "";

                if (content.progImage) {
                    let loader = new Image();
                    loader.onload = function() {
                        if (img._inLoad === content.progImage) {
                            img.style.backgroundImage = "url(" + content.progImage + ")";
                            img._inLoad = null;

                            // Adjust the aspect ratio of the image. By default images are fit by height

                            let w = loader.width, h = loader.height;
                            if (9 * w > 16 * h) {
                                img.classList.add("fitHeight");
                            } else {
                                img.classList.remove("fitHeight");
                            }
                        }
                    };
                    loader.onerror = function() {
                        img._inLoad = null;
                        img.classList.remove("fitHeight");
                        img.style.backgroundImage = "url(" + content.defaultProgImage + ")";
                    };

                    loader.src = content.progImage;
                    img._inLoad = content.progImage;

                } else {
                    img._inLoad = null;
                    img.classList.remove("fitHeight");

                    // Only set the default image if the v/b is in presenting and we have some programmes.
                    // If not, we need to wait a bit before deciding if we prresent the default image or not.

                    if (content.clazz === "presenting") {
                        if (hasProgrammes) {
                            img.style.backgroundImage = "url(" + content.defaultProgImage + ")";
                        }
                        // If not we're waiting for a new image to be retrieved
                    } else if (content.clazz === "error" || content.clazz === "unrealized" || content.clazz === "stopped") {
                        img.style.backgroundImage = "none";
                    }
                }

                msg.innerHTML = content.message;
            });
        }

        _setVideoContent(content) {
            this.__internal__.intances.forEach(function(videoBroadcast) {

                if (!videoBroadcast.firstElementChild) {
                    videoBroadcast.innerHTML = "<div class='creating'><video autoplay='true' class='programmeImage' loop='true'></video><div class='message'></div></div>";
                }

                let root = videoBroadcast.firstElementChild,
                    video = root.firstElementChild,
                    msg = root.lastElementChild;

                root.className = content.clazz;
                root.style.cssText = content.style || "";

                let channel = videoBroadcast.currentChannel;

                if (content.progImage) {
                    video.poster = content.progImage;
                }
                if (channel) {
                    if (videoBroadcast._videoLastMinorChannel !== channel.minorChannel) {
                        // change video, not same channel
                        videoBroadcast._video = video; // TODO: use it to simulate live (sound, timeshift, ...)
                        let mp4 = ctx.config.videoBroadcastVideoEndpoint
                        + channel.minorChannel
                        + ctx.config.videoBroadcastVideoExtension;
                        video.src = mp4;

                        // seek to date simulated, if videoBroadcastStartDate is present
                        if (ctx.config.videoBroadcastStartDate) {
                            let now = Date.now();
                            let startDate = new Date(ctx.config.videoBroadcastStartDate || now);
                            let delta = (now - startDate) / 1000;
                            video.currentTime = delta;
                        }
                    }
                } else {
                    videoBroadcast._video = null;
                    video.src = "//:0";
                }
                videoBroadcast._videoLastMinorChannel = channel && channel.minorChannel;

                msg.innerHTML = content.message;
            });
        }
    }

    return VideoBroadcastObject;
};
