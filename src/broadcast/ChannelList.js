"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * A ChannelList represents a collection of Channel objects. See Annex K for the definition of the collection template.
 * In addition to the methods and properties defined for generic collections, the ChannelList class supports the additional
 * properties and methods defined below.
 */
let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Collection = require("../shared/Collection");

    return class ChannelList extends Collection {

        /*
         * Description:
         * Return the first channel in the list with the specified channel identifier. Returns null if no
         * corresponding channel can be found.
         *
         * Arguments:
         * - channelID: (Type: String)The channel identifier of the channel to be retrieved, which is a value as
         * defined for the ccid and ipBroadcastID properties of the Channel object
         * as defined in section 7.13.11.
         *
         * Return: Channel
         */
        getChannel(channelID) {
            for (let channel of this) {
                if (channel.ccid === channelID || channel.ipBroadcastID === channelID) {
                    return channel;
                }
            }
            return null;
        }

        /*
         * Description:
         * Return the first (IPTV or non-IPTV) channel in the list that matches the specified DVB or
         * ISDB triplet (original network ID, transport stream ID, service ID).
         * Where no channels of type ID_ISDB_* or ID_DVB_* are available, or no channel identified
         * by this triplet are found, this method SHALL return null.
         *
         * Arguments:
         * - onid: (Type: Integer) The original network ID of the channel to be retrieved.
         *
         * - tsid: (Type: Integer) The transport stream ID of the channel to be retrieved. If set to null the client
         * SHALL retrieve the channel defined by the combination of onid and sid. This
         * makes it possible to retrieve the correct channel also in case a remultiplexing
         * took place which led to a changed tsid.
         *
         * - sid: (Type: Integer) The service ID of the channel to be retrieved.
         *
         * - nid: (Type: Integer) An optional argument, indicating the network id to be used select the channel
         * when the channel list contains more than one entry with the same onid , tsid
         * and sid.
         *
         * Return: Channel
         */
        getChannelByTriplet(onid, tsid, sid, nid) {
            for (let channel of this) {
                if (channel.onid === onid
                        && channel.tsid === tsid
                        && channel.sid === sid
                        && (nid === undefined || channel.nid == nid)) {
                    return channel;
                }
            }

            return null;
        }

        /*
         * Description:

         * Return the first (IPTV or non-IPTV) channel in the list with the specified ATSC source ID.
         * Where no channels of type ID_ATSC_* are available, or no channel with the specified
         * source ID is found in the channel list, this method SHALL return null .
         *
         * Arguments:
         * - sourceID: (Type: Integer) The ATSC source_ID of the channel to be returned.
         *
         * Return: Channel
         */
        getChannelBySourceID(sourceID) {
            for (let channel of this) {
                if (channel.sourceID === sourceID) {
                    return channel;
                }
            }
            return null;
        }

    };
};
