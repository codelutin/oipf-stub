"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.19 The DVBSChannelScanParameters class
 *
 * The DVBSChannelScanParameters class represents the parameters needed to perform a channel scan on a DVB-S or
 * DVB-S2 network. This class implements the interface defined by ChannelScanParameters, with the following
 * additions.
 *
 * The properties that are undefined when performing startScan() are considered to be auto detected.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./ChannelScanParameters");

    return class DVBSChannelScanParameters {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         *Integer startFrequency
         *
         * The start frequency of the scan, in kHz.
         */
        get startFrequency() {
            return this.__internal__.getField("startFrequency", 3000000);
        }

        set startFrequency(value) {
            this.__internal__.setField("startFrequency", value);
        }

        /**
         * Integer endFrequency
         *
         * The end frequency of the scan, in kHz.
         */
        get endFrequency() {
            return this.__internal__.getField("endFrequency", 30000000);
        }

        set endFrequency(value) {
            this.__internal__.setField("endFrequency", value);
        }

        /**
         * Integer modulationModes
         *
         * The modulation modes to be scanned. Valid values are:
         *   Value             Description
         *      1              QPSK modulation
         *      2              8PSK modulation
         *      4              QAM16 modulation
         *
         * More than one of these values may be ORed together in order to indicate that more than one modulation
         * mode should be scanned.
         */
        get modulationModes() {
            return this.__internal__.getField("modulationModes", 7);
        }

        set modulationModes(value) {
            if (value >= 1 && value <= 7) {
                this.__internal__.setField("modulationModes", value);
            }
        }

        /**
         * String symbolRate
         *
         * A comma-separated list of the symbol rates to be scanned, in symbols/sec.
         */
        get symbolRate() {
            // FIXME i don't know if "" is default correct value
            return this.__internal__.getField("symbolRate", "");
        }

        set symbolRate(value) {
            this.__internal__.setField("symbolRate", value);
        }

        /**
         * Integer polarisation
         *
         * The polarisation to be scanned. Valid values are:
         *   Value       Description
         *     1         Horizontal polarisation
         *     2         Vertical polarisation
         *     4         Right-handed/clockwise circular polarisation
         *     8         Left-handed/counter-clockwise circular polarization
         *
         * More than one of these values may be ORed together in order to indicate that more than one polarisation
         * should be scanned.
         */
        get polarisation() {
            return this.__internal__.getField("polarisation", 15);
        }

        set polarisation(value) {
            if (value >= 1 && value <= 15) {
                this.__internal__.setField("polarisation", value);
            }
        }

        /*
         * String codeRate
         *
         * The code rate, e.g. "3/4" or "5/6".
         */
        get codeRate() {
            // FIXME check default value
            return this.__internal__.getField("codeRate", "3/4");
        }

        set codeRate(value) {
            this.__internal__.setField("codeRate", value);
        }

        /**
         * Number orbitalPosition
         *
         * The orbitalPosition property is used to resolve DiSEqC switch/motor. The value is the orbital position of
         * the satellite, negative value for west, positive value for east. For example, Astra 19.2 East would have
         * orbitalPosition 19.2. Thor 0.8 West would have orbitalPosition -0.8.
         */
        get orbitalPosition() {
            return this.__internal__.getField("orbitalPosition", 19.2);
        }

        set orbitalPosition(value) {
            this.__internal__.setField("orbitalPosition", value);
        }

        /**
         * Integer networkId
         *
         * The network ID of the network to be scanned, or undefined if all networks should be scanned.
         */
        get networkId() {
            return this.__internal__.getField("networkId", undefined);
        }

        set networkId(value) {
            this.__internal__.setField("networkId", value);
        }

    };
};
