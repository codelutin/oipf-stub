"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.18 The DVBTChannelScanParameters class
 *
 * The DVBTChannelScanParameters class represents the parameters needed to perform a channel scan on a DVB-T or
 * DVB-T2 network. This class implements the interface defined by ChannelScanParameters, with the following
 * additions.
 * The properties that are undefined when performing startScan() are considered to be auto detected.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./ChannelScanParameters");

    return class DVBTChannelScanParameters {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         * Integer startFrequency
         *
         * The start frequency of the scan, in kHz.
         */
        get startFrequency() {
            return this.__internal__.getField("startFrequency", 30000);
        }

        set startFrequency(value) {
            this.__internal__.setField("startFrequency", value);
        }


        /**
         * Integer endFrequency
         *
         * The end frequency of the scan, in kHz.
         */
        get endFrequency() {
            return this.__internal__.getField("endFrequency", 3000000);
        }

        set endFrequency(value) {
            this.__internal__.setField("endFrequency", value);
        }

        /**
         * Integer raster
         *
         * The raster size represented in kHz (typically 7000 or 8000).
         */
        get raster() {
            return this.__internal__.getField("raster", 7000);
        }

        set raster(value) {
            this.__internal__.setField("raster", value);
        }

        /**
         * String ofdm
         *
         * The Orthogonal Frequency Division Multiplexing (OFDM) for the indicating frequency. Valid values are:
         *     Value                Description
         *    MODE_1K            OFDM mode 1K
         *    MODE_2K            OFDM mode 2K
         *    MODE_4K            OFDM mode 4K
         *    MODE_8K            OFDM mode 8K
         *   MODE_16K            OFDM mode 16K
         *   MODE_32K            OFDM mode 32K
         */
        get ofdm() {
            return this.__internal__.getField("ofdm", "MODE_1K");
        }

        set ofdm(value) {
            if (value.match(/MODE_(1|2|4|8|16|32)K/)) {
                this.__internal__.setField("ofdm", value);
            }
        }

        /**
         * Integer modulationModes
         *
         * The modulation modes to be scanned. Valid values are:
         *   Value                 Description
         *     1              QPSK modulation
         *     4              QAM16 modulation
         *     8              QAM32 modulation
         *    16              QAM64 modulation
         *    32              QAM128 modulation
         *    64              QAM256 modulation
         * More than one of these values may be ORed together in order to indicate that more than one modulation
         * mode should be scanned.
         */
        get modulationModes() {
            return this.__internal__.getField("modulationModes", 1);
        }

        set modulationModes(value) {
            this.__internal__.setField("modulationModes", value);
        }

        /**
         * String bandwidth
         *
         * The expected bandwidth. Valid values are:
         *       Value                 Description
         *   BAND_1.7MHZ          1.7 MHz bandwidth
         *    BAND_5MHz           5 MHz bandwidth
         *    BAND_6MHz           6 MHz bandwidth
         *    BAND_7MHz           7 MHz bandwidth
         *    BAND_8MHz           8 MHz bandwidth
         *    BAND_10MHz          10 MHz bandwidth
         */
        get bandwidth() {
            return this.__internal__.getField("bandwidth", "BAND_1.7MHZ");
        }

        set bandwidth(value) {
            if (value.match(/BAND_(1.7|5|6|7|8|10)MHZ/)) {
                this.__internal__.setField("bandwidth", value);
            }
        }

    };
};
