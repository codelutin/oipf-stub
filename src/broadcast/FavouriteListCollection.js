"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.12 The FavouriteListCollection class
 *
 *          typedef Collection<FavouriteList> FavouriteListCollection
 *
 * The FavouriteListCollection class represents a collection of FavouriteList objects. See Annex K for the
 * definition of the collection template. In addition to the methods and properties defined for generic collections, the
 * FavouriteListCollection class supports the additional methods defined below.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Collection = require("../shared/Collection");
    let FavouriteList = require("./FavouriteList");

    return class FavouriteListCollection extends Collection {

        /**
         * FavouriteList getFavouriteList( String favID )
         *
         * Description:
         *
         * Return the first favourite list in the collection with the given favListID.
         *
         * Arguments:
         *
         * - favID
         *   The ID of a favourite list.
         */
        getFavouriteList(favID) {
            let result;
            for (let f of this) {
                if (f.favID === favID) {
                    result = f;
                    break;
                }
            }
            return result;
        }

        /* ------------------------------------------------------------------- *
         * 7.13.12.2 Extensions to FavouriteListCollection
         *
         * If an OITF has indicated support for extended tuner control (i.e. by giving value true to element
         * <extendedAVControl> as specified in section 9.3.6 in its capability description), the OITF SHALL support the
         * following additional constants and methods on the FavouriteListCollection object.
         *
         * The functionality as described in this section is subject to the security model of section 10.1.3.8.
         * ------------------------------------------------------------------ */

        /**
         * FavouriteList createFavouriteList( String name )
         *
         * Description:
         *
         * Create a new favourite list and add it to the collection. The new favourite list SHALL be
         * returned.
         *
         * Arguments
         * - name
         *   The name to be associated to the new favourite list.
         */
        createFavouriteList(name) {
            this.push(new FavouriteList({
                name: name
            }));
        }

        /**
         * Boolean remove( Integer index )
         *
         * Description:
         *
         * Remove the list at the specified index from the collection. This method SHALL return true
         * of the operation succeeded, or false if an invalid index was specified.
         *
         * Arguments:
         * - index
         *   The index of the list to be removed.
         */
        remove(index) {
            let result = index >= 0 && index < this.length;

            if (result) {
                let rest = this.slice(index + 1);
                this.length = index;
                this.push.apply(this, rest);
            }

            return result;
        }

        /**
         * Boolean commit()
         *
         * Description
         *
         * Commit any changes to the collection to persistent storage. This method SHALL return
         * true of the operation succeeded, or false if it failed (e.g. due to insufficient space to store
         * the collection).
         *
         * If a server has indicated that it requires control of the tuner functionality of an OITF in the
         * server capability description for a particular service, then the OITF SHOULD send an
         * updated Client Channel Listing to the server using HTTP POST over TLS as described in
         * section 4.8.1.1.
         */
        commit() {
            localStorage.setItem("oipf.favouriteListCollection", JSON.stringify(this));
            return true;
        }

        /**
         * Boolean activateFavouriteList( string favID )
         *
         * Description:
         *
         * Active the favourite list from the collection. This method SHALL return true if the operation
         * succeeded, or false if an invalid index was specified. A newly created favourite list has to
         * be committed before it can be activated.
         *
         * Arguments:
         * - favID
         *   The ID of a favourite list.
         */
        activateFavouriteList(/*favID*/) {
            //let f = this.getFavouriteList(favID);
            // FIXME what we do with f now ?
            return false;
        }

    };
};
