"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.13.20 The DVBCChannelScanParameters class
 *
 *The DVBCChannelScanParameters class represents the parameters needed to perform a channel scan on a DVB-C
 *or DVB-C2 network. This class implements the interface defined by ChannelScanParameters, with the following
 *additions.
 *
 *The properties that are undefined when performing startScan() are considered to be auto detected.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Channel = require("./ChannelScanParameters");

    return class DVBCChannelScanParameters {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         * Integer startFrequency
        *
        * The start frequency of the scan, in kHz.
       */
        get startFrequency() {
            return this.__internal__.getField("startFrequency", 6000);
        }

        set startFrequency(value) {
            this.__internal__.setField("startFrequency", value);
        }

      /**
       * Integer endFrequency
       *
       * The end frequency of the scan, in kHz.
       */
        get endFrequency() {
            return this.__internal__.getField("endFrequency", 65000);
        }

        set endFrequency(value) {
            this.__internal__.setField("endFrequency", value);
        }

      /**
       * Integer raster
       *
       * The raster size represented in kHz (typically 7000 or 8000).
       */
        get raster() {
            return this.__internal__.getField("raster", 7000);
        }

        set raster(value) {
            this.__internal__.setField("raster", value);
        }

      /**
       * Boolean startNetworkScanOnNIT
       *
       * The scan mode for scanning. A false value indicates to scan complete range, a true value indicates scan
       * terminates when a valid NIT is found. The frequency scan is replaced by a scan based on NIT. If networkId
       * is set and the value of this property is set to true the scan continues until there is a match on both.
       */
        get startNetworkScanOnNIT() {
            return this.__internal__.getField("startNetworkScanOnNIT", false);
        }

        set startNetworkScanOnNIT(value) {
            this.__internal__.setField("startNetworkScanOnNIT", value);
        }

        /**
         * Integer modulationModes
         *
         * The modulation modes to be scanned. Valid values are:
         *   Value              Description
         *      4               QAM16 modulation
         *      8               QAM32 modulation
         *      16              QAM64 modulation
         *      32              QAM128 modulation
         *      64              QAM256 modulation
         *      128             QAM1024 modulation
         *      256             QAM4096 modulation
         *
         * More than one of these values may be ORed together in order to indicate that more than one modulation
         * mode should be scanned.
         */
        get modulationModes() {
            return this.__internal__.getField("modulationModes", 508);
        }

        set modulationModes(value) {
            this.__internal__.setField("modulationModes", value);
        }

        /**
         * String symbolRate
         *
         * A comma-separated list of the symbol rates to be scanned, in symbols/sec.
         */
        get symbolRate() {
            // FIXME check if empty string is valid default value
            return this.__internal__.getField("symbolRate", "");
        }

        set symbolRate(value) {
            this.__internal__.setField("symbolRate", value);
        }

        /**
         * Integer networkId
         *
         * The network ID of the network to be scanned, or undefined if all networks should be scanned.
         */
        get networkId() {
            return this.__internal__.getField("networkId", undefined);
        }

        set networkId(value) {
            this.__internal__.setField("networkId", value);
        }

    };
};
