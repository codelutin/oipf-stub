"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * STV custom extensions that allows for Audio/Video input management.
 */
module.exports = function(ctx) {

    class VideoInputObject extends HTMLObjectElement {

        createdCallback() {
            ctx.__internal__.sharedInit("VideoInputObject", this);

            // if first instanciation fixe all fields
            if (!this.__internal__.intances) {
                this.__internal__.intances = [];
            }

            this.__internal__.intances.push(this);
        }

        attachedCallback() {
            this._setupInputContent();
        }

        get _configurationObject() {
            if (!this.__internal__.configurationObject) {
                this.__internal__.configurationObject = this.__internal__.oipfObjectFactory.createConfigurationObject();
            }
            return this.__internal__.configurationObject;
        }

        /**
         * Integer width
         * The width of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false. Changing the width property corresponds to changing the width property
         * through the HTMLObjectElement interface, and must have the same effect as changing the width through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.width), at least for values
         * specified in pixels.
         */
        get width() {
            let result = this.__internal__.getField("width");
            return result;
        }

        set width(value) {
            this.__internal__.setField("width", value);
        }

        /**
         * Integer height
         * The height of the area used for rendering the video object. This property is only writable if property
         * fullScreen has value false. Changing the height property corresponds to changing the height property
         * through the HTMLObjectElement interface, and must have the same effect as changing the height through
         * the DOM Level 2 Style interfaces (i.e. CSS2Properties interface style.height), at least for values
         * specified in pixels.
         */
        get height() {
            let result = this.__internal__.getField("height");
            return result;
        }

        set height(value) {
            this.__internal__.setField("height", value);
        }

        /**
         * readonly Boolean fullScreen
         * Returns true if this video object is in full-screen mode, false otherwise. The default value is false.
         */
        get fullScreen() {
            let result = this.__internal__.getField("fullScreen");
            return result;
        }

        /**
         * void setFullScreen( Boolean fullscreen )
         * Sets the rendering of the video content to full-screen (fullscreen = true) or windowed (fullscreen =
         * false) mode (as per [Req. 5.7.1.c] of [CEA-2014-A]). If this indicates a change in mode, this SHALL
         * result in a change of the value of property fullScreen. Changing the mode SHALL NOT affect the
         * z-index of the video object.
         */
        setFullScreen(fullscreen) {
            this.__internal__.setField("fullScreen", fullscreen);
            localStorage.setItem("oipf.videobroadcast.fullscreen", fullscreen);
        }

        /**
         * void setAudioInput( AVInput audioInput )
         * Sets audio source for this video object. If the audioInput.avType is not “audio” or “audio/video” this
         * method has no effect - the previously set input is still used. If audioInput.avType is “audio/video”
         * then only the audio part of input is set. If audioInput is null then the audio is not beeing presented in video/input object.
         * Arguments
         * audioInput AVInput to use for video object
         */
        setAudioInput(audioInput) {
            this.__internal__.setField("audioInput", audioInput);
            if (audioInput) {
                localStorage.setItem("oipf.videobroadcast.audioInput", audioInput.name);
            } else {
                localStorage.removeItem("oipf.videobroadcast.audioInput");
            }
        }

        /**
         * void setVideoInput( AVInput videoInput )
         * Sets video source for this video object. If the videoInput.avType is not “video” or “audio/video” this
         * method has no effect - the previously set input is still used. If audioInput.avType is “audio/video”
         * then only the video part of input is set. If audioInput is null then the video is not beeing presented in
         * video/input object.
         * Arguments
         * videoInput AVInput to use for video object
         */
        setVideoInput(videoInput) {
            this.__internal__.setField("videoInput", videoInput);
            if (videoInput) {
                localStorage.setItem("oipf.videobroadcast.videoInput", videoInput.name);
            } else {
                localStorage.removeItem("oipf.videobroadcast.videoInput");
            }
            this._setupInputContent();
        }

        /**
         * AVInput getAudioInput()
         * Gets audio source from this video object. If the audio input is not set the method returns null.
         */
        getAudioInput() {
            return this.__internal__.getField("audioInput", function() {
                let audioName = localStorage.getItem("oipf.videobroadcast.audioInput");
                let inputs = this._configurationObject.localSystem.inputs;
                let input, rst;
                for (let i = 0, l = inputs.length; i < l; i++) {
                    input = inputs[i];
                    // only restore active input
                    if (input.name === audioName && input.currentAVInputStatus === 1) {
                        rst = input;
                    }
                }
                return rst;
            }.bind(this));
        }

        /**
         * AVInput getVideoInput()
         * Gets video source from this video object. If the video input is not set the method returns null.
         */
        getVideoInput() {
            return this.__internal__.getField("videoInput", function() {
                let audioName = localStorage.getItem("oipf.videobroadcast.videoInput");
                let inputs = this._configurationObject.localSystem.inputs;
                let input, rst;
                for (let i = 0, l = inputs.length; i < l; i++) {
                    input = inputs[i];
                    // only restore active input
                    if (input.name === audioName && input.currentAVInputStatus === 1) {
                        rst = input;
                    }
                }
                return rst;
            }.bind(this));
        }

        /**
         * void stop()
         * Stop presenting input audio and video.
         */
        stop() {

        }

        /**
         * Override parent to display different visual effect when input source is not TV.
         */
        _setupInputContent() {
            let videoInput = this.getVideoInput();
            let type = videoInput && videoInput.type;

            // no special output or tv one
            if (!type || type.indexOf("tv-") !== -1) {
                this._setContent("<div class='inactive' />");
            } else {
                this._setContent("<div class='active " + type + "'><div class='message'>" + videoInput.userLabel + "</div></div>");
            }
        }

        _setContent(innerHTML) {
            this.__internal__.intances.forEach(function(videoBroacast) {
                videoBroacast.innerHTML = innerHTML;
            });
        }
    }

    return VideoInputObject;
};
