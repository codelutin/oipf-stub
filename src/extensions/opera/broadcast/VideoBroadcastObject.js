"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let _VideoBroadcastObject = require("../../../broadcast/VideoBroadcastObject");

    class VideoBroadcastObject extends _VideoBroadcastObject {

        attachedCallback() {
            if (!this.__internal__.localSystem) {
                this.__internal__.localSystem = this.__internal__.oipfObjectFactory.createConfigurationObject().localSystem;
                this.__internal__.localSystem.addEventListener("_SystemProperty", this.onSystemPropertyModified.bind(this));
            }
        }

        onSystemPropertyModified() {
            this._setupCurrentChannelImage(this.playState);
        }

        /**
         * Modify style to display channel information on HTML Element (as image, or text).
         */
        _setupCurrentChannelImage(playState) {

            // use some system properties
            let style;
            if (ctx.config.emulateImageSettingsOnVB) {
                if (this.__internal__.localSystem) { // not yet attached
                    let brightness = this.__internal__.localSystem.getSystemProperty("brightness");
                    brightness = parseInt(brightness, 10) * 2;
                    let saturation = this.__internal__.localSystem.getSystemProperty("saturation");
                    saturation = parseInt(saturation, 10) * 2;
                    let contrast = this.__internal__.localSystem.getSystemProperty("contrast");
                    contrast = parseInt(contrast, 10) * 2;

                    // use * 2 to move from 0% to 200% with default to 100%
                    style = "-webkit-filter:" +
                    " brightness(" + brightness + "%)" +
                    " saturate(" + saturation + "%)" +
                    " contrast(" + contrast + "%);";
                }
            }

            super._setupCurrentChannelImage(playState, style);
        }

        /**
         * Return current broadcast vbd time in seconds.
         */
        getDvbTime() {
            return ~~(Date.now() / 1000); // eslint-disable-line no-bitwise
        }
    }

    return VideoBroadcastObject;
};
