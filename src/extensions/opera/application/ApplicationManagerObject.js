"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let ApplicationCollection = require("../../../application/ApplicationCollection");
    let _ApplicationManagerObject = require("../../../application/ApplicationManagerObject", true);

    return class ApplicationManagerObject extends _ApplicationManagerObject {

        constructor() {
            super();

            this.__internal__.eventDispatchPriorities = new Map();
            this.__internal__.allApplications = new Map();
            let zIndexes = this.__internal__.zIndexes = [];

            this.__internal__.applyZindexes = function() {
                zIndexes.forEach((application, index) => {
                    if (application.__internal__.document.defaultView) {
                        application.__internal__.document.defaultView.frameElement.style.zIndex = index;
                    }
                });
            };

            this.__internal__.removeAppZindexe = function(application) {
                let index = zIndexes.indexOf(application);
                zIndexes.splice(index, 1);
            };
        }

        /**
         * @return {ApplicationCollection} returns an immutable collection of
         *                                 all Applications.
         */
        getApplications() {
            let array = Array.from(this.__internal__.allApplications.values());
            return new ApplicationCollection(array);
        }

        /**
         * @param  {String} id application id
         * @return {Application} Returns the Application with the given id.
         *                       If no such application exists, returns null.
         */
        getApplicationById(id) {
            return this.__internal__.allApplications.get(id);
        }

        getOwnerApplication(doc) {
            let application = super.getOwnerApplication(doc);
            this.__internal__.allApplications.set(application.id, application);
            return application;
        }

    };
};
