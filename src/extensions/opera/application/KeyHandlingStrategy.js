/* eslint no-bitwise: 0 */

"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let _KeyHandlingStrategy = require("../../../application/KeyHandlingStrategy", true);
    let EventFactory = require("../../../application/EventFactory");

    return class KeyHandlingStrategy extends _KeyHandlingStrategy {

        constructor(appMgr) {
            super(appMgr);

            this.ALT_MASK = 0x10000;
            this.CTRL_MASK = 0x20000;
            this.SHIFT_MASK = 0x40000;
        }

        testKeyset(event, application) {

            let shift = event.shiftKey;

            // On certain strange keyboards one needs to press shift for getting the digits.
            // On these keyboards, shift has to be cancelled for getting the true code to pass
            // to the application.

            if (event.keyCode >= 48 && event.keyCode <= 57) {
                let keyId = parseInt(event.keyIdentifier.replace(/U\+0*/, ""), 16);
                if (keyId >= 48 && keyId <= 57) {
                    shift = false;
                }
            }

            let keyCode = event.keyCode
                            | (event.altKey && this.ALT_MASK)
                            | (shift && this.SHIFT_MASK)
                            | (event.ctrlKey && this.CTRL_MASK);

            return application.privateData.keyset.__internal__.keyCodes.has(keyCode);
        }

        keyEventCapture(e) {
            if (this._letItPass(e)) {
                return false;
            }
            if (!e.forwarded) {
                let forwardEvent = EventFactory.createKeyEvent(e);

                let appMgr = this.__internal__.appMgr;
                let activeApplications = appMgr.__internal__.activeApplications;
                let eventDispatchPriorities = appMgr.__internal__.eventDispatchPriorities;

                forwardEvent.applications = activeApplications.slice(0);

                let priorities = Array.from(eventDispatchPriorities.values());
                priorities.sort(function(a, b) {
                    return b.eventDispatchPriority - a.eventDispatchPriority;
                });

                priorities.forEach((application) => {
                    if (!application.active) {
                        forwardEvent.applications.push(application);
                    }
                });

                this.dispatchEvent(forwardEvent);

                e.stopPropagation();
                e.preventDefault();
            }
        }

    };
};
