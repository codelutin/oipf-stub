"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let _Application = require("../../../application/Application", true);

    return class Application extends _Application {

        constructor(appMgr, doc) {
            super(appMgr, doc);

            this.__internal__.createWindow = function(applicationParent, uri) {
                let docParent = window.document;

                let iframe = docParent.createElement("iframe");

                iframe.onload = function() {
                    let app = appMgr.__internal__.uriApplications.get(uri);
                    app.__internal__.document = iframe.contentDocument;
                    app.__internal__.setEventListeners(app);

                    appMgr.__internal__.zIndexes.unshift(app);
                    appMgr.__internal__.applyZindexes();
                };

                iframe.src = uri;
                iframe.name = uri.replace("/index.html", "");

                docParent.body.appendChild(iframe);

                return iframe.contentWindow;
            };

            // Default id
            let id = this.window.APP_NAME;
            this.__internal__.id = id;

            // Default zindex
            if (doc.body) {
                appMgr.__internal__.zIndexes.unshift(this);
                appMgr.__internal__.applyZindexes();
            }
        }

        /**
         * The createApplication call takes one additional parameter, the id of
         * the Application to be created. This id is required, cannot be null
         * and cannot be an existing id.
         */
        createApplicationWithId(uri, createChild, id) {
            let application = super.createApplication(uri, createChild);

            application.__internal__.id = id;

            let appMgr = this.__internal__.appMgr;
            appMgr.__internal__.allApplications.set(id, application);

            return application;
        }

        destroyApplication() {
            super.destroyApplication();

            let appMgr = this.__internal__.appMgr;
            appMgr.__internal__.allApplications.delete(this.__internal__.id);

            this.window.frameElement.remove();
        }

        /**
         * This property allows for fine grained control for dispatching events
         * to the Applications that are not active. More specifically it allows
         * to order the Applications to which these events are to be dispatched.
         */
        set eventDispatchPriority(value) {
            let appMgr = this.__internal__.appMgr;
            appMgr.__internal__.eventDispatchPriorities.set(value, this);

            this.__internal__.eventDispatchPriority = value;
        }

        get eventDispatchPriority() {
            return this.__internal__.eventDispatchPriority;
        }

        /**
         * Each Application has an id. The id is given at creation time and
         * cannot be changed. 2 applications cannot have the same id, creating
         * an app with an already existing id doesn’t do anything.
         */
        get id() {
            return this.__internal__.id;
        }

        /**
         * Explicit z-index order management.
         */
        bringToFront() {
            let appMgr = this.__internal__.appMgr;
            let zIndexes = appMgr.__internal__.zIndexes;

            appMgr.__internal__.removeAppZindexe(this);
            zIndexes.push(this);
            appMgr.__internal__.applyZindexes();
        }

        /**
         * See above.
         */
        sendToBottom() {
            let appMgr = this.__internal__.appMgr;
            let zIndexes = appMgr.__internal__.zIndexes;

            appMgr.__internal__.removeAppZindexe(this);
            zIndexes.unshift(this);
            appMgr.__internal__.__internal__.applyZindexes();
        }

        /**
         * Explicit z-index order management above another Application.
         * @param  {Application} app another Application
         */
        moveAbove(app) {
            let appMgr = this.__internal__.appMgr;
            let zIndexes = appMgr.__internal__.zIndexes;

            appMgr.__internal__.removeAppZindexe(this);
            let index = zIndexes.indexOf(app);
            zIndexes.splice(index + 1, 0, this);
            appMgr.__internal__.applyZindexes();
        }

        /**
         * Explicit z-index order management below another Application.
         * @param  {Application} app another Application
         */
        moveBelow(app) {
            let appMgr = this.__internal__.appMgr;
            let zIndexes = appMgr.__internal__.zIndexes;

            appMgr.__internal__.removeAppZindexe(this);
            let index = zIndexes.indexOf(app);
            zIndexes.splice(index, 0, this);
            appMgr.__internal__.applyZindexes();
        }

    };
};
