"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let _Configuration = require("../../../configuration/Configuration", true);

    return class Configuration extends _Configuration {

        /*
         * Maximum duration (in minutes) of time shift buffer. Setting this property SHALL change the current maximum
         * time shift buffer duration.
         *
         * Type: Integer
         */
        get maxTimeShiftBufferDuration() {
            if (!this.__internal__.maxTimeShiftBufferDuration) {
                this.__internal__.maxTimeShiftBufferDuration = parseInt(localStorage.getItem("oipf.configuration.maxTimeShiftBufferDuration") || "0", 10);
            }
            return this.__internal__.maxTimeShiftBufferDuration;
        }

        set maxTimeShiftBufferDuration(maxTimeShiftBufferDuration) {
            this.__internal__.maxTimeShiftBufferDuration = maxTimeShiftBufferDuration;
            localStorage.setItem("oipf.configuration.maxTimeShiftBufferDuration", maxTimeShiftBufferDuration);
        }

        /*
         * The default padding (measured in seconds) to be added at the start of a reminder.
         *
         * Type: Integer
         */
        get reminderStartPadding() {
            if (!this.__internal__.reminderStartPadding) {
                this.__internal__.reminderStartPadding = parseInt(localStorage.getItem("oipf.configuration.reminderStartPadding") || "0", 10);
            }
            return this.__internal__.reminderStartPadding;
        }

        set reminderStartPadding(reminderStartPadding) {
            this.__internal__.reminderStartPadding = reminderStartPadding;
            localStorage.setItem("oipf.configuration.reminderStartPadding", reminderStartPadding);
        }

    };
};
