"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The AVInput class represents an audio or video input on the local platform.
 */
module.exports = function(ctx) {

    return class AVInput {

        constructor(fields) {
            ctx.__internal__.init(this, [
                "AVInputStatusChange"
            ]);

            this.__internal__.setValues(fields);
        }

        /**
         * readonly String name
         *
         * The name of the input. Each input SHALL have a name that is unique on the local system.
         * For example “HDMI0”, “HDMI1”, etc.
         */
        get name() {
            return this.__internal__.getField("name", undefined);
        }

        /**
         * readonly String avType
         *
         * The audio/video type of the input. Valid values are:
         * “audio”
         * “video”
         * “audio/video”
         */
        get avType() {
            return this.__internal__.getField("avType", undefined);
        }

        /**
         * readonly String type
         *
         * The type of the input. Valid values are:
         * “hdmi”,
         * “scart”,
         * “component”,
         * “composant”,
         * “tv-analogue”,
         * “tv-dvb-t”,
         * “tv-dvb-c”,
         * “tv-dvb-s”,
         * “tv-dvb” - for inputs that mix several tuner sources (e.g. terrestrial & satellite) as one single input
         * “tv-atsc-t”,
         * “tv-isdb-t”,
         * “tv-isdb-s”,
         * “tv-isdb” - same remark as for “tv-dvb”,
         * “pc-vga”
         * “pc-dvi”
         * “pc” - for generic monitor inputs that don’t specify the type of connection
         *
         * Particular platforms MAY define other inputs.
         */
        get type() {
            return this.__internal__.getField("type", undefined);
        }

        /**
         * String userLabel
         *
         * User assigned label, as UTF-8. Empty string if unassigned. Setting this value SHALL persistently
         * assign the label to the AVInput.
         */
        get userLabel() {
            return this.__internal__.getField("userLabel", undefined);
        }

        set userLabel(userLabel) {
            this.__internal__.setField("userLabel", userLabel);
        }

        /**
         * readonly Integer currentAVInputStatus
         *
         * Read the current status of input. Return values are:
         *
         * Value   Description
         * 0       Status is unknown
         * 1       The input is connected
         * 2       The input is disconnected
         */
        get currentAVInputStatus() {
            return this.__internal__.getField("currentAVInputStatus", function() {
                return parseInt(localStorage.getItem("oipf.configuration.localsystem.input." + this.name + ".currentAVInputStatus"), 10) || 0;
            }.bind(this));
        }

        // used in OipfStubActions
        set _currentAVInputStatus(status) {
            this.__internal__.setField("currentAVInputStatus", status);
            this.__internal__.eventEmitter.emit("AVInputStatusChange", status);
            localStorage.setItem("oipf.configuration.localsystem.input." + this.name + ".currentAVInputStatus", status);
        }

        /**
         * function onAVInputStatusChange
         *
         * This function is the DOM 0 event handler for events relating to actions carried out on an item in a
         * content catalogue. The specified function is called with the following arguments:
         * Integer status – The type of status that the event refers to. Valid values are:
         *
         * Value  Description
         * 0      Status is unknown
         * 1      The input is connected
         * 2      The input is disconnected
         */
        get onAVInputStatusChange() {
            this.__internal__.eventEmitter.getOneListener("AVInputStatusChange");
        }

        set onAVInputStatusChange(callback) {
            this.__internal__.eventEmitter.addOneListener("AVInputStatusChange", callback);
        }

    };
};
