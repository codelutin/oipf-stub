"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let _LocalSystem = require("../../../configuration/LocalSystem");
    let AVInputCollection = require("./AVInputCollection");
    let AVInput = require("./AVInput");

    return class LocalSystem extends _LocalSystem {

        constructor() {
            super([
                "_SystemProperty" // not generic one, but for convenience
            ]);

            // default values for systemProperty
            this.__internal__.DEFAULT_SYSTEM_PROPERTY = {
                brightness: 50,
                saturation: 50,
                contrast: 50
            };
        }

        /**
         * readonly AVInputCollection inputs
         *
         * A collection of AVInput objects representing the audio and video inputs of the platform. Applications MAY
         * use these objects to configure and control the available inputs.
         */
        get inputs() {
            return this.__internal__.getField("inputs", function() {
                let configInput = ctx.config.default.inputs || [];
                let inputs = new AVInputCollection();
                for (let i of configInput) {
                    inputs.push(new AVInput(i));
                }
                return inputs;
            });
        }

        /**
         * Boolean hbbTvEnabled
         *
         * Flag indicating whether the HbbTV support is enabled. Setting this property SHALL enable or disable
         * the HbbTV support.
         */
        get hbbTvEnabled() {
            return this.__internal__.getField("hbbTvEnabled", false);
        }

        set hbbTvEnabled(value) {
            this.__internal__.setField("hbbTvEnabled", value);
        }

        /**
         * readonly String tvMode
         *
         * The type of the TV mode. Setting this property SHALL change the current TV mode. Valid values are
         * “home”, “store”, or “hotel”.
         */
        get tvMode() {
            return this.__internal__.getField("tvMode", undefined);
        }

        set tvMode(value) {
            this.__internal__.setField("tvMode", value);
        }

        /**
         * String energySavingMode
         *
         * The type of the energy mode. Setting this property SHALL change the current energy mode. Valid values
         * are “off”, “minimum”, “medium”, “maximum”, “auto”.
         */
        get energySavingMode() {
            return this.__internal__.getField("energySavingMode", undefined);
        }

        set energySavingMode(value) {
            this.__internal__.setField("energySavingMode", value);
        }

        /**
         * Generic method to set system property. The property and its value are passed as String allowing for maximum
         * interoperability on different platforms. The exact list of properties and its values is platform specific
         * and should be agreed outside of this document.
         *
         * Example properties: “picture-brightness”, “picture-backlight”, “sound-enable-surround”, etc.
         */
        setSystemProperty(property, value) {
            this.__internal__.setField(property, value);
            localStorage.setItem("oipf.configuration.localsystem." + property, value);
            this.__internal__.eventEmitter.emit("_SystemProperty", property, value);
        }

        /**
         * Generic method to get system property. The property value is method return value. The property and its value
         * are passed as String allowing for maximum interoperability on different platforms. The exact list of
         * properties and its values is platform specific and should be agreed outside of this document. The return
         * value may be null.
         *
         * Example properties: “picture-brightness”, “picture-backlight”, “sound-enable-surround”, etc.
         */
        getSystemProperty(property) {
            return this.__internal__.getField(property, function() {
                return localStorage.getItem("oipf.configuration.localsystem." + property) || this.__internal__.DEFAULT_SYSTEM_PROPERTY[property];
            }.bind(this));
        }
    };
};
