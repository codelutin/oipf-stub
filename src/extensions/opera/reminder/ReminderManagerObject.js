/* eslint no-bitwise: 0 */
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let ReminderCollection = require("./ReminderCollection");
    let Reminder = require("./Reminder");

    return class ReminderManagerObject {

        constructor() {
            ctx.__internal__.init(this, [
                "ReminderStateChanged"
            ]);

            this.__internal__.REMINDER_EVENT_UNKNOWN = 0;
            this.__internal__.REMINDER_EVENT_STARTED = 1;
            this.__internal__.REMINDER_EVENT_DELETED = 2;
        }

        get _configurationObject() {
            if (!this.__internal__.configurationObject) {
                this.__internal__.configurationObject = this.__internal__.oipfObjectFactory.createConfigurationObject();
            }
            return this.__internal__.configurationObject;
        }

        /**
         * Init reminder collection and setup timeout for exiting non elapsed reminders.
         */
        initReminders() {
            let _reminders = new ReminderCollection();
            this.__internal__.setField("reminders", _reminders);

            let data = JSON.parse(localStorage.getItem("oipf.reminder.reminders"));
            data && data.forEach(d => {
                let r = new Reminder(d);
                let prepared = this._prepareReminderTimer(r);

                if (prepared) {
                    _reminders.push(r);
                } else {
                    console.warn("[OIPF] Remove elapsed reminder", r.reminderID);
                }
            });

            //force save to remove elapsed from storage
            this._saveReminders();
        }

        /**
         * Prepare timer.
         *
         * @param r reminder to setup
         * @return true if reminder has been prepared, false if timer is elapsed
         */
        _prepareReminderTimer(r) {
            let prepared = false;

            // compute timeout
            let now = Date.now();
            let delta = r.startTime * 1000 - now;

            if (r.repeatDays) {
                while (delta <= 0) {
                    delta += 24 * 60 * 60 * 1000; // add 24h
                }
            }

            if (delta > 0) {
                let delay = delta - this._getReminderPadding(r) * 1000;
                r.__internal__.timeout = setTimeout(this._fireReminderStarted.bind(this, r), delay);
                prepared = true;
                //debug
                console.log("[OIPF] Reminder", r.reminderID, "will trigger in", delay, "ms");
            }

            return prepared;
        }

        /**
         * Get padding for reminder accoring to reminder configuration or global configuration (in seconds);
         *
         * @return Integer (in s)
         */
        _getReminderPadding(reminder) {
            let result = reminder.startPadding;
            if (result === undefined) {
                result = this._configurationObject.configuration.reminderStartPadding;
            }
            return result;
        }

        /**
         * Returns all the reminders that are scheduled but which have not yet started.
         *
         * @return ReminderCollection
         */
        get reminders() {
            let _reminders = this.__internal__.getField("reminders");
            if (!_reminders) {
                this.initReminders();
                _reminders = this.__internal__.getField("reminders");
            }
            return _reminders;
        }

        /**
         * Save reminder in local storage.
         */
        _saveReminders() {
            let data = JSON.stringify(this.reminders);
            localStorage.setItem("oipf.reminder.reminders", data);
        }

        /**
         * Requests the manager to add the reminder of the programme identified by the programmeID property of the
         * programme.
         * If the reminder was added successfully, the resulting Reminder object is returned. If the reminder could
         * not be added due to programme start time being in the past the value null is returned.
         *
         * @return Reminder
         */
        remind(programme) {
            let reminder = this.remindAt(programme.startTime, undefined, programme.channelID);
            reminder.__internal__.setField("programmeID", programme.programmeID);

            this._saveReminders();

            return reminder;
        }

        /**
         * Requests the manager to add the remainder of the broadcast to be received over the channel identified by
         * channelID, starting at startTime.
         * If the reminder was added successfully, the resulting Reminder object is returned. If the reminder could
         * not be added due to start time being in the past the value null is returned.
         *
         * @param startTime The start of the time period of the recording measured in seconds since midnight (GMT) on
         *      1/1/1970. If the start time occurs in the past the method SHALL return null.
         * @param repeatDays Bitfield indicating which days of the week the reminder SHOULD be repeated. Values are as follows:
         *      Day         Bitfield Value
         *      Sunday    0x01 (i.e. 00000001)
         *      Monday    0x02 (i.e. 00000010)
         *      Tuesday   0x04 (i.e. 00000100)
         *      Wednesday 0x08 (i.e. 00001000)
         *      Thursday  0x10 (i.e. 00010000)
         *      Friday    0x20 (i.e. 00100000)
         *      Saturday  0x40 (i.e. 01000000)
         *
         *      These bitfield values can be ‘OR’-ed together to repeat a reminder on more than one day of a week (e.g. weekdays)
         *      A value of 0x00 indicates that the reminder will not be repeated.
         * @param channelID The identifier of the channel from which the broadcasted content is to be reminded.
         *      Specifies either a ccid or ipBroadcastID (as defined by the Channel object in OIPF section 7.13.11)
         * @return Reminder
         */
        remindAt(startTime, repeatDays, channelID) {
            let reminder = new Reminder();

            reminder.__internal__.setField("startTime", startTime);
            reminder.__internal__.setField("repeatDays", repeatDays);
            reminder.__internal__.setField("channelID", channelID);

            this._prepareReminderTimer(reminder);
            this.reminders.push(reminder);
            this._saveReminders();

            return reminder;
        }

        /**
         * Remove a reminder. As with the remind() method, only the programmeID property of the reminder SHALL be used
         * to identify the reminder to remove where this property is available. The other data contained in the reminder
         * SHALL NOT be used when removing a reminder created using methods other than remindAt(). For reminders
         * created using remindAt(), the data used to identify the reminder to remove is implementation dependent.
         *
         * @param reminder The reminder to be removed.
         */
        remove(reminder) {
            // remove
            this.__internal__.setField("reminders", this.reminders.filter(r => {
                let result = r.reminderID !== reminder.reminderID;

                // cancel timer
                result && clearTimeout(r.__internal__.timeout);

                this.__internal__.eventEmitter.emit("ReminderStateChanged", reminder.id, this.__internal__.REMINDER_EVENT_DELETED);

                return result;
            }));

            this._saveReminders();
        }

        /**
         * Returns the Reminder object for which the value of the reminderID property corresponds to the given
         * reminderID parameter. If such a Reminder does not exist, the method returns null.
         *
         * @param reminderID Identifier corresponding to the reminderID property of a Reminder object.
         * @return Reminder
         */
        getReminder(reminderID) {
            let results = this.__internal__.reminders.find(r => r.reminderID !== reminderID);
            return results && results[0];
        }

        get onReminderStateChanged() {
            return this.__internal__.eventEmitter.getOneListener("ReminderStateChanged");
        }

        set onReminderStateChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("ReminderStateChanged", callback);
        }

        _fireReminderStarted(reminder) {
            console.log("[OIPF] Reminder event fired", reminder);

            let started = false;
            if (reminder.repeatDays) {

                // test if event should be fired for that day
                let now = new Date();
                let day = now.getDay();
                let powDay = Math.pow(2, day);
                if (powDay & reminder.repeatDays) {
                    started = true;
                }

                // prepare next timer
                this._prepareReminderTimer(reminder);
            } else {
                started = true;
                // delete reminder if not repeatDays
                this.remove(reminder);
            }

            if (started) {
                // fire
                this.__internal__.eventEmitter.emit("ReminderStateChanged", reminder.id, this.__internal__.REMINDER_EVENT_STARTED);
            }
        }
    };
};
