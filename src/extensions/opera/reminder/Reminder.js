"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let Utils = require("../../../OipfStubUtils");

/*
 * The Reminder object represents a reminder in the system. The values of the properties of a Reminder (except for
 * startPadding) are provided when the object is created using one of the remind() methods in section 3.7, for example
 * by using a corresponding Programme object as argument for the remind() method, and can not be changed for this
 * reminder object (except for startPadding).
 */
module.exports = function(ctx) {

    return class Reminder {

        constructor(values) {
            ctx.__internal__.init(this);

            this.__internal__.setValues(values);
        }

        /**
         * An identifier for this reminder. This value SHALL be unique to this reminder.
         *
         * @return String
         */
        get reminderID() {
            let result = this.__internal__.getField("reminderID");
            if (!result) {
                result = Utils.nonce();
                this.__internal__.setField("reminderID", result);
            }
            return result;
        }

        /**
         * The amount of padding to add at the start of a reminder, in seconds. If the value of this property is
         * undefined, an OITF defined start padding will be used. The default OITF defined start padding MAY be
         * changed through property reminderStartPadding of the Configuration class as defined in section 3.5.
         * Positive values of this property SHALL cause the reminder to start earlier than the specified start time;
         * negative values SHALL cause the reminder to start later than the specified start time.
         *
         * @return Integer
         */
        get startPadding() {
            return this.__internal__.getField("startPadding");
        }

        set startPadding(startPadding) {
            this.__internal__.setField("startPadding", startPadding);
        }

        /**
         * The start time of the reminder, measured in seconds since midnight (GMT) on 1/1/1970. The value for the
         * startPadding property can be used to indicate if the reminder has to be started before the startTime
         * (as defined by the Programme class).
         *
         * @return Integer
         */
        get startTime() {
            return this.__internal__.getField("startTime");
        }

        /**
         * Bitfield indicating which days of the week the reminder SHOULD be repeated. Values are as follows:
         *
         *     Day       Bitfield Value
         * Sunday       0x01 (i.e. 00000001)
         * Monday       0x02 (i.e. 00000010)
         * Tuesday      0x04 (i.e. 00000100)
         * Wednesday    0x08 (i.e. 00001000)
         * Thursday     0x10 (i.e. 00010000)
         * Friday       0x20 (i.e. 00100000)
         * Saturday     0x40 (i.e. 01000000)
         *
         * These bitfield values can be arithmetically summed to repeat a reminder on more than one day of a week (e.g. weekdays)
         * A value of 0x00 indicates that the reminder will not be repeated.
         *
         * @return Integer
         */
        get repeatDays() {
            return this.__internal__.getField("repeatDays");
        }

        /**
         * Channel ID of the broadcast channel where the reminder is set.
         *
         * @return String
         */
        get channelID() {
            return this.__internal__.getField("channelID");
        }

        /**
         * The unique identifier of the reminder.For reminders created using the oipfReminderManger.remindAt() method,
         * the value of this property MAY be undefined.
         *
         * @return String
         */
        get programmeID() {
            return this.__internal__.getField("programmeID");
        }

        // to json is used because getter are not enumerable in ES6 classes
        toJSON() {
            return this.__internal__.getValues();
        }
    };
};
