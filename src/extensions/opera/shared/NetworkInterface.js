"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * The NetworkInterface class represents a physical or logical network interface in the receiver.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let _NetworkInterface = require("../../../shared/NetworkInterface.js");
    let WifiNetworkInterface = require("./WifiNetworkInterface.js");
    let WifiNetworkInterfaceCollection = require("./WifiNetworkInterfaceCollection.js");

    return class NetworkInterface extends _NetworkInterface {

        constructor(index, fields) {
            super(index, fields, [
                "ScanWifiNetworkCompleted",
                "InternetConnectionChanged"
            ]);
        }

        /**
         * readonly Boolean internetConnected
         * Checks internet connection. Returns true if WifiNetwork is connected to Internet, false otherwise. Please
         * note it is different to connected property of OIPF NetworkInterface. OIPF connected checks if the interface
         * is connected to router or access point. This function checks that the Internet can be actually reached to
         * distinguish a case where NetworkInterface may be connected but without internet access.
         */
        get internetConnected() {
            return this.__internal__.getField("internetConnected", undefined);
        }

        /**
         * readonly String networkType
         * Return NetworkInterface type. The allowed values are: “ethernet”, “wifi”, “moca”, “docsis”.
         */
        get networkType() {
            return this.__internal__.getField("networkType", undefined);
        }

        /**
         * readonly WifiNetworkCollection wifiNetworks
         * Returns collection of WifiNetworks. This method only works if the networkType is “wifi”. If networkType is
         * not the “wifi” the null value is returned. In order for this method to work you need to trigger wifi
         * scanning procedure using scanWifiNetworks() method. If the scanWifiNetworks() is not called the wifiNetworks
         * may return null value.
         */
        get wifiNetworks() {
            let result = this.__internal__.getField("wifiNetworks");

            if (!(result instanceof WifiNetworkInterfaceCollection)) {
                let wifiNetworks = result;
                result = new WifiNetworkInterfaceCollection();
                for (let w of wifiNetworks) {
                    result.push(new WifiNetworkInterface(w));
                }
                this.__internal__.setField("wifiNetworks", result);
            }

            return result;
        }

        /**
         * readonly WifiNetwork joinedNetwork
         * Returns a WifiNetwork that OITF is currently joined with. This may be null value if the networkType is not
         * “wifi” or the WifiNetwork is not joined.
         */
        get joinedNetwork() {
            return this.__internal__.getField("joinedNetwork", undefined);
        }

        /**
         * Boolean scanWifiNetworks()
         * Starts wifi networks search. Returns true if search is started successfully, false otherwise ie: the
         * networkType is not “wifi”. When the scan is done the ScanWifiNetworksCompleted event is called.
         */
        scanWifiNetworks() {
            this.__internal__.eventEmitter.emit("ScanWifiNetworkCompleted", false);
        }

        /**
         * Boolean joinWifiNetwork( WifiNetwork wifiNetwork, String password )
         * Joins given WifiNetwork using given password. Returns true if join is successfull and false otherwise ie: the networkType is not “wifi”.
         * Arguments
         * wifiNetwork: WifiNetwork to join.
         * password : Password to join WifiNetwork. May be null if WifiNetwork publicAccess property is true.
         */
        joinWifiNetwork() {
            this.__internal__.eventEmitter.emit("InternetConnectionChanged", 1);
        }

        /**
         * function onScanWifiNetworkCompleted( Boolean wifiNetworkListChanged )
         * This function is the DOM 0 event handler for events related to wifi network scan. The specified function is called with the following arguments:
         * Boolean wifiNetworkListChanged – True if the wifi scan changed the list of available wifi networks, false otherwise.
         */
        get onScanWifiNetworkCompleted() {
            return this.__internal__.eventEmitter.getOneListener("ScanWifiNetworkCompleted");
        }

        set onScanWifiNetworkCompleted(callback) {
            this.__internal__.eventEmitter.addOneListener("ScanWifiNetworkCompleted", callback);
        }

        /**
         * function onInternetConnectionChanged( Integer internetConnectionState )
         * This function is the DOM 0 event handler for events relating to actions carried out on an item in a content
         * catalogue. The specified function is called with the following arguments:
         * Integer internetConnectionState – The type of status that the event refers to. Valid values are:
         * Value        Description
         * 0            Status is unknown
         * 1            Internet is connected
         * 2            Internet is disconnected
         */
        get onInternetConnectionChanged() {
            return this.__internal__.eventEmitter.getOneListener("InternetConnectionChanged");
        }

        set onInternetConnectionChanged(callback) {
            this.__internal__.eventEmitter.addOneListener("InternetConnectionChanged", callback);
        }

    };
};
