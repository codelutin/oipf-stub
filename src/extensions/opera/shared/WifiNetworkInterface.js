"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The NetworkInterface class represents a physical or logical network interface in the receiver.
 */
module.exports = function(ctx) {

    return class WifiNetworkInterface {

        constructor(values) {
            ctx.__internal__.init(this);

            this.__internal__.setValues(values);
        }

        /**
         * readonly Integer id
         * An identifier for this WifiNetwork. This value SHALL be unique to this WifiNetwork.
         */
        get id() {
            return this.__internal__.getField("id", undefined);
        }

        /**
         * readonly String networkName
         * WifiNetwork name that is presented to end user to distinguish between WifiNetworks.
         */
        get networkName() {
            return this.__internal__.getField("networkName", undefined);
        }

        /**
         * readonly Boolean publicAccess
         * True if WifiNetwork is publicly accessible. False otherwise.
         */
        get publicAccess() {
            return this.__internal__.getField("publicAccess", undefined);
        }

        /**
         * readonly Integer signalStrength
         * WifiNetwork signal strength. The value is in percent between 0 - no signal to 100 - max signal strength.
         */
        get signalStrength() {
            return this.__internal__.getField("signalStrength", undefined);
        }

        /**
         * readonly String networkMacAddress
         * WifiNetwork MAC address.
         */
        get networkMacAddress() {
            return this.__internal__.getField("networkMacAddress", undefined);
        }

        /**
         * readonly Integer radioChannel
         * WifiNetwork radio channel number.
         */
        get radioChannel() {
            return this.__internal__.getField("radioChannel", undefined);
        }

    };
};
