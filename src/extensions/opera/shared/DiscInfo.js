"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

 /**
  * The DiscInfo class provides details of the storage usage and capacity in the OITF.
  * A DiscInfo instance obtained from the oipfDownloadManager provides reports relating to downloads. A DiscInfo
  * instance obtained from oipfRecordingScheduler provides reports relating to recordings. If recordings and
  * downloads use the same pool of storage space (e.g. disc partition), DiscInfo instances obtained via either route would
  * have the same values for the properties. If recordings and downloads use different pools of storage space (e.g. different
  * disc partitions) the DiscInfo instances obtained by each route would report the correct values for the route in which
  * they were obtained.
 */
module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let _DiscInfo = require("../../../shared/DiscInfo", true);

    return class DiscInfo extends _DiscInfo {

        /**
         * readonly Boolean isFormatRequired
         * Flag indicating whether the disc needs formatting or not.
         */
        get isFormatRequired() {
            return this.__internal__.getField("isFormatRequired", undefined);
        }

        /**
         * void formatDisc()
         * Function that formats disc and makes it ready for PVR or content download usage.
         */
        formatDisc() {

        }
    };
};
