"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;
let videoinputCount = 0;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let _OipfObjectFactory = require("../../OipfObjectFactory", true);

    let ReminderManagerObject = require("./reminder/ReminderManagerObject");
    let VideoInputObject = require("./broadcast/VideoInputObject");

    return class OipfObjectFactory extends _OipfObjectFactory {

        constructor() {
            super();

            // overload supported objects
            this.__internal__.oipfObject.ReminderManagerObject = ReminderManagerObject;
            this.__internal__.oipfObject.VideoInputObject = VideoInputObject;
        }

        _registerElements() {
            super._registerElements();

            // register elements managed by this factory
            if (!this.__internal__.VideoInputObjectElt) {
                this.__internal__.VideoInputObjectElt =
                    document.registerElement("video-input" + (videoinputCount ? videoinputCount : ""), {
                        prototype: VideoInputObject.prototype,
                        extends: "object"
                    });
                videoinputCount++;
            }

        }

        /**
         * This method SHALL return true if and only if an object of the specified type is supported by the OITF,
         * otherwise it SHALL return false.
         *
         * @param   {String} mimeType If the value of the argument is one of the MIME types defined in tables 1 to 4 of
         *                            [OIPF_MEDIA2] or one of the DAE defined mime types listed below then an accurate
         *                            indication of the OITF’s support for that MIME type SHALL be returned.
         *
         *                            For other values, it is recommended that an OITF returns a value which accurately
         *                            reflects its support for the specified MIME type.
         *
         *                            --------------------------------------------
         *                                          DAE MIME Type
         *                            --------------------------------------------
         *                            application/oipfReminderManager
         *                            --------------------------------------------
         */
        isObjectSupported(mimeType) {

            let mimeTypeMethod = {
                "application/oipfReminderManager": true,
                "video/input": true
            };

            return super.isObjectSupported(mimeType) || mimeTypeMethod[mimeType];
        }

        /**
         * If the object type is supported, each of these methods SHALL return an instance of the corresponding embedded
         * object. This may be a new instance or existing instance. For example, the object will likely be a global
         * singleton object and calls to this method may return the same instance.
         *
         * Since objects do not claim scarce resources when they are instantiated, instantiation SHALL never fail if the
         * object type is supported. If the method name to create the object is not supported, the OITF SHALL throw an
         * error with the name property set to the value "TypeError".
         *
         * If the object is supported, the method SHALL return a JavaScript Object which implements the interface for the
         * specified object.
         */
        createReminderManagerObject() {
            let result = this.__internal__.createOipfObject(true, "ReminderManagerObject", arguments);
            return result;
        }

        createVideoInputObject() {
            let VideoInputObjectElt = this.__internal__.VideoInputObjectElt;

            // new instance
            let videoInputObject = this.__internal__.createOipfObject(false, "VideoInputObject", arguments, function() {
                return new VideoInputObjectElt();
            });

            return videoInputObject;
        }

    };
};
