"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    return class Register {

        /**
         * Redifine ctx.setRequire to use other file.
         *
         * WARNING: Order matters here !
         */
        static register() {
            console.log("[OIPF] Register 'Opera' extension into oipf stub");

            // application
            let KeyHandlingStrategy = require("./application/KeyHandlingStrategy");
            ctx.setRequire("./KeyHandlingStrategy", KeyHandlingStrategy);

            let Application = require("./application/Application");
            ctx.setRequire("./Application", Application);

            let ApplicationManagerObject = require("./application/ApplicationManagerObject");
            ctx.setRequire("./application/ApplicationManagerObject", ApplicationManagerObject);

            // videobroadcast
            let VideoBroadcastObject = require("./broadcast/VideoBroadcastObject");
            ctx.setRequire("./broadcast/VideoBroadcastObject", VideoBroadcastObject);

            // configuration
            let NetworkInterface = require("./shared/NetworkInterface");
            ctx.setRequire("../shared/NetworkInterface", NetworkInterface);

            let Configuration = require("./configuration/Configuration");
            ctx.setRequire("./Configuration", Configuration);

            let LocalSystem = require("./configuration/LocalSystem");
            ctx.setRequire("./LocalSystem", LocalSystem);

            // factory
            let OipfObjectFactory = require("./OipfObjectFactory");
            ctx.setRequire("./OipfObjectFactory", OipfObjectFactory);
        }
    };
};
