"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The OITF SHALL support the scheduling of recordings of broadcasts through the use of the following non-visual
 * embedded object:
 * <object type= " application/oipfRecordingScheduler " />
 * Note that the functionality in this section SHALL adhere to the security model as specified in section 10.1.
 * Which channels can be recorded SHALL be indicated by the ipBroadcast , DASH and HAS attributes in the PVR
 * capability indication (see section 9.3.3). Within the channels indicated by these attributes, recording of both channels
 * stored in the channel list and locally defined channels SHALL be supported.
 */
let Utils = require("../OipfStubUtils");
let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let DiscInfo = require("../shared/DiscInfo");
    let Programme = require("../shared/Programme");
    let Recording = require("./Recording");
    let RecordingCollection = require("./RecordingCollection");
    let ScheduledRecording = require("./ScheduledRecording");
    let ScheduledRecordingCollection = require("./ScheduledRecordingCollection");

    return class RecordingSchedulerObject {

        constructor() {
            ctx.__internal__.init(this, [
                "PVREvent"
            ]);

            this.__internal__.discInfo = new DiscInfo(ctx.config.default.discInfo);

            // 1: The recording has started.
            this._EVENT_RECORDING_STARTED = 1;
            // 2: The recording has stopped, having completed.
            this._EVENT_RECORDING_STOPPED = 2;
            // 3: The recording sub-system is unable to record due to resource limitations.
            this._EVENT_RECORDING_UNABLE_TO_RECORD = 3;
            // 4: There is insufficient storage space available. (Some of the recording may be available).
            this._EVENT_RECORDING_INSUFFICIENT_STORAGE = 4;
            // 6: The recording has stopped before completion due to unknown (probably hardware) failure.
            this._EVENT_RECORDING_STOPPED_ERROR = 6;
            // 7: The recording has been newly scheduled.
            this._EVENT_RECORDING_NEW_SCHEDULE = 7;
            // 8: The recording has been deleted (for complete or in-progress recordings) or removed from
            //    the schedule (for scheduled recordings).
            this._EVENT_RECORDING_DELETE = 8;
            // 9: The recording is due to start in a short time.
            this._EVENT_RECORDING_DUE_TO_START = 9;
            // 10: The recording has been updated. For performance reasons, OITFs SHOULD NOT dispatch
            //     events with the state when only the duration of an in-progress recording has changed.
            this._EVENT_RECORDING_UPDATED = 9;
        }

        get _configurationObject() {
            if (!this.__internal__.configurationObject) {
                this.__internal__.configurationObject = this.__internal__.oipfObjectFactory.createConfigurationObject();
            }
            return this.__internal__.configurationObject;
        }

        _loadRecordings() {
            let recordings = new RecordingCollection();
            this.__internal__.setField("recordings", recordings);

            let data = JSON.parse(localStorage.getItem("oipf.recording.recordings"));
            if (data) {
                for (let r of data) {
                    if (r.id) {
                        let recording = new Recording(r);
                        // compute fake size using "100Mb per minute"
                        this.discInfo._reduceSize(recording.recordingDuration / 60 * 100);
                        recordings.push(r);
                    } else if (r.scheduleID) {
                        let scheduledRecording = new ScheduledRecording(r);
                        if (this._prepareScheduledRecordingTimer(scheduledRecording)) {
                            recordings.push(scheduledRecording);
                        } else {
                            console.warn("[OIPF] Remove elapsed recording", scheduledRecording.scheduleID);
                        }
                    }
                }
            }

            // force save to remove elapsed for storage
            this._saveRecordings();
        }

        _saveRecordings() {
            let data = JSON.stringify(this.recordings);
            localStorage.setItem("oipf.recording.recordings", data);
        }

        /**
         * readonly ScheduledRecordingCollection recordings
         * Provides a list of scheduled and recorded programmes in the system. This property may only provide access
         * to a subset of the full list of recordings, as determined by the value of the manageRecordings attribute of
         * the <recording> element in the client capability description (see section 9.3.3).
         */
        get recordings() {
            let recordings = this.__internal__.getField("recordings", undefined);
            if (!recordings) {
                this._loadRecordings();
                recordings = this.__internal__.getField("recordings", undefined);
            }
            return recordings;
        }

        /**
         * readonly DiscInfo discInfo
         * Get information about the status of the local storage device. The DiscInfo class is defined in section 7.16.4.
         */
        get discInfo() {
            return this.__internal__.discInfo;
        }

        /**
         * Requests the scheduler to schedule the recording of the programme identified by the
         * programmeID property of the programme.
         * If the programmeIDType of the programme has the value ID_TVA_GROUP_CRID then the
         * ScheduledRecording object returned by this method SHALL be a “parent” scheduled
         * recording object that conceptually represents the recording. Each individual programme in
         * the SHALL be represented by a separate ScheduledRecording object. Note that
         * ScheduledRecording objects for individual programmes may not be created until the CRID
         * has been partially or completely resolved. The start time, duration and other properties of the
         * programme SHALL NOT be used for scheduling any recording.
         *
         * Individual programmes SHALL be recorded if any entries in a programme’s associated
         * groupCRIDs collection matches the group CRID specified in the programmeID property of
         * any “parent” recording.
         *
         * The other data contained in the programme object is used solely for annotation of the
         * (scheduled) recording. If such programme metadata is provided, it SHALL be retained in the
         * ScheduledRecording object that is returned if the recording of the programme was
         * scheduled successfully, reflecting the possibility that not all relevant metadata might be
         * available to the scheduler. When the programme is recorded, the metadata in the associated
         * Recording object SHALL be updated with the metadata from the broadcast stream if such
         * metadata is available. If the recording could not be scheduled due to a scheduling conflict or
         * lack of resources the value null is returned.
         * Note that the actual implementation of this method should enable the scheduler to identify
         * the domain of the service that issues the scheduling request in order to support future
         * retrieval of the scheduled recording through the getScheduledRecordings method.
         *
         * @return ScheduledRecording
         */
        record(programme) {
            let scheduledRecording = new ScheduledRecording({
                scheduleID: Utils.nonce(),
                name: programme.name,
                longName: programme.longName,
                description: programme.description,
                longDescription: programme.longDescription,
                startTime: programme.startTime,
                duration: programme.duration,
                channel: programme.channel,
                programmeID: programme.programmeID,
                programmeIDType: programme.programmeIDType,
                parentalRatings: programme.parentalRatings
            });
            scheduledRecording.__internal__.setField("state", scheduledRecording.RECORDING_SCHEDULED);

            this._prepareScheduledRecordingTimer(scheduledRecording);
            this.recordings.push(scheduledRecording);
            this._saveRecordings();

            // fire event
            this.__internal__.eventEmitter.emit("PVREvent", {
                state: this._EVENT_RECORDING_NEW_SCHEDULE,
                recording: scheduledRecording
            });

            return scheduledRecording;
        }

        /**
         * ScheduledRecording recordAt( Integer startTime, Integer duration, Integer repeatDays, String channelID )
         *
         * Requests the scheduler to schedule the recording of the broadcast to be received over the
         * channel identified by channelID , starting at startTime and stopping at startTime +
         * duration . If the recording was scheduled successfully, the resulting ScheduledRecording
         * object is returned. If the recording could not be scheduled due to a scheduling conflict or lack
         * of resources the value null is returned.
         * The OITF SHOULD associate metadata with recordings scheduled using this method. This
         * metadata MAY be obtained from the broadcast being recorded (for example DVB-SI in an
         * MPEG-2 transport stream) or from other sources of metadata. If an application anticipates
         * that the OITF may not be able to obtain any metadata, it SHOULD instead of using this
         * method;
         * • create a Programme object (using the createProgramme() method) containing the
         * channelID, startTime and duration
         * • populate that Programme object with metadata
         * • pass that Programme object to the record(Programme) method.
         * Note that the actual implementation of this method should enable the scheduler to identify
         * the domain of the service that issues the scheduling request in order to support future
         * retrieval of the scheduled recording through the getScheduledRecordings() method.
         */
        recordAt(startTime, duration, repeatDays, channelID) {
            // get channel
            let channelList = this.getChannelConfig().channelList;
            let channel = channelList.getChannel(channelID);

            // build programme
            let programme = this.createProgrammeObject();
            programme.startTime = startTime;
            programme.duration = duration;
            programme.channelID = channelID;
            programme.__internal__.setField("channel", channel);

            // schedule record
            let scheduledRecording = this.record(programme);
            scheduledRecording.__internal__.setField("repeatDays", repeatDays);

            return scheduledRecording;
        }

        /**
         * ScheduledRecordingCollection getScheduledRecordings()
         *
         * Returns a subset of all the recordings that are scheduled but which have not yet started. The
         * subset SHALL include only scheduled recordings that were scheduled using a service from
         * the same FQDN as the domain of the service that calls the method.
         */
        getScheduledRecordings() {
            let scheduledRecordings = this.recordings.filter(function(r) {
                return !r.id;
            });
            return new ScheduledRecordingCollection(scheduledRecordings);
        }

        /**
         * Description:
         * Returns the channel line-up of the tuner in the form of a ChannelConfig
         * object as defined in section 7.13.9. This includes the favourite lists.
         *
         * The ChannelConfig object returned from this function SHALL be identical
         * to the ChannelConfig object returned from the getChannelConfig() method
         * on the video/broadcast object as defined in section 7.13.1.3.
         *
         * Return:
         * - ChannelConfig
         */
        getChannelConfig() {
            return ctx.dataProvider.getChannelConfig();
        }

        /**
         * void remove( ScheduledRecording recording )
         *
         * Remove a recording (either scheduled, in-progress or completed).
         * For non-privileged applications, recordings SHALL only be removed when they are
         * scheduled but not yet started and the recording was scheduled by the current service.
         * As with the record() method, only the programmeID property of the scheduled recording
         * SHALL be used to identify the scheduled recording to remove where this property is
         * available. The other data contained in the scheduled recording SHALL NOT be used when
         * removing a recording scheduled using methods other than recordAt() . For recordings
         * scheduled using recordAt() , the data used to identify the recording to remove is
         * implementation dependent.
         * If the programmeIDType property has the value ID_TVA_GROUP_CRID then the OITF
         * SHALL cancel the recording of the specified group.
         * If an A/V Control object is presenting the indicated recording then the state of the A/V
         * Control object SHALL be automatically changed to 6 (the error state).
         */
        remove(recording) {
            recording.__internal__.scheduleTimeout && clearTimeout(recording.__internal__.scheduleTimeout);
            recording.__internal__.dueToStartTimeout && clearTimeout(recording.__internal__.dueToStartTimeout);
            recording.__internal__.recordingTimeout && clearTimeout(recording.__internal__.recordingTimeout);

            if (recording.state === recording.RECORDING_REC_STARTED) {
                this.stop(recording);
            }

            // compute index
            let index = -1;
            for (let i = 0, l = this.recordings.length; i < l; i++) {
                let r = this.recordings.item(i);
                if (r.id && r.id === recording.id ||
                    r.scheduleID && r.scheduleID === recording.scheduleID) {
                    index = i;
                    break;
                }
            }

            if (index !== -1) {
                this.recordings.splice(index, 1);
                this._saveRecordings();

                // fire event
                this.__internal__.eventEmitter.emit("PVREvent", {
                    state: this._EVENT_RECORDING_DELETE,
                    recording: recording
                });
            }
        }

        /**
         * Programme createProgrammeObject()
         *
         * Factory method to create an instance of Programme .
         */
        createProgrammeObject() {
            return new Programme();
        }

        /**
         * Recording getRecording( String id )
         * Returns the Recording object for which the value of the Recording.id property
         * corresponds to the given id parameter. If such a Recording does not exist, the method
         * returns null .
         */
        getRecording(id) {
            let recordings = this.recordings.filter(function(r) {
                return r.id === id;
            });
            return recordings[0];
        }

        /**
         * void stop( Recording recording )
         * Description Stop an in-progress recording. The recording SHALL NOT be deleted.
         */
        stop(recording) {
            // test new recording state
            let expectedDuration = recording.duration + this._getRecordingStartPadding(recording) + this._getRecordingEndPadding(recording);
            if (recording.recordingDuration < expectedDuration) {
                recording.__internal__.setField("state", recording.RECORDING_REC_PARTIALLY_COMPLETED);
            } else {
                recording.__internal__.setField("state", recording.RECORDING_REC_COMPLETED);
            }

            // update size
            // compute fake size using "100Mb per minute"
            this.discInfo._reduceSize(recording.recordingDuration / 60 * 100);

            // fire event
            this.__internal__.eventEmitter.emit("PVREvent", {
                state: this._EVENT_RECORDING_STOPPED,
                recording: recording
            });

            this._saveRecordings();
        }

        /**
         * Update the recordings property to show the current status of all recordings.
         */
        refresh() {

        }

        /**
         * Boolean update( String id, Integer startTime, Integer duration, Integer repeatDays )
         *
         * Requests the scheduler to update a scheduled or ongoing recording.
         * For scheduled recordings the properties startTime , duration and repeatDays can be
         * modified.
         * For ongoing recordings only the duration property may be modified.
         * This method SHALL return true if the operation succeeded, or false if for any reason it
         * rescheduling is not possible (e.g. the updated recording overlaps with another scheduled
         * recording and there are insufficient system resources to do both. ).
         * If the method returns false then no changes SHALL be made to the recording.
         */
        update(/*id, startTime, duration, repeatDays*/) {

        }

        /**
         * function onPVREvent( Integer state, ScheduledRecording recording )
         * This function is the DOM 0 event handler for notification of changes in the state of recordings. The specified
         * function is called with the following arguments:
         *
         * Integer state – The current state of the recording. One of:
         *   Value  Description
         *   1      The recording has started.
         *   2      The recording has stopped, having completed.
         *   3      The recording sub-system is unable to record due to resource limitations.
         *   4      There is insufficient storage space available. (Some of the recording may be available).
         *   6      The recording has stopped before completion due to unknown (probably hardware) failure.
         *   7      The recording has been newly scheduled.
         *   8      The recording has been deleted (for complete or in-progress recordings) or removed from
         *          the schedule (for scheduled recordings).
         *   9      The recording is due to start in a short time.
         *   10     The recording has been updated. For performance reasons, OITFs SHOULD NOT dispatch
         *          events with the state when only the duration of an in-progress recording has changed.
         *
         * ScheduledRecording recording – The recording to which this event refers.
         */
        get onPVREvent() {
            return this.__internal__.eventEmitter.getOneListener("PVREvent");
        }

        set onPVREvent(callback) {
            this.__internal__.eventEmitter.addOneListener("PVREvent", callback);
        }

        /**
         * Get padding for reminder accoring to reminder configuration or global configuration (in seconds);
         *
         * @return Integer (in s)
         */
        _getRecordingStartPadding(recording) {
            let result = recording.startPadding;
            if (result === undefined) {
                result = this._configurationObject.configuration.pvrStartPadding;
            }
            return result || 0;
        }

        /**
         * Get padding for reminder accoring to reminder configuration or global configuration (in seconds);
         *
         * @return Integer (in s)
         */
        _getRecordingEndPadding(recording) {
            let result = recording.endPadding;
            if (result === undefined) {
                result = this._configurationObject.configuration.pvrEndPadding;
            }
            return result || 0;
        }

        /**
         * Setup scheduledRecording timeout to start recording.
         *
         * @return true if timer has been scheduled, false if startTime is elapsed
         */
        _prepareScheduledRecordingTimer(scheduledRecording) {
            let result = false, now = Date.now();

            let delta = scheduledRecording.startTime * 1000 - now;
            if (scheduledRecording.repeatDays) {
                while (delta <= 0) {
                    delta += 1000 * 60 * 60 * 24; // add 24 hours to find next valid date
                }
            }

            if (delta > 0) {
                result = true;
                let delay = delta - this._getRecordingStartPadding(scheduledRecording) * 1000;
                let delayDueToStart = delay - 15 * 1000; // 15 seconds before
                scheduledRecording.__internal__.scheduleTimeout = setTimeout(this._beforeStartRecording.bind(this, scheduledRecording), delayDueToStart);
                console.log("[OIPF] Recording of", scheduledRecording.scheduleID, "will start in", delta, "ms");
            }

            return result;
        }

        /**
         * Method called by timemout setup on scheduledRecording by _prepareScheduledRecordingTimer.
         */
        _beforeStartRecording(scheduledRecording) {
            console.log("[OIPF] Recording of", scheduledRecording.scheduleID, "due to start in a short time");

            let willStart;
            if (scheduledRecording.repeatDays) {

                // test if event should be fired for that day
                let now = new Date();
                let day = now.getDay();
                let powDay = Math.pow(2, day);
                if (powDay & scheduledRecording.repeatDays) { //eslint-disable-line no-bitwise
                    willStart = true;
                }

                // prepare next timer
                this._prepareScheduledRecordingTimer(scheduledRecording);
            } else {
                willStart = true;
            }

            if (willStart) {

                // fire event
                this.__internal__.eventEmitter.emit("PVREvent", {
                    state: this._EVENT_RECORDING_DUE_TO_START,
                    recording: scheduledRecording
                });

                // prepare start in 15 seconds
                scheduledRecording.__internal__.dueToStartTimeout = setTimeout(this._startRecording.bind(this, scheduledRecording),
                    15 * 1000);
            }
        }

        /**
         * Method called by timemout setup on scheduledRecording by _prepareScheduledRecordingTimer.
         */
        _startRecording(scheduledRecording) {
            console.log("[OIPF] Recording of", scheduledRecording.scheduleID, "started");

            // XXX: for now, delete old, to add a recording object
            if (!scheduledRecording.repeatDays) {
                this.remove(scheduledRecording);
            }

            let data = scheduledRecording.toJSON();
            data.state = scheduledRecording.RECORDING_REC_STARTED;
            data.id = Utils.nonce();
            data.recordingStartTime = Date.now() / 1000;
            data.recordingDuration = 0;
            let recording = new Recording(data);
            this.recordings.push(recording);

            // fire event
            this.__internal__.eventEmitter.emit("PVREvent", {
                state: this._EVENT_RECORDING_STARTED,
                recording: recording
            });

            // setup recording timer
            recording.__internal__.recordingMethodBound = this._updateRecording.bind(this, recording);
            recording.__internal__.recordingMethodBound();
        }

        /**
         * This method is called at regular timeout to update recordingDuration and stop recording at the end.
         */
        _updateRecording(recording) {
            let now = Date.now();
            let recordingDuration = (now - recording.recordingStartTime * 1000) / 1000;
            recording.__internal__.setField("recordingDuration", recordingDuration);
            if (recording.recordingDuration + this._getRecordingEndPadding(recording) >= recording.duration) {
                this.stop(recording);
            } else {
                this._saveRecordings();
                // stub update every 5 seconds
                recording.__internal__.recordingTimeout = setTimeout(recording.__internal__.recordingMethodBound, 5000);
            }
        }
    };
};
