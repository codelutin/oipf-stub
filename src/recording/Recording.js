"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * The Recording class represents an in-progress or completed recording being made available through the extended
 * PVR management functionality as defined in section 7.10.4. Recordings for which no data has yet been recorded are
 * represented by the ScheduledRecording class.
 */
module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let ScheduledRecording = require("./ScheduledRecording.js");

    return class Recording extends ScheduledRecording {

        constructor(values) {
            super(values);
        }

        /**
         * readonly String uri
         * A uri identifying the content item in local storage according to [RFC3986]. The format of the URI is outside the
         * scope of this specification except that;
         * • the scheme SHALL NOT be one that is included in this specification
         * • the URI SHALL NOT include a fragment
         */
        get uri() {
            return this.__internal__.getField("uri", undefined);
        }

        /**
         * readonly String id
         * An identifier for this recording. This value SHALL be unique to this recording and so can be used to compare
         * two recording objects to see if they refer to the same recording. The OITF SHALL guarantee that recording
         * identifiers are unique in relation to download identifiers and CODAsset identifiers.
         */
        get id() {
            return this.__internal__.getField("id", undefined);
        }

        /**
         * Boolean doNotDelete
         * If true , then this recording should not be automatically deleted by the system.
         */
        get doNotDelete() {
            return this.__internal__.getField("doNotDelete", false);
        }

        set doNotDelete(value) {
            this.__internal__.setField("doNotDelete", value);
        }

        /**
         * Integer saveDays
         * The number of days for which an individual or manual recording SHOULD be saved. Recordings older than
         * this value MAY be deleted. If the value of this property is undefined , the default save duration SHALL be
         * used.
         */
        get saveDays() {
            return this.__internal__.getField("saveDays", false);
        }

        set saveDays(value) {
            this.__internal__.setField("saveDays", value);
        }

        /**
         * Integer saveEpisodes
         * The number of episodes of a series-link that SHOULD be saved. Older episodes MAY be deleted. This is
         * only valid when set on the latest scheduled recording in the series. If the value of this property is undefined ,
         * the default value SHALL be used.
         */
        get saveEpisodes() {
            return this.__internal__.getField("saveEpisodes", undefined);
        }

        set saveEpisodes(value) {
            this.__internal__.setField("saveEpisodes", value);
        }

        /**
         * readonly Boolean blocked
         * Flag indicating whether the programme is blocked due to parental control settings or conditional access
         * restrictions.
         * The blocked and locked properties work together to provide a tri-state flag describing the status of a
         * programme. This can best be described by the following table:

         * Description                                                        blocked locked
         * No parental control applies.                                       false   false
         * Item is above the parental rating threshold (or manually blocked); true    true
         * no PIN has been entered to view it and so the item cannot
         * currently be viewed.
         * Item is above the parental rating threshold (or manually blocked);
         * the PIN has been entered and so the item can be viewed.            true false
         * Invalid combination – OITFs SHALL NOT support this combination     false true
         */
        get blocked() {
            return this.__internal__.getField("blocked", false);
        }

        /**
         * readonly Integer showType
         * Flag indicating the type of show. This field SHALL take one of the following values:
         * Value Description
         * 0     The show is live.
         * 1     The show is a first-run show.
         * 2     The show is a rerun.
         */
        get showType() {
            return this.__internal__.getField("showType", 0);
        }

        /**
         * readonly Boolean subtitles
         * Flag indicating whether subtitles or closed-caption information is available.
         */
        get subtitles() {
            return this.__internal__.getField("subtitles", false);
        }

        /**
         * readonly StringCollection subtitleLanguages
         * Supported subtitle languages, indicated by their ISO 639-2 language codes as defined in [ISO 639-2].
         */
        get subtitleLanguages() {
            return this.__internal__.getField("subtitleLanguages", undefined);
        }

        /**
         * readonly Boolean isHD
         * Flag indicating whether the programme has high-definition video.
         */
        get isHD() {
            return this.__internal__.getField("isHD", false);
        }

        /**
         * readonly Boolean is3D
         * Flag indicating whether the programme has 3D video.
         */
        get is3D() {
            return this.__internal__.getField("is3D", false);
        }

        /**
         * readonly Integer audioType
         * Bitfield indicating the type of audio that is available for the programme. Since more than one type of audio
         * may be available for a given programme, the value of this field SHALL consist of one or more of the following
         * values ORed together:
         * Value  Description
         * 1     Mono audio
         * 2     Stereo audio
         * 4     Multi-channel audio
         */
        get audioType() {
            return this.__internal__.getField("audioType", 0);
        }

        /**
         * readonly Boolean isMultilingual
         * Flag indicating whether more than one audio language is available for this recording.
         */
        get isMultilingual() {
            return this.__internal__.getField("isMultilingual", false);
        }

        /**
         * readonly StringCollection audioLanguages
         * Supported audio languages, indicated by their ISO 639-2 language codes as defined in [ISO 639-2].
         */
        get audioLanguages() {
            return this.__internal__.getField("audioLanguages", undefined);
        }

        /**
         * readonly StringCollection genres
         * A collection of genres that describe this programme.
         */
        get genres() {
            return this.__internal__.getField("genres", undefined);
        }

        /**
         * readonly Integer recordingStartTime
         * The actual start time of the recording, including any padding, measured in seconds since midnight (GMT) on
         * 1/1/1970. This MAY not be the same as the scheduled start time of the recorded programme (e.g. due to a
         * recording starting late, or due to start/end padding). For recordings that have not yet started, the value of
         * this field SHALL be undefined .
         */
        get recordingStartTime() {
            return this.__internal__.getField("recordingStartTime", undefined);
        }

        /**
         * readonly Integer recordingDuration
         * The actual duration of the recording, including any padding, measured in seconds. This MAY not be the
         * same as the scheduled duration of the recording (e.g. due to a recording finishing early, or due to start/end
         * padding). For recordings that have not yet started, the value of this field SHALL be undefined .
         */
        get recordingDuration() {
            return this.__internal__.getField("recordingDuration", undefined);
        }

        /**
         * readonly BookmarkCollection bookmarks
         * A collection of the bookmarks set in a recording. If no bookmarks are set, the collection SHALL be empty.
         */
        get bookmarks() {
            return this.__internal__.getField("bookmarks", undefined);
        }

        /**
         * readonly Boolean locked
         * Flag indicating whether the current state of the parental control system prevents the recording from being
         * viewed (e.g. a correct parental control PIN has not been entered to allow the recording to be viewed).
         */
        get locked() {
            return this.__internal__.getField("locked", false);
        }
    };
};
