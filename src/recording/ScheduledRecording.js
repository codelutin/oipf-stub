"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * The ScheduledRecording object represents a scheduled programme in the system, i.e. a recording that is scheduled
 * but which has not yet started. For group recordings (e.g. recording an entire series), a ScheduledRecording object is
 * also used to represent a “parent” recording that enables management of the group recording without representing any of
 * the actual recordings in the group. The values of the properties of a ScheduledRecording (except for
 * startPadding and endPadding ) are provided when the object is created using one of the record() methods in
 * section 7.10.1, for example by using a corresponding Programme object as argument for the record() method, and
 * can not be changed for this scheduled recording object (except for startPadding and endPadding ).
 */
module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let Channel = require("../broadcast/Channel");

    return class ScheduledRecording {

        constructor(values) {
            ctx.__internal__.init(this);

            this.__internal__.setValues(values);

            // If values.channel is a raw object we convert it to the corresponding business object
            if (values.channel && values.channel.__internal__ == null) {
                this.__internal__.setField("channel", new Channel(0, values.channel));
            }

            // Recording has been newly scheduled.
            this.RECORDING_SCHEDULED = 0;
            // Recording has started.
            this.RECORDING_REC_STARTED = 1;
            // Recording has successfully completed.
            this.RECORDING_REC_COMPLETED = 2;
            // The recording has only partially completed due to
            // insufficient storage space, a clash or hardware failure.
            // There are three possible conditions for this:
            // 1) The end of the recording is missed.
            // 2) The start of the recording is missed.
            // 3) A piece from the centre of the recording is missed
            // (e.g. due to the receiver rebooting or a transient failure
            // of the network connection).
            this.RECORDING_REC_PARTIALLY_COMPLETED = 3;
            // An error has been encountered. Refer to detailed error codes for details on the error.
            this.RECORDING_ERROR = 90;
            // The recording sub -system is unable to record due to resource limitations.
            this.ERROR_REC_RESOURCE_LIMITATION = 91;
            // There is insufficient storage space available. (Some of the recording may be available).
            this.ERROR_INSUFFICIENT_STORAGE = 92;
            // Recording has stopped before completion due to unknown (probably hardware) failure.
            this.ERROR_REC_UNKNOWN = 93;

            // Used in the programmeIDType property to indicate that the
            // programme is identified by its TV-Anytime CRID (Content
            // Reference Identifier).
            this.ID_TVA_CRID = 0;
            // Used in the programmeIDType property to indicate that the
            // programme is identified by a DVB URL referencing a DVB-SI
            // event as enabled by section 4.1.3 of [OIPF_META2]. Support for
            // this constant is OPTIONAL.
            this.ID_DVB_EVENT = 1;
            // Used in the programmeIDType property to indicate that the
            // Programme object represents a group of programmes identified
            // by a TV-Anytime group CRID.
            this.ID_TVA_GROUP_CRID = 2;
        }

        /**
         * readonly Integer state
         * The state of the recording. Valid values are:
         * RECORDING_REC_STARTED
         * RECORDING_REC_COMPLETED
         * RECORDING_REC_PARTIALLY_COMPLETED
         * RECORDING_SCHEDULED
         * RECORDING_ERROR
         */
        get state() {
            return this.__internal__.getField("state", undefined);
        }

        /**
         * readonly Integer error
         * If the state of the recording has changed due to an error, this field contains an error code detailing the type of
         * error. This is only valid if the value of the state argument is RECORDING_ERROR or
         * RECORDING_REC_PARTIALLY_COMPLETED otherwise this property SHALL be null . Valid values are:
         * ERROR_REC_RESOURCE_LIMITATION
         * ERROR_INSUFFICIENT_STORAGE
         * ERROR_REC_UNKNOWN
         */
        get error() {
            return this.__internal__.getField("error", undefined);
        }

        /**
         * readonly String scheduleID
         * An identifier for this scheduled recording. This value SHALL be unique to this scheduled recording. For a
         * recording object this identifier can be used to associate which scheduled recording object this recording was
         * created from.
         */
        get scheduleID() {
            return this.__internal__.getField("scheduleID", undefined);
        }

        /**
         * String customID
         * An identifier for this scheduled recording. This value is an identifier that the DAE application can set in order
         * to keep track of scheduled recordings. It is not changed by the OITF.
         */
        get customID() {
            return this.__internal__.getField("customID", undefined);
        }

        set customID(value) {
            this.__internal__.setField("customID", value);
        }

        /**
         * Integer startPadding
         * The amount of padding to add at the start of a scheduled recording, in seconds. If the value of this property is
         * undefined , an OITF defined start padding will be used. The default OITF defined start padding MAY be
         * changed through property pvrStartPadding of the Configuration class as defined in section 7.3.2.
         * When a recording is due to start, the OITF MAY use a smaller amount of padding in order to avoid conflicts
         * with other recordings.
         * Positive values of this property SHALL cause the recording to start earlier than the specified start time (i.e.
         * the actual duration of the recording shall be increased); negative values SHALL cause the recording to start
         * later than the specified start time (i.e. the actual duration of the recording shall be decreased).
         */
        get startPadding() {
            return this.__internal__.getField("startPadding", undefined);
        }

        set startPadding(value) {
            this.__internal__.setField("startPadding", value);
        }

        /**
         * Integer endPadding
         * The amount of padding to add at the end of a scheduled recording, in seconds. If the value of this property is
         * undefined , an OITF defined end padding will be used. The default OITF defined end padding MAY be
         * changed through property pvrEndPadding of the Configuration class as defined in section 7.3.2. When a
         * recording is in progress, the OITF MAY use a smaller amount of padding in order to avoid conflicts with other
         * recordings.
         * Positive values of this property SHALL cause the recording to end later than the specified end time (i.e. the
         * actual duration of the recording shall be increased); negative values SHALL cause the recording to end
         * earlier than the specified end time (i.e. the actual duration of the recording shall be decreased).
         */
        get endPadding() {
            return this.__internal__.getField("endPadding", undefined);
        }

        set endPadding(value) {
            this.__internal__.getField("endPadding", value);
        }

        /**
         * readonly Integer repeatDays
         * Bitfield indicating which days of the week the recording SHOULD be repeated. Values are as follows:
         * Day              Bitfield Value
         * Sunday          0x01 (i.e. 00000001)
         * Monday          0x02 (i.e. 00000010)
         * Tuesday         0x04 (i.e. 00000100)
         * Wednesday       0x08 (i.e. 00001000)
         * Thursday        0x10 (i.e. 00010000)
         * Friday          0x20 (i.e. 00100000)
         * Saturday        0x40 (i.e. 01000000)
         * These bitfield values can be arithmetically summed to repeat a recording on more than one day of a week
         * (e.g. weekdays)
         * A value of 0x00 indicates that the recording will not be repeated.
         */
        get repeatDays() {
            return this.__internal__.getField("repeatDays", undefined);
        }

        /**
         * String name
         * The short name of the scheduled recording, e.g. 'Star Trek: DS9'.
         */
        get name() {
            return this.__internal__.getField("name", undefined);
        }

        set name(value) {
            this.__internal__.setField("name", value);
        }

        /**
         * String longName
         * The long name of the scheduled recording, e.g. 'Star Trek: Deep Space Nine'. If the long name is not
         * available, this property will be undefined .
         */
        get longName() {
            return this.__internal__.getField("longName", undefined);
        }

        set longName(value) {
            this.__internal__.setField("longName", value);
        }

        /**
         * String description
         * The description of the scheduled recording, e.g. an episode synopsis. If no description is available, this
         * property will be undefined .
         */
        get description() {
            return this.__internal__.getField("description", undefined);
        }

        set description(value) {
            this.__internal__.setField("description", value);
        }

        /**
         * String longDescription
         * The long description of the programme. If no description is available, this property will be undefined .
         */
        get longDescription() {
            return this.__internal__.getField("longDescription", undefined);
        }

        set longDescription(value) {
            this.__internal__.setField("longDescription", value);
        }

        /**
         * readonly Integer startTime
         * The start time of the scheduled recording, measured in seconds since midnight (GMT) on 1/1/1970. The
         * value for the startPadding property can be used to indicate if the recording has to be started before the
         * startTime (as defined by the Programme class).
         */
        get startTime() {
            return this.__internal__.getField("startTime", undefined);
        }

        /**
         * readonly Integer duration
         * The duration of the scheduled recording (in seconds). The value for the endPadding property can be used to
         * indicate how long the recording has to be continued after the specified duration of the recording.
         */
        get duration() {
            return this.__internal__.getField("duration", undefined);
        }

        /**
         * readonly Channel channel
         * Reference to the broadcast channel where the scheduled programme is available.
         */
        get channel() {
            return this.__internal__.getField("channel", undefined);
        }

        /**
         * readonly Boolean isManual
         * true if the recording was scheduled using oipfRecordingScheduler.recordAt() or using a terminal-
         * specific approach that does not use guide data to determine what to record, false otherwise.
         * If false , then any fields whose name matches a field in the Programme object contains details from the
         * programme guide on the programme that has been recorded. If true , only the channel , startTime and
         * duration properties are required to be valid.
         */
        get isManual() {
            return this.__internal__.getField("isManual", undefined);
        }

        /**
         * readonly String programmeID
         * The unique identifier of the scheduled programme or series, e.g. a TV-Anytime CRID (Content Reference
         * Identifier). For recordings scheduled using the oipfRecordingScheduler.recordAt() method, the value
         * of this property MAY be undefined.
         */
        get programmeID() {
            return this.__internal__.getField("programmeID", undefined);
        }

        /**
         * readonly Integer programmeIDType
         * The type of identification used to reference the programme, as indicated by one of the ID_* constants
         * defined in section 7.10.2.1. For recordings scheduled using the oipfRecordingScheduler.recordAt()
         * method, the value of this property MAY be undefined.
         */
        get programmeIDType() {
            return this.__internal__.getField("programmeIDType", undefined);
        }

        /**
         * readonly Integer episode
         * The episode number for the programme if it is part of a series. This property is undefined when the
         * programme is not part of a series or the information is not available.
         */
        get episode() {
            return this.__internal__.getField("episode", undefined);
        }

        /**
         * readonly Integer totalEpisodes
         * If the programme is part of a series, the total number of episodes in the series. This property is undefined
         * when the programme is not part of a series or the information is not available.
         */
        get totalEpisodes() {
            return this.__internal__.getField("totalEpisodes", undefined);
        }

        /**
         * readonly ParentalRatingCollection parentalRatings
         * A collection of parental rating values for the programme for zero or more parental rating schemes supported
         * by the OITF. The value of this property is typically provided by a corresponding “ Programme ” object that is
         * used to schedule the recording and can not be changed for this scheduled recording object. If no parental
         * rating information is available for this scheduled recording, this property is a ParentalRatingCollection
         * object (as defined in section 7.9.5) with length 0.
         * Note that if the parentalRating property contains a certain parental rating (e.g. PG-13) and the broadcast
         * channel associated with this scheduled recording has metadata that says that the content is rated PG-16,
         * then the conflict resolution is implementation dependent.
         * Note that this property was formerly called “parentalRating” (singular not plural).
         */
        get parentalRatings() {
            return this.__internal__.getField("parentalRatings", undefined);
        }

        /**
         * String customMetadata
         * Application-specific information for this recording. This value is information that the DAE application can set
         * in order to retain additional information on this scheduled recording. It is not changed by the OITF.
         * The OITF SHALL support values up to and including 16 K Bytes in size. Strings longer than this MAY get
         * truncated.
         */
        get customMetadata() {
            return this.__internal__.getField("customMetadata", undefined);
        }

        set customMetadata(value) {
            this.__internal__.setField("customMetadata", value);
        }

        // to json is used because getter are not enumerable in ES6 classes
        toJSON() {
            let result = Object.assign({}, this.__internal__.getValues());
            if (this.channel) {
                result.channel = this.channel.toJSON();
            }
            return result;
        }
    };
};
