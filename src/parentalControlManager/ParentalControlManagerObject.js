"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.9.1 The application/oipfParentalControlManager embedded object
 *If an OITF supports parental controls as indicated by value "true" for element <parentalcontrol> (as defined by
 *section 9.3.5) in its capability profile, the OITF SHALL support the
 *application/oipfParentalControlManager object with the following interface.
 */
let requireJS = require;
// FIXME to implement
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let ParentalRating = require("./ParentalRating");
    let ParentalRatingCollection = require("./ParentalRatingCollection");

    return class ParentalControlManagerObject {

        /**
         * fields can be have:
         *  - parentalRatingSchemes: collection of rating schemes known by the OITF.
         *
         * - lockedTime [default: 60s]: locked out for a period
         *
         * - pinFaildLock [default: 3]: number of pin try enter, before lock
         */
        constructor(fields) {
            ctx.__internal__.init(this);
            this.__internal__.setValues(fields);
        }

        /**
         * readonly ParentalRatingSchemeCollection parentalRatingSchemes
         *
         * A reference to the collection of rating schemes known by the OITF.
         */
        get parentalRatingSchemes() {
            let result = this.__internal__.getField("parentalRatingSchemes", []);
            // FIXME convert array to ParentalRatingSchemeCollection
            return result;
        }

        /**
         * readonly Boolean isPINEntryLocked
         *
         * The lockout status of the parental control PIN. If the incorrect PIN has been entered too many times in the
         * configured timeout period, parental control PIN entry SHALL be locked out for a period of time determined by
         * the OITF.
         */
        get isPINEntryLocked() {
            return !!this.__internal__.getField("isPINEntryLocked", false);
        }

        /**
         * Integer setParentalControlStatus( String pcPIN, Boolean enable )
         *
         * Description:
         *
         * As defined in [OIPF_CSP2], the OITF shall prevent the consumption of a programme when
         * its parental rating doesn't meet the parental rating criterion currently defined in the OITF.
         * Calling this method with enable set to false will temporarily allow the consumption of any
         * blocked programme.
         *
         * Setting the parental control status using this method SHALL set the status until the
         * consumption of any of all the blocked programmes terminates (e.g. until the content item
         * being played is changed), or another call to the setParentalControlStatus() method is
         * made.
         *
         * Setting the parental control status using this method has the following effect; for the
         * Programme and Channel objects as defined in sections 7.16.2 and 7.13.11, the blocked
         * property of a programme or channel SHALL be set to true for programmes whose parental
         * rating does not meet the applicable parental rating criterion, but the locked property
         * SHALL be set to false.
         *
         * This operation to temporarily disable parental rating control SHALL be protected by the
         * parental control PIN (i.e. through the pcPIN argument). The return value indicates the
         * success of the operation, and SHALL take one of the following values:
         *
         *   Value       Description
         *      0        The PIN is correct.
         *      1        The PIN is incorrect.
         *      2        PIN entry is locked because an invalid PIN has been entered too many
         *               times. The number of invalid PIN attempts before PIN entry is locked is
         *               outside the scope of this specification.
         *
         * Arguments
         *
         * - pcPIN:
         *   The parental control PIN.
         *
         * - enable:
         *   Flag indicating whether parental control should be enabled.
         */
        setParentalControlStatus(pcPIN, enable) {
            let result = this.verifyParentalControlPIN(pcPIN);
            if (result === 0) {
                this.__internal__.getField("parentalControlStatus", enable);
            }
            return result;
        }

        /**
         * Boolean getParentalControlStatus()
         *
         * Description
         *
         * Returns a flag indicating the temporary parental control status set by
         * setParentalControlStatus(). Note that the returned status covers parental control
         * functionality related to all rating schemes, not only the rating scheme upon which the
         * method is called.
         *
         */
        getParentalControlStatus() {
            return this.__internal__.getField("parentalControlStatus", false);
        }

        /**
         * Boolean getBlockUnrated()
         * Description
         *
         * Returns a flag indicating whether or not the OITF has been configured by the user to block
         * content for which a parental rating is absent.
         */
        getBlockUnrated() {
            return this.__internal__.getField("blockUnrated", false);
        }

        /**
         * Integer setParentalControlPIN( String oldPcPIN, String newPcPIN )
         * Description
         *
         * Set the parental control PIN.
         *
         * This operation SHALL be protected by the parental control PIN (if PIN entry is enabled).
         *
         * The return value indicates the success of the operation, and SHALL take one of the
         *
         * following values:
         *   Value      Description
         *      0       The PIN is correct.
         *      1       The PIN is incorrect.
         *      2       PIN entry is locked because an invalid PIN has been entered too many times.
         *              The number of invalid PIN attempts before PIN entry is locked is outside the
         *              scope of this specification.
         *
         * Arguments
         * - oldPcPIN
         *   The current parental control PIN.
         *
         * - newPcPIN
         *   The new value for the parental control PIN.
         *
         */
        setParentalControlPIN(oldPcPIN, newPcPIN) {
            let result = this.verifyParentalControlPIN(oldPcPIN);
            if (result === 0) {
                this.__internal__.getField("parentalControlPIN", newPcPIN);
            }
            return result;
        }

        /**
         * Integer unlockWithParentalControlPIN( String pcPIN, Object target )
         *
         * Description
         *
         * Unlock the object specified by target for viewing if pcPIN contains the correct parental
         * control PIN.
         *
         * The object type of target can be one of the following:
         *
         * - video/broadcast object, in which case the content being presented through this
         *   object SHALL be unlocked until a new channel is selected.
         *
         * - A/V Control object, in which case the content being presented through this object
         *   SHALL be unlocked until a new item of content is played using this object
         *
         * Otherwise an Invalid Object error SHALL be returned.
         * The return value indicates the success of the operation, and SHALL take the following
         * values:
         *
         *      Value                                     Description
         *         0         The PIN is correct.
         *         1         The PIN is incorrect.
         *         2         PIN entry is locked because an invalid PIN has been entered too
         *                   many times. The number of invalid PIN attempts before PIN entry is
         *                   locked is outside the scope of this specification.
         *         3         Invalid object.
         *
         * Arguments
         *
         * - pcPIN
         *   The parental control PIN.
         *
         * - target
         *   The object to be unlocked.
         *
         */
        unlockWithParentalControlPIN(pcPIN, target) {
            let result;
            // if target is <video> or <audio> target must have function play
            if (target && (target.tagName === "video-broadcast" || target.play)) {
                result = this.verifyParentalControlPIN(pcPIN);
                if (result === 0) {
                    // FIXME unlock target, how ?
                }
            } else {
                result = 3;
            }
            return result;
        }

        /**
         * Integer verifyParentalControlPIN( String pcPIN )
         *
         * Description
         *
         * Verify that the PIN specified by pcPIN is the correct parental control PIN.
         *
         * This method will return one of the following values:
         *
         *           Value                                     Description
         *         0         The PIN is correct.
         *         1         The PIN is incorrect.
         *         2         PIN entry is locked because an invalid PIN has been entered too
         *                   many times. The number of invalid PIN attempts before PIN entry is
         *                   locked is outside the scope of this specification.
         *
         * Arguments
         * - pcPIN
         *   The parental control PIN to be verified.
         */
        verifyParentalControlPIN(pcPIN) {
            let result;
            if (this.isPINEntryLocked()) {
                result = 2;
            } else {
                let pin = this.__internal__.getField("parentalControlPIN", undefined);
                if (!pin || pin === pcPIN) {
                    // ok
                    this.__internal__.setField("tryNumber", 0);
                    result = 0;
                } else {
                    let tryNumber = this.__internal__.getField("tryNumber", 0) + 1;
                    this.__internal__.setField("tryNumber", tryNumber);
                    let pinFaildLock = this.__internal__.getField("pinFaildLock", 3);
                    if (tryNumber > pinFaildLock) {
                        let lockedTime = this.__internal__.getField("lockedTime", 60);
                        let __internal__ = this.__internal__;
                        this.__internal__.setField("isPINEntryLocked", setTimeout(function() {
                            __internal__.setField("isPINEntryLocked", undefined);
                        }, lockedTime * 1000));
                    }
                    result = 1;
                }
            }
            return result;
        }

        /**
         * Integer setBlockUnrated( String pcPIN, Boolean block )
         *
         * Description*
         *
         * Set whether programmes for which no parental rating has been retrieved from the metadata
         * client nor defined by the service provider should be blocked automatically by the terminal.
         * This operation SHALL be protected by the parental control PIN (if PIN entry is enabled).
         * The return value indicates the success of the operation, and SHALL take one of the
         * following values:
         *
         *      Value        Description
         *         0         The PIN is correct.
         *         1         The PIN is incorrect.
         *         2         PIN entry is locked because an invalid PIN has been entered too
         *                   many times. The number of invalid PIN attempts before PIN entry is
         *                   locked is outside the scope of this specification.
         *
         * Arguments:
         *
         * - pcPIN
         *   The parental control PIN.
         *
         * - block
         *   Flag indicating whether programmes SHALL be blocked.
         */
        setBlockUnrated(pcPIN, block) {
            let result = this.verifyParentalControlPIN(pcPIN);
            if (result === 0) {
                this.__internal__.getField("blockUnrated", block);
            }
            return result;
        }
    };
};
