"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

/**
 * 7.9.3 The ParentalRatingSchemeCollection class
 *
 *     typedef Collection<ParentalRatingScheme> ParentalRatingSchemeCollection
 *
 * A ParentalRatingSchemeCollection represents a collection of parental rating schemes, e.g. as returned by property
 * parentalRatingSchemes of the "application/oipfParentalControlManager" object as defined in section
 * 7.9.1. Next to the properties and methods defined below a ParentalRatingSchemeCollection object SHALL
 * support the array notation to access the parental rating scheme objects in this collection.
 *
 *  See Annex K for the definition of the collection template. In addition to the methods and properties defined for generic
 * collections, the ParentalRatingSchemeCollection class supports the additional properties and methods defined
 * below.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Collection = require("../shared/Collection");
    let ParentalRatingScheme = require("./ParentalRatingScheme");

    return class ParentalRatingSchemeCollection extends Collection {

        /**
         * ParentalRatingScheme addParentalRatingScheme( String name, String values )
         *
         * Description:
         *
         * Create a new ParentalRatingScheme object and adds it to the
         * ParentalRatingSchemeCollection. Applications MAY use this method to register
         * additional parental rating schemes with the platform. When registered, the new parental
         * rating scheme SHALL (temporarily) be accessible through the parentalRatingSchemes
         * property of the "application/oipfParentalControlmanager" object as defined in
         * section 7.9.1.
         * The application SHALL make sure that the values are ordered in such a way to allow the
         * rating values to be compared in the manner as defined for the threshold property for the
         * respective parental rating system.
         * This method returns a reference to the ParentalRatingScheme object representing the
         * added scheme. If the value of the name parameter corresponds to an already-registered
         * rating scheme, this method returns a reference to the existing ParentalRatingScheme
         * object. If the newly defined rating scheme was not known to the OITF, the scheme MAY
         * be stored persistently, and the OITF may offer a UI to set the parental rating blocking
         * criteria for the newly added parental rating scheme.
         * If the OITF has successfully stored (persistently or not persistently) the additional parental
         * rating scheme, the method SHALL return a non-null ParentalRatingScheme object.
         *
         * Arguments:
         *
         * - name:
         *   A unique string identifying the parental rating scheme to which this
         *   value refers. See property name in section 7.9.1.1 for more information
         *   about possible values.
         *
         * - values:
         *   A comma-separated list of the possible values in the rating scheme, in
         *   ascending order of severity. In case the rating scheme is one of the
         *   [MPEG-7] rating classification schemes, this means that the list of
         *   parental rating values contains the values as specified by the <Name>
         *   elements of the <Term> elements in the order of appearance as they
         *   are defined for the classification scheme, with the exception of the
         *   Internet Content Rating Association (ICRA) based ratings, for which
         *   the reverse order has to be applied. The values must be ordered in
         *   such a way to allow the rating values to be compared in the manner
         *   as defined for property threshold for the respective parental rating
         *   system.
         */
        addParentalRatingScheme(name, values) {
            this.push(new ParentalRatingScheme({
                name: name,
                values: values
            }));
        }

        /**
         * ParentalRatingScheme getParentalRatingScheme( String name )
         *
         * Description:
         * This method returns a reference to the ParentalRatingScheme object that is associated
         * with the given scheme as specified through parameter "name". If the value of name does
         * not correspond to the name property of any of the ParentalRatingScheme objects in the
         * ParentalRatingSchemeCollection, the method SHALL return undefined.
         *
         * Arguments:
         *
         * - name
         *   The unique name identifying a parental rating scheme.
         */
        getParentalRatingScheme(name) {
            let result;
            for (let s of this) {
                if (s.name === name) {
                    result = s;
                    break;
                }
            }
            return result;
        }
    };
};
