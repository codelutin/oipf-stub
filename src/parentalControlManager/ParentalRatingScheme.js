"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.9.2 The ParentalRatingScheme class
 *
 *          typedef Collection<String> ParentalRatingScheme
 *
 * A ParentalRatingScheme describes a single parental rating scheme that may be in use for rating content, e.g. the
 * MPAA or BBFC rating schemes. It is a collection of strings representing rating values, which next to the properties and
 * methods defined below SHALL support the array notation to access the rating values in this collection. For the natively
 * OITF supported parental rating systems the values SHALL be ordered by the OITF to allow the rating values to be
 * compared in the manner as defined for property threshold for the respective parental rating system. Using a threshold
 * as defined in this API may not necessarily be the proper way in which parental rating filtering is applied on the OITF,
 * e.g. the US FCC requirements take precedence for device to be imported to the US.
 *
 * The parental rating schemes supported by a receiver MAY vary between deployments.
 *
 * See Annex K for the definition of the collection template. In addition to the methods and properties defined for generic
 * collections, the ParentalRatingScheme class supports the additional properties and methods defined below.
 */

// FIXME to implement
module.exports = function(ctx) {

    return class ParentalRatingScheme {


        constructor(fields) {
            ctx.__internal__.init(this);
            this.__internal__.setValues(fields);

            let values = this.__internal__.getField("values", []);
            if (!Array.isArray(values)) {
                values = values.split(",");
                this.__internal__.setField("values", values);
            }
        }

        /**
         * readonly String name
         *
         * The unique name that identifies the parental rating scheme. Valid strings include:
         *
         * - the URI of one of the MPEG-7 classification schemes representing a parental rating scheme as
         *   defined by the "uri" attribute of one of the parental rating <ClassificationScheme> elements in
         *   [MPEG-7].
         *
         * - the string value "urn:oipf:GermanyFSKCS" to represent the GermanyFSK rating scheme as
         *   defined in [OIPF_META2].
         *
         * - the string value "dvb-si": this means that the scheme of a minimum recommended age encoded as
         *   per ratings 0x01 to 0x0f in the parental rating descriptor from [EN 300 468], is used to represent the
         *   parental rating values.
         *
         * NOTE: If the broadcaster defined range from 0x10 to 0xff is used then that would be a different parental
         * rating scheme and not "dvb-si".
         *
         * If the value of "name" is "dvb-si", the ParentalRatingScheme remains empty (i.e.
         * ParentalRatingScheme.length == 0).
         */
        get name() {
            return this.__internal__.getField("name", undefined);
        }

        /**
         * readonly ParentalRating threshold
         *
         * The parental rating threshold that is currently in use by the OITF's parental control system for this rating
         * scheme, which is encoded as a ParentalRating object in the following manner:
         *
         * If the value of the "name" property of the ParentalRatingScheme object is unequal to "dvb-si", then:
         *
         * - the "value" property of the threshold object represents the value for which items with a
         *   ParentalRating.value greater or equal to the "value" property of the threshold object have been
         *   configured by the OITF's parental control subsystem to be blocked.
         *
         * - the "labels" property of the threshold object represents the bit map of zero or more flags for which
         *   items with a ParentalRating.labels property with any of the same flags set have been
         *   configured by the OITF's parental control subsystem to be blocked.
         *
         * If the value of the name property of the ParentalRatingScheme object is "dvb-si", the threshold indicates
         * a minimum recommended age encoded as per [EN 300 468] at which or above which the content is being
         * blocked by the OITF's parental control subsystem
         *
         * Note that the value property as an index into the ParentalRating object that defines the threshold can be
         * 1 larger than the value of ParentalRatingScheme.length to convey that no content is being blocked by
         * the parental control subsystem.
         *
         */
        get threshold() {
            return this.__internal__.getField("threshold", undefined);
        }

        /**
         * Integer indexOf( String ratingValue )
         *
         * Description
         *
         * Return the index of the rating represented by attribute ratingValue inside the parental
         * rating scheme string collection, or -1 if the rating value cannot be found in the collection.
         *
         * Arguments:
         *
         * - ratingValue
         *   The string representation of a parental rating value. See property name
         *   in section 7.9.1.1 for more information about possible values. Values are
         *   not case sensitive.
         *
         */
        indexOf(ratingValue) {
            let values = this.__internal__.getField("values", []);
            let result = values.indexOf(ratingValue);
            return result;
        }

        /**
         * String iconUri( Integer index )
         *
         * Description
         *
         * Return the URI of the icon representing the rating at index in the rating scheme, or
         * undefined if no item is present at that position. If no icon is available, this method SHALL
         * return null.
         *
         * Arguments:
         * - index:
         *   The index of the parental rating scheme.
         *
         */
        iconUri(/*index*/) {
            //. FIXME
            return this.__internal__.getField("iconUri", undefined);
        }
    };
};
