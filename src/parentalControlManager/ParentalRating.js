"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.9.4 The ParentalRating class
 * A ParentalRating object describes a parental rating value for a programme or channel. The ParentalRating
 * object identifies both the rating scheme in use, and the parental rating value within that scheme.
 * In case of a BCG the values of the properties in this object will be read from the ParentalGuidance element that is
 * the child of a programme's BCG description.
 */
module.exports = function(ctx) {

    return class ParentalRating {


        constructor(fields) {
            ctx.__internal__.init(this);
            this.__internal__.setValues(fields);
        }

        /**
         * The string representation of the parental rating value for the respective rating scheme denoted by property
         * scheme.
         *
         * Valid strings include:
         *
         * - if the value of property scheme represents one of the parental rating classification scheme names
         *   identified by [MPEG-7]: the string representation of one of the parental rating values as defined by
         *   one of the <Name> elements.
         *
         * - if the value of property scheme is "urn:oipf:GermanyFSKCS" , the string representation of one the
         *   values for the GermanyFSK rating scheme as defined in [OIPF_META2].
         *
         * - if the value of property scheme is equal to "dvb-si", this means that the scheme of a minimum
         *   recommended age encoded as per ratings 0x01 to 0x0f in the parental rating descriptor from [EN
         *   300 468], which corresponds to rating_type 0 in [IEC62455].
         *
         *   NOTE: If the broadcaster defined range from 0x10 to 0xff is used then that would be a different parental
         *   rating scheme and not "dvb-si".
         *
         * An example of a valid parental rating value is "PG-13".
        *
        * Visibility Type: readonly String
         */
        get name() {
            return this.__internal__.getField("name", undefined);
        }

        /**
         * readonly String scheme
         * Unique name identifying the parental rating guidance scheme to which this parental rating value refers. Valid
         * strings include:
         *
         * - the URI of one of the MPEG-7 classification schemes representing a parental rating scheme as
         *   defined by the "uri" attribute of one of the parental rating <ClassificationScheme> elements in
         *   [MPEG-7].
         *
         * - the string value "urn:oipf:GermanyFSKCS" to represent the GermanyFSK rating scheme as
         *   defined in [OIPF_META2].
         *
         * - the string value "dvb-si": this means that the scheme of a minimum recommended age encoded as
         *   per [EN 300 468], is used to represent the parental rating values.
         *
         * Visibility Type: readonly String
         */
        get scheme() {
            return this.__internal__.getField("scheme", undefined);
        }

        /**
         * The parental rating value represented as an index into the set of values defined as part of the
         * ParentalRatingScheme identified through property "scheme".
         *
         * If an associated ParentalRatingScheme object can be found by calling method
         * getParentalRatingScheme() on property parentalRatingSchemes of the
         * application/oipfParentalControlManager object and the value of property scheme is not equal to
         * "dvb-si", then the value property SHALL represent the index of the parental rating value inside the
         * ParentalRatingScheme object, or -1 if the value cannot be found. If the value of property scheme is equal
         * to "dvb-si", then this property SHALL be the integer representation of the string value of ParentalRating
         * property name.
         *
         * If no associated ParentalRatingScheme object can be found by calling method
         * getParentalRatingScheme on property parentalRatingSchemes of the
         * application/oipfParentalControlManager object, then the value property SHALL have value
         * undefined.
         *
         * Visibility Type: readonly Integer
         */
        get value() {
            return this.__internal__.getField("value", undefined);
        }

        /**
         * The labels property represents a set of parental advisory flags that may provide additional information about
         * the rating.
         *
         * The value of this field is a 32 bit integer value that represents a binary mask corresponding to the sum of
         * zero or more label values defined in the table below. If no labels have been explicitly set, the value for the
         * "labels" property SHALL be 0.
         *
         * Valid labels include:
         *    +-------+----------------------------------------------------------------------------------------+
         *    | Value | Description                                                                            |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |    1  | Indicates that a content item features sexual suggestive dialog.                       |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |    2  | Indicates that a content item features strong language.                                |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |    4  | Indicates that a content item features sexual situations.                              |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |    8  | Indicates that a content item features violence.                                       |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |   16  | Indicates that a content item features fantasy violence.                               |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |   32  | Indicates that a content item features disturbing scenes.                              |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |   64  | Indicates that a content item features portrayals of discrimination.                   |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |  128  | Indicates that a content item features scenes of illegal drug use.                     |
         *    +-------+----------------------------------------------------------------------------------------+
         *    |  256  | Indicates that a content item features strobing that could impact viewers suffering    |
         *    |       | from Photosensitive epilepsy                                                           |
         *    +-------+----------------------------------------------------------------------------------------+
         *
         * Visibility Type: readonly Integer
         */
        get labels() {
            return this.__internal__.getField("labels", 0);
        }

        /**
         * readonly String region
         *
         * The region to which the parental rating value applies as an alpha-2 region code as defined in ISO 3166-1.
         * Returns undefined if no specific region has been defined.
         */
        get region() {
            return this.__internal__.getField("region", undefined);
        }

    };
};
