"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.3.9 The Tuner class
 * A Tuner object represents the source of broadcast content provided through a
 * physical tuner in the OITF. Each Tuner object is represented by a
 *  <video_broadcast> element in the capability description as defined in
 *  section 9.3.1.
 *
 * A Tuner object that is capable of tuning at the same time to multiple
 * transponders SHALL have the nrstreams attribute of the <video_broadcast>
 * element set to a value equal to the number of transponders.
 *
 * A Tuner object that is capable of tuning to transponders of different types
 * SHALL include all those types in the types attribute of the <video_broadcast>
 * element.
 *
 * NOTE: An OITF may contain a physical tuner that has its capabilities split
 * into multiple Tuner objects to fit the restrictions on the <video_broadcast>
 * element outlined above and in section 9.3.1.
 */
let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let IntegerCollection = require("./shared/IntegerCollection");

    return class Tuner {

        constructor(index, fields) {
            ctx.__internal__.init(this);
            this.__internal__.index = index;
            this.__internal__.setValues(fields);
        }

        /**
         * A unique identifier of the tuner.
         *
         * Visibility Type: readonly Integer
         */
        get id() {
            return this.__internal__.getField("id", this.__internal__.index);
        }

        /**
         * The name of the tuner as designated in OITF.
         *
         * Visibility Type: readonly String
         */
        get name() {
            return this.__internal__.getField("name", this.__internal__.index + "");
        }

        /**
         * Returns a collection of the types supported by the tuner. The types are
         * according to the ID types in section 7.13.11.1 under Channel object.
         *
         * Visibility Type: readonly IntegerCollection
         */
        get idTypes() {
            let result = this.__internal__.getField("idTypes", [0]);
            // FIXME poussin 20150729
            // let result = new IntegerCollection();
            // result.push.apply(result, values);
            return result;
        }


        /**
         * The property enables (true) and disables (false) the tuner. Reading the
         * property provides the current state, enabled or disabled. Attempting to
         * disable the tuner while the resource is in use has no effect and the
         * tuner SHALL continue to be enabled. While disabled:
         * - any external power feed (if applicable) SHALL be turned off;
         * - the value of the signalInfo property is not defined;
         * - the value of the lnbInfo property is not defined;
         * - the tuner SHALL NOT be available for use by any JavaScript object
         *   (e.g. the video/broadcast object) or by the underlying OITF system
         *   (e.g. to perform a scheduled recording). Note the property enableTuner
         *   is available in order to re-enable the tuner and get access to the
         *   tuner again.
         *
         * The set value of the property SHALL persist after OITF restarts.
         *
         * Visibility Type: read/write Boolean
         */
        get enableTuner() {
            let result = this.__internal__.getField("enableTuner", true);
            return result;
        }

        set enableTuner(b) {
            // FIXME poussin 20150729 how to know if tuner is in used ? video broadcast must send message when play ?
            // if (!this.__internal__.isInUsed) {
            this.__internal__.setField("enableTuner", b);
            // }
        }

        /**
         * The property returns a SignalInfo object with signal information for
         * example signal strength.
         *
         * Visibility Type: readonly SignalInfo
         */
        get signalInfo() {
            let result = this.__internal__.getField("signalInfo", undefined);
            // FIXME poussin 20150729 let result = new SignalInfo(value);

            return result;
        }

        /**
         * The property returns a LNBInfo object with information regarding the LNB
         * associated with the tuner.
         *
         * Visibility Type: readonly LNBInfo
         */
        get lnbInfo() {
            let result = this.__internal__.getField("lnbInfo", undefined);
            // FIXME poussin 20150729 let result = new LNBInfo(value);

            return result;
        }

        /**
         *Indicates the physical interface associated with the tuner.
         *
         * Visibility Type: readonly Integer
         */
        get frontEndPosition() {
            return this.__internal__.getField("frontEndPosition", 0);
        }

        /**
         * The property turns on (true) and off (false) the power applied to the
         * external interface of the tuner unless the tuner is disabled. Reading the
         * property provides the current value, on or off. Attempting to modify the
         * property while the resource is in use has no effect. The value of the
         * property SHALL persist after OITF restarts.
         *
         * For DVB-S/S2 power is supplied to the LNB(s) and if present the DiSEqC
         * switch.
         *
         * For DVB-T/T2 a supply +5V is supplied to the antenna with built in
         * amplifier. Note that applying power may have adverse effects to the
         * external equipment if it has its own power supply. It is a strong
         * recommendation to indicate to the end user a possible adverse effect
         * before using this method.
         *
         * For DVB-C/C2 there is no effect.
         *
         * Reading the property provides the current value.
         *
         * Visibility Type: readonly Boolean
         */
        get powerOnExternal() {
            let result = this.__internal__.getField("powerOnExternal", true);

            return result;
        }

        set powerOnExternal(b) {
            // FIXME poussin 20150729 how to know if tuner is in used ? video broadcast must send message when play ?
            // if (!this.__internal__.isInUsed) {
            this.__internal__.setField("powerOnExternal", b);
            // FIXME poussin 20150729 how to persist state between two session, is it really needed ?
            // }
        }

    };
};
