"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/*
 * Description:
 * The LocalSystem object allows hardware settings related to the local device to be read and modified.
 *
 * Note: The standbyState property has been removed from this class.
 */
module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);
    let AVOutput = require("../shared/AVOutput");
    let AVOutputCollection = require("../shared/AVOutputCollection");
    let NetworkInterface = require("../shared/NetworkInterface");
    let NetworkInterfaceCollection = require("../shared/NetworkInterfaceCollection");

    return class LocalSystem {

        constructor(events) {
            ctx.__internal__.init(this, events);

            /*
             * The OITF is in the off state and no power is consumed.
             * This is the case of a power outage or if the OITF has the
             * ability to be completely turned off. Scheduled recording is
             * not expected to work.
             */
            this.OFF = 0;

            /*
             * The OITF is in normal working mode with user
             * interaction. The DAE applications may render any
             * presentation graphically.
             */
            this.ON = 1;

            /*
             * The OITF is in the lowest possible power consumption
             * state (meeting regulations and certifications). The OITF
             * may support wake-up from a passive standby in order, for
             * example, to perform a scheduled recording.
             */
            this.PASSIVE_STANDBY = 2;

            /*
             * The OITF is in an intermediate power consumption state.
             * The output to the display shall be inactive. In this state
             * DAE applications may continue to operate.
             */
            this.ACTIVE_STANDBY = 3;

            /*
             * The OITF is in the lowest possible power consumption
             * state (meeting regulations and certifications). If th
             * platform supports hibernate mode then the OITF stores
             * all applications in volatile memory to allow for quick
             * startup.
             */
            this.PASSIVE_STANDBY_HIBERNATE = 4;

            /*
             * The OITF shall restart and return to a ON state.
             */
            this.RESTART = 5;

            /*
             * Restart the OITF and reset all settings and data to an
             * initial/factory state. The exact settings and data to be
             * reset are implementation dependant. The use of the this
             * operation with the setPowerState method is subject to
             * security control defined in section 10.1.3.8
             */
            this.FACTORY_RESET = 6;

            /*
             * No startup URL is known.
             */
            this.STARTUP_URL_NONE = 0;

            /*
             * The startup URL is derived from DHCP procedures.
             */
            this.STARTUP_URL_DHCP = 1;

            /*
             * The startup URL is derived through TR-069 procedures.
             */
            this.STARTUP_URL_TR069 = 2;

            /*
             * The startup URL is that which is configured through the OITF firmware.
             */
            this.STARTUP_URL_PRECONFIGURED = 3;

            /*
             * The startup URL is obtained through other
             * (perhaps non-standardized) procedures.
             */
            this.STARTUP_URL_OTHER = 9;
        }

        /*
         * Description:
         * Private OITF Identifier. This property SHALL take the value undefined
         * except when accessed by applications meeting
         * either of the following criteria:
         *
         * - The application is signalled in an SD&S service provider discovery
         * record with an application usage of
         * urn:oipf:cs:ApplicationUsageCS:2009:hni-igi where
         * the SD&S service provider discovery record was obtained by
         * the OITF through the procedure defined in section 5.4.1.2 of [OIPF_PROT2].
         *
         * - The URL of the application was discovered directly through
         * the procedure defined in section 5.4.1.2 of [OIPF_PROT2].
         *
         * In these two cases, it SHALL take the same value as defined for the
         * DHCP client identifier in DHCP option 61
         * in section 12.1.1.1 of [OIPF_PROT2].
         *
         * Visibility Type: readonly String
         */
        get deviceID() {
            return this.__internal__.getField("deviceID", function() {
                return ctx.config.default.deviceID;
            });
        }

        /*
         * Description:
         * Indicates whether the system has finished initialising.
         * A value of true indicates that the system is ready.
         *
         * Visibility Type: readonly Boolean
         */
        get systemReady() {
            return this.__internal__.getField("systemReady", false);
        }

        /*
         * Description:
         * String identifying the vendor name of the device.
         *
         * Visibility Type: readonly String
         */
        get vendorName() {
            return this.__internal__.getField("vendorName", function() {
                return ctx.config.default.vendorName;
            });
        }

        /*
         * Description:
         * String identifying the model name of the device.
         *
         * Visibility Type: readonly String
         */
        get modelName() {
            return this.__internal__.getField("modelName", function() {
                return ctx.config.default.modelName;
            });
        }

        /*
         * Description:
         * String identifying the name of the family that the device belongs to.
         * Devices in a family differ only by details that do not impact
         * the behaviour of the OITF aspect of the device, e.g. screen size,
         * remote control, number of HDMI ports, size of hard disc.
         * Family names are allocated by the vendor and the combination of
         * vendorName and familyName should uniquely identify a family of devices.
         * Different vendors may use the same familyName, although
         * they are recommended to use conventions that avoid this.
         *
         * Visibility Type: readonly String
         */
        get familyName() {
            return this.__internal__.getField("familyName", function() {
                return ctx.config.default.familyName;
            });
        }

        /*
         * Description:
         * String identifying the version number of the platform firmware.
         *
         * Visibility Type: readonly String
         */
        get softwareVersion() {
            return this.__internal__.getField("softwareVersion", function() {
                return ctx.config.default.softwareVersion;
            });
        }

        /*
         * Description:
         * String identifying the version number of the platform firmware.
         *
         * Visibility Type: readonly String
         */
        get hardwareVersion() {
            return this.__internal__.getField("hardwareVersion", function() {
                return ctx.config.default.hardwareVersion;
            });
        }

        /*
         * Description:
         * String identifying the version number of the platform firmware.
         *
         * Visibility Type: readonly String
         */
        get serialNumber() {
            return this.__internal__.getField("serialNumber", function() {
                return ctx.config.default.serialNumber;
            });
        }

        /*
         * Description:
         * Release version of the OIPF specification implemented by the OITF.
         * For instance, if the OITF implements release 2 version “1.0”,
         * this property should be set to 2.
         *
         * Visibility Type: readonly Integer
         */
        get releaseVersion() {
            return this.__internal__.getField("releaseVersion", function() {
                return ctx.config.default.releaseVersion;
            });
        }

        /*
         * Description:
         * Major version of the OIPF specification implemented by the OITF.
         * For instance, if the OITF implements release 2 version “2.0”,
         * this property should be set to 2.
         *
         * Visibility Type: readonly Integer
         */
        get majorVersion() {
            return this.__internal__.getField("majorVersion", function() {
                return ctx.config.default.majorVersion;
            });
        }

        /*
         * Description:
         * Minor version of the OIPF specification implemented by the OITF.
         * For instance, if the OITF implements release 2 version “2.0”,
         * this property should be set to 0.
         *
         * Visibility Type: readonly Integer
         */
        get minorVersion() {
            return this.__internal__.getField("minorVersion", function() {
                return ctx.config.default.minorVersion;
            });
        }

        /*
         * Description:
         * Profile of the OIPF specification implemented by the OITF.
         * Values of this field are not defined in this specification.
         *
         * Visibility Type: readonly String
         */
        get oipfProfile() {
            return this.__internal__.getField("oipfProfile", undefined);
        }

        /*
         * Description:
         * Flag indicating whether the platform has PVR capability (local PVR).
         * Note: This property is deprecated in favour of the pvrSupport property.
         *
         * Visibility Type: readonly Boolean
         */
        get pvrEnabled() {
            return this.__internal__.getField("pvrEnabled", true);
        }

        /*
         * Description:
         * Flag indicating whether the platform has CI+ capability.
         *
         * Visibility Type: readonly String
         */
        get pvrEnabled() {
            return this.__internal__.getField("ciplusEnabled", undefined);
        }

        /*
         * Description:
         * The powerState property provides the DAE application the ability
         * to determine the current state of the OITF.
         * The property is limited to the ACTIVE_STANDBY or ON states.
         *
         * Visibility Type: readonly Integer
         */
        get powerState() {
            return this.__internal__.getField("powerState", this.ACTIVE_STANDBY);
        }

        /*
         * Description:
         * The previousPowerState property provides the DAE application the ability
         * to retrieve the previous state.
         *
         * Visibility Type: readonly String
         */
        get previousPowerState() {
            return this.__internal__.getField("previousPowerState", undefined);
        }

        /*
         * Description:
         * The time that the OITF entered the current power state.
         * The time is represented in seconds since midnight (GMT) on 1/1/1970.
         *
         * Visibility Type: readonly String
         */
        get timeCurrentPowerState() {
            return this.__internal__.getField("timeCurrentPowerState", undefined);
        }

        /*
         * Description:
         * A collection of Tuner objects representing the physical tuners available in the OITF.
         *
         * Visibility Type: readonly TunerCollection
         */
        get tuners() {
            return this.__internal__.getField("tuners", undefined);
        }

        /*
         * Description:
         * Get or set the overall system volume. Valid values for this property are in the range 0 - 100. The OITF SHALL store this setting persistently.
         *
         * Type: Integer
         */
        get volume() {
            let result = this.__internal__.getField("volume", function() {
                return localStorage.getItem("oipf.configuration.localsystem.volume") || 50;
            });
            return result;
        }

        /*
         * Description:
         * Get or set the overall system volume. Valid values for this property are in the range 0 - 100. The OITF SHALL store this setting persistently.
         *
         * Type: Integer
         */
        set volume(volume) {
            this.__internal__.setField("volume", volume);
            localStorage.setItem("oipf.configuration.localsystem.volume", volume);
        }

        /*
         * Description:
         * Get or set the mute status of the default audio output(s). A value of true indicates that the default output(s) are currently muted.
         *
         * Type: Boolean
         */
        get mute() {
            return this.__internal__.getField("mute", function() {
                return localStorage.getItem("oipf.configuration.localsystem.mute") === "true";
            });
        }

        /*
         * Description:
         * Get or set the mute status of the default audio output(s). A value of true indicates that the default output(s) are currently muted.
         *
         * Type: Boolean
         */
        set mute(mute) {
            this.__internal__.setField("mute", mute);
            localStorage.setItem("oipf.configuration.localsystem.mute", mute);
        }

        /*
         * Description:
         * A collection of AVOutput objects representing the audio and video outputs of the platform. Applications
         * MAY use these objects to configure and control the available outputs.
         *
         * Visibility Type: readonly AVOutputCollection
         */
        get outputs() {
            return this.__internal__.getField("outputs", function() {
                let configOutput = ctx.config.default.outputs || [];
                let outputs = new AVOutputCollection();
                let index = 0;
                for (let o of configOutput) {
                    outputs.push(new AVOutput(index++, o));
                }
                return outputs;
            });
        }

        /*
         * Description:
         * A collection of NetworkInterface objects representing
         * the available network interfaces.
         *
         * Visibility Type: readonly NetworkInterfaceCollection
         */
        get networkInterfaces() {
            return this.__internal__.getField("networkInterfaces", function() {
                let configInterfaces = ctx.config.default.networkInterfaces || [];
                let result = new NetworkInterfaceCollection();
                let index = 0;
                for (let i of configInterfaces) {
                    result.push(new NetworkInterface(index++, i));
                }
                return result;
            });
        }

        /*
         * Description:
         * Read whether the display is currently in a 2D or 3D mode.
         * Return values are:
         * ------------------------------------------------------------------------
         * Value        |              Description
         * ------------- ----------------------------------------------------------
         * 1            |   Indicates platform support for the NTSC TV standard.
         * ------------- ----------------------------------------------------------
         * 2            |   Indicates platform support for the PAL-BGH TV standard.
         * ------------- ----------------------------------------------------------
         * 4            |   Indicates platform support for the SECAM TV standard.
         * ------------- ----------------------------------------------------------
         * 8            |   Indicates platform support for the PAL-M TV standard.
         * ------------- ----------------------------------------------------------
         * 16           |   Indicates platform support for the PAL-N TV standard.
         * ------------------------------------------------------------------------
         * Values are stored as a bitfield.
         *
         * Visibility Type: Integer
         */
        get tvStandardsSupported() {
            return this.__internal__.getField("tvStandardsSupported", 0);
        }

        /*
         * Description:
         * Get the TV standard for which the analogue video outputs are currently
         * configured. This property can take one or more of the following values:
         * ------------------------------------------------------------------------
         * Value        |              Description
         * ------------- ----------------------------------------------------------
         * 0            |   Indicates there are no analogue video outputs
         * ------------- ----------------------------------------------------------
         * 1            |   Indicates platform support for the NTSC TV standard.
         * ------------- ----------------------------------------------------------
         * 2            |   Indicates platform support for the PAL-BGH TV standard.
         * ------------- ----------------------------------------------------------
         * 4            |   Indicates platform support for the SECAM TV standard.
         * ------------- ----------------------------------------------------------
         * 8            |   Indicates platform support for the PAL-M TV standard.
         * ------------- ----------------------------------------------------------
         * 16           |   Indicates platform support for the PAL-N TV standard.
         * ------------------------------------------------------------------------
         *
         * Visibility Type: Integer
         */
        get tvStandard() {
            return this.__internal__.getField("tvStandard", 0);
        }

        /*
         * Description:
         * Flag indicating the type of PVR support used by the application.
         * This property may take zero or more of the following values:
         * ------------------------------------------------------------------------
         * Value        |              Description
         * ------------- ----------------------------------------------------------
         *              | PVR functionality is not supported. This is the default
         * 0            | value if <recording> as specified in section 9.3.3
         *              | has value false .
         * ------------- ----------------------------------------------------------
         *              | PVR functionality is supported in the OITF. This is the default
         * 1            | value if <recording> as specified in section 9.3.3 has value
         *              |
         * ------------- ----------------------------------------------------------
         *
         * Visibility Type: readonly Integer
         */
        get pvrSupport() {
            return this.__internal__.getField("pvrSupport", 1);
        }

        /*
         * Description:
         * Indicates any information used at startup time of the OITF.
         *
         * Visibility Type: readonly StartupInformation
         */
        get startupInformation() {
            return this.__internal__.getField("startupInformation", undefined);
        }

        /*
         * Description:
         * Set the resolution of the graphics plane. If the specified resolution
         * is not supported by the OITF, this method SHALL return false.
         * Otherwise, this method SHALL return true.
         *
         * Arguments:
         * - width: The width of the display, in pixels.
         *
         * - height: The height of the display, in pixels.
         *
         * Return: Boolean
         */
        setScreenSize(/*width, height*/) {

        }

        /*
         * Description:
         * Set the TV standard to be used on the analogue video outputs.
         * Returns false if the requested mode cannot be set.
         *
         * Arguments:
         * - tvStandard: The TV standard to be set.
         * Valid values are defined in the description of the
         * tvStandard property in section 7.3.3.2.
         *
         * Return: Boolean
         * FIXME: Yannis - 23/04/2015 - Take intp account that the value of
         * "tvStandard" can be stored as a bitfield.
         */
        setTVStandard(/*tvStandard*/) {

        }

        /*
         * Description:
         * Set the type of PVR support used by the application.
         * The types of PVR supported by the receiver MAY not be supported
         * by the application; in this case, the return value indicates
         * the pvr support that has been set.
         *
         * Arguments:
         * - state: The type of PVR support desired by the application.
         * More than one type of PVR functionality MAY be specified,
         * allowing the receiver to automatically select the
         * appropriate mechanism. Valid values are:
         *
         * - height: The height of the display, in pixels.
         *
         * Return: Boolean
         */
        setPvrSupport(/*state*/) {

        }

        /*
         * Description:
         * The setPowerState() method allows the DAE application to
         * modify the OITF state. The power state change may be restricted
         * for some values of type, for example OFF, PASSIVE_STANDBY,
         * RESTART and FACTORY_RESET . A call to setPowerState() with a
         * restricted value of type SHALL return false .
         *
         * Arguments:
         * - type: The type values that may be specified are defined
         * in section 7.3.3.1
         *
         * Return: Boolean
         */
        setPowerState(/*type*/) {

        }

        /*
         * Description:
         * Set the credentials for the specified protocol to use for digest a
         * uthentication negotiation for all subsequent requests to the specified
         * domain. The credentials are persistently stored overwriting any previous
         * set credentials. If domain is null the provided credentials SHALL apply
         * for all domains. Returns true if credentials are successfully set,
         * false otherwise.
         * If digest authentication is not supported for
         * the specified protocol then return false . The valid values are
         * the strings “ http ” and “ https ”.
         * Setting of Digest Credentials on the same protocol and domain
         * SHALL update the username and password.
         * If the credentials, when used, are incorrect then the behaviour
         * SHALL be the same as any other time that stored credentials
         * are incorrect, e.g. saved values from a user prompt.
         * The credentials SHALL be used (if necessary) in all requests
         * made by DAE applications. The credentials MAY be used in requests
         * made by other components such as media players, DLNA clients, etc.
         *
         * Arguments:
         * - protocol: The protocol to apply the credentials.
         *
         * - domain: The domain to which the credentials apply.
         *
         * - username: The username to be used in the digest authentication.
         *
         * - password: The password to be used in the digest authentication.
         *
         * Return: Boolean
         */
        setDigestCredentials(/*protocol, domain, username, password*/) {

        }

        /*
         * Description:
         * Clear any previously set digest credentials for the specified domain.
         * If domain is null all set credentials are cleared.
         * Returns true if the digest credentials for the given protocol and
         * domain were cleared or do not exist,
         * or false if credentials failed to be cleared.
         *
         * Arguments:
         * - protocol: The protocol to apply the credentials. The value should
         * be the same as one of those specified for
         * the setDigestCredentials() method.
         *
         * - domain: The domain to which the credentials apply.

         *
         * Return: Boolean
         */
        clearDigestCredentials(/*protocol, domain, username, password*/) {

        }

        /*
         * Description:
         * Check if digest credentials are currently defined
         * for the specified protocol and domain.
         * Returns true if credentials have been set by a previous call to
         * setDigestCredentials() , otherwise returns false .
         *
         * Arguments:
         * - protocol: The protocol to apply the credentials.
         * The value should be the same as one of those specified for
         * the setDigestCredentials() method.
         *
         * - domain: The domain to which the credentials apply.
         *
         * Return: Boolean
         */
        hasDigestCredentials(/*protocol, domain, username, password*/) {

        }

        /*
         * The function that is called when the power state has changed. The specified function
         * is called with the argument powerState:
         * • Integer powerState – the new power state.
         */
        get onPowerStateChange() {
            return this.__internal__.eventEmitter.getOneListener("PowerStateChange");
        }

        set onPowerStateChange(callback) {
            this.__internal__.eventEmitter.addOneListener("PowerStateChange", callback);
        }

    };
};
