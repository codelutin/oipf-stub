"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * The OITF SHALL implement the “ application/oipfConfiguration ” object as defined below. This object
 * provides an interface to the configuration and user settings facilities within the OITF.
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let Configuration = require("./Configuration");
    let LocalSystem = require("./LocalSystem");

    return class ConfigurationObject {

        constructor() {
            ctx.__internal__.init(this);
        }

        /*
         * Description:
         * Accesses the configuration object that sets defaults and shows system settings.
         *
         * Visibility Type: readonly Configuration
         */
        get configuration() {
            if (!this.__internal__.configuration) {
                this.__internal__.configuration = new Configuration();
            }
            return this.__internal__.configuration;
        }

        /*
         * Accesses the object representing the platform hardware.
         *
         * Visibility Type: readonly LocalSystem
         */
        get localSystem() {
            if (!this.__internal__.localSystem) {
                this.__internal__.localSystem = new LocalSystem();
            }
            return this.__internal__.localSystem;
        }

        /*
         * The function that is called when the IP address of a network interface has changed. The specified function
         * is called with two arguments (NetworkInterface)item and (String)ipAddress. The ipAddress may have the
         * value undefined if a previously assigned address has been lost.
         */
        set onIpAddressChange(callback) {
            this.__internal__.eventEmitter.addOneListener("IpAddressChange", callback);
        }

    };
};
