"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.16.5.2 The AVComponent class
 *
 * AVComponent represents a component within a complete media stream - a single stream of video, audio or data that can
 * be played or manipulated. This is not necessary for basic playback, record or EPG services. However, it provides a
 * mechanism to get at extended streams for enhanced services.
 *
 * For forward compatibility the DAE application SHALL check the value of the type property to ensure that it is
 * accessing an AVComponent object of the correct type.
 */
module.exports = function(ctx) {

    return class AVOutput {

        constructor(index, field) {
            ctx.__internal__.init(this, [
                "3DModeChange"
            ]);
            this.__internal__.index = index;
            this.__internal__.setValues(field);
        }

        /**
         * readonly String name
         * The name of the output. Each output SHALL have a name that is unique on the local system. At least one
         * of the outputs SHALL have the name " all " and SHALL represent all available outputs on the platform. The
         * results of reading properties from the " all " AVOutput are implementation specific.
         */
        get name() {
            return this.__internal__.getField("name", undefined);
        }

        /**
         * readonly String type
         * The type of the output. Valid values are “ audio ”, “ video ”, or “ both ”.
         */
        get type() {
            return this.__internal__.getField("type", undefined);
        }

        /**
         * Boolean enabled
         * Flag indicating whether the output is enabled. Setting this property SHALL enable or disable the output.
         */
        get enabled() {
            return this.__internal__.getField("enabled", function() {
                return localStorage.getItem("oipf.configuration.localsystem.output." + this.__internal__.index + ".enabled") === "true";
            }.bind(this));
        }

        set enabled(value) {
            this.__internal__.setField("enabled", value);
            localStorage.setItem("oipf.configuration.localsystem.output." + this.__internal__.index + ".enabled", value);
        }

        /**
         * Boolean subtitleEnabled
         * Flag indicating whether the subtitles are enabled. The language of the displayed subtitles is determined by
         * a combination of the value of the Configuration.preferredSubtitleLanguage property (see section
         * 7.3.2) and the subtitles available in the stream. For audio outputs, setting this property will have no effect.
         */
        get subtitleEnabled() {
            return this.__internal__.getField("subtitleEnabled", undefined);
        }

        set subtitleEnabled(value) {
            this.__internal__.setField("subtitleEnabled", value);
        }

        /**
         * String videoMode
         * Read or set the video format conversion mode, for which hardware support MAY be available on the
         * device. Valid values are:
         * normal
         * stretch
         * zoom
         */
        get videoMode() {
            return this.__internal__.getField("videoMode", undefined);
        }

        set videoMode(value) {
            this.__internal__.setField("videoMode", value);
        }

        /**
         * String digitalAudioMode
         * Read or set the output mode for digital audio outputs for which hardware support MAY be available on the
         * device. Valid values are shown below.
         * Value         Behaviour
         * ac3           Output AC-3 audio.
         * uncompressed  Output uncompressed PCM audio.
         * For video-only outputs, setting this property SHALL have no effect.
         */
        get digitalAudioMode() {
            return this.__internal__.getField("digitalAudioMode", undefined);
        }

        set digitalAudioMode(value) {
            this.__internal__.setField("digitalAudioMode", value);
        }

        /**
         * String audioRange
         * Read or set the range for digital audio outputs for which hardware support MAY be available on the device.
         * Valid values are shown below
         * Value     Behaviour
         * normal    Use the normal audio range.
         * narrow    Use a narrow audio range.
         * wide      Use a wide audio range.
         * For video-only outputs, setting this property SHALL have no effect.
         */
        get audioRange() {
            return this.__internal__.getField("audioRange", undefined);
        }

        set audioRange(value) {
            this.__internal__.setField("audioRange", value);
        }

        /**
         * String hdVideoFormat
         * Read or set the video format for HD and 3D video outputs for which hardware support MAY be available on
         * the device. Valid values are:
         * 480i
         * 480p
         * 576i
         * 576p
         * 720i
         * 720p
         * 1080i
         * 1080p
         * 720p_TaB
         * 720p_SbS
         * 1080i_SbS
         * 1080p_TaB
         * 1080p_SbS
         * For audio-only or standard-definition outputs, setting this property SHALL have no effect.
         */
        get hdVideoFormat() {
            return this.__internal__.getField("hdVideoFormat", undefined);
        }

        set hdVideoFormat(value) {
            this.__internal__.setField("hdVideoFormat", value);
        }

        /**
         * String tvAspectRatio
         * Indicates the output display aspect ratio of the display device connected to this output for which hardware
         * support MAY be available on the device. Valid values are:
         * 4:3
         * 16:9
         * For audio-only outputs, setting this property SHALL have no effect.
         */
        get tvAspectRatio() {
            return this.__internal__.getField("tvAspectRatio", undefined);
        }

        set tvAspectRatio(value) {
            this.__internal__.setField("tvAspectRatio", value);
        }

        /**
         * readonly StringCollection supportedVideoModes
         * Read the video format conversion modes that may be used when displaying a 4:3 input video on a 16:9
         * output display or 16:9 input video on a 4:3 output display. The assumption is that the hardware supports
         * conversion from either format and there is no distinction between the two. See the definition of the
         * videoMode property for valid values.
         * For audio outputs, this property will have the value null .
         */
        get supportedVideoModes() {
            return this.__internal__.getField("supportedVideoModes", undefined);
        }

        /**
         * readonly StringCollection supportedDigitalAudioModes
         * Read the supported output modes for digital audio outputs. See the definition of the digitalAudioMode
         * property for valid values.
         * For video outputs, this property will have the value null .
         */
        get supportedDigitalAudioModes() {
            return this.__internal__.getField("supportedDigitalAudioModes", undefined);
        }

        /**
         * readonly StringCollection supportedAudioRanges
         * Read the supported ranges for digital audio outputs. See the definition of the audioRange property for
         * valid values.
         * For video outputs, this property will have the value null .
         */
        get supportedAudioRanges() {
            return this.__internal__.getField("supportedAudioRanges", undefined);
        }

        /**
         * readonly StringCollection supportedHdVideoFormats
         * Read the supported HD and 3D video formats. See the definition of the hdVideoFormat property for valid
         * values.
         * For audio outputs, this property will have the value null .
         */
        get supportedHdVideoFormats() {
            return this.__internal__.getField("supportedHdVideoFormats", undefined);
        }

        /**
         * readonly StringCollection supportedAspectRatios
         * Read the supported TV aspect ratios. See the definition of the tvAspectRatio property for valid values.
         * For audio outputs, this property will have the value null .
         */
        get supportedAspectRatios() {
            return this.__internal__.getField("supportedAspectRatios", undefined);
        }

        /**
         * readonly Integer current3DMode
         * Read whether the display is currently in a 2D or 3D mode. Return values are:
         * Value
         * Description
         * 0 The display is in a 2D video mode
         * 1 The display is in a 3D video mode
         */
        get current3DMode() {
            return this.__internal__.getField("current3DMode", undefined);
        }

        /**
         * This function is the DOM 0 event handler for events relating to actions carried out on an item in a content
         * catalogue. The specified function is called with the following arguments:
         * • Integer action – The type of action that the event refers to. Valid values are:
         * Value  Description
         * 0      The display changed from a 3D to a 2D video mode
         * 1      The display changed from a 2D to a 3D video mode
         */
        get on3DModeChange() {
            return this.__internal__.eventEmitter.getOneListener("3DModeChange");
        }

        set on3DModeChange(callback) {
            this.__internal__.eventEmitter.addOneListener("3DModeChange", callback);
        }
    };
};
