"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;


/**
 * 7.16.5.5   The AVSubtitleComponent class
 *
 * The AVSubtitleComponent class implements the AVComponent interface.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let AVComponent = require("./AVComponent");

    return class AVSubtitleComponent extends AVComponent {

        constructor(fields) {
            super(fields);
        }

        /**
         * readonly String language
         *
         * An ISO 639-2 language code representing the language of the stream, as defined in [ISO 639-2].
         */
        get language() {
            return this.__internal__.getField("language", "");
        }

        /**
         * readonly Boolean hearingImpaired
         *
         * Has value true if the stream is intended for the hearing-impaired (e.g. contains a written description of the
         * sound effects), false otherwise.
         */
        get hearingImpaired() {
            return this.__internal__.getField("hearingImpaired", false);
        }

    };
};
