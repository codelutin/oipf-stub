"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The NetworkInterface class represents a physical or logical network interface in the receiver.
 */
module.exports = function(ctx) {

    return class NetworkInterface {

        constructor(index, values, events) {
            ctx.__internal__.init(this, events);

            this.__internal__.index = index;
            this.__internal__.setValues(values);
        }

        /**
         * readonly String ipAddress
         * The IP address of the network interface, in dotted-quad notation for IPv4 or colon-hexadecimal notation for
         * IPv6. This property SHALL take the value undefined if no IP address has been assigned. The IP address
         * may be link local, private or global, depending on which address block it belongs to, as reserved by IANA.
         */
        get ipAddress() {
            return this.__internal__.getField("ipAddress", undefined);
        }

        /**
         * readonly String macAddress
         * The colon-separated MAC address of the network interface.
         */
        get macAddress() {
            return this.__internal__.getField("macAddress", undefined);
        }

        /**
         * readonly Boolean connected
         * Flag indicating whether the network interface is currently connected.
         */
        get connected() {
            return this.__internal__.getField("connected", undefined);
        }

        /**
         * Boolean enabled
         * Flag indicating whether the network interface is enabled. Setting this property SHALL enable or disable
         * the network interface.
         */
        get enabled() {
            return this.__internal__.getField("enabled", undefined);
        }

        set enabled(value) {
            this.__internal__.setField("enabled", value);
        }
    };
};
