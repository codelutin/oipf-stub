"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * The DiscInfo class provides details of the storage usage and capacity in the OITF.
 * A DiscInfo instance obtained from the oipfDownloadManager provides reports relating to downloads. A DiscInfo
 * instance obtained from oipfRecordingScheduler provides reports relating to recordings. If recordings and
 * downloads use the same pool of storage space (e.g. disc partition), DiscInfo instances obtained via either route would
 * have the same values for the properties. If recordings and downloads use different pools of storage space (e.g. different
 * disc partitions) the DiscInfo instances obtained by each route would report the correct values for the route in which
 * they were obtained.
*/
module.exports = function(ctx) {

    return class DiscInfo {

        constructor(values) {
            ctx.__internal__.init(this);

            this.__internal__.setValues(values);

            // compute free
            let free = this.total - this.reserved;
            this.__internal__.setField("free", free);
        }

        /**
         * readonly Integer free
         * The space (in megabytes) available on the storage device.
         */
        get free() {
            return this.__internal__.getField("free", undefined);
        }

        /**
         * readonly Integer total
         * The total capacity (in megabytes) of the storage device. Depending upon the system, free MAY be less than
         * total as some of the disc space MAY be used for management purposes.
         */
        get total() {
            return this.__internal__.getField("total", undefined);
        }

        /**
         * readonly Integer reserved
         * The space (in megabytes) reserved.
         */
        get reserved() {
            return this.__internal__.getField("reserved", undefined);
        }

        /**
         * Called by recordingScheduler to reduce remaining storage size.
         *
         * @param {integer} size (in mb)
         */
        _reduceSize(size) {
            let remaining = this.free - size;
            this.__internal__.setField("free", remaining);
        }

        /**
         * Called by recordingScheduler to increase size when a recording is deleted.
         *
         * @param {integer} size (in mb)
         */
        _increaseSize(size) {
            let remaining = this.free + size;
            this.__internal__.setField("free", remaining);
        }
    };
};
