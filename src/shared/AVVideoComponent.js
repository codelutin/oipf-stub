"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;


/**
 * 7.16.5.3   The AVVideoComponent class
 *
 * The AVVideoComponent class implements the AVComponent interface.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let AVComponent = require("./AVComponent");

    return class AVVideoComponent extends AVComponent {

        constructor(fields) {
            super(fields);
        }

        /**
         * readonly Number aspectRatio
         *
         * Indicates the aspect ratio of the video or undefined if the aspect ratio is not known. Values SHALL be
         * equal to width divided by height, rounded to a float value with two decimals, e.g. 1.78 to indicate 16:9 and
         * 1.33 to indicate 4:3.
         */
        get aspectRatio() {
            return this.__internal__.getField("aspectRatio", 1.78);
        }

    };
};
