"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.16.5.2 The AVComponent class
 *
 * AVComponent represents a component within a complete media stream - a single stream of video, audio or data that can
 * be played or manipulated. This is not necessary for basic playback, record or EPG services. However, it provides a
 * mechanism to get at extended streams for enhanced services.
 *
 * For forward compatibility the DAE application SHALL check the value of the type property to ensure that it is
 * accessing an AVComponent object of the correct type.
 */
module.exports = function(ctx) {

    return class AVComponent {

        constructor(/*fields*/) {
            ctx.__internal__.init(this);
        }

        /**
         * readonly Integer componentTag
         *
         * The component tag identifies a component. The component tag identifier corresponds to the component_tag
         * in the component descriptor in the ES loop of the stream in the PMT [EN 300 468], or undefined if the
         * component is not carried in an MPEG-2 TS.
         */
        get componentTag() {
            return this.__internal__.getField("componentTag", undefined);
        }

        /**
         * readonly Integer pid
         *
         * The MPEG Program ID (PID) of the component in the MPEG2-TS in which it is carried, or undefined if the
         * component is not carried in an MPEG-2 TS.
         */
        get pid() {
            return this.__internal__.getField("pid", undefined);
        }

        /**
         * readonly Integer type
         *
         * Type of the component stream. Valid values for this field are given by the constants listed in section
         * 7.16.5.1.1.
         */
        get type() {
            return this.__internal__.getField("type", 0);
        }

        /**
         * readonly String encoding
         *
         * The encoding of the stream. The value of video format or audio format defined in section 3 of
         * [OIPF_MEDIA2] SHALL be used. For subtitle components, the following values are used (all according to
         * section 6 of [OIPF_MEDIA2]):
         *     Value         Description
         *    DVB-SUBT       DVB subtitles
         *    EBU-SUBT       EBU Teletext based subtitles
         *    CEA-SUBT       CEA-708C Closed Captions
         *    3GPP-TT        3GPP Timed Text
         */
        get encoding() {
            return this.__internal__.getField("encoding", "DVB-SUBT");
        }

        /**
         * readonly Boolean encrypted
         *
         * Flag indicating whether the component is encrypted or not.
         */
        get encrypted() {
            return this.__internal__.getField("encrypted", false);
        }

    };
};
