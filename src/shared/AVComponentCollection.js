"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;


/**
 * 7.16.5.6 The AVComponentCollection class
 *
 *          typedef Collection<AVComponent> AVComponentCollection
 *
 * An AVComponentCollection represents a collection of AVComponent objects. See Annex K for the definition of
 * the collection template.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let Collection = require("./Collection.js");

    return class AVComponentCollection extends Collection {

    };
};
