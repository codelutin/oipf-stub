"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let requireJS = require;

/**
 * 7.16.5.4   The AVAudioComponent class
 *
 * The AVAudioComponent class implements the AVComponent interface.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);
    let AVComponent = require("./AVComponent");

    return class AVAudioComponent extends AVComponent {

        constructor(fields) {
            super(fields);
        }

        /**
         * readonly String language
         *
         * An ISO 639-2 language code representing the language of the stream, as defined in [ISO 639-2].
         */
        get language() {
            return this.__internal__.getField("language", "");
        }

        /**
         * readonly Boolean audioDescription
         *
         * Has value true if the stream contains an audio description intended for people with a visual impairment, false
         * otherwise.
         */
        get audioDescription() {
            return this.__internal__.getField("audioDescription", false);
        }

        /**
         * readonly Integer audioChannels
         *
         * Indicates the number of channels present in this stream (e.g. 2 for stereo, 5 for 5.1, 7 for 7.1).
         */
        get audioChannels() {
            return this.__internal__.getField("audioChannels", 2);
        }
    };
};
