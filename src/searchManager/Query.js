"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.12.3 The Query class
 *
 * The Query class represents a metadata query that the user wants to carry
 * out. This may be a simple search, or a complex search involving Boolean
 * logic. Queries are immutable; an operation on a query SHALL return a new
 * Query object, allowing applications to continue referring to the original
 * query.
 *
 * The examples below show how more complex queries can be constructed:
 *
 *     Query qa = mySearch.createQuery("Title", 6, "Terminator");
 *     Query qb = mySearch.createQuery("SpokenLanguage", 0, "fr-CA");
 *     Query qc = qb.and(qa.not());
 */
module.exports = function(ctx) {

    return class Query {

        constructor(field, comparison, value) {
            ctx.__internal__.init(this);
            if (field) {
                this.__internal__.constraints = {
                    type: "constraints",
                    field: field,
                    comparison: comparison,
                    value: value
                };
            }
        }

        /**
         * Description:
         * Create a query based on the logical AND of the predicates represented by
         * the query currently being operated on and the argument query.
         *
         * Arguments:
         * - query [type: Query]
         *   The second predicate for the AND operation.
         *
         * Return:
         * - Query
         */
        and(query) {
            let result = new this.constructor();
            result.__internal__.constraints = {
                type: "and",
                query1: this,
                query2: query
            };
            return result;
        }

        /**
         * Description:
         * Create a query based on the logical OR of the predicates represented by
         * the query currently being operated on and the argument query.
         *
         * Arguments:
         * - query [type: Query]
         *   The second predicate for the OR operation.
         *
         * Return:
         * - Query
         */
        or(query) {
            let result = new this.constructor();
            result.__internal__.constraints = {
                type: "or",
                query1: this,
                query2: query
            };
            return result;
        }

        /**
         * Description:
         * Create a query that is the logical negation of the predicates represented
         * by the query currently being operated on.
         *
         * Return:
         * - Query
         */
        not() {
            let result = new this.constructor();
            result.__internal__.constraints = {
                type: "not",
                query: this
            };
            return result;
        }

        _eval(target) {
            let result;
            let c = this.__internal__.constraints;

            if (c.type === "not") {
                result = !c.query._eval(target);
            } else if (this.__internal__.constraints.type === "or") {
                result = c.query1._eval(target) || c.query2._eval(target);
            } else if (this.__internal__.constraints.type === "and") {
                result = c.query1._eval(target) && c.query2._eval(target);
            } else if (this.__internal__.constraints.type === "constraints") {
                result = target._checkCond(c.field, c.comparison, c.value);
            } else {
                // unknown type
                throw "Query type unknown: '" + c.type + "'";
            }
            return result;
        }

    };
};
