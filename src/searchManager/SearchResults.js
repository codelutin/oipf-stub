"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.12.4 The SearchResults class
 *
 * The SearchResults class represents the results of a metadata search. Since
 * the result set may contain a large number of items, applications request a
 * `window' on to the result set, similar to the functionality provided by the
 * OFFSET and LIMIT clauses in SQL.
 *
 * Applications MAY request the contents of the result in groups of an
 * arbitrary size, based on an offset from the beginning of the result set.
 * The data SHALL be fetched from the appropriate source, and the application
 * SHALL be notified when the data is available.
 *
 * The set of results SHALL only be valid if a call to getResults()has been
 * made and a MetadataSearch event notifying the application that results are
 * available has been dispatched. If this event has not been dispatched, the
 * set of results SHALL be empty (i.e. the value of the totalSize property
 * SHALL be 0 and calls to item() SHALL return undefined).
 *
 * In addition to the properties and methods defined below a SearchResults
 * object SHALL support the array notation to access the results in this
 * collection.
 */
let requireJS = require;
let Utils = require("../OipfStubUtils");

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Collection = require("../shared/Collection");

    return class SearchResults extends Collection {

        constructor(metadataSearch) {
            super();
            ctx.__internal__.init(this);
            this.__internal__.metadataSearch = metadataSearch;
        }

        /**
         * The current offset into the total result set.
         *
         * Visibility Type: readonly Integer
         */
        get offset() {
            return this.__internal__.getField("offset", 0);
        }

        /**
         * The total number of items in the result set.
         *
         * The value of this property SHALL be zero until getResults() has been
         * called and a MetadataSearch event notifying the application that
         * results are available has been dispatched.
         *
         * Visibility Type: readonly Integer
         */
        get totalSize() {
            return this.__internal__.getField("totalSize", 0);
        }

        /**
         * Description:
         * Perform the search and retrieve the specified subset of the items that match
         * the query.
         *
         * Results SHALL be returned asynchronously. A MetadataSearch event with
         * state=0 SHALL be dispatched when results are available.
         *
         * This method SHALL always return false.
         *
         * Arguments
         * - offset [type: Integer]
         *   The number of items at the start of the result set to be skipped before
         *   data is retrieved.
         *
         * - count [type: Integer]
         *   The number of results to retrieve.
         *
         * Return:
         * - Boolean
         */
        getResults(offset, count) {
            this.__internal__.abort = false;
            this._computeResult(offset, count).then(this._onResult.bind(this, offset, count));
        }

        /**
         *
         * Description:
         * Abort any outstanding request for results and remove any query,
         * constraints or ordering rules set on the MetadataSearch object that
         * is associated with this SearchResults object. Regardless of whether
         * or not there is an outstanding request for results, items currently
         * in the collection SHALL be removed (i.e. the value of the length
         * property SHALL be 0 and any calls to item()SHALL return undefined).
         * All cached search results SHALL be discarded.
         *
         * Return:
         * - undefined
         */
        abort() {
            this.__internal__.metadataSearch.constraints = {};
            this._abort();
        }

        _abort() {
            this.__internal__.abort = true;
            this.length = 0;
            this.__internal__.result = null;

            let eventEmitter = this.__internal__.metadataSearch.__internal__.searchManagerObject.__internal__.eventEmitter;
            eventEmitter.emit("MetadataSearch", this.__internal__.metadataSearch, 3);
        }

        _onResult(offset, count, result) {
            if (!this.__internal__.abort) {
                this.__internal__.setField("offset", offset);
                this.length = 0;
                this.push.apply(this, result.slice(offset, offset + count));

                let eventEmitter = this.__internal__.metadataSearch.__internal__.searchManagerObject.__internal__.eventEmitter;
                eventEmitter.emit("MetadataSearch", this.__internal__.metadataSearch, 0);
            }
        }

        _computeResult(offset, count) {
            if (this.__internal__.result && this.__internal__.result.length >= offset + count) {
                return Promise.resolve(this.__internal__.result);
            } else {
                let result = this.__internal__.result = [];
                let cons = this.__internal__.metadataSearch.__internal__.constraints;

                // common callback used by all methods
                let logErr = function(err) {
                    console.log("err", err);
                    return [];
                };
                let pushAllResults = function(progs) {
                    for (let p of progs) {
                        result.push(p);
                    }
                };
                let sort = this._sort.bind(this);
                let sortAllResults = function() {
                    cons.orderBy && cons.orderBy.length && sort(cons.orderBy, result);
                    if (cons.count) {
                        result.length = cons.count;
                    }

                    return result;
                };

                let resultPromise;
                if (ctx.dataProvider.useMetadataConstraint()) {
                    resultPromise = ctx.dataProvider.getProgrammesByConstraint(cons, offset, count)
                        .catch(logErr)
                        .then(pushAllResults)
                        .then(sortAllResults);

                } else {

                    // XXX: this part is too specific to XmlTv and can't be used with
                    // provider working with constraints
                    let channels;
                    let channelList = ctx.dataProvider.getChannelConfig().channelList;
                    if (cons.channels) {
                        channels = [];
                        for (let c of cons.channels) {
                            let realChannel = Utils.getChannel(channelList, c);
                            realChannel && channels.push(realChannel);
                        }
                    } else {
                        channels = channelList;
                    }

                    let promise = [];
                    let filter = function(progs) {
                        for (let p of progs) {
                            if (this._checkConstraints(cons, p)) {
                                result.push(p);
                            }
                        }
                    };

                    for (let channel of channels) {
                        let prom = ctx.dataProvider.getProgrammes(Utils.getChannelIndex(channelList, channel), channel);
                        prom.catch(logErr);
                        prom.then(filter.bind(this));
                        promise.push(prom);
                    }

                    resultPromise = Utils.waitAllPromise(promise).then(sortAllResults);
                }

                return resultPromise;
            }
        }

        _sort(orderBy, list) {
            // FIXME
            return list.sort(function(a, b) {
                let result = 0;
                let ob;
                let fieldName;
                let v1, v2;
                let i = 0;
                while (result === 0 && (ob = orderBy[i++])) {
                    fieldName = ob.field.replace(/[^.]+\./, ""); // remove 'Programme.' prefix
                    // some field (ex; _endTime) are only in __internal__
                    v1 = (a[fieldName] || a.__internal__.getField(fieldName)) + "";
                    v2 = (b[fieldName] || b.__internal__.getField(fieldName)) + "";
                    if (a.__internal__.fieldType[fieldName] === "number") {
                        v1 = parseFloat(v1);
                        v2 = parseFloat(v2);
                        result = v1 - v2;
                    } else {
                        if (v1 > v2) {
                            result = 1;
                        }
                        if (v1 < v2) {
                            result = -1;
                        }
                    }
                    if (!ob.ascending) {
                        result = -result;
                    }
                }
                return result;
            });
        }

        _checkConstraints(cons, prog) {
            let query = cons.query;
            //let rating = cons.rating;

            let result = true;
            if (query) {
                result = query._eval(prog);
            }

            // FIXME do rating

            return result;
        }

    };
};
