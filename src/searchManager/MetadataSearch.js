"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * 7.12.2 The MetadataSearch class
 *
 * A MetadataSearch object represents a query of the metadata about available
 * programmes. Applications can create MetadataSearch objects using the
 * createSearch() method on the application/oipfSearchManager object. When
 * metadata queries are performed on a remote server, the protocol used is
 * defined in section 4.1.2.2 of [OIPF_META2].
 *
 * Each search consists of three steps:
 * 1. Definition of the query. The application creates a MetadataSearch
 *    object, and either creates its associated Query object, or sets a query
 *    using the findProgrammesFromStream() method, and sets any applicable
 *    constraints and result ordering.
 *
 * 2. Acquisition of results. The OITF acquires some or all of the items that
 *    match the specified query and constraints, and caches the requested subset
 *    of the results. This is typically triggered by a call to getResults().
 *
 * 3. Retrieval. The application accesses the results via the SearchResults
 *   class. The MetadataSearch and SearchResults classes work together to
 *   manage an individual search. For every search, the MetadataSearch object
 *   and its corresponding SearchResults object SHALL be in one of three
 *   states as described in Table 7. Figure 13 below shows the transitions
 *   between these states:
 *
 *
 * Figure 13: State machine for a metadata search (informative)
 *
 *                MetadataSearch.  __
 *     findProgrammesFromStream() /   \+---------+
 *                                \--->|   Idle  |<----------------------------------------------+
 *                                     +---------+                                               |
 *                                       |     ^                                                 |
 *                                       |     | SearchResults.abort()                           |
 *                                       |     | OR                                              |
 *                                       |     | MetadataSearch.findProgrammesFromStream()       |
 *                                       |     | OR                                              |
 *                                       |     | changing the query, constraints or ordering     |
 *            SearchResults.getResults() |     | rules on the MetadataSearch object              |
 *                                       |     | OR                                              |
 *                                       |     | insufficient resources to retrieve all of       |
 *                                       |     | the requested results                           |
 *                                 __    v     |                                                 |
 *     SearchResults.getResults() /   \+---------+                                               |
 *                                \--->|Searching|                                               |
 *                                     +---------+                                               |
 *                   MetadataSearchEvent |     ^                                                 |
 *                       state=0         v     | SearchResults.getResults()                      |
 *                                     +---------+                                               |
 *                                     |  Found  |-----------------------------------------------+
 *                                     +---------+         SearchResults.abort()
 *                                                         OR MetadataSearch.findProgrammesFromStream()
 *                                                         OR changing the query, constraints or ordering rules
 *                                                         on the MetadataSearch object.
 *
 * Table 7: Metadata search states (normative)
 *
 * State      Description
 *
 * Idle       The search is idle; no results are available. This is the initial
 *            state of the search. In this state, the application can set or
 *            modify the query, constraints or ordering rules that are applied
 *            to the search.
 *
 *            No search results are available in this state ­ any calls to
 *            SearchResults.item() SHALL return undefined and the values of the
 *            length and totalSize properties on the SearchResults object SHALL
 *            return zero. Any search results that have been cached by the
 *            terminal SHALL be discarded when the Idle state is entered.
 *
 *            Calling the SearchResults.getResults() method SHALL cause a state
 *            transition to the Searching state.
 *
 * Searching  Results are being retrieved and are not yet available to
 *            applications.
 *
 *            If the terminal has not previously cached the full set of search
 *            results, the terminal performs the search to gather the requested
 *            results.
 *
 *            If a new version of the metadata is detected (e.g. due to an EIT
 *            update) while the search is in this state, results SHALL be
 *            retrieved from either the new or original version of the metadata
 *            but SHALL NOT be retrieved from a combination of the two versions.
 *
 *            Calls to SearchResults.item() SHALL return undefined.
 *
 *            Any modification of the search parameters (e.g. changing the
 *            query or adding/removing constraints, or calling
 *            findProgrammesFromStream()) by the application SHALL stop the
 *            current search and cause a transition to the Idle state. The
 *            terminal SHALL dispatch a MetadataSearch event with state=3.
 *
 *            When all requested results have been found, the terminal SHALL
 *            dispatch a MetadataSearch event with state=0 and a state
 *            transition to the Found state SHALL occur.
 *
 *            If the search cannot be completed due to a lack of resources or
 *            any other reason, the terminal SHALL dispatch a MetadataSearch
 *            event with state=4 and a state transition to the Idle state SHALL
 *            occur.
 *
 *            Calls to the SearchResults.getResults()method SHALL abort the
 *            retrieval of search results and attempt to retrieve the
 *            newly-requested set of results instead.
 *
 *            NOTE: Calling getResults() when in the searching state may be
 *            used to fetch a group of items starting at a different offset or
 *            with a different count.
 *
 * Found      Search results are available and can be retrieved by applications.
 *            The data exposed via the SearchResults.item() method is static
 *            and never changes as a result of any updates to the underlying
 *            metadata database until SearchResults.getResults() is next called.
 *
 *            If a new version of the metadata is detected (e.g. due to an EIT
 *            update), a MetadataUpdate event is dispatched with action=1.
 *            Subsequent calls to SearchResult.getResults() SHALL return
 *            results based on the updated metadata.
 *
 *            Calls to SearchResults.getResults() SHALL cause a state
 *            transition to the Searching state.
 *
 *            Any modification of the search parameters (e.g. changing the
 *            query or adding/removing constraints, or calling
 *            findProgrammesFromStream()) by the application SHALL cause the
 *            current set of results to be discarded and SHALL cause a
 *            transition to the Idle state. The terminal SHALL dispatch a
 *            MetadataSearch event with state=3.
 *
 * The findProgrammesFromStream() method acts as a shortcut for setting a query
 * and a set of constraints on the MetadataSearch object . Regardless of
 * whether the query and constraints are set explicitly by the application or
 * via findProgrammesFromStream(), results are retrieved using the getResults()
 * method.
 *
 * Changes to the search parameters (e.g. changing the query or adding/removing
 * constraints or modifying the search target, or calling
 * findProgrammesFromStream()) SHALL be applied when the getResults() method on
 * the corresponding SearchResults object is called. Due to the nature of
 * metadata queries, searches are asynchronous and events are used to notify
 * the application that results are available. MetadataSearch events SHALL be
 * targeted at the application/oipfSearchManager object.
 *
 * The present document is intentionally silent about the implementation of the
 * search mechanism and the algorithm for retrieving and caching search results
 * except where described in Table 7 above. When performing a search, the
 * receiver MAY gather all search results and cache them (or cache a set of
 * pointers into the full database), or gather only the subset of search
 * results determined by the getResults() parameters, or take an alternative
 * approach not described here.
 */
let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Query = require("./Query");
    let SearchResults = require("./SearchResults");

    return class MetadataSearch {

        constructor(searchManagerObject, searchTarget) {
            ctx.__internal__.init(this);
            this.__internal__.searchManagerObject = searchManagerObject;
            this.__internal__.searchTarget = searchTarget;
            this.__internal__.constraints = {};
        }

        /**
         * The target(s) of the search. Valid values are:
         *
         *    Value     Description
         *    1         Metadata relating to scheduled content SHALL be searched.
         *    2         Metadata relating to on-demand content SHALL be searched.
         *
         * These values SHALL be treated as a bitfield, allowing searches to be
         * carried out across multiple search targets.
         *
         * Visibility Type: readonly Integer
         */
        get searchTarget() {
            return this.__internal__.searchTarget;
        }

        /**
         * The subset of search results that has been requested by the application.
         *
         * Visibility Type: readonly SearchResults
         */
        get result() {
            if (!this.__internal__.result) {
                this.__internal__.result = new SearchResults(this);
            }
            return this.__internal__.result;
        }

        /**
         * Description:
         *
         * Set the query terms to be used for this search, discarding any
         * previously-set query terms.
         * Setting the search parameters using this method will implicitly remove
         * any existing constraints, ordering or queries created by prior calls to
         * methods on this object.
         *
         * Arguments:
         *
         * - query [type: Query]
         *   The query terms to be used
         *
         * Return:
         * - undefined
         */
        setQuery(query) {
            // change constraints must force result abort
            this._resultAbort();
            this.__internal__.constraints = {
                query: query
            };
        }

        /**
         * void
         * Description:
         * Constrain the search to only include results whose parental rating value
         * is below the specified threshold.
         *
         * Arguments:
         * - scheme [type: ParentalRatingScheme]
         *   The parental rating scheme upon which the constraint SHALL be based.
         *   If the value of this argument is null, any existing parental rating
         *   constraints SHALL be cleared.
         *
         * - threshold [type: Integer]
         *   The threshold above which results SHALL NOT be returned. If the value
         *   of this argument is null, any existing constraint for the specified
         *   parental rating scheme SHALL be cleared.
         *
         * Return:
         * - undefined
         */
        addRatingConstraint(scheme, threshold) {
            // change constraints must force result abort
            this._resultAbort();
            if (scheme && threshold) {
                let rc = this.__internal__.constraints.rating
                        = this.__internal__.constraints.rating || new Map();
                rc.set(scheme, threshold);
            } else if (scheme) {
                this.__internal__.constraints.rating.delete(scheme);
            } else {
                this.__internal__.constraints.rating = null;
            }
        }

        /**
         * Description:
         * Constrain the search to only include results whose parental rating value
         * is below the threshold currently set by the user.
         *
         * Return:
         * - undefined
         */
        addCurrentRatingConstraint() {
            // change constraints must force result abort
            this._resultAbort();
            // FIXME
        }

        /**
         * Description:
         *
         * Constrain the search to only include results from the specified channels.
         * If a channel constraint has already been set, subsequent calls to
         * addChannelConstraint()SHALL add the specified channels to the list of
         * channels from which results should be returned.
         *
         * For CoD searches, adding a channel constraint SHALL have no effect.
         *
         * Arguments:
         *
         * - channels [type: Channel or List]
         *   The channels from which results SHALL be returned. If the value of this
         *   argument is null, any existing channel constraint SHALL be removed.
         *
         * Return:
         * - undefined
         */
        addChannelConstraint(channelOrList) {
            // change constraints must force result abort
            this._resultAbort();
            if (channelOrList) {
                let cc = this.__internal__.constraints.channels
                        = this.__internal__.constraints.channels || [];
                if (Array.isArray(channelOrList)) {
                    cc.push.apply(cc, channelOrList);
                } else {
                    cc.push(channelOrList);
                }
            } else {
                this.__internal__.constraints.channels = null;
            }
        }

        /**
         * Description:
         * Set the order in which results SHOULD be returned in future. Any
         * existing search results SHALL not be re-ordered. Subsequent calls to
         * orderBy() will apply further levels of ordering within the order defined
         * by previous calls. For example:
         *
         *                   orderBy("ServiceName", true);
         *                   orderBy("PublishedStart", true);
         *
         * will cause results to be ordered by service name and then by start time
         * for results with the same channel number.
         *
         * Arguments:[type: String]
         * - field
         *   The name of the field by which results SHOULD be sorted. A value of
         *   null indicates that any currently-set order SHALL be cleared and the
         *   default sort order should be used.
         *
         * - ascending [type: Boolean]
         *   Flag indicating whether the results SHOULD be returned in ascending or
         *   descending order.
         *
         * Return:
         * - undefined
         */
        orderBy(field, ascending) {
            // change constraints must force result abort
            this._resultAbort();
            if (field) {
                let ob = this.__internal__.constraints.orderBy = this.__internal__.constraints.orderBy || [];
                ob.push({field: field, ascending: ascending});
            } else {
                this.__internal__.constraints.orderBy = null;
            }
        }

        /**
         * Description:
         *
         * Create a metadata query for a specific value in a specific field within
         * the metadata. Simple queries MAY be combined to create more complex
         * queries. Applications SHALL follow the JavaScript type conversion rules
         * to convert non-string values into their string representation, if
         * necessary.
         *
         * Arguments:
         *
         * - field [type: String]
         *   The name of the field to compare. Fields are identified using the
         *   format <classname>.<propertyname> where classname SHALL be one of
         *   "Programme", "CODAsset", "CODService" or "CODFolder" and
         *   <propertyname> SHALL be a valid property name on the corresponding
         *   class.
         *
         * - comparison [type: Integer]
         *   The type of comparison. One of:
         *   Value     Description
         *   0         True if the specified value is equal to the value of
         *             the specified field.
         *   1         True if the specified value is not equal to the value
         *             of the specified field.
         *   2         True if the value of the specified field is greater
         *             than the specified value.
         *   3         True if the value of the specified field is greater
         *             than or equal to the specified value.
         *   4         True if the value of the specified field is less than
         *             the specified value.
         *   5         True if the value of the specified field is less than
         *             or equal to the specified value.
         *   6         True if the string value of the specified field
         *             contains the specified value. This operation SHALL
         *             be case insensitive, and SHALL match parts of a
         *             word as well as whole words (e.g. a value of "term"
         *             will match a field value of "Terminator").
         *
         * - value [type: String]
         *   The value to check. Applications SHALL follow the JavaScript type
         *   conversion rules to convert non-string values into their string
         *   representation, if necessary
         *
         * Return:
         * - Query
         */
        createQuery(field, comparison, value) {
            return new Query(field, comparison, value);
        }

        /**
         * Description:
         *
         * Set a query and constraints for retrieving metadata for programmes from
         * a given channel and given start time from metadata contained in the
         * stream as defined in section 4.1.3 of [OIPF_META2]. Setting the search
         * parameters using this method will implicitly remove any existing
         * constraints, ordering or queries created by prior calls to methods on
         * this object.
         *
         * This method does not cause the search to be performed;
         * applications must call getResults() to retrieve the results.
         * Applications SHALL be notified of the progress of the search via
         * MetadataSearch events as described in section 7.12.1.2.
         *
         * Arguments:
         *
         * - channel [type: Channel]
         *   The channel for which programme information should be found.
         *
         * - startTime [type: Integer]
         *   The start of the time period for which results should be returned
         *   measured in  seconds since midnight (GMT) on 1/1/1970. The start time
         *   is inclusive; any  programmes starting at the start time, or which are
         *   showing at the start time,  will be included in the search results.
         *   If null, the search will start from the  current time.
         *
         * - count[type: Integer]
         *   Optional argument giving the maximum number of programmes for which
         *   information should be fetched. This places an upper bound on the
         *   number of results that will be present in the result set - for instance,
         *   specifying a value of 2 for this argument will result in at most two
         *   results being returned by calls to getResults() even if a call to
         *   getResults() requests more results.
         *
         *   If this argument is not specified, no restrictions are imposed on the
         *   number of results which may be returned by calls to getResults().
         *
         * Return:
         * - undefined
         */
        findProgrammesFromStream(channel, startTime, count) {
            // change constraints must force result abort
            this._resultAbort();
            let query = this.createQuery("Programme._endTime", 3/*>=*/, startTime || Date.now() / 1000);
            this.setQuery(query);
            this.addChannelConstraint(channel);
            this.__internal__.constraints.count = count;
        }

        _resultAbort() {
            this.__internal__.result && this.__internal__.result._abort();
        }

    };
};
