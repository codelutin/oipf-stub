/* eslint no-bitwise: 0 */
"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

/*
 * The Keyset object permits applications to define which key events they request
 * to receive. There are two means of defining this. Common key events are
 * represented by constants defined in this class which are combined in a
 * bit-wise mask to identify a set of key events. Less common key events are not
 * included in one of the defined constants and form part of an array.
 *
 * The supported key events indicated through the capability mechanism in
 * section 9.3 SHALL be the same as the maximum set of key events available to
 * the browser as indicated through this object.
 *
 * The default set of key events available to broadcast-related applications
 * shall be none. The default set of key events available to broadcast-independent
 * or service provider related applications which do not call Keyset.setValue()
 * SHALL be all those indicated by the constants in this class which are supported
 * by the OITF excluding those indicated by OTHER.
 */
module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let VKCodes = require("./VKCodes.js");

    return class Keyset {

        constructor() {
            ctx.__internal__.init(this);

            let codes = this.__internal__.codes = new VKCodes();

            this.__internal__.value = 0;

            this.__internal__.supportsPointer = false;
            this.__internal__.maximumValue = 0;
            this.__internal__.maximumOtherKeys = 0;

            let vkGroups = this.__internal__.vkGroups = new Map();
            vkGroups.set(0, []);
            vkGroups.set(this.RED, [codes.VK_RED]);
            vkGroups.set(this.GREEN, [codes.VK_GREEN]);
            vkGroups.set(this.YELLOW, [codes.VK_YELLOW]);
            vkGroups.set(this.BLUE, [codes.VK_BLUE]);
            vkGroups.set(this.NAVIGATION, [
                codes.VK_UP, codes.VK_DOWN, codes.VK_LEFT, codes.VK_BACK,
                codes.VK_RIGHT, codes.VK_ENTER, codes.VK_BACK_SPACE
            ]);
            vkGroups.set(this.VCR, [
                codes.VK_PLAY, codes.VK_PAUSE, codes.VK_STOP,
                codes.VK_NEXT, codes.VK_PREV, codes.VK_FAST_FWD,
                codes.VK_REWIND, codes.VK_PLAY_PAUSE
            ]);
            vkGroups.set(this.SCROLL, [codes.VK_PAGE_UP, codes.VK_PAGE_DOWN]);
            vkGroups.set(this.INFO, [codes.VK_INFO]);
            vkGroups.set(this.NUMERIC, [
                codes.VK_0, codes.VK_1, codes.VK_2, codes.VK_3,
                codes.VK_4, codes.VK_5, codes.VK_6, codes.VK_7,
                codes.VK_8, codes.VK_9
            ]);
            vkGroups.set(this.ALPHA, [
                codes.VK_A, codes.VK_B, codes.VK_C, codes.VK_D,
                codes.VK_E, codes.VK_F, codes.VK_G, codes.VK_H,
                codes.VK_I, codes.VK_J, codes.VK_K, codes.VK_L,
                codes.VK_M, codes.VK_N, codes.VK_O, codes.VK_P,
                codes.VK_Q, codes.VK_R, codes.VK_S, codes.VK_T,
                codes.VK_U, codes.VK_V, codes.VK_W, codes.VK_X,
                codes.VK_Y, codes.VK_Z
            ]);
            vkGroups.set(this.OTHER, []);

            this.__internal__.keyCodes = new Set();
        }

        /**
         * Used to identify the VK_RED key event.
         */
        get RED() {
            return 0x1;
        }

        /**
         * Used to identify the VK_GREEN key event.
         */
        get GREEN() {
            return 0x2;
        }

        /**
         * Used to identify the VK_YELLOW key event.
         */
        get YELLOW() {
            return 0x4;
        }

        /**
         * Used to identify the VK_BLUE key event.
         */
        get BLUE() {
            return 0x8;
        }

        /**
         * Used to identify the VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_ENTER and
         * VK_BACK key events.
         */
        get NAVIGATION() {
            return 0x10;
        }

        /**
         * Used to identify the VK_PLAY, VK_PAUSE, VK_STOP, VK_NEXT, VK_PREV,
         * VK_FAST_FWD, VK_REWIND, VK_PLAY_PAUSE key events.
         */
        get VCR() {
            return 0x20;
        }

        /**
         * Used to identify the VK_PAGE_UP and VK_PAGE_DOWN key events.
         */
        get SCROLL() {
            return 0x40;
        }

        /**
         * Used to identify the VK_INFO key event.
         */
        get INFO() {
            return 0x80;
        }

        /**
         * Used to identify the number events, 0 to 9.
         */
        get NUMERIC() {
            return 0x100;
        }

        /**
         * Used to identify all alphabetic events.
         */
        get ALPHA() {
            return 0x200;
        }

        /**
         * Used to indicate key events not included in one of the other
         * constants in this class.
         */
        get OTHER() {
            return 0x400;
        }

        /**
         * The value of the keyset which this DAE application will receive.
         * @return {Number}
         */
        get value() {
            return this.__internal__.value;
        }

        /**
         * If the OTHER bit in the value property is set then this indicates
         * those key events which are available to the browser which are not
         * included in one of the constants defined in this class, If the OTHER
         * bit in the value property is not set then this property is meaningless.
         * @return {Array}
         */
        get otherKeys() {
            return this.__internal__.otherKeys;
        }

        /**
         * In combination with maximumOtherKeys, this indicates the maximum set
         * of key events which are available to the browser. When a bit in this
         * maximumValue has value 0, the corresponding key events are never
         * available to the browser.
         * @return {Number}
         */
        get maximumValue() {
            return this.__internal__.maximumValue + this.__internal__.maximumOtherKeys;
        }

        /**
         * If the OTHER bit in the maximumValue property is set then, in
         * combination with maximumValue, this indicates the maximum set of key
         * events which are available to the browser. For key events which are
         * not included in one of the constants defined in this class, if they
         * are not listed in this array then they are never available to the
         * browser. If the OTHER bit in the value property is not set then
         * this property is meaningless.
         * @return {Number}
         */
        get maximumOtherKeys() {
            return this.__internal__.maximumOtherKeys;
        }

        /**
         * Applications that have been designed to handle Mouse Events can
         * express it by using this property.
         *
         * Applications SHALL set this property to true to indicate that they
         * support a pointer based interaction model, i.e. that they listen to
         * and handle Mouse Events as included in the Web Standards TV profile
         * [OIPF_DAE2_WEB]. They SHALL set it to false otherwise. If not set, an
         * OITF SHALL assume that the application does not support a pointer
         * based interaction model.
         *
         * Based on the value of this property, an OITF MAY decide to enable or
         * disable the rendering of a free moving cursor.
         *
         * Note: OITFs are not required to support a pointer based input device
         * even though they are recommended to do so. If pointer based input
         * devices are supported, this is expressed via the +POINTER UI Profile
         * fragment as described in section 9.2.
         * @return {Boolean}
         */
        get supportsPointer() {
            return this.__internal__.supportsPointer;
        }

        /**
         * Sets the value of the keyset which this DAE application requests to
         * receive. Where more than one DAE application is running, the events
         * delivered to the browser SHALL be the union of the events requested
         * by all running DAE applications. Under these circumstances, applications
         * may receive events which they have not requested to receive.
         *
         * The return value indicates which keys will be delivered to this DAE
         * application encoded as bit-wise mask of the constants defined in this
         * class.
         * @param {Number} value The value is a number which is a bit-wise mask
         *                       of the constants defined in this class. For example:
         *                       myKeyset = myApplication.privateData.keyset;
         *                       myKeyset.setValue(0x00000013);
         *                       myKeyset.setValue(myKeyset.INFO | myKeyset.NUMERIC);
         * @param {Array} otherKeys This parameter is optional. If the value
         *                          parameter has the OTHER bit set then it is
         *                          used to indicate the key events that the
         *                          application wishes to receive which are not
         *                          represented by constants defined in this class.
         */
        setValue(value, otherKeys) {
            this.__internal__.value = value;
            this.__internal__.otherKeys = otherKeys;

            let keyCodes = [];

            let vkGroups = this.__internal__.vkGroups;
            for (let key of vkGroups.keys()) {
                let vkGroup = vkGroups.get(value & key);
                keyCodes = keyCodes.concat(vkGroup);
            }

            this.__internal__.maximumValue = keyCodes.length;

            if (otherKeys) {
                this.__internal__.maximumOtherKeys = otherKeys.length;
            } else {
                this.__internal__.maximumOtherKeys = 0;
            }

            if (value & this.OTHER && otherKeys) {
                keyCodes = keyCodes.concat(otherKeys);
            }

            this.__internal__.keyCodes = new Set(keyCodes);
        }

        /**
         * Return the URI of the icon representing the physical key or other
         * mechanism that is used by the terminal to generate the key event for
         * the given keycode passed. It SHALL return null if the key has no icon
         * associated with it.
         * @param  {Number} code The VK_ constant for the key whose icon should
         *                       be returned.
         * @return {String} URI of the icon
         */
        getKeyIcon(/*code*/) {}

        /**
         * Return the textual label representing the physical key or other
         * mechanism that is used by the terminal to generate the key event for
         * the given keycode passed. It SHALL return null if the key has no
         * textual label associated with it.
         * @param  {Number} code The VK_ constant for the key whose textual
         *                       label should be returned.
         * @return {String} label
         */
        getKeyLabel(code) {
            return this.__internal__.codes.getKeyLabel(code);
        }

    };
};
