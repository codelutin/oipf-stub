"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let EventFactory = require("./EventFactory");

    return class KeyHandlingStrategy {

        constructor(appMgr) {
            ctx.__internal__.init(this);

            this.__internal__.appMgr = appMgr;
        }

        testKeyset(event, application) {
            return application.privateData.keyset.__internal__.keyCodes.has(event.keyCode);
        }

        _checkAppReload(event) {
            return event.type === "keydown" && (event.metaKey || event.crtKey) && event.keyIdentifier === "U+0052";
        }

        _letItPass(event) {
            return this._checkAppReload(event);
        }

        dispatchEvent(event) {

            if (this._letItPass(event)) {
                return true;
            }

            let application = event.applications.shift();
            if (application) {

                if (this.testKeyset(event, application)) {

                    application.__internal__.eventEmitter.emit(event.type
                    .replace("keypress", "KeyPress")
                    .replace("keydown", "KeyDown")
                    .replace("keyup", "KeyUp"), event);

                    application.window.document.dispatchEvent(event);

                } else {
                    this.dispatchEvent(event);
                }
            }
        }

        keyEventCapture(e) {
            if (this._letItPass(e)) {
                return false;
            }
            if (!e.forwarded) {
                let forwardEvent = EventFactory.createKeyEvent(e);

                let appMgr = this.__internal__.appMgr;
                let activeApplications = appMgr.__internal__.activeApplications;

                forwardEvent.applications = activeApplications.slice(0);
                this.dispatchEvent(forwardEvent);

                e.stopPropagation();
                e.preventDefault();
            }
        }

        keyEventBubble(e) {
            if (this._letItPass(e)) {
                return false;
            }
            if (e.forwarded) {
                let forwardEvent = EventFactory.createKeyEvent(e);
                forwardEvent.applications = e.applications;
                this.dispatchEvent(forwardEvent);

                e.stopPropagation();
                e.preventDefault();
            }
        }

    };
};
