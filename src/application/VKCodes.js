"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * VK_* key codes mapping.
 *
 * The constant values for VK_* key codes defined by CEA2014-A  Annex.
 */
module.exports = function(ctx) {

    return class VKCodes {

        constructor() {
            ctx.__internal__.init(this);

            let mapping = this.__internal__.mapping = new Map();
            mapping.set("VK_ENTER", 13);
            mapping.set("VK_LEFT", 37);
            mapping.set("VK_UP", 38);
            mapping.set("VK_RIGHT", 39);
            mapping.set("VK_DOWN", 40);
            mapping.set("VK_SPACE", 32);
            mapping.set("VK_BACK_SPACE", 8);
            mapping.set("VK_0", 48);
            mapping.set("VK_1", 49);
            mapping.set("VK_2", 50);
            mapping.set("VK_3", 51);
            mapping.set("VK_4", 52);
            mapping.set("VK_5", 53);
            mapping.set("VK_6", 54);
            mapping.set("VK_7", 55);
            mapping.set("VK_8", 56);
            mapping.set("VK_9", 57);
            mapping.set("VK_A", 65);
            mapping.set("VK_B", 66);
            mapping.set("VK_C", 67);
            mapping.set("VK_D", 68);
            mapping.set("VK_E", 69);
            mapping.set("VK_F", 70);
            mapping.set("VK_G", 71);
            mapping.set("VK_H", 72);
            mapping.set("VK_I", 73);
            mapping.set("VK_J", 74);
            mapping.set("VK_K", 75);
            mapping.set("VK_L", 76);
            mapping.set("VK_M", 77);
            mapping.set("VK_N", 78);
            mapping.set("VK_O", 79);
            mapping.set("VK_P", 80);
            mapping.set("VK_Q", 81);
            mapping.set("VK_R", 82);
            mapping.set("VK_S", 83);
            mapping.set("VK_T", 84);
            mapping.set("VK_U", 85);
            mapping.set("VK_V", 86);
            mapping.set("VK_W", 87);
            mapping.set("VK_X", 88);
            mapping.set("VK_Y", 89);
            mapping.set("VK_Z", 90);
            mapping.set("VK_RED", 403);
            mapping.set("VK_GREEN", 404);
            mapping.set("VK_YELLOW", 405);
            mapping.set("VK_BLUE", 406);
            mapping.set("VK_HELP ", 156);
            mapping.set("VK_SEARCH", 112);
            mapping.set("VK_AUDIODESCRIPTION", 113);
            mapping.set("VK_HD", 114);
            mapping.set("VK_PLAY", 415);
            mapping.set("VK_PAUSE", 19);
            mapping.set("VK_PLAY_PAUSE", 402);
            mapping.set("VK_STOP", 413);
            mapping.set("VK_PREV", 424);
            mapping.set("VK_NEXT", 425);
            mapping.set("VK_FAST_FWD", 417);
            mapping.set("VK_REWIND", 412);
            mapping.set("VK_INFO", 457);
            mapping.set("VK_SUBTITLE", 460);
            mapping.set("VK_BACK", 111);
            mapping.set("VK_VOLUME_UP", 447);
            mapping.set("VK_VOLUME_DOWN", 448);
            mapping.set("VK_MUTE", 449);

            let revert = this.__internal__.revert = new Map();
            for (let key of mapping.keys()) {
                let value = this.__internal__.mapping.get(key);

                Object.defineProperty(this, key, {
                    enumerable: true,
                    configurable: false,
                    writable: false,
                    value: value
                });

                revert.set(value, key);
            }
        }

        /**
         * Return the textual label representing the physical key or other
         * mechanism that is used by the terminal to generate the key event for
         * the given keycode passed. It SHALL return null if the key has no
         * textual label associated with it.
         * @param  {Number} code The VK_ constant for the key whose textual
         *                       label should be returned.
         * @return {String} label
         */
        getKeyLabel(code) {
            return this.__internal__.revert.get(code);
        }

    };
};
