"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * The WidgetDescriptor class is used to implement the characteristics of a DAE
 * Widget. It extends the Widget interface defined in section 11.3 of this
 * specification with the properties below.
 */
module.exports = function(ctx) {

    return class WidgetDescriptor {

        constructor() {
            ctx.__internal__.init(this);
        }

        /**
         * The URI of the installed Widget. It can be used as an argument to
         * ApplicationManager.createApplication() to run the Widget. The value
         * of this property SHOULD NOT represent the actual location of the
         * Widget on the OITF’s local storage.
         * @return {String}
         */
        get localURI() {
            return ctx.__internal__.localURI;
        }

        /**
         * The URI of the location from where the Widget package was downloaded.
         * This property SHALL match the URI used as argument of
         * createApplication() or installWidget() when installing the Widget.
         * @return {String}
         */
        get downloadURI() {
            return ctx.__internal__.downloadURI;
        }

        /**
         * A collection of URI strings for all the available default icons.
         * Default icons are defined in [WidgetsPackaging]. This collection only
         * contains URIs for the icons currently available in the Widget package.
         * @return {StringCollection}
         */
        get defaultIcon() {
            return ctx.__internal__.defaultIcon;
        }

        /**
         * A collection of URI strings for all the custom icons of the current
         * Widget. Custom icons are defined in [Widgets-Packaging].
         * @return {StringCollection}
         */
        get customIcons() {
            return ctx.__internal__.customIcons;
        }

        /**
         * This flag indicates the running state of the Widget.
         * @return {Boolean}
         */
        get running() {
            return ctx.__internal__.running;
        }

    };
};
