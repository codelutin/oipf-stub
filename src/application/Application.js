"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An OITF SHALL support a non-visual embedded object of type “application/oipfApplicationManager”,
 * with the following JavaScript API, to enable applications to access the privileged
 * functionality related to application lifecycle and management that is provided by
 * the application model.
 *
 * TODO:
 * 	- Manage widgets
 * 	- Application visualization mode
 * 	- Events ApplicationTopmost, ApplicationNotTopmost, ApplicationDestroyRequest and ApplicationHibernateRequest
 *  - Handle z-index
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let ApplicationPrivateData = require("./ApplicationPrivateData");
    let KeyHandlingStrategy = require("./KeyHandlingStrategy");

    return class Application {

        constructor(appMgr, doc) {
            ctx.__internal__.init(this, [
                "ApplicationActivated",
                "ApplicationDeactivated",
                "ApplicationShown",
                "ApplicationHidden",
                "ApplicationPrimaryReceiver",
                "ApplicationNotPrimaryReceiver",
                "ApplicationTopmost",
                "ApplicationNotTopmost",
                "ApplicationDestroyRequest",
                "ApplicationHibernateRequest",
                "KeyPress",
                "KeyDown",
                "KeyUp"
            ]);

            this.__internal__.appMgr = appMgr;
            this.__internal__.document = doc;

            this.__internal__.visible = false;
            this.__internal__.active = false;
            this.__internal__.primaryReceiver = false;

            this.__internal__.privateData = new ApplicationPrivateData();
            this.__internal__.parentApplication = null;
            this.__internal__.childrenApplication = new Set();

            this.__internal__.createWindow = function(applicationParent, uri) {
                return applicationParent.window.open(uri, "Application", "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes");
            };

            let keyHandlingStrategy = this.__internal__.keyHandlingStrategy = new KeyHandlingStrategy(appMgr);
            this.__internal__.setEventListeners = function(app) {
                let w = app.window;
                w.addEventListener("load", appMgr.__internal__.eventEmitter.emit.bind(appMgr, "ApplicationLoaded", this), false);
                w.addEventListener("unload", appMgr.__internal__.eventEmitter.emit.bind(appMgr, "ApplicationUnloaded", this), false);

                let keyEventCapture = keyHandlingStrategy.keyEventCapture.bind(keyHandlingStrategy);
                let keyEventBubble = keyHandlingStrategy.keyEventBubble.bind(keyHandlingStrategy);

                w.addEventListener("keypress", keyEventCapture, true);
                w.addEventListener("keydown", keyEventCapture, true);
                w.addEventListener("keyup", keyEventCapture, true);

                w.addEventListener("keypress", keyEventBubble, false);
                w.addEventListener("keydown", keyEventBubble, false);
                w.addEventListener("keyup", keyEventBubble, false);
            };

            this.__internal__.setEventListeners(this);
        }

        /**
         * @return {Boolean} true if the application is visible, false otherwise.
         *                   The value of this property is not affected by the
         *                   application's Z-index or position relative to other
         *                   applications. Only calls to the show() and hide()
         *                   methods will affect its value.
         */
        get visible() {
            return this.__internal__.visible;
        }

        /**
         * @return {Boolean} true if the application is in the list of currently
         *                   active applications, false otherwise.
         */
        get active() {
            return this.__internal__.active;
        }

        /**
         * @return {StringCollection} StringCollection object containing the
         *                            names of the permissions granted to this
         *                            application
         */
        get permissions() {
            return this.__internal__.permissions;
        }

        /**
         * @return {Boolean} true if the application receives cross application
         *                   events before any other application, false otherwise.
         */
        get isPrimaryReceiver() {
            return this.__internal__.primaryReceiver;
        }

        /**
         * A strict subset of the DOM Window object representing the
         * application. No symbols from the Window object are accessible through
         * this property except the following:
         * 		• void postMessage( any message, String targetOrigin )
         * @return {window} Window object representing the application
         */
        get window() {
            return this.__internal__.document.defaultView;
        }

        /**
         * Access the current application’s private data object.
         *
         * If an application attempts to access the privateData property of an
         * Application object for a different application, the OITF SHALL throw
         * an error as defined in section 10.1.1.
         * @return {ApplicationPrivateData} private data for the application
         */
        get privateData() {
            return this.__internal__.privateData;
        }

        /**
         * Issued when an application focus change occurs to inform the
         * recipient of the event that the application is now focussed.
         */
        set onApplicationActivated(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationActivated", callback);
        }
        get onApplicationActivated() {
            this.__internal__.eventEmitter.getOneListener("ApplicationActivated");
        }

        /**
         * Issued when an application focus change occurs to inform the
         * recipient of the event that the application is now no longer focussed.
         */
        set onApplicationDeactivated(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationDeactivated", callback);
        }
        get onApplicationDeactivated() {
            this.__internal__.eventEmitter.getOneListener("ApplicationDeactivated");
        }

        /**
         * Issued when an application has become visible.
         */
        set onApplicationShown(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationShown", callback);
        }
        get onApplicationShown() {
            this.__internal__.eventEmitter.getOneListener("ApplicationShown");
        }

        /**
         * Issued when an application has become hidden.
         */
        set onApplicationHidden(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationHidden", callback);
        }
        get onApplicationHidden() {
            this.__internal__.eventEmitter.getOneListener("ApplicationHidden");
        }

        /**
         * This event is issued to indicate that the target is now at the front
         * of the active application list.
         */
        set onApplicationPrimaryReceiver(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationPrimaryReceiver", callback);
        }
        get onApplicationPrimaryReceiver() {
            this.__internal__.eventEmitter.getOneListener("ApplicationPrimaryReceiver");
        }

        /**
         * This event is issued to indicate that the target is no longer at the
         * front of the active application list.
         */
        set onApplicationNotPrimaryReceiver(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationNotPrimaryReceiver", callback);
        }
        get onApplicationNotPrimaryReceiver() {
            this.__internal__.eventEmitter.getOneListener("ApplicationNotPrimaryReceiver");
        }

        /**
         * This event is issued to indicate that the target is now the topmost
         * (i.e. it has the highest Z-index and is not obscured by any other
         * visible applications, for OITFs where multiple applications are
         * visible simultaneously.
         */
        set onApplicationTopmost(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationTopmost", callback);
        }
        get onApplicationTopmost() {
            this.__internal__.eventEmitter.getOneListener("ApplicationTopmost");
        }

        /**
         * This event is issued to indicate that the target is no longer at the
         * topmost application. For OITFs where only one application is visible
         * at a time, this event indicates that the application is no longer
         * visible to the user.
         */
        set onApplicationNotTopmost(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationNotTopmost", callback);
        }
        get onApplicationNotTopmost() {
            this.__internal__.eventEmitter.getOneListener("ApplicationNotTopmost");
        }

        /**
         * This event is issued to indicate that the target application is about
         * to be terminated. It is not issued when an application calls
         * destroyApplication() method for itself (i.e. to exit itself).
         *
         * Non-responsive applications SHOULD be forcibly terminated by the
         * OITF, including the case where listeners for ApplicationDestroyRequest
         * events do not return promptly. The determination of when an
         * application is "non-responsive" is terminal-specific.
         *
         * If an application does not register a listener for this event and
         * there is a need for the system to terminate the application, then the
         * application SHALL be terminated immediately.
         */
        set onApplicationDestroyRequest(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationDestroyRequest", callback);
        }
        get onApplicationDestroyRequest() {
            this.__internal__.eventEmitter.getOneListener("ApplicationDestroyRequest");
        }

        /**
         * This event is issued to indicate that the OITF is about to enter a
         * hibernate mode.
         *
         * The OITF SHALL start a short watchdog timer (e.g. 2 seconds). During
         * this period the application may take any actions (for example to
         * store the currently viewed channel in case of an unsuccessful
         * start-up).
         */
        set onApplicationHibernateRequest(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationHibernateRequest", callback);
        }
        get onApplicationHibernateRequest() {
            this.__internal__.eventEmitter.getOneListener("ApplicationHibernateRequest");
        }

        /**
         * Generated when a key has been pressed by the user. May also be
         * generated when a key is held down during a key-repeat.
         */
        set onKeyPress(callback) {
            this.__internal__.eventEmitter.addOneListener("KeyPress", callback);
        }
        get onKeyPress() {
            this.__internal__.eventEmitter.getOneListener("KeyPress");
        }

        /**
         * Generated when a key pressed by the user has been released.
         */
        set onKeyUp(callback) {
            this.__internal__.eventEmitter.addOneListener("KeyUp", (e) => {
                callback(e);
                if (!e.stoppedPropagation) {
                    this.__internal__.keyHandlingStrategy.dispatchEvent(e);
                }
            });
        }
        get onKeyUp() {
            this.__internal__.eventEmitter.getOneListener("KeyUp");
        }

        /**
         * Generated when a key has been pressed by the user.
         */
        set onKeyDown(callback) {
            this.__internal__.eventEmitter.addOneListener("KeyDown", (e) => {
                callback(e);
                if (!e.stoppedPropagation) {
                    this.__internal__.keyHandlingStrategy.dispatchEvent(e);
                }
            });
        }
        get onKeyDown() {
            this.__internal__.eventEmitter.getOneListener("KeyDown");
        }

        /**
         * If the application visualization mode as defined by method
         * getApplicationVisualizationMode() in section 7.2.1.3, is:
         *
         * 1 : Make the application visible.
         *
         * 2 : Make the application visible. Calling this method from the
         * application itself may have no effect.
         *
         * 3 : Request to make the application visible.
         *
         * This method only affects the visibility of an application. In the
         * case where more than one application is visible, calls to this method
         * will not affect the z-index of the application with respect to any
         * other visible applications.
         */
        show() {
            if (this.window && this.window.document && this.window.document.body) {
                this.window.document.body.style.display = null;
            }
            this.__internal__.visible = true;
            this.__internal__.eventEmitter.emit("ApplicationShown");
        }

        /**
         * If the application visualization mode as defined by method
         * getApplicationVisualizationMode() in section 7.2.1.3, is:
         *
         * 1 : Make the application invisible.
         *
         * 2 : Make the application invisible. Calling this method from the
         * application itself may have no effect.
         *
         * 3 : Request to make the application invisible.
         *
         * Calling this method has no effect on the lifecycle of the application.
         *
         * Note: Broadcast independent applications should not call this method.
         * Doing so may result in only the background being visible to the user
         */
        hide() {
            if (this.window && this.window.document && this.window.document.body) {
                this.window.document.body.style.display = "none";
            }
            this.__internal__.visible = false;
            this.__internal__.eventEmitter.emit("ApplicationHidden");
        }

        /**
         * Move the application to the front of the active applications list.
         * If the application has been hidden using Application.hide(), this
         * method does not cause the application to be shown.
         *
         * If the application visualization mode as defined by method
         * getApplicationVisualizationMode() in section 7.2.1.3, is:
         *
         * 1 : The application’s Window object SHALL be moved to the top of the
         * stack of visible applications. In addition, the application’s Window
         * object SHALL gain input focus if argument gainFocus has value true.
         *
         * 2 : The application’s Window object SHALL be moved to the top of the
         * stack of visible applications. In addition, the application’s Window
         * object SHALL gain input focus if argument gainFocus has value true.
         * Calling this method from the application itself MAY have no effect.
         *
         * 3 : Request to make the application’s Window object visible. Once
         * visible, the application SHALL be given input focus, irrespective of
         * the value for argument gainFocus.
         * @param  {Boolean} gainFocus take the focus.
         */
        activateInput(gainFocus) {
            let appMgr = this.__internal__.appMgr;

            if (appMgr.__internal__.activeApplications.length) {
                let application = appMgr.__internal__.activeApplications[0];
                application.primaryReceiver = false;
                this.__internal__.eventEmitter.emit("ApplicationNotPrimaryReceiver");
            }

            this.__internal__.eventEmitter.emit("ApplicationPrimaryReceiver");
            this.__internal__.primaryReceiver = true;
            this.__internal__.active = true;

            // Remove old activation
            let activeApplications = appMgr.__internal__.activeApplications;
            let index = activeApplications.indexOf(this);
            if (index !== -1) {
                activeApplications.splice(index, 1);
            }

            activeApplications.unshift(this);

            if (gainFocus) {

                if (appMgr.__internal__.focusApplication) {
                    appMgr.__internal__.eventEmitter.emit("ApplicationDeactivated");
                }
                appMgr.__internal__.focusApplication = this;
                this.__internal__.eventEmitter.emit("ApplicationActivated");

                if (this.window) {
                    this.window.focus();
                }
            }
        }

        /**
         * Remove the application from the active applications list. This has no
         * effect on the lifecycle of the application and MAY have no effect on
         * the resources it uses. Applications which are not active will receive
         * no cross-application events, unless their Application object is the
         * target of the event (as for the events defined in section 7.2.6).
         * Applications may still be manipulated via their Application object or
         * their DOM tree.
         */
        deactivateInput() {
            let appMgr = this.__internal__.appMgr;
            let primaryReceiver = this.__internal__.primaryReceiver;

            if (primaryReceiver) {
                this.__internal__.primaryReceiver = false;
                this.__internal__.eventEmitter.emit("ApplicationNotPrimaryReceiver");
            }

            this.__internal__.active = false;

            let index = appMgr.__internal__.activeApplications.indexOf(this);
            if (index !== -1) {
                appMgr.__internal__.activeApplications.splice(index, 1);
            }

            if (primaryReceiver && appMgr.__internal__.activeApplications.length) {
                let application = appMgr.__internal__.activeApplications[0];
                application.primaryReceiver = true;
                this.__internal__.eventEmitter.emit("ApplicationPrimaryReceiver");
            }
        }

        /**
         * Create a new application and add it to the application tree. Calling
         * this method does not automatically show the newly-created application.
         *
         * This call is asynchronous and may return before the new application
         * is fully loaded. An ApplicationLoaded event will be targeted at the
         * Application object when the new application has fully loaded.
         *
         * If the application cannot be created, this method SHALL return null.
         * @param  {String} uri The URI of the first page of the application to
         *                      be created or the localURI of a Widget
         * @param  {Boolean} createChild Flag indicating whether the new application
         *                               is a child of the current application.
         *                               A value of true indicates that the new
         *                               application should be a child of the current
         *                               application; a value of false indicates
         *                               that it should be a sibling.
         * @return {Application} application object
         */
        createApplication(uri, createChild) {
            let appMgr = this.__internal__.appMgr;

            let w = this.__internal__.createWindow(this, uri);

            let doc = w.document;
            let application = new this.constructor(appMgr, doc);

            appMgr.__internal__.uriApplications.set(uri, application);

            if (createChild) {
                application.__internal__.parentApplication = this;
                this.__internal__.childrenApplication.add(application);

            } else if (this.__internal__.parentApplication) {
                application.__internal__.parentApplication = this.__internal__.parentApplication;
                this.__internal__.parentApplication.add(application);
            }

            return application;
        }

        /**
         * Terminate the application, detach it from the application tree, and
         * make any resources used available to other applications. When an
         * application is terminated, any child applications shall also be
         * terminated.
         */
        destroyApplication() {
            this.__internal__.childrenApplication.forEach(function(child) {
                child.destroyApplication();
            });

            let parent = this.__internal__.parentApplication;
            if (parent) {
                this.__internal__.parentApplication.childrenApplication.delete(this);
                this.__internal__.parentApplication = null;
            }

            let appMgr = this.__internal__.appMgr;
            appMgr.__internal__.uriApplications.delete(this.window.location.pathname);

            this.deactivateInput();

            this.window.location = "about:blank";
            this.window.close();
        }

        /**
         * Starts a Widget installed on the OITF. The behaviour of this method
         * is equivalent to that of Application.createApplication().
         *
         * The Widget is identified by its WidgetDescriptor. To get a list of
         * the WidgetDescriptor objects for the installed Widgets one can check
         * ApplicationManager.widgets property. If the Widget is already running
         * or fails to start this call will return null.
         * @param  {WidgetDescriptor} wd a WidgetDescriptor object for a Widget
         *                               installed on the OITF.
         * @param  {Boolean} createChild Flag indicating whether the new application
         *                               is a child of the current application.
         *                               A value of true indicates that the new
         *                              application should be a child of the current
         *                              application; a value of false indicates
         *                              that it should be a sibling.
         * @return {Application}             application object
         */
        startWidget(/*wd, createChild*/) {}

        /**
         * Terminate a running Widget. The behaviour of this method is equivalent
         * to that of Application.destroyApplication().
         *
         * Calling this method will detach the Widget from the application tree,
         * and make any resources used available to other applications. When a
         * Widget is terminated, any child applications shall also be terminated.
         * @param  {WidgetDescriptor} wd A WidgetDescriptor object for a Widget
         *                               installed on the OITF.
         */
        stopWidget(/*wd*/) {}

    };

};
