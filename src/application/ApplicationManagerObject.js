"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * An OITF SHALL support a non-visual embedded object of type “application/oipfApplicationManager”,
 * with the following JavaScript API, to enable applications to access the privileged
 * functionality related to application lifecycle and management that is provided by
 * the application model.
 *
 * TODO:
 * 	- Manage widgets
 * 	- Manage memory
 * 	- Application visualization mode
 */
let requireJS = require;

module.exports = function(ctx) {

    let require = ctx.require.bind(ctx, requireJS);

    let Application = require("./Application");
    let ApplicationCollection = require("./ApplicationCollection");

    return class ApplicationManagerObject {

        constructor() {
            ctx.__internal__.init(this, [
                "LowMemory",
                "ApplicationLoaded",
                "ApplicationUnloaded",
                "ApplicationLoadError",
                "WidgetInstallation",
                "WidgetUninstallation"
            ]);

            this.__internal__.uriApplications = new Map();
            this.__internal__.activeApplications = [];
            this.__internal__.focusApplication = null;
        }

        /**
         * The Widget installation has started. This state SHALL be used to
         * indicate that the download of the Widget package is completed
         * (possibly because the Widget was already stored locally) and the
         * OITF is ready to start the Widget installation process. This state
         * SHALL NOT be signalled if the package download fails.
         */
        get WIDGET_INSTALLATION_STARTED() {
            return 0;
        }

        /**
         * The Widget installation has completed successfully.
         */
        get WIDGET_INSTALLATION_COMPLETED() {
            return 1;
        }

        /**
         * The Widget installation has failed either because the Widget package
         * download failed or because, after the download, the Widget installation
         *  process failed.
         */
        get WIDGET_INSTALLATION_FAILED() {
            return 2;
        }

        /**
         * The Widget uninstallation has started.
         */
        get WIDGET_UNINSTALLATION_STARTED() {
            return 3;
        }

        /**
         * The Widget uninstallation has completed successfully.
         */
        get WIDGET_UNINSTALLATION_COMPLETED() {
            return 4;
        }

        /**
         * The Widget uninstallation has failed.
         */
        get WIDGET_UNINSTALLATION_FAILED() {
            return 5;
        }

        /**
         * The local storage device is full.
         */
        get WIDGET_ERROR_STORAGE_AREA_FULL() {
            return 10;
        }

        /**
         * The Widget cannot be downloaded.
         */
        get WIDGET_ERROR_DOWNLOAD() {
            return 11;
        }

        /**
         * The Widget package is corrupted or is an Invalid Zip Archive (as
         * defined in [WidgetsPackaging]).
         */
        get WIDGET_ERROR_INVALID_ZIP_ARCHIVE() {
            return 12;
        }

        /**
         * Widget's Signature Validation failed.
         */
        get WIDGET_ERROR_INVALID_SIGNATURE() {
            return 13;
        }

        /**
         * Other reason.
         */
        get WIDGET_ERROR_GENERIC() {
            return 14;
        }

        /**
         * The Widget exceeded the maximum size for a single widget allowed by
         * the platform, as defined in section 9.1.
         */
        get WIDGET_ERROR_SIZE_EXCEEDED() {
            return 15;
        }

        /**
         * The user and/or the OITF denied theinstallation or update of a Widget.
         */
        get WIDGET_ERROR_PERMISSION_DENIED() {
            return 16;
        }

        /**
         * The function that is called when the OITF is running low on available
         * memory for running DAE applications. The exact criteria determining
         * when to generate such an event is implementation specific.
         */
        set onLowMemory(callback) {
            this.__internal__.eventEmitter.addOneListener("LowMemory", callback);
        }

        get onLowMemory() {
            return this.__internal__.eventEmitter.getOneListener("LowMemory");
        }

        /**
         * The function that is called immediately prior to a load event being
         * generated in the affected application. The specified function is
         * called with one argument appl, which provides a reference to the
         * affected application.
         */
        set onApplicationLoaded(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationLoaded", callback);
        }

        get onApplicationLoaded() {
            return this.__internal__.eventEmitter.getOneListener("ApplicationLoaded");
        }

        /**
         * The function that is called immediately prior to an unload event
         * being generated in the affected application. The specified function
         * is called with one argument appl, which provides a reference to the
         * affected application.
         */
        set onApplicationUnloaded(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationUnloaded", callback);
        }

        get onApplicationUnloaded() {
            return this.__internal__.eventEmitter.getOneListener("ApplicationUnloaded");
        }

        /**
         * The function that is called when the OITF fails to load either the
         * file containing the initial HTML document of an application or an
         * XML AIT file (e.g. due to an HTTP 404 error, an HTTP timeout, being
         * unable to load the file from a DSM-CC object carousel or due to the
         * file not being either an HTML file or a XML AIT file as appropriate),
         * All properties of the Application object referred to by appl SHALL
         * have the value undefined and calling any methods on that object
         * SHALL fail.
         */
        set onApplicationLoadError(callback) {
            this.__internal__.eventEmitter.addOneListener("ApplicationLoadError", callback);
        }

        get onApplicationLoadError() {
            return this.__internal__.eventEmitter.getOneListener("ApplicationLoadError");
        }

        /**
         * The callback function that is called during the installation process
         * of a Widget. The function is called with three arguments:
         *
         * 	• WidgetDescriptor wd - the WidgetDescriptor for the installed
         * 	Widget. Some attributes of this argument may not have been
         * 	initialised and may be null when the function is called until
         * 	the Widget is successfully installed.
         *
         * 	• Integer state - the state of the installation; valid values
         * 	are:
         * 		o WIDGET_INSTALLATION_STARTED
         * 		o WIDGET_INSTALLATION_COMPLETED
         * 		o WIDGET_INSTALLATION_FAILED as defined in section 7.2.1.1.
         *
         * 	• Integer reason: indicates the reason for installation failure.
         * 	This is only valid if the value of the state argument is
         * 	WIDGET_INSTALLATION_FAILED otherwise this argument SHALL be
         * 	null. Valid values for this field are:
         * 		o WIDGET_ERROR_STORAGE_AREA_FULL
         * 		o WIDGET_ERROR_DOWNLOAD
         * 		o WIDGET_ERROR_INVALID_ZIP_ARCHIVE
         * 		o WIDGET_ERROR_INVALID_SIGNATURE
         * 		o WIDGET_ERROR_GENERIC
         * 		o WIDGET_ERROR_SIZE_EXCEEDED
         * 		o WIDGET_ERROR_PERMISSION_DENIED
         */
        set onWidgetInstallation(callback) {
            this.__internal__.eventEmitter.addOneListener("WidgetInstallation", callback);
        }

        get onWidgetInstallation() {
            return this.__internal__.eventEmitter.getOneListener("WidgetInstallation");
        }

        /**
         * The function that is called during the uninstallation process of a
         * Widget. The function is called with two arguments, defined below:
         *
         * 	• WidgetDescriptor wd - the WidgetDescriptor of the Widget to be
         * 	 uninstalled.
         *
         * 	• Integer state - the state of the installation; valid values
         * 	are:
         * 		o WIDGET_UNINSTALLATION_STARTED
         * 		o WIDGET_UNINSTALLATION_COMPLETED
         * 		o WIDGET_UNINSTALLATION_FAILED
         */
        set onWidgetUninstallation(callback) {
            this.__internal__.eventEmitter.addOneListener("WidgetUninstallation", callback);
        }
        get onWidgetUninstallation() {
            return this.__internal__.eventEmitter.getOneListener("WidgetUninstallation");
        }

        /**
         * A collection of WidgetDescriptor objects for the Widgets currently installed on the OITF.
         * @return {WidgetDescriptorCollection} the widgets installed
         */
        get widgets() {}

        /**
         * @return {Number} Returns the current mode used by the OITF to
         * visualize applications, where by a return value:
         * ----------------------------------------------------------------------------------------------------------------
         * Value |                                          Description
         * ------ ---------------------------------------------------------------------------------------------------------
         * 1     |  corresponds to the application visualization mode as defined by bullet 1) of section 4.4.6, i.e.
         *       |  multiple applications visible simultaneously with DAE applications managing their own visibility.
         * ------ ---------------------------------------------------------------------------------------------------------
         * 2     |  corresponds to the application visualization mode as defined by bullet 2) of section 4.4.6, i.e.
         *       |  multiple applications visible simultaneously with OITF managing the size, position, visibility of
         *       |  applications
         * ------ ---------------------------------------------------------------------------------------------------------
         * 3     |  corresponds to the application visualization mode as defined by bullet 3) of section 4.4.6, i.e.
         *       |  only a single application visible at any time.
         * ------ ---------------------------------------------------------------------------------------------------------
         */
        getApplicationVisualizationMode() {
            return 0;
        }

        /**
         * Get the application that the specified document is part of. If the
         * document is not part of an application, or the calling application
         * does not have permission to access that application, this method
         * will return null.
         * @param  {Document} doc The document for which the Application object
         *                        should be obtained.
         * @return {Application} application for the document.
         */
        getOwnerApplication(doc) {
            let uri = doc.location.toString();
            let application = this.__internal__.uriApplications.get(uri);
            if (!application) {
                application = new Application(this, doc);
                this.__internal__.uriApplications.set(uri, application);
            }

            return application;
        }

        /**
         * Get the applications that are children of the specified application
         * @param  {Application} application parent appplication.
         * @return {ApplicationCollection} children for the application.
         */
        getChildApplications(application) {
            let array = Array.from(application.__internal__.childrenApplication);
            return new ApplicationCollection(array);
        }

        /**
         * Provide a hint to the execution environment that a garbage
         * collection cycle should be initiated. The OITF is not required to
         * act upon this hint.
         */
        gc() {}

        /**
         * Attempts to install on the OITF a Widget located at the URI passed.
         * If the Widget is stored on a remote server it SHALL first be
         * downloaded. This specification does not specify where the OITF
         * stores the Widget package, nor does it define what happens to the
         * original package after the installation process has finished
         * (regardless of whether it succeeded or failed).
         *
         * When trying to install a Widget with an “id” that collides with the
         * id of an already installed Widget (where the “id” is defined in
         * section 7.6.1 of [Widgets-Packaging] along with the extension
         * defined in section 11.1 of this specification), the OITF SHOULD ask
         * the user for confirmation before installing the Widget. The OITF
         * SHOULD provide information about the conflict (e.g. the version
         * numbers, if available) to allow the user to decide whether to
         * proceed with the installation or to cancel it.
         *
         * If the user confirms the installation, then the new Widget SHALL
         * replace the one already installed; any storage area associated with
         * the replaced Widget SHALL be retained. Note that the user can also
         * choose to downgrade a Widget, i.e. install an old version of the
         * Widget to replace the installed, more recent, one.
         * @param  {String} uri uri where find the widget.
         */
        installWidget(/*uri*/) {}

        /**
         * Uninstalls a Widget. If this Widget is running it will be stopped.
         * Any storage areas associated with the uninstalled Widget SHALL be
         * deleted.
         * @param  {WidgetDescriptor} wd widget descriptor.
         */
        uninstallWidget(/*wd*/) {}

    };
};
