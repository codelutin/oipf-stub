"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

module.exports = function() {

    return class EventFactory {

        static createKeyEvent(event) {
            let keyEvent = EventFactory.copyKeyEvent(event);

            Object.defineProperty(keyEvent, "forwarded", {
                value: true
            });

            let stopPropagation = keyEvent.stopPropagation.bind(keyEvent);
            keyEvent.stopPropagation = function() {
                stopPropagation();

                Object.defineProperty(keyEvent, "stoppedPropagation", {
                    value: true
                });
            };

            return keyEvent;
        }

        static copyKeyEvent(event) {
            let keyEvent = new KeyboardEvent(event.type, {
                bubbles: event.bubbles,
                cancelable: event.cancelable
            });

            // This is a workaround for setting keycode in keyboard event
            // (chromium bug)

            Object.defineProperty(keyEvent, "keyCode", {
                value: event.keyCode
            });

            Object.defineProperty(keyEvent, "keyIdentifier", {
                value: event.keyIdentifier
            });

            Object.defineProperty(keyEvent, "which", {
                value: event.keyCode
            });

            Object.defineProperty(keyEvent, "altKey", {
                value: event.altKey
            });

            Object.defineProperty(keyEvent, "ctrlKey", {
                value: event.ctrlKey
            });

            Object.defineProperty(keyEvent, "metaKey", {
                value: event.metaKey
            });

            Object.defineProperty(keyEvent, "shiftKey", {
                value: event.shiftKey
            });

            return keyEvent;
        }

        static createMouseEvent(event) {
            let mouseEvent = new MouseEvent(event.type, {
                bubbles: event.bubbles,
                cancelable: event.cancelable,
                view: event.view,
                detail: event.detail,
                pageX: event.pageX || event.layerX,
                pageY: event.pageY || event.layerY,
                clientX: event.clientX,
                clientY: event.clientY,
                ctrlKey: event.ctrlKey,
                altKey: event.altKey,
                shiftKey: event.shiftKey,
                metaKey: event.metaKey,
                button: event.button,
                relatedTarget: event.relatedTarget
            });

            mouseEvent.forwarded = true;

            return mouseEvent;
        }

        static createMouseWheelEvent(event) {
            let mouseEvent = new WheelEvent(event.type, {
                bubbles: event.bubbles,
                cancelable: event.cancelable,
                view: event.view,
                detail: event.detail,
                pageX: event.pageX || event.layerX,
                pageY: event.pageY || event.layerY,
                clientX: event.clientX,
                clientY: event.clientY,
                ctrlKey: event.ctrlKey,
                altKey: event.altKey,
                shiftKey: event.shiftKey,
                metaKey: event.metaKey,
                button: event.button,
                relatedTarget: event.relatedTarget,
                deltaX: event.deltaX,
                deltaY: event.deltaY,
                deltaZ: event.deltaZ,
                deltaMode: event.deltaMode
            });

            mouseEvent.forwarded = true;

            return mouseEvent;
        }
    };
};
