"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * TODO:
 * 	- Wake-up
 * 	- Current channel
 */
let requireJS = require;

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    let Keyset = require("./Keyset");

    return class ApplicationPrivateData {
        constructor() {
            ctx.__internal__.init(this);

            this.__internal__.keyset = new Keyset();
        }

        /**
         * The object representing the user input events sent to the DAE application.
         * @return {Keyset} current keyset
         */
        get keyset() {
            return this.__internal__.keyset;
        }

        /**
         * For a broadcast-related application, the value of the property contains
         * the channel whose AIT is currently controlling the lifecycle of this
         * application. If no channel is being presented, or if the application is
         * not broadcast-related, the value of this property shall be null.
         * @return {Channel} current channel
         */
        get currentChannel() {}

        /**
         * The wakeupApplication property is set if there has been a
         * prepareWakeupApplication() request by that application.
         * @return {Boolean}
         */
        get wakeupApplication() {}

        /**
         * The wakeupOITF property is set if there has been a call to the
         * prepareWakeupOITF() method.
         * @return {Boolean}
         */
        get wakeupOITF() {}

        /**
         * Let application developer query information about the current memory
         * available to the application. This is used to help during application
         * development to find application memory leaks and possibly allow an
         * application to make decisions related to its caching strategy
         * (e.g. for images).
         *
         * Returns the available memory to the application or -1 if the
         * information is not available.
         *
         * For example:
         * var app = appman.getOwnerApplication(window.document);
         * debug("[APP] free mem = " + app.privateData.getFreeMem() + "\n");
         * @return {Number} available memory value
         */
        getFreeMem() {}

        /**
         * The prepareWakeupApplication() method allows the DAE application to
         * set-up the OITF to wake-up at a specified time. The wake-up is
         * limited to the OITF being in the PASSIVE_STANDBY state at the
         * specified time. If the timer expires while the DAE application is in
         * a different state it is silently ignored.
         *
         * Only one wake-up is to be supported for a DAE application. If a
         * previous wake-up request had been registered it SHALL be overwritten.
         *
         * If the wake-up fails to be set-up this operation SHALL return false.
         * Failure may be due to OITF expecting to change to an OFF power state
         * which would not allow the wake-up request to survive.
         * @param  {String} uri The URI from which the content can be fetched.
         * @param  {String} token The token is a string which the application may
         *                        retrieve with clearWakeupToken().
         * @param  {Date} time The time when the wake-up is to occur.
         * @return {Boolean}
         */
        prepareWakeupApplication(/*uri, token, time*/) {}

        /**
         * The prepareWakeupOITF() method allows the DAE application to set-up
         * the OITF to wake-up at a specified time. The wake-up is limited to
         * the OITF being in the PASSIVE_STANDBY or PASSIVE_STANDBY_HIBERNATE
         * state at the specified time. If the timer expires while the DAE
         * application is in a different state it is silently ignored.
         *
         * Unlike prepareWakeupApplication() this method applies to all the DAE
         * applications and not limited to a single DAE application.
         *
         * If the wake-up fails to be set-up this operation SHALL return false.
         * Failure may be due to OITF expecting to change to an OFF power state
         * which would not allow     the wake-up request to survive.
         * @param  {Date} time The time when the wake-up is to occur.
         * @return {Boolean}
         */
        prepareWakeupOITF(/*time*/) {}

        /**
         * The clearWakeupToken() method shall return the token set in
         * prepareWakeupApplication() method. The wake-up token should be
         * cleared once it is read in order to limit usage to only when the DAE
         * application starts up.
         * @return {String}
         */
        clearWakeupToken() {}
    };
};
