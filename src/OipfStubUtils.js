"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class GetSetObject {

    constructor(values) {
        this.setValues(values);
    }

    setValues(values) {
        this.__values__ = values || {};
    }

    getField(prop, defaultValueOrFn) {
        let result;
        if (Object.getOwnPropertyDescriptor(this.__values__, prop)) {
            result = this.__values__[prop];
        } else if (defaultValueOrFn instanceof Function) {
            result = defaultValueOrFn();
            this.setField(prop, result);
        } else {
            result = defaultValueOrFn;
        }
        return result;

    }

    setField(prop, value) {
        this.__values__[prop] = value;
    }

    // mainly used in toJSON
    getValues() {
        return this.__values__;
    }

}

module.exports = {

    createGetSetObject(values) {
        return new GetSetObject(values);
    },

    /**
     * wait all promise and return all result as Array (the order is not guaranteed)
     */
    waitAllPromise(list) {
        return new Promise(function(resolve) {
            let result = [];
            if (!list || !list.length) {
                resolve(result);
            } else {
                let count = list.length;

                let done = function(value) {
                    count--;
                    result.push(value);
                    if (!count) {
                        resolve(result);
                    }
                };

                for (let p of list) {
                    if (!(p instanceof Promise)) {
                        p = Promise.resolve(p);
                    }
                    p.then(done, done);
                }
            }
        });
    },

    /**
     * @param channelList this.getChannelConfig().channelList;
     * @param c channel present in channelList
     */
    getChannel(channelList, channel) {
        let channels = channelList;
        let realChannel = channels[channel.__internal__ && channel.__internal__.index];
        if (!realChannel) {
            // channel not find with index, channel arguement is channel create
            // by user. Try to find channel in channel list
            for (let c of channels) {
                if (this.isSameChannel(c, channel)) {
                    realChannel = c;
                    break;
                }
            }
        }
        return realChannel;
    },

    getChannelIndex(channelList, channel) {
        let c = this.getChannel(channelList, channel);
        return c.__internal__.index;
    },

    /**
     * Test if two channel represent same thing. Comparaison depend on idType
     */
    isSameChannel(c1, c2) {
        let result = c1 === c2;
        if (!result && c1 && c2) {
            result = c1.idType === c2.idType;
            if (result) {
                if (c1.idType === 0) {
                    result = c1.freq === c2.freq;
                } else if (c1.idType === 13) {
                    // Used in the idType property to indicate a channel that is
                    // identified through its delivery system descriptor as
                    // defined by DVB-SI [EN 300 468] section 6.2.13.
                    console.warn("I don't know how check equality for idType", c1.idType);
                    // FIXME poussin 20150803 read section 6.2.13 to find good comparaison method
                } else if (c1.idType >= 10 && c1.idType <= 22) {
                    // test on dvb
                    result = c1.onid === c2.onid
                            && c1.tsid === c2.tsid
                            && c1.sid === c2.sid;
                } else if (c1.idType === 30) {
                    // test on sourceID
                    result = c1.sourceID === c2.sourceID;
                } else if (c1.idType === 40 || c1.idType === 41) {
                    // ipBroadcastID
                    result = c1.ipBroadcastID === c2.ipBroadcastID;
                } else {
                    // unknown idType
                    console.warn("unknown idType", c1.idType);
                    result = false;
                }
            }
        }

        return result;
    },

    nonce() {
        if (!this._nonce) {
            this._nonce = Date.now();
        }
        return this._nonce++;
    }
};
