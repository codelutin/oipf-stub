"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let fs = require("fs");

let moment = require("moment");
let DataProvider = require("./DataProvider.js");
let JSZip = require("jszip");
let JSZipUtils = require("jszip-utils");
let defaultChannelInfo = require("../../data/fr_tnt_channels.json");

/*
 * Provider interface fo all data provider.
 */
module.exports = class AbstractXmlTvProvider extends DataProvider {

    get DEFAULT_END_POINT() { return "./data/fr_tnt.xml"; }

    constructor(option) {
        super();

        option = option || {};
        this.maxDate = option.maxDate && moment(option.maxDate).unix(); // in second
        this.endpoint = option.endpoint || this.DEFAULT_END_POINT;
        this.channelInfo = {};
        if (option.acceptChannelWithoutDVB === false) { // undefined must be true
            this.acceptChannelWithoutDVB = false;
        } else {
            this.acceptChannelWithoutDVB = true;
        }
        for (let info of option.channelInfo || defaultChannelInfo) {
            this.channelInfo[info.name] = info;
        }

        let storageKey = "oipf.provider.xmltv.cache";
        let oldData = localStorage.getItem(storageKey);
        if (oldData) {
            this.cache = JSON.parse(oldData);
        } else {
            this.cache = {data: {}};
        }
        this.cache.storageKey = storageKey;

        let cacheExpiration = option.cacheExpiration || [1, "days"];
        if (Array.isArray(cacheExpiration)) {
            this.cache.expiration = moment.duration.apply(moment.duration, cacheExpiration).asMilliseconds();
        } else {
            this.cache.expiration = moment.duration(cacheExpiration).asMilliseconds();
        }
    }

    /**
     * Fetch local file content.
     *
     * @returns {Promise} fetch promise
     */
    fetchLocalData() {
        let endpoint = this.endpoint.replace(/^file:(\/\/)?/, "");
        let promise = new Promise(function(resolve, reject) {
            fs.readFile(endpoint, function(err, data) {
                if (err) {
                    reject(err);
                } else if (endpoint.indexOf(".zip") !== -1) {
                    let zip = new JSZip(data),
                        files = zip.file(/^.*$/);

                    resolve(files[0].asText());
                } else {
                    resolve(data.toString());
                }
            });
        });
        return promise;
    }

    /**
     * Fetch an empty content.
     *
     * @returns {Promise} fetch promise
     */
    fetchEmptyData() {
        return new Promise(function(resolve) { resolve(""); });
    }

    /**
    * Date must be formated as: YYYYMMDDHHmmss
    */
    parseDateYYYYMMDDHHmmss(ds) {
        let result = new Date(
            ds.substring(0, 4),     // Year
            ds.substring(4, 6) - 1, // month
            ds.substring(6, 8),     // day
            ds.substring(8, 10),    // hour
            ds.substring(10, 12),   // minute
            ds.substring(12, 14));  // second
        return result;
    }

    fetchData() { return Promise.reject(); }

    /**
     * Parse channels from xml content.
     *
     * @returns {Promise} promise resolved with channels as json data
     */
    getChannels() {
        return this.fetchData()
            .then(function(data) {
                return data.channels;
            }).catch(function(error) {
                console.log("can't fetch", error);
                return [];
            });
    }

    /**
     * Parse programme from xml content.
     *
     * @param {String} ccid channel ccid
     * @returns {Promise} promise resolved with programmes as json data
     */
    getProgrammes(index, channel) {
        return this.fetchData()
            .then(function(data) {
                return data.programmes[channel.sourceID] || [];
            }).catch(function(error) {
                console.log("can't fetch", error);
                return [];
            });
    }

};
