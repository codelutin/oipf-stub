"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Take a DataProvider and generate all missing data to oipf need.
 * Transforme promise to static data. Event is throw only when data is
 * available for client.
 *
 * Data are modified directly in initial DataProvider Array
 * This class is used internaly in OipfStubContext
 */
let requireJS = require;
let Utils = require("../OipfStubUtils");

module.exports = function(ctx) {
    let require = ctx.require.bind(ctx, requireJS);

    return class OipfDataProvider {

        constructor() {
            ctx.__internal__.init(this, ["ChannelListUpdate"]);

            ctx.addEventListener("DataProviderChange", this._updateDataProvider.bind(this));
        }

        _updateDataProvider(dataProvider) {
            this._provider = dataProvider;
            this._provider.guideDaysAvailable()
                    .catch(Promise.resolve.bind(Promise, 0)) // go to then with default value
                    .then(this.__internal__.setField.bind(this.__internal__, "guideDaysAvailable"));

            this._provider.getTuners()
                    .catch(Promise.resolve.bind(Promise, [])) // go to then with default value
                    .then(this.__internal__.setField.bind(this.__internal__, "tuners"));

            let me = this; // eslint-disable-line consistent-this
            this._provider.getChannels()
                .catch(Promise.resolve.bind(Promise, [])) // go to then with default value
                .then(function(channels) {
                    me.__internal__.setField("channels", channels);
                    me.__internal__.eventEmitter.emit("ChannelListUpdate");
                });
        }

        /**
         * Return array of all tuners available. Tuner is represented by
         * Object with some property:
         * {
         *     tunerID: [optional String], `// default position in array.
         *     name: [optional String],     // default tunerid
         *     idTypes: [optional Array of Number], // default [0] and freq is generated in channel
         * }
         *
         * @param {String} tunerId optional, need if your provider support more than one tuner
         * @returns {Array of Object}
         */
        getTuners() {
            return this.__internal__.getField("tuners", []);
        }

        /**
         * Object with some property:
         * {
         * Return array of all channels available. Channel is represented by
         *     ccid: [optional String], `// default 'ccid:{tunerID.}majorChannel{.minorChannel}'.
         *     tunerId: [optional String],
         *     name: [optional String],
         *     majorChannel: [optional Number], // default position in array
         *     minorChannel: [optional Number]
         *     idType: [optionnal Number] // default 30
         *     logoUrl: [optional String]
         *
         *     // for idType: 0
         *     freq: [optional Number] // if idType === 0 default majorChannel * 1000 + minorChannel
         *
         *     // for idType: 10, 11, 12, 14, 15, 16, 20, 21, 22
         *     onid: [optional String]
         *     tsid: [optional String]
         *     sid: [optional String]
         *
         *     // for idType: 30
         *     sourceId: [optional String] // if idType === 30 default tunerId
         * }
         *
         * @param {Number} tuner index optional, need if your provider support more than
         *                       one tuner. index is same as in #getTuners()
         * @returns {Array of Object}
         */
        getChannels() {
            return this.__internal__.getField("channels", []);
        }

        /**
         * Give the number of guide days available. By default
         * 0, and not program search can be done
         * @returns {Number} number of guide days available
         */
        guideDaysAvailable() {
            return this.__internal__.getField("guideDaysAvailable", 0);
        }

        /**
         * Return programmes for given index (order as returned by #getChannels())
         * Programs is represented by
         * Object with some property:
         * {
         *     ccid: [needed String],// channel Id
         *     pid: [needed String], // program Id
         *     name: [needed String],
         *     airingTimeStart: [needed Date],
         *     airingTimeEnd: [needed Date]
         * }
         * @param   {Number} Channel index. Index is same as in #getChannels()
         * @returns {Array of Object} Programs represented as Object
         */
        getProgrammes(channelIndex, channel) {
            let providerChannel = this.getChannels()[channelIndex];
            return this._provider.getProgrammes(channelIndex, providerChannel)
                    .then(function(programmes) {
                        let Programmes = require("../shared/Programme");

                        let result = [];
                        for (let p of programmes) {
                            p.channel = channel;
                            p.channelID = channel.ccid;
                            result.push(new Programmes(p));
                        }
                        return result;
                    });
        }

        /**
         * Used for:
         * - OipfObjectFactory.createChannelConfig (7.1.2)
         * - application/oipfRecordingScheduler.getChannelConfig (7.10.1.1)
         * - application/oipfSearchManager.getChannelConfig (7.12.1.3)
         * - video/broadcast.getChannelConfig (7.13.1.3)
         *
         * Those function must return same instance as describe in Spec
         */
        getChannelConfig() {
            if (!this.__internal__.channelConfig) {
                let ChannelConfig = require("../broadcast/ChannelConfig");
                this.__internal__.channelConfig = new ChannelConfig();
            }
            return this.__internal__.channelConfig;
        }

        useMetadataConstraint() {
            return this._provider.useMetadataConstraint();
        }

        getProgrammesByConstraint(cons, offset, count) {
            let channels = [];
            let channelList = this.getChannelConfig().channelList;
            if (cons.channels) {
                channels = [];
                for (let c of cons.channels) {
                    let realChannel = Utils.getChannel(channelList, c);
                    realChannel && channels.push(realChannel);
                }
            }

            return this._provider.getProgrammesByConstraint(cons, channels, offset, count)
                .then(function(programmes) {
                    let Programmes = require("../shared/Programme");

                    let result = [];
                    for (let p of programmes) {
                        p.channelID = p.channel.ccid;
                        result.push(new Programmes(p));
                    }
                    return result;
                });
        }
    };
};
