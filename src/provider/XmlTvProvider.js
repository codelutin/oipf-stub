"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let fs = require("fs");

let moment = require("moment");
let AbstractXmlTvProvider = require("./AbstractXmlTvProvider.js");
let JSZip = require("jszip");
let JSZipUtils = require("jszip-utils");
let XMLParser = require("exml-easysax");
let defaultChannelInfo = require("../../data/fr_tnt_channels.json");

/*
 * Provider interface fo all data provider.
 */
module.exports = class XmlTvProvider extends AbstractXmlTvProvider {

    /**
     * Fetch xml content depending on endpoint configuration (local file or remote url).
     *
     * @returns {Promise} fetch promise
     */
    fetchXmlData(parser) {
        let result;
        if (this.endpoint === "" || this.endpoint.indexOf("none") === 0) {
            result = this.fetchEmptyData();
        } else if (this.endpoint.indexOf("file:") === 0) {
            result = this.fetchLocalData().then(function(text) {
                parser.write(text);
            });
        } else {
            result = this.fetchRemoteData(parser);
        }
        return result;
    }

    /**
     * Fetch remote endpoint url.
     *
     * @returns {Promise} fetch promise
     */
    fetchRemoteData(parser) {
        let promise;

        let endpoint = this.endpoint;
        if (endpoint.indexOf(".zip") !== -1) {
            promise = new Promise(function(resolve, reject) {
                JSZipUtils.getBinaryContent(endpoint, function(err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            let zip = new JSZip(data);
                            let files = zip.file(/^.*\.xml$/);
                            let xml = files[0].asText(); // use first xml file
                            resolve(xml);
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            }).then(function(xml) {
                parser.write(xml);
            });
        } else {
            promise = new Promise(function(resolve, reject) {
                let decoder = new TextDecoder();
                let read = function(reader) {
                    reader.read().then(function(result) {
                        if (result.done) {
                            resolve();
                        } else {
                            let chunk = decoder.decode(result.value);
                            parser.write(chunk);
                            if (parser.saxParser.state.end) {
                                reader.cancel();
                                resolve();
                            } else {
                                read(reader);
                            }
                        }
                    });
                };

                fetch(endpoint)
                .then(function(response) {
                    read(response.body.getReader());
                }).catch(function(e) {
                    reject(e);
                });
            });
        }
        return promise;
    }

    fetchData() {
        let result;
        let cache = this.cache;
        let cacheKey = this.endpoint;
        let value = cache.data[cacheKey] || {};

        if (value.promise && !value.data) {
            result = value.promise;
        } else {
            if (value.data) {
                result = Promise.resolve(value.data);
            }

            if (!value.data || value.date + cache.expiration < Date.now()) {
                // no data or need refresh for next call
                let maxDate = this.maxDate;
                let channelInfo = this.channelInfo;
                let acceptChannelWithoutDVB = this.acceptChannelWithoutDVB;
                let parseDate = this.parseDateYYYYMMDDHHmmss;

                console.log("################### LOAD XML DATA");
                let time = Date.now();

                let channels = [];
                let programmes = {};
                let done = {count: 0}; // data source has same programme many time, result must return only one

                let parser = new XMLParser.Parser();

                parser.on("tv/channel", function(attrs) {
                    let channel = {sourceID: attrs.id, idType: 30};

                    // can be "1", "C1.telerama.fr"
                    let sourceID = channel.sourceID.replace(/\D/g, "");
                    channel.majorChannel = parseInt(sourceID, 10);
                    parser.on("display-name/$content", XMLParser.assign(channel, "name"));

                    parser.on("$content", function() { // call on end tag
                        let info = channelInfo[channel.name + " HD"] || channelInfo[channel.name];
                        if (acceptChannelWithoutDVB || info) {
                            Object.assign(channel, info || {});
                            channels.push(channel);
                            channels["cid-" + channel.sourceID] = channel;
                        }
                    });
                });

                parser.on("tv/programme", function(attrs) {

                    let startDate = parseDate(attrs.start).valueOf() / 1000; // in second
                    // on some xml tv files, programmes are not ordered by dates
                    if (startDate <= maxDate) {
                        console.log("programme found");
                        let key = attrs.channel + "-" + attrs.start;
                        if (!done[key]) {
                            done[key] = true;
                            done.count++;
                            let channelId = attrs.channel;
                            let prog = {};
                            let progs = programmes[channelId] = programmes[channelId] || [];
                            progs.push(prog);

                            // channel
                            prog.channel = channels["cid-" + channelId];

                            // parse dates
                            let endDate = parseDate(attrs.stop).valueOf() / 1000; // in second
                            prog.startTime = startDate;
                            prog.duration = endDate - startDate;

                            parser.on("title/$content", XMLParser.assign(prog, "name"));
                            parser.on("sub-title/$content", XMLParser.assign(prog, "subtitle"));
                            parser.on("desc/$content", XMLParser.assign(prog, "description"));
                            parser.on("category/$content", function(text) {
                                prog.genre = prog.genre || [];
                                prog.genre.push(text);
                            });
                            // parser.on("rating", "value", "$content", XMLParser.assign(prog, "rating"));
                            parser.on("icon", function(iconAttrs) {
                                prog._logoURL = iconAttrs.src;
                            });
                        }
                    }
                });

                let promise = this.fetchXmlData(parser)
                    .then(function() {
                        parser.end();
                        console.log("################### time: ", Date.now() - time, "progs: ", done.count);
                        // sort because json file order matters
                        channels = channels.sort(function(c1, c2) {
                            return c1.minorChannel - c2.minorChannel;
                        });
                        return {channels, programmes};
                    });

                promise.then(function(data) {
                    cache.data[cacheKey] = {date: Date.now(), data};
                    localStorage.setItem(cache.storageKey, JSON.stringify(cache));
                    return data;
                });

                promise.catch(function(error) {
                    console.log("can't fetch", error);
                    return [];
                });

                cache.data[cacheKey] = {promise, date: value.date, data: value.data};
                if (!result) {
                    result = promise;
                }
            }
        }

        return result;
    }
};
