"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let fs = require("fs");

let moment = require("moment");
let AbstractXmlTvProvider = require("./AbstractXmlTvProvider.js");
let JSZip = require("jszip");
let JSZipUtils = require("jszip-utils");
let defaultChannelInfo = require("../../data/fr_tnt_channels.json");

/*
 * Provider interface fo all data provider.
 */
module.exports = class JsonTvProvider extends AbstractXmlTvProvider {

    get DEFAULT_END_POINT() { return "./data/fr_tnt.json"; }

    /**
     * Fetch json content depending on endpoint configuration (local file or remote url).
     *
     * @returns {Promise} fetch promise
     */
    fetchJsonData() {
        let result;
        if (this.endpoint === "" || this.endpoint.indexOf("none") === 0) {
            result = this.fetchEmptyData();
        } else if (this.endpoint.indexOf("file:") === 0) {
            result = this.fetchLocalData().then(function(text) {
                this.processData(text);
            });
        } else {
            result = this.fetchRemoteData();
        }
        return result;
    }

    /**
     * Fetch remote endpoint url.
     *
     * @returns {Promise} fetch promise
     */
    fetchRemoteData() {
        let promise;

        let endpoint = this.endpoint;
        if (endpoint.indexOf(".zip") !== -1) {
            promise = new Promise(function(resolve, reject) {
                JSZipUtils.getBinaryContent(endpoint, function(err, data) {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            let zip = new JSZip(data);
                            let files = zip.file(/^.*\.json$/);
                            let json = files[0].asText(); // use first xml file
                            resolve(json);
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            }).then(function(json) {
                return this.processData(json);
            });
        } else {
            promise = fetch(endpoint)
            .then(function(response) {
                return response.json();
            }).then((json) => this.processData(json));
        }
        return promise;
    }

    processData(json) {
        // no data or need refresh for next call
        let maxDate = this.maxDate;
        let channelInfo = this.channelInfo;
        let acceptChannelWithoutDVB = this.acceptChannelWithoutDVB;
        let parseDate = this.parseDateYYYYMMDDHHmmss;

        console.log("################### LOAD JSON DATA");
        let time = Date.now();

        let channels = [];
        let programmes = {};
        let done = {count: 0}; // data source has same programme many time, result must return only one

        let jsonChannels = json.channel, jsonChannel;
        for (let i = 0, l = jsonChannels.length; i < l; i++) {
            jsonChannel = jsonChannels[i];

            // Process channels
            const channel = {sourceID: jsonChannel["@attributes"].id, idType: 30};

            // can be "1", "C1.telerama.fr"
            const sourceID = channel.sourceID.replace(/\D/g, "");
            channel.majorChannel = parseInt(sourceID, 10);
            channel.name = jsonChannel["display-name"];

            const info = channelInfo[channel.name + " HD"] || channelInfo[channel.name];
            if (acceptChannelWithoutDVB || info) {
                Object.assign(channel, info || {});
                channels.push(channel);
                channels["cid-" + channel.sourceID] = channel;
            }
        }

        // Process programmes
        let jsonProgrammes = json.programme, jsonProgramme, jsonProgrammeAttrs;
        for (let i = 0, l = jsonProgrammes.length; i < l; i++) {
            jsonProgramme = jsonProgrammes[i];
            jsonProgrammeAttrs = jsonProgramme["@attributes"];
            let startDate = parseDate(jsonProgrammeAttrs.start).valueOf() / 1000; // in second
            // on some xml tv files, programmes are not ordered by dates
            if (startDate <= maxDate) {
                console.log("programme found");
                let key = jsonProgrammeAttrs.channel + "-" + jsonProgrammeAttrs.start;
                if (!done[key]) {
                    done[key] = true;
                    done.count++;
                    let channelId = jsonProgrammeAttrs.channel;
                    let prog = {};
                    let progs = programmes[channelId] = programmes[channelId] || [];
                    progs.push(prog);

                    // channel
                    prog.channel = channels["cid-" + channelId];

                    // parse dates
                    let endDate = parseDate(jsonProgrammeAttrs.stop).valueOf() / 1000; // in second
                    prog.startTime = startDate;
                    prog.duration = endDate - startDate;
                    prog.name = jsonProgramme.title;
                    prog.subtitle = jsonProgramme["sub-title"];
                    prog.description = jsonProgramme.desc;
                    if (jsonProgramme.category) {
                        for (let j = 0, m = jsonProgramme.category.length; j < m; j++) {
                            prog.genre = prog.genre || [];
                            prog.genre.push(jsonProgramme.category[j]);
                        }
                    }
                    prog._logoURL = jsonProgramme.icon && jsonProgramme.icon["@attributes"].src;
                }
            }
        }

        console.log("################### time: ", Date.now() - time, "progs: ", done.count);

        // sort because json file order matters
        channels = channels.sort(function(c1, c2) {
            return c1.minorChannel - c2.minorChannel;
        });

        // Storing result
        const cache = this.cache;
        cache.data[this.endpoint] = {date: Date.now(), data: {channels, programmes}};
        localStorage.setItem(cache.storageKey, JSON.stringify(cache));

        return {channels, programmes};
    }

    fetchData() {
        let result;
        let cache = this.cache;
        let cacheKey = this.endpoint;
        let value = cache.data[cacheKey] || {};

        if (value.promise && !value.data) {
            result = value.promise;
        } else {
            if (value.data) {
                result = Promise.resolve(value.data);
            }

            if (!value.data || value.date + cache.expiration < Date.now()) {
                let promise = this.fetchJsonData();

                result = result || promise;
                cache.data[cacheKey] = {promise, date: value.date, data: value.data};
            }
        }

        return result;
    }

};
