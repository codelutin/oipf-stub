"use strict";

/*
 * oipf-stub, (C) 2015 Code Lutin (SAS).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Provider interface fo all data provider.
 * Your implementation must override some or all method.
 *
 * All method return rejected Promise. Default values are in OipfDataProvider
 */
module.exports = class DataProvider {

    /**
     * Return array of all tuners available. Tuner is represented by
     * Object with some property:
     * {
     *     tunerID: [optional String], `// default position in array.
     *     name: [optional String],     // default tunerid
     *     idTypes: [optional Array of Number], // default [0] and freq is generated in channel
     * }
     *
     * @param {String} tunerId optional, need if your provider support more than one tuner
     * @returns {Array of Object}
     */
    getTuners() {
        return Promise.reject();
    }

    /**
     * Return array of all channels available. Channel is represented by
     * Object with some property:
     * {
     *     ccid: [optional String], `// default 'ccid:{tunerID.}majorChannel{.minorChannel}'.
     *     tunerId: [optional String],
     *     name: [optional String],
     *     majorChannel: [optional Number], // default position in array
     *     minorChannel: [optional Number]
     *     idType: [optionnal Number] // default 30
     *     logoUrl: [optional String]
     *
     *     // for idType: 0
     *     freq: [optional Number] // if idType === 0 default majorChannel * 1000 + minorChannel
     *
     *     // for idType: 10, 11, 12, 14, 15, 16, 20, 21, 22
     *     onid: [optional String]
     *     tsid: [optional String]
     *     sid: [optional String]
     *
     *     // for idType: 30
     *     sourceId: [optional String] // if idType === 30 default tunerId
     * }
     *
     * @param {Number} tuner index optional, need if your provider support more than
     *                       one tuner. index is same as in #getTuners()
     * @returns {Array of Object}
     */
    getChannels() {
        return Promise.reject();
    }

    /**
     * Give the number of guide days available. By default
     * 0, and not program search can be done
     * @returns {Number} number of guide days available
     */
    guideDaysAvailable() {
        return Promise.reject();
    }

    /**
     * Return whether getProgrammes or getProgrammesByQuery should be used on this provider.
     */
    useMetadataConstraint() {
        return false;
    }

    /**
     * Return programs for given ccid (Channel Id as returned by #getChannels())
     * Programs is represented by
     * Object with some property:
     * {
     *     ccid: [needed String],// channel Id
     *     pid: [needed String], // program Id
     *     name: [needed String],
     *     airingTimeStart: [needed Date],
     *     airingTimeEnd: [needed Date]
     * }
     * @param   {Number} index. Index is same as in #getChannels()
     * @param   {Objact} channel one channel as returned by getChannels
     * @returns {Array of Object} Programs represented as Object
     */
    getProgrammes(/*index, channel*/) {
        return Promise.reject();
    }

    /**
     * Return programmes for given query constraints.
     *
     * @param   {constraint} constraint metadata search's contraint
     * @param   {offset} first result wanted
     * @param   {count} number of result expected (need if constraint doesn't contains maxResult value)
     * @returns {Array of Object} Programs represented as Object
     */
    getProgrammesByConstraint(/*constraint, offset, count*/) {
        return Promise.reject();
    }

};
