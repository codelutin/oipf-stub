OIPF Stub
=========

Contributions
-------------

The preferred way of communication is using mailing lists:

* [oipf-stub-users@list.forge.codelutin.com](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/oipf-stub-users) : users mailing list
* [oipf-stub-devel@list.forge.codelutin.com](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/oipf-stub-devel) : developer mailing list
* [oipf-stub-commits@list.forge.codelutin.com](http://list.forge.codelutin.com/cgi-bin/mailman/listinfo/oipf-stub-commits) : mailing list used to receive GIT commits

Usage
-----

To use stub in your project you must write some code
example::

    var OipfStubContext = require("oipf-stub/src/OipfStubContext");

    // create config to override default oipf configuration
    var config = {
        emitterMaxListeners: 15,
        videoBroadcastVideoEndpoint: "/myvbvideo/",
        videoBroadcastVideoExtension: ".webm",
        videoBroadcastVideoStartDate: new Date();
    };

    // You can define some oipf object as native, if your browser support only some (hybride stub)
    if (window.oipfObjectFactory) {
        config.oipfObject = {
            VideoBroadcastObject: "native",
            ApplicationManagerObject: "native",
            ConfigurationObject: "native",
            ReminderManagerObject: "native"
        };
    }

    var context = window.oipfContext = new OipfStubContext(config);

    // create the stubbed oipfObjectFactory
    window._stubbedOipfObjectFactory = context.createOipfObjectFactory();

    // register it on window if native doesn't exist
    if (!window.oipfObjectFactory) {
        window.oipfObjectFactory = window._stubbedOipfObjectFactory;
    }

Create and use extension
------------------------

:TODO:

context.registerExtensions("MyExtension");


OIPF Stub implementation
------------------------

### Implemented

### Completed

- 7.1  OBJECT FACTORY API

- 7.2     APPLICATION MANAGEMENT APIS
- 7.2.1    The application/oipfApplicationManager embedded object
- 7.2.2    The Application class
- 7.2.3    The ApplicationCollection class
- 7.2.4    The ApplicationPrivateData class
- 7.2.5    The Keyset class
- 7.2.6    New DOM events for application support

- 7.12 METADATA APIS
- 7.12.1 The application/oipfSearchManager embedded object
- 7.12.2 The MetadataSearch class
- 7.12.3 The Query class
- 7.12.4 The SearchResults class
- 7.12.5 The MetadataSearchEvent class
- 7.12.6 The MetadataUpdateEvent class


### In progress

- 7.3     CONFIGURATION AND SETTING APIS
- 7.3.1    The application/oipfConfiguration embedded object
- 7.3.2    The Configuration class
- 7.3.3    The LocalSystem class
- 7.3.4    The NetworkInterface class
- 7.3.5    The AVOutput class
- 7.3.6    The NetworkInterfaceCollection class
- 7.3.7    The AVOutputCollection class
- 7.3.8    The TunerCollection class
- 7.3.9    The Tuner class
- 7.3.10   The SignalInfo class
- 7.3.11   The LNBInfo class
- 7.3.12   The StartupInformation class

- 7.9  PARENTAL RATING AND PARENTAL CONTROL APIS
- 7.9.1  The application/oipfParentalControlManager embedded object
- 7.9.2  The ParentalRatingScheme class
- 7.9.3  The ParentalRatingSchemeCollection class
- 7.9.4  The ParentalRating class
- 7.9.5  The ParentalRatingCollection class

- 7.10 SCHEDULED RECORDING APIS
- 7.10.1 The application/oipfRecordingScheduler embedded object
- 7.10.2 The ScheduledRecording class
- 7.10.3 The ScheduledRecordingCollection class
- 7.10.4 Extension to application/oipfRecordingScheduler for control of recordings
- 7.10.5 The Recording class
- 7.10.6 The RecordingCollection class
- 7.10.7 The PVREvent class
- 7.10.8 The Bookmark class
- 7.10.9 The BookmarkCollection class

- 7.13 SCHEDULED CONTENT AND HYBRID TUNER APIS
- 7.13.1  The video/broadcast embedded object
- 7.13.2  Extensions to video/broadcast for recording and time-shift
- 7.13.3  Extensions to video/broadcast for access to EIT p/f
- 7.13.4  Extensions to video/broadcast for playback of selected components
- 7.13.5  Extensions to video/broadcast for parental ratings errors
- 7.13.6  Extensions to video/broadcast for DRM rights errors
- 7.13.7  Extensions to video/broadcast for current channel information
- 7.13.8  Extensions to video/broadcast for creating channel lists from SD&S fragments
- 7.13.9  The ChannelConfig class
- 7.13.10   The ChannelList class
- 7.13.11   The Channel class
- 7.13.12   The FavouriteListCollection class
- 7.13.13   The FavouriteList class
- 7.13.14 Extensions to video/broadcast for channel scan
- 7.13.15 The ChannelScanEvent class
- 7.13.16 The ChannelScanOptions class
- 7.13.17 The ChannelScanParameters class
- 7.13.18 The DVBTChannelScanParameters class
- 7.13.19 The DVBSChannelScanParameters class
- 7.13.20 The DVBCChannelScanParameters class
- 7.13.22 The ATSCTChannelScanParameters class

- 7.16 SHARED UTILITY CLASSES AND FEATURES
- 7.16.1  Base collections
- 7.16.2  The Programme class
- 7.16.3  The ProgrammeCollection class
- 7.16.4  The DiscInfo class
- 7.16.5  Extensions for playback of selected media components
- 7.16.6  Additional support for protected content

### Not implemented

- 7.2     APPLICATION MANAGEMENT APIS
- 7.2.8    Widget APIs

- 7.4     CONTENT DOWNLOAD APIS
- 7.4.1    The application/oipfDownloadTrigger embedded object
- 7.4.2    Extensions to application/oipfDownloadTrigger
- 7.4.3    The application/oipfDownloadManager embedded object
- 7.4.4    The Download class
- 7.4.5    The DownloadCollection class
- 7.4.6    The DRMControlInformation class
- 7.4.7    The DRMControlInfoCollection class

- 7.5     CONTENT ON DEMAND METADATA APIS
- 7.5.1    The application/oipfCodManager embedded object
- 7.5.2    The ContentCatalogueCollection class
- 7.5.3    The ContentCatalogue class
- 7.5.4    The ContentCatalogueEvent class
- 7.5.5   The CODFolder class
- 7.5.6   The CODAsset class
- 7.5.7   The CODService class

- 7.6     CONTENT SERVICE PROTECTION API
- 7.6.1   The application/oipfDrmAgent embedded object

- 7.7     GATEWAY DISCOVERY AND CONTROL APIS
- 7.7.1   The application/oipfGatewayInfo embedded object

- 7.8     COMMUNICATION SERVICES APIS
- 7.8.1   The application/oipfCommunicationServices embedded object
- 7.8.2   Extensions to application/oipfCommunicationServices for presence and messaging services
- 7.8.3   The UserData class
- 7.8.4   The UserDataCollection class
- 7.8.5   The FeatureTag class
- 7.8.6   The FeatureTagCollection class
- 7.8.7   The Contact class
- 7.8.8   The ContactCollection class
- 7.8.9   Extensions to application/oipfCommunicationServices for voice telephony services
- 7.8.10  Extensions to application/oipfCommunicationServices for video telephony services
- 7.8.11  The DeviceInfo class
- 7.8.12  The DeviceInfoCollection class
- 7.8.13  The CodecInfo class
- 7.8.14  The CodecInfoCollection class

- 7.11 REMOTE MANAGEMENT APIS
- 7.11.1 The application/oipfRemoteManagement embedded object

- 7.13 SCHEDULED CONTENT AND HYBRID TUNER APIS
- 7.13.21 Extensions to video/broadcast for synchronization

- 7.14 MEDIA PLAYBACK APIS
- 7.14.1  The A/V Control object
- 7.14.2  Extensions to A/V Control object for playback through Content-Access Streaming Descriptor
- 7.14.3  Extensions to A/V Control object for trickmodes
- 7.14.4  Extensions to A/V Control object for playback of selected components
- 7.14.5  Extensions to A/V Control object for parental rating errors
- 7.14.6  Extensions to A/V Control object for DRM rights errors
- 7.14.7  Extensions to A/V Control object for playing media objects
- 7.14.8  Extensions to A/V Control object for UI feedback of buffering A/V content
- 7.14.9  DOM events for A/V Control object
- 7.14.10   Playback of memory audio
- 7.14.11   Extensions to A/V Control object for media queuing
- 7.14.12   Extensions to A/V Control object for volume control
- 7.14.13   Extensions to A/V Control object for resource management

- 7.15 MISCELLANEOUS APIS
- 7.15.1  The application/oipfMDTF embedded object
- 7.15.2  The application/oipfStatusView embedded object
- 7.15.3  The application/oipfCapabilities embedded object
- 7.15.4  The Navigator class
- 7.15.5  Debug print API

- 7.17 DLNA RUI REMOTE CONTROL FUNCTION APIS
- 7.17.1  The application/oipfRemoteControlFunction embedded object

Install
-------
To install oipf-stub with all necessary dependencies, just run:

> npm install

Test
----
Test are running thew npm using "mocha"

First "mocha" has to be installed as global dependency:

> npm install -g mocha

Then, to run tests, just run:

> npm test
